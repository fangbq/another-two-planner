printall:-
	nl,nl,
	runs(Run),
	write('Run : '),write(Run),nl,nl,
	print_states(Run),nl,
	print_tuples(Run), nl,nl,!.

print_tuples(R):-
	g_array_size(bck(R),S1),
	g_array_size(tuple(R,0),S2),
	print_tuples(R,0,S1,0,S2),!.

print_tuples(_,C1,C1,_,_).
print_tuples(R,C1,S1,_,S2):-
	write('For state '),write(C1),write(': '),
	print_bels(R,C1,0,S2),
	NC1 is C1  + 1,
	print_tuples(R,NC1,S1,0,S2).

print_bels(_,_,S,S):-
	nl.
print_bels(R,C1,C,S2):-
	g_read(tuple(R,C1,C),Bels),
	write(Bels),write('  '),
	NC is C + 1,
	print_bels(R,C1,NC,S2).

print_states:-
	runs(Run),
	print_states(Run),!.
	
print_states(Run):-
	g_array_size(bck(Run),Size),
	print_states(Run,0,Size),!.

print_states(_,C,C).
print_states(R,C,S):-
	g_read(bck(R,C),[L,B]),
	write('State '),write(C),write(':'),write(L),write('  Accessible: '),write(B),nl,
	NC is C + 1,
	print_states(R,NC,S),!.



