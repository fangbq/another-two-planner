update(A):-
	observe(A),!.
	
	
observe(A):-
	runs(R),
	g_array_size(bck(R),Size),
	observe(R,0,Size,A),
	update_bels(R),	
	NRun is R + 1,
	retract(runs(_)),
	asserta(runs(NRun)),!.

observe(_,Size,Size,_).
observe(R,C,Size,A):-
	\+ formula(R,A,[C]),
	g_read(bck(R,C), [Inter,_]),
	NR is R + 1,
	g_assign(bck(NR,C), [Inter,0]),
	NC is C + 1,
	observe(R,NC,Size,A)
	;
	NC is C + 1,
	NR is R + 1,
	g_read(bck(R,C), [Inter,X]),
	g_assign(bck(NR,C), [Inter,X]),
	observe(R,NC,Size,A).


update_bels(R):-
	g_array_size(bck(R),Size),
	findallstates(R,0,Size,Res),
	num_agents(Agents),
	update_all_bels(R,0,Size,0,Agents,Res).


findallstates(_,C,C,[]).
findallstates(R,C,Size,[C|T]):-
NR is R + 1,
	g_read(bck(NR,C),[_,1]),
	NC is C + 1,
	findallstates(R,NC,Size,T).
findallstates(R,C,Size,T):-
NR is R + 1,
	\+ g_read(bck(NR,C),[_,1]),
	NC is C + 1,
	findallstates(R,NC,Size,T).


update_all_bels(_,Size,Size,_,_,_).
update_all_bels(R,Cs,Size,Agents,Agents,Res):-
	NCs is Cs + 1,
	update_all_bels(R,NCs,Size,0,Agents,Res).
update_all_bels(R,Cs,Size,Ca,Agents,Res):-
	g_read(tuple(R,Cs,Ca),BeliefState),
	intersection(Res,BeliefState,NBeliefs),
	NR is R + 1,
	g_assign(tuple(NR,Cs,Ca),NBeliefs),
	NCa is Ca + 1,
	update_all_bels(R,Cs,Size,NCa,Agents,Res).

