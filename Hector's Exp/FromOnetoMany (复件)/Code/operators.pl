regress(Runs):-
	runs(Current),
	Res is Current - Runs,
	Res > 0,
	retract(runs(_)),
	asserta(runs(Res)),
	nl,write('Back at run '),write(Res),nl,!,true
	;
	nl,write('Cannot regress.'),nl,!,true.



cformula(and([]),_).
cformula(and([H|T]),W):-
	cformula(H,W),
	cformula(and(T),W),!,true
	;
	!,false.

cformula(neg(_),[]).
cformula(neg(A),W):-
	\+ cformula(A,W),
%%	cformula(neg(A),W),
	!,true
	;
	!,false.

%cformula(impl(_,_),_).
cformula(impl(A1,A2),W):-
	cformula(A1,W), 
	\+ cformula(A2,W), 
	!,false
	;
%	cformula(impl(A1,A2),W),
	!,true.
cformula(impl(_,_),_).

cformula(or([]),_):-
	!,false.
cformula(or([H|T]),W):-
	cformula(H,W),!,true
	;
	cformula(or(T),W),!.
	
	
cformula(A,W):-
	atom(A), 
	cevaluate(A,W),
	!,true
	;
	atom(A),
	\+ cevaluate(A,W),
	!,false.

cevaluate(H,Inter):-
	member(H,Inter),!.
	
	
formula(_,k(_,_),[]):-!,true.
formula(R,k(Agent,Formula),[S|T]):-
	agents(AllAgents),
	nth0(AgentTag, AllAgents, Agent),
	g_read(tuple(R,S,AgentTag),Beliefstate),
	Beliefstate \= [],
	formula(R,Formula,Beliefstate),
	formula(R,k(Agent,Formula),T)
	;
	!,fail.

formula(_,and([]),_).
formula(_,and(_),[]).
formula(R,and([H|T]),[S|Ts]):-
	formula(R,H,[S]),
	formula(R,and(T),[S|Ts]),
	formula(R,and([H|T]),Ts),!,true
	;
	!,false.

formula(_,neg(_),[]).
formula(R,neg(A),[S|T]):-
	\+ formula(R,A,[S]),
	formula(R,neg(A),T),
	!,true
	;
	!,false.

formula(_,impl(_,_),[]).
formula(R,impl(A1,A2),[S|Ts]):-
	formula(R,A1,[S]),
	\+ formula(R,A2,[S]),
	!,false
	;
	formula(R,impl(A1,A2),Ts),
	!,true.

formula(_,or(_),[]).	
formula(_,or([]),_):-
	!,false.
formula(R,or([H|T]),[S|Ts]):-
	formula(R,H,[S]),!,true
	;
	formula(R,or(T),[S]),!.
	
formula(_,A,[]):-
	atom(A),!.
formula(R,A,[S|T]):-
	atom(A),
	g_read(bck(R,S),[Inter,_]),
	evaluate(A,Inter),
	formula(R,A,T),
	!,true
	;
	atom(A),
	g_read(bck(R,S),[Inter,_]),
	\+ evaluate(A,Inter),
	!,false.

evaluate(H,Inter):-
	member(H,Inter),!.
	
	
	





	

