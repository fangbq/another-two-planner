:- dynamic( usedstates/1).
:- dynamic( partial_s/1).
:- dynamic( allstates/1).
:- dynamic( runs/1).
:- dynamic( static_atoms/1).
:- dynamic( state/2).
:- dynamic( agents/1).
:- dynamic( run/0).
:- dynamic( atoms/1).
:- dynamic( num_agents/1).
:- dynamic( beliefstate/1).
:- dynamic( temp_ksense_states/2).
:- dynamic( num_events/1).
:- dynamic( constraint/1).
:- dynamic( possibleactions/1).
:- dynamic( axioms_to_add/1).
:- dynamic( prec_axioms_to_add/1).
:- dynamic( break_dis/1). 
:- dynamic( goal/1).
:- dynamic( filename/1).
:- dynamic( diz/1).
:- dynamic( invariant/1).
:- dynamic( p_commonknowledge/1).
:- dynamic( n_commonknowledge/1).
:- dynamic( user_input_states/1).
:- dynamic( find_plan/0).

:- dynamic( dstatesused/1).
:- dynamic( dstatesusedepistemic/1).

:- include('operators.pl').
:- include('action.pl').
:- include('sense.pl').
:- include('printing.pl').
:- include('observe.pl').
:- include('query.pl').
:- include('Translation8_g.pl').

:-initialization(init).

runs(0).
temp_ksense_states([],[]).
filename(_).
beliefstate(_).
dstatesused([]).

dstatesusedepistemic([]).

init:-
	retract(runs(_)),
	asserta(runs(0)),
	g_assign(flag,0),
	argument_value(1,File),
	argument_value(2,File2),
	retract(filename(_)),
	asserta(filename(File2)),
	consult(File),
	write('Creating all possible states...'),init_states,!,write('Done'),nl,
	write('Creating all tuples...'),init_tuples,!,write('Done'),nl,
	write('Running input file...'),nl,run,write('Done'),nl,
	t,
	!.
init:-
	!,true.


init2:-
	retract(runs(_)),
	asserta(runs(0)),
	g_assign(flag,0),
	argument_value(1,File),
	argument_value(2,File2),
	retract(filename(_)),
	asserta(filename(File2)),
	consult(File),
	write('Creating all possible states...'),init_states2,write('Done'),nl,
	write('Creating all tuples...'),init_tuples,write('Done'),nl,
	write('Running input file...'),nl,run,write('Done'),nl,
%	t_rel,					%%%%%%%	 MALAKAAAAAAAAAAAAAAA
	!.
init2:-
	!,true.


init_states:-

	user_input_states([]),
	atoms(Atoms),
	num_events(Events),
	length(Atoms,Length),nl,
	
	p_commonknowledge(PCK),
	
	n_commonknowledge(NCK),	
	

	append(PCK,NCK,CK),
	subtract(Atoms,CK,Atoms2),

	invariant(Inv),
	
	flatten(Inv,Inv2),

	subtract(Atoms2,Inv2,Atoms3),	


	

	sort(Atoms3,Atoms4),nl,
		findall(State, subseq0(Atoms4,State), NBCK), 
	
	addpos(PCK,NBCK,NBCK2),
	sort(NBCK2,NBCK3),
	length(Inv,Le),

	findall(In, combinv(Inv,1,Le,[],In),INV), 



	createstates2(INV,NBCK3,[],NBCK4),
	length(NBCK4,L1),


	filter(NBCK4,NBCK5),	write(NBCK5),nl,
	length(NBCK5,L2),nl,write('Actual Initial States:'),write(L2),nl,
	size(NBCK5,0,SizeofBck),
	g_assign(bck,g_array(Events, g_array(SizeofBck))),				
	insert_states(0,Events,0,NBCK5),
	init_orig_beliefs(0,SizeofBck).


init_states:-
	num_events(Events),
	user_input_states(IS),
	length(IS,L2),nl,write('Actual Initial States:'),write(L2),nl,
	size(IS,0,SizeofBck),
	g_assign(bck,g_array(Events, g_array(SizeofBck))),				
	insert_states(0,Events,0,IS),
	init_orig_beliefs(0,SizeofBck).

createstates2([],NBC2,L,L).
createstates2([H|T],NBC2,L,NBCK3):-
	addpos(H,NBC2,L2),
	append(L2,L,L3),
	createstates2(T,NBC2,L3,NBCK3).

addpos(_,[],[]).
addpos(PCK,[S|T],[S2|T2]):-
	append(PCK,S,S2),
	addpos(PCK,T,T2).

combinv(Inv,C,Le,S,S):-
	C > Le.
combinv(Inv,C,Le,S,S2):-
	nth(C,Inv,List),
	member(X,List),
	append([X],S,S1),
	NC is C + 1,
	combinv(Inv,NC,Le,S1,S2).




onlyrelevantatoms([],Atoms,[]).
onlyrelevantatoms([H|T],Atoms,Res):-
	member(H,Atoms),
	onlyrelevantatoms(T,Atoms,Res).
onlyrelevantatoms([H|T],Atoms,[H|T2]):-
	onlyrelevantatoms(T,Atoms,T2).

addrestliterals(PCK,[],[]).
addrestliterals(PCK,[H|T],[N|T2]):-
 	append(PCK,H,N),
 	addrestliterals(PCK,T,T2).





init_states2:-
num_events(Events),
	usedstates(NBCK),
	size(NBCK,0,SizeofBck),
	g_assign(bck,g_array(Events, g_array(SizeofBck))),				
	insert_states(0,Events,0,NBCK),
	init_orig_beliefs(0,SizeofBck).

init_orig_beliefs(C,Rounded):-
	init_orig_beliefs(C,Rounded,BS),
	retract(beliefstate(_)),
	asserta(beliefstate(BS)).

init_orig_beliefs(C,C,[]):-
	!.
init_orig_beliefs(C,Rounded,[C|T]):-
	NC is C + 1,
	init_orig_beliefs(NC,Rounded,T).


init_tuples:-
	num_events(Events),
	beliefstate(BS),
	g_array_size(bck(0),Size),
	agents(Agents),
	length(Agents,L),
	g_assign(tuple, g_array(Events, g_array(Size,g_array(L,[])))),
	fix_init_tuple(0,Size,0,L,BS).
	
	
fix_init_tuple(Size,Size,_,_,_):-!,true.
fix_init_tuple(S1,Size,A,A,BS):-
	NS1 is S1 + 1,
	fix_init_tuple(NS1,Size,0,A,BS).	
fix_init_tuple(S1,Size,A,NA,BS):-
	g_assign(tuple(0,S1,A),BS),
	AA is A + 1,
	fix_init_tuple(S1,Size,AA,NA,BS).


init_indiv_beliefs:-
	num_agents(Size),
	beliefstate(BS),
	g_assign(indiv_beliefs, 
	g_array(Size, BS)).
	

subseq0b(List,S):-
	subseq0(List,S),
	constraint(S).

subseq0(List, List).
subseq0(List, Rest):- subseq1(List, Rest).

subseq1([_|Tail], Rest) :- subseq0(Tail, Rest).
subseq1([Head|Tail], [Head|Rest]) :- subseq1(Tail, Rest).%,constraint(Rest).



	






insert_states(R,R,_,_):-
	!,true.
insert_states(IR,R,IC,BCK):-
	insert_states(IR,IC,BCK),
	NR is IR + 1,
	insert_states(NR,R,IC,BCK).


insert_states(_,_,[]):-
	!,true.
insert_states(0,C,[H|T]):-
	g_assign(bck(0,C),[H,1]),
	NC is C + 1,
	insert_states(0,NC, T).
insert_states(R,C,[H|T]):-
	g_assign(bck(R,C),[_,_]),
	NC is C + 1,
	insert_states(R,NC, T).


filter([],[]):-!,true.
filter([H|T],[H|T2]):-
	constraint(H),
	filter(T,T2).
filter([H|T],T2):-
	filter(T,T2).

size([],SizeofBck,SizeofBck):-!,true.
size([H|T],C,SizeofBck):-
	NC is C + 1,
	size(T,NC,SizeofBck).

	
reset:-
	consult('input.pl'),
	consult('init.pl'),!.
