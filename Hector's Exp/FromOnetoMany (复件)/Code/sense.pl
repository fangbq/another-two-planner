sense_simul(Agent,Formulas,Beliefs):-
	runs(Run),
	agents(Ag),
	g_array_size(bck(Run),Size),
	nth0(AgentTag, Ag, Agent),
	return_states(Run,Agent,Formulas,0,Size,Beliefs),!,true.

insert_bels(_,_,[]).
insert_bels(Run,AgentTag,[[W,BS]|T]):-
	new_inters(BS,[NBS]),
	NR is Run + 1,
	g_assign(tuple(NR,W,AgentTag),NBS),
	insert_bels(Run,AgentTag,T).


new_inters([N],[N]).
new_inters([H1,H2|T],NBS):-
	intersection(H1,H2,N),
	new_inters([N|T],NBS).



return_states(_,_,_,C,C,[]).
return_states(Run,Agent,Formulas,C,Size,[NBS|T]):-
	ret_models(Agent,Run,C,Formulas,Res), 
	new_inters(Res,[NBS]),
	NC is C + 1,
	return_states(Run,Agent,Formulas,NC,Size,T).
	
ret_models(_,_,_,[],[]):-!.
ret_models(Agent,Run,C,[A|T],[Res|T1]):-
	agents(AllAgents),
	nth0(AgentTag, AllAgents, Agent),
	g_read(tuple(Run,C,AgentTag),BS),
	models(Run,A,C,BS,Res),
	ret_models(Agent,Run,C,T,T1).
	

sense(Agent,A):-
	runs(Run),
	g_array_size(bck(Run),Size),
	fix_belief_set(Run,Agent,A,0,Size),
	fix_states_valuation(Run,0,Size),
	NRun is Run + 1,
	retract(runs(_)),
	asserta(runs(NRun)),!.

sense([Agent|T],[F|Tf]):-
	runs(R),
	agents(Ag),
	g_array_size(bck(R),Size),
	fix_states_valuation(R,0,Size),
	restagents([Agent|T],Remaining,Ag),
	fixremainingbels(R,0,Size,Remaining),
	ck_sense(R,[Agent|T],[F|Tf],Beliefsets),
	g_array_size(bck(R),Size),
	updateindivbels(R,[Agent|T],Beliefsets,Size),
	NRun is R + 1,
	retract(runs(_)),
	asserta(runs(NRun)),!.


fix_states_valuation(_,C,C):-!.
fix_states_valuation(R,C,Size):-
	NR is R + 1,
	g_read(bck(R,C),L),
	g_assign(bck(NR,C),L),
	NC is C + 1,
	fix_states_valuation(R,NC,Size).
	
	
	
fixremainingbels(_,_,_,[]):-
	!,true.
fixremainingbels(R,Size,Size,[A|T]):-
	fixremainingbels(R,0,Size,T).
fixremainingbels(R,C,Size,[A|T]):-
	agents(Ag),
	nth0(AgentTag, Ag, A),
	g_read(tuple(R,C,AgentTag),BS),
	NR is R + 1,
	g_assign(tuple(NR,C,AgentTag),BS),
	NC is C + 1,
	fixremainingbels(R,NC,Size,[A|T]).



restagents(_,[],[]):-!,true.
restagents(SA,Remaining,[A|T]):-
	member(A,SA),
	restagents(SA,Remaining,T).
restagents(SA,[A|T2],[A|T]):-
	restagents(SA,T2,T).


fix_belief_set(_,_,_,C,C):-!.	
fix_belief_set(R,Agent,A,C,Size):-
	agents(AllAgents),
	num_agents(Agents),
	nth0(AgentTag, AllAgents, Agent),
	g_read(tuple(R,C,AgentTag),BeliefState),
	models(R,A,C,BeliefState,NBeliefState), 
	NR is R + 1,
	assignallnew(0,Agents,AgentTag,NR,C,NBeliefState), 
	NC is C + 1,
	fix_belief_set(R,Agent,A,NC,Size).

assignallnew(Agents,Agents,_,_,_,_):-
	!,true.
assignallnew(AgentTag,Agents,AgentTag,NR,C,NBeliefState):-
	g_assign(tuple(NR,C,AgentTag),NBeliefState),
	NA is AgentTag + 1,
	assignallnew(NA,Agents,AgentTag,NR,C,NBeliefState).
assignallnew(Agent,Agents,AgentTag,NR,C,NBeliefState):-
	R is NR - 1,
	g_read(tuple(R,C,Agent),BS), 
	g_assign(tuple(NR,C,Agent),BS),
	NA is Agent + 1,
	assignallnew(NA,Agents,AgentTag,NR,C,NBeliefState).


updateindivbels(_,[],_,_):-!.
updateindivbels(R,[Agent|T],[H|T2],Size):-
	g_array_size(bck(R),Size),
	aux_updateindivbels(R,Agent,H,0,Size),
	updateindivbels(R,T,T2,Size).

	
aux_updateindivbels(_,_,[],C,C):-!.
aux_updateindivbels(R,Agent,[H|T2],C,Size):-	
	agents(AllAgents),
	nth0(AgentTag, AllAgents, Agent),
	NR is R + 1,
	g_assign(tuple(NR,C,AgentTag),H),
	NC is C + 1,
	aux_updateindivbels(R,Agent,T2,NC,Size).

ck_sense(_,[],_,[]):-!.	
ck_sense(R,[Agent|T],[F|Tf],[BeliefSet|T2]):-
	g_array_size(bck(R),Size),
	ck_fix_belief_set(R,Agent,F,0,Size,BeliefSet),		
	ck_sense(R,T,Tf,T2).
	

	
ck_fix_belief_set(_,_,_,C,C,[]):-!.	
ck_fix_belief_set(R,Agent,[A|At],C,Size,NBeliefState):-
	agents(AllAgents),
	nth0(AgentTag, AllAgents, Agent),
	g_read(tuple(R,C,AgentTag),BeliefState),
	sense_simul(Agent,[A|At],NBeliefState).
ck_fix_belief_set(R,Agent,A,C,Size,[NBeliefState|T]):-
	agents(AllAgents),
	nth0(AgentTag, AllAgents, Agent),
	g_read(tuple(R,C,AgentTag),BeliefState),
	models(R,A,C,BeliefState,NBeliefState),
	NC is C + 1,
	ck_fix_belief_set(R,Agent,A,NC,Size,T).
	
models(_,_,_,[],[]):-!.

models(R,A,C,[H|T],[H|T2]):-
	formula(R,A,[C]),
	formula(R,A,[H]),
	models(R,A,C,T,T2).


models(R,A,C,[H|T],[H|T2]):-
	\+ formula(R,A,[C]),
	\+ formula(R,A,[H]),
	models(R,A,C,T,T2).
models(R,A,C,[_|T],NB):-
	models(R,A,C,T,NB).


create_sensinglist_for_all_agents(_,C,C,[]).
create_sensinglist_for_all_agents([H|T],C,Length,[[H|T]|T2]):-
	NC is C + 1,
	create_sensinglist_for_all_agents([H|T],NC,Length,T2).
	
sense([H|T]):-
	agents(AllAgents),
	length(AllAgents,Length),
	create_sensinglist_for_all_agents([H|T],0,Length,Result),
	sense(AllAgents,Result),!.
	
sense(A):-
	runs(R),
	num_agents(NumAgents),
	g_array_size(bck(R),Size),
	partition(R,A,0,Size,True,False),!,
	remove_states_from_all(R,0,NumAgents,True,False),
	fix_states_valuation(R,0,Size),
	NRun is R + 1,
	retract(runs(_)),
	asserta(runs(NRun)),!.
	
partition(_,_,S,S,[],[]):-!.

partition(R,A,SpecificState,AllStates,[SpecificState|T],False):-
	formula(R,A,[SpecificState]),!,
	NS is SpecificState + 1,
	partition(R,A, NS, AllStates, T, False),!.

partition(R,A,SpecificState,AllStates,True, [SpecificState|T]):-
	\+ formula(R,A,[SpecificState]),!,
	NS is SpecificState + 1,
	partition(R,A, NS, AllStates, True, T),!.
	
partition(R,A,SpecificState,AllStates,True, False):-
	NS is SpecificState + 1,
	partition(R,A, NS, AllStates, True, False),!.


remove_states_from_all(_,C,C,_,_):-!.	
remove_states_from_all(R,C,AllAgents,True,False):-
	NC is C + 1,
	remove_states(R,C,True,True),!,
	remove_states(R,C,False,False),!,
	remove_states_from_all(R,NC,AllAgents,True,False),!.
	
	
	
remove_states(_,_,[],_):-!.	
remove_states(R,Agent,[H|T],True):-
	g_read(tuple(R,H,Agent),BeliefState),
	intersection(True, BeliefState, Intersection),!,
	NR is R + 1,
	g_assign(tuple(NR,H,Agent),Intersection),!,
	remove_states(R,Agent,T,True),!.
	

intersection([], _, []):-!.
intersection([H|T], BeliefState, [H|T2]):-
	member(H,BeliefState),
	intersection(T, BeliefState, T2),!.
intersection([_|T], BeliefState, T2):-
	intersection(T, BeliefState, T2),!.

