writeonscreen(_,[]):-
	!.
writeonscreen(N,[[_,_,H]|T]):-
	write(N),write(': '),write(H),nl,nl,
	NN is N + 1,
	writeonscreen(NN,T).

search(Init,Query):-
	runs(R),
	possibleactions(PA),
	write('Searching..'),nl,
	dsearch(1,Init,Query,R,PA,PA,SOL),nl,
	write('Actions:'),nl,
	writeonscreen(1,SOL),!.

dsearch(Depth,Init,Query,R,[A1|T1],PA,Sol):-
	search(1,Depth,Init,Query,R,[A1|T1],PA,Sol).
dsearch(Depth,Init,Query,R,[A1|T1],PA,Sol):-
	ND is Depth + 1,
	runs(Rt),
	retract(runs(_)),
	asserta(runs(R)),
	dsearch(ND,Init,Query,R,[A1|T1],PA,Sol).

search(C,S,_,_,_,_,_,_):-
	C > S,false.
search(C,Depth,Init,Query,R,[],PA,[A1|T2]):-
	NC is C + 1,
	search(NC,Depth,Init,Query,R,PA,PA,[A1|T2]).
search(C,Depth,Init,Query,R,[[Name,Prec,A1]|T1],PA,[[Name,Prec,A1]|T2]):-
	C < Depth,
	iDholds(Prec),
	A1, 						
	runs(CurrentR),
	\+ comparetopast(CurrentR),	
	NC is C + 1,
	search(NC,Depth,Init,Query,R,[[Name,Prec,A1]|T1],PA,T2).
	
search(C,Depth,Init,Query,R,[[Name,Prec,A1]|T1],PA,S):-
	C < Depth,
	iDholds(Prec),						
	runs(Rt),
	retract(runs(_)),
	NRt is Rt - 1, 
	asserta(runs(NRt)),
	search(C,Depth,Init,Query,R,T1,PA,S).
search(C,Depth,Init,Query,R,[[Name,Prec,A1]|T1],PA,S):-		
	C < Depth,
	\+ iDholds(Prec),
	search(C,Depth,Init,Query,R,T1,PA,S).
search(Depth,Depth,Init,Query,R,[[Name,Prec,A1]|T1],PA,[[Name,Prec,A1]]):-
	iDholds(Prec),
	A1, 
 	runs(Rt),
	iDholds(Init,Query),!,true.
 	
search(Depth,Depth,Init,Query,R,[[Name,Prec,A1]|T1],PA,S):-
 	\+ iDholds(Prec),
 	search(Depth,Depth,Init,Query,R,T1,PA,S).
search(Depth,Depth,Init,Query,R,[[Name,Prec,A1]|T1],PA,S):-
	iDholds(Prec),	
	runs(Rt),
	NRt is Rt - 1, 
	retract(runs(_)),
	asserta(runs(NRt)),
 	search(Depth,Depth,Init,Query,R,T1,PA,S).

 



s_hquery(R,[[F,T]|Tail],States):-
	g_array_size(bck(R),Size),
	s_hquery(R,[[F,T]|Tail],0,Size,States),!.

s_hquery(_,_,W,W,[]):-!,true.
s_hquery(R,Forms,World,Size,[World|T]):-
	s_satisf(Forms,World),
	NW is World + 1,
	s_hquery(R,Forms,NW,Size,T),!.
s_hquery(R,Forms,World,Size,T):-
	NW is World + 1,
	s_hquery(R,Forms,NW,Size,T),!.	

s_satisf([],_):-
	!,true.

s_satisf([[F,T]|Tail],World):-
	runs(R),
	T >= R,
	s_satisf(Tail,World),!.
s_satisf([[F,T]|Tail],World):-
	runs(R),
	T < R,
	formula(T,F,[World]),
	s_satisf(Tail,World)
	;
	!,fail.	


untillnow(_,[],[]):-!,true.
untillnow(R,[[F|T]|T1],S):-
	T > R,
	untillnow(R,T1,S).
untillnow(R,[[F|T]|T1],[[F|T]|T2]):-
	untillnow(R,T1,T2).


holdsT([H|T],Q):-								
	runs(R),
	g_array_size(bck(R),Size),
	rquery([H|T],0,Size,States),	write('States satisfying condition: '),write(States),nl,
	test_query(States,Q,R),!.	

test_query([],_,_):-
	write('Satisfied'),nl,!,true.
test_query([H|T],Q,R):-
	g_read(bck(R,H),[_,1]),
	formula(R,Q,[H]),
	test_query(T,Q,R)
	;
	write('Not Satisfied'),nl,!,true.


rquery(_,W,W,[]):-!,true.
rquery(Forms,World,Size,[World|T]):-
	runs(R),
	g_read(bck(R,World),[_,1]),
	satisf(Forms,World),
	NW is World + 1,
	rquery(Forms,NW,Size,T).
rquery(Forms,World,Size,T):-
	NW is World + 1,
	rquery(Forms,NW,Size,T).	


formulatrueinallstates(R,A,[]):-
	!,true.
formulatrueinallstates(R,A,[S|T]):-
	formula(R,A,[S]),
	formulatrueinallstates(R,A,T)
	;
	!,false.

retunposssubset(R,[],[]):-!.
retunposssubset(R,[S0|T0],[S0|T]):-
	g_read(bck(R,S0),[_,1]),
	retunposssubset(R,T0,T).
retunposssubset(R,[S0|T0],S):-
	retunposssubset(R,T0,S).



iDholds(Init,A):-
	runs(R),
	s_hquery(0,Init,States),
	retunposssubset(R,States,States2),!,
	formulatrueinallstates(R,A,States2),
	!,true
	;
	!,false.
	

iDholds(A):-
	runs(R),
	lquery(A,States),
	g_array_size(bck(R),Size),
	iDcompare(R,0,Size,States),!.
holds(A):-
	runs(R),
	lquery(A,States),
	g_array_size(bck(R),Size),
	compare(R,0,Size,States),!.


lquery(A,States):-					
	runs(R),
	g_array_size(bck(R),Size),
	lquery(R,A,0,Size,States),!.
lquery(_,_,C,C,[]):-
	!,true.
lquery(R,A,C,NUM,[C|T]):-
	g_read(bck(R,C),[_,1]),
	formula(R,A,[C]),
	NC is C + 1,
	lquery(R,A,NC,NUM,T).
lquery(R,A,C,NUM,S):-
	g_read(bck(R,C),[_,0]),
	NC is C + 1,
	lquery(R,A,NC,NUM,S).
lquery(R,A,C,NUM,S):-
	\+ formula(R,A,[C]),
	NC is C + 1,
	lquery(R,A,NC,NUM,S).

compare(_,C,C,_):-
	write('Satisfied'),nl,
	!,true.
compare(R,C,Size,States):-
	g_read(bck(R,C),[_,1]),
	member(C,States),
	NC is C + 1,
	compare(R,NC,Size,States)
	;
	\+ g_read(bck(R,C),[_,1]),
	NC is C + 1,
	compare(R,NC,Size,States)
	;
	write('Not Satisfied'),nl,!,true.


iDcompare(_,C,C,_):-
	!,true.
iDcompare(R,C,Size,States):-
	g_read(bck(R,C),[_,1]),
	member(C,States),
	NC is C + 1,
	iDcompare(R,NC,Size,States)
	;
	\+ g_read(bck(R,C),[_,1]),
	NC is C + 1,
	iDcompare(R,NC,Size,States)
	;
	!,false.

hquery([[F,T]|Tail]):-					
	runs(R),
	g_array_size(bck(R),Size),
	hquery([[F,T]|Tail],0,Size),!.

hquery(_,W,W):-!,true.
hquery(Forms,World,Size):-
	satisf(Forms,World),
	write(World),nl,
	NW is World + 1,
	hquery(Forms,NW,Size)
	;
	NW is World + 1,
	hquery(Forms,NW,Size).	

satisf([],_):-
	!,true.
satisf([[F,T]|Tail],World):-
	g_read(bck(T,World),[_,1]),
	formula(T,F,[World]),
	satisf(Tail,World)
	;
	!,fail.	
	
	

implies(F,A):-						
	runs(R),
	g_array_size(bck(R),Size),
	iquery(R,F,A,0,Size),!.
iquery(_,_,_,C,C):-
	write('Satisfied'),nl,!,true.
iquery(R,F,A,C,NUM):-
	formula(0,F,[C]),
	formula(R,A,[C]),


iquery(R,F,A,NC,NUM).
iquery(R,F,A,C,NUM):-
	formula(0,F,[C]),
	\+ formula(R,A,[C]),
	write('Not Satisfied'),nl,!,true.
iquery(R,F,A,C,NUM):-
	NC is C + 1,
	iquery(R,F,A,NC,NUM).


comparetopast(CurrentR):-
	N is CurrentR - 1,
	num_agents(Agents),
	g_array_size(bck(CurrentR),NumStates),
	sameruns(CurrentR,N,0,Agents,0,NumStates),
	!,true.


comparetopast(CurrentR):-
	N is CurrentR - 1,
	num_agents(Agents),
	g_array_size(bck(CurrentR),NumStates),
	sameruns(CurrentR,N,0,Agents,0,NumStates), 
	comparetopast(0,CurrentR).

comparetopast(-1,_):-
	!,false.					
comparetopast(R,Run):-
	Run is R + 1,
	!,false.					
comparetopast(Run,CurrentR):-
	num_agents(Agents),
	g_array_size(bck(CurrentR),NumStates),
	sameruns(CurrentR,Run,0,Agents,0,NumStates),	
	!,true.
comparetopast(Run,CurrentR):-
	NRun is Run + 1,
	comparetopast(NRun,CurrentR).	
	
	
sameruns(CurrentR,Run,TheAgent,Agents,States,States):-
	NAgent is TheAgent + 1,
	sameruns(CurrentR,Run,NAgent,Agents,0,States),!.	
sameruns(CurrentR,Run,Agents,Agents,TheState,States):-				
	!,true.	

sameruns(CurrentR,Run,TheAgent,Agents,TheState,States):-
	TheState < States,
	g_read(bck(CurrentR,TheState),InAcCur),
	g_read(bck(Run,TheState),InAcSom),
	compareinterpretations(InAcCur,InAcSom),
	g_read(tuple(CurrentR,TheState,TheAgent),Bcur),
	g_read(tuple(Run,TheState,TheAgent),Bsom),
	comparebeliefs(Bcur,Bsom),
	NState is TheState + 1,
	sameruns(CurrentR,Run,TheAgent,Agents,NState,States),!.
	
sameruns(CurrentR,Run,TheAgent,Agents,TheState,States):-
	TheState < States,
	!,false.

sameruns(CurrentR,Run,TheAgent,Agents,TheState,States):-
	NAgent is TheAgent + 1,
	sameruns(CurrentR,Run,NAgent,Agents,0,States),!.

comparebeliefs(Bcur,Bsom):-
	subtract(Bcur,Bsom,[]),
	subtract(Bsom,Bcur,[]).

compareinterpretations([In,Ac],[In2,Ac]):-
	subtract(In,In2,[]),
	subtract(In2,In,[]).


