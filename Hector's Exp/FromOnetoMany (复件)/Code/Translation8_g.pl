%Pdiz(_).
check_m(C):-
	once(diz(LOL)),
	once(memberchk(C,LOL)),!,true.

adddiz([]):-!,true.
adddiz([H|T]):-
	asserta(diz(H)),
	adddiz(T).

fix_the_ds:-
	runs(Runs),
	once(g_array_size(bck(Runs),Size)),
	once(lessds(0,0,Size,Result)),
	once(adddiz(Result)).
lessds(Size,_,Size,[]):-
	!,true.
lessds(C,Size,Size,Result):-
	NC is C + 1,
	lessds(NC,NC,Size,Result).
lessds(C,C1,Size,[[C,C1]|T]):-
	NC is C1 + 1,
	lessds(C,NC,Size,T).

t:-
	fix_the_ds,!,
	t1,!,
	t2,!,true,nl,write('-----------------------------'),nl,
	write('PDDL files created'),nl,
	halt.
	
t1:-
	atoms(Atoms),
	runs(Runs),
	g_array_size(bck(Runs),Size), write('Possible States: '),write(Size),nl,
	open('Axioms',write,ID),
	add_axioms_c(Atoms,ID),			write('0.0'),nl,
	possibleactions(PA),			
	return_all_formulas_preconditions(PA,EPrec),	write('0.2'),nl,
	flatten(EPrec,EPrec2_1),
	sort(EPrec2_1,EPrec2),write('0.4'),nl,
	remove_unnecessary_precs(EPrec2,EPrec21),write('0.6'),nl,
	flatten(EPrec21,EPrec31),
	sort(EPrec31,EPrec3),
	add_precondition_axioms(EPrec3,ID),write('0.8'),nl,
	
	return_all_formulas(PA,EpistemicFormulasInActions),write('1'),nl,		%% but not goal
	goal(Goal),
	add_goal_formulas(0,Goal,0,Size,ID),write('2'),nl,
	fix_epistemic_formulas(Goal,GoalFormulas),write('3'),nl,
	append(GoalFormulas,Atoms,GoalFormulas2),write('4'),nl,
	remove_unnecessary_precs(EpistemicFormulasInActions,EpistemicFormulasInActions2),write('5'),nl,
	append(GoalFormulas2,EpistemicFormulasInActions2,EpistemicFormulas3),!,write('6'),nl,
	fix_epistemic_formulas(EpistemicFormulas3,EpistemicFormulas),!,write('7'),nl,
%	fix_epistemic_formulas(EpistemicFormulasInActions2,EpistemicFormulas),

	sort(EpistemicFormulas,MoreAXIOMS),write('8'),nl,
	add_deep_epistemic_formulas(MoreAXIOMS,MoreAXIOMS2),write('9'),nl,
	
	
			fix_epistemic_formulas(MoreAXIOMS2,MoreAXIOMS3),write('10'),nl,
			sort(MoreAXIOMS3,MoreAXIOMS6),write('11'),nl,
			onlytheepistemic(MoreAXIOMS6,EF),write('12'),nl,
			
%	onlytheepistemic(MoreAXIOMS2,EF),write('10'),nl,
	add_rest_axioms(EF,ID),write('13'),nl,
	close(ID),
	open('Actions',write,ID2),
	read_possibleactions(PA,ID2),write('14'),nl,
	close(ID2).
	
t2:-
	filename(File),
	atom_concat(File,'-domain.pddl',Dom),
	atom_concat(File,'-inst.pddl',Inst),
	agents(Agents),write('15'),nl,
	runs(Runs),
	g_array_size(bck(Runs),Size),write('16'),nl,
	open('InitialStates',write,ID),write('17'),nl,
	possible_states_interpretations(0,Size,ID), write('18'),nl,
	close(ID),write('19'),nl,
	create_autodomain_header,write('20'),nl,
	create_autoinstance_header,write('21'),nl,
	open('EpistemicFormulas',append,IDt),write('22'),nl,
	add_dss(IDt,Agents,0,Size,0,Size),write('23'),nl,
	write(IDt,')'),nl(IDt),write('24'),nl,
	close(IDt),	write('25'),nl,
	system('cat EpistemicFormulas >> autodomain.pddl'),write('26'),nl,
	system('cat Actions >> autodomain.pddl'),write('27'),nl,
	system('cat Axioms >> autodomain.pddl'),write('28'),nl,
	open('autodomain.pddl',append,IDt2),write('29'),nl,
	write(IDt2,')'),nl(IDt2),write('30'),nl,
	close(IDt2),	write('31'),nl,
	rename_file('autodomain.pddl',Dom),

	rename_file('autoinstance.pddl',Inst),	runs(Runs),
	g_array_size(bck(Runs),Size), write('Possible States: '),write(Size),nl,
	system('rm  Goal EpistemicFormulas InitialStates Actions Axioms'),write('32'),nl.


return_all_formulas_preconditions([],[]).
return_all_formulas_preconditions([[_,Prec,_]|T],[Prec|T2]):-
	return_all_formulas_preconditions(T,T2).

add_precondition_axioms([],ID).
add_precondition_axioms([H|T],ID):-
	add_axioms_d([H],ID),
	add_precondition_axioms(T,ID).
	







create_autodomain_header:-
		open('autodomain.pddl',write,ID),
		write(ID,'(define (domain dhmiourgimeno)'),nl(ID),
		write(ID,'(:requirements :strips :derived-predicates :conditional-effects )'),nl(ID),
		write(ID,'(:predicates'),nl(ID),
		close(ID).

create_autoinstance_header:-
		open('autoinstance.pddl',write,ID),
		write(ID,'(define (problem dhmiourgimeno)'),nl(ID),
		write(ID,'(:domain dhmiourgimeno)'),nl(ID),
		write(ID,'(:init'),nl(ID),
		close(ID),
		system('cat InitialStates >> autoinstance.pddl'),
		open('autoinstance.pddl',append,ID2),
		nl(ID2),
		write(ID2,')'),nl(ID2),
		write(ID2,'  (:goal (and '),nl(ID2),
		system('cat Goal >> autoinstance.pddl'),		
		write(ID2,')))'),nl(ID2).


add_goal_formulas(_,[],_,_,_).
add_goal_formulas(C,[H|T],Size,Size,ID):-
	add_goal_formulas(C,T,0,Size,ID).
add_goal_formulas(GN,[or(H)|T],C,Size,ID):-
	number_atom(GN,GNatom),
	number_atom(C,Catom),
	atom_concat('(goal',GNatom,Res0),
	atom_concat(Res0,')',Res),
	open('EpistemicFormulas',append,IDt),
	write(IDt,Res),
	nl(IDt),
	close(IDt),
	open('Goal',append,IDt2),
	write(IDt2,Res),
	nl(IDt2),
	close(IDt2),
	add_goal_axiom(Res,Catom,or(H),ID),
	NC is C + 1,
	NGN is GN + 1,
	add_goal_formulas(NGN,[or(H)|T],NC,Size,ID).

add_goal_formulas(GN,[H|T],C,Size,ID):-
	number_atom(GN,GNatom),
	number_atom(C,Catom),
	atom_concat('(goal',GNatom,Res0),
	atom_concat(Res0,')',Res),
	open('EpistemicFormulas',append,IDt),
	write(IDt,Res),
	nl(IDt),
	close(IDt),
	open('Goal',append,IDt2),
	write(IDt2,Res),
	nl(IDt2),
	close(IDt2),
	add_goal_axiom(Res,Catom,H,ID),
	NC is C + 1,
	NGN is GN + 1,
	add_goal_formulas(NGN,[H|T],NC,Size,ID).	

add_goal_axiom(Res,Catom,or(H),ID):-
	write(ID,'(:derived '),
	write(ID,Res),
	nl(ID),
	write(ID,'(or '),
	add_disjunction_of_goal(ID,Catom,H),
	write(ID,'))'),nl(ID).

add_goal_axiom(Res,Catom,H,ID):-
	write(ID,'(:derived '),
	write(ID,Res),
	nl(ID),
	write(ID,'(or '),
%	add_axioms([H],ID),				%	% ANTI AUTOU TO APOKATW MALAKA
	add_disjunction_of_goal(ID,Catom,[H]),
	write(ID,'))'),nl(ID).

add_disjunction_of_goal_aux(ID,Catom,or(H)):-
	add_disjunction_of_goal(ID,Catom,H).
add_disjunction_of_goal_aux(ID,Catom,H):-
	add_disjunction_of_goal(ID,Catom,[H]).




add_disjunction_of_goal(ID,Catom,[]):-
	agents([H|T]),
	write(ID,'(s'),write(ID,Catom),write(ID,'s'),write(ID,Catom),write(ID,H),
	write(ID,') ').
add_disjunction_of_goal(ID,Catom,[neg(H)|T]):-
	sensing_atom(H,Res,_),
	atom_concat(Res,'s',Res0),
	atom_concat(Res0,Catom,Res1),
	write(ID,'(not('),write(ID,Res1),write(ID,')) '),
	add_disjunction_of_goal(ID,Catom,T).
add_disjunction_of_goal(ID,Catom,[H|T]):-
	sensing_atom(H,Res,_),
	atom_concat(Res,'s',Res0),
	atom_concat(Res0,Catom,Res1),
	write(ID,'('),write(ID,Res1),write(ID,') '),
	add_disjunction_of_goal(ID,Catom,T).
	



atoms_per_state(C,[],ID):-!,true.
atoms_per_state(C,[H|T],ID):-
	number_atom(C,Catom),
	atom_concat('s',Catom,State),
	atom_concat(H,State,StateF),
	write(ID,'('),write(ID,StateF),write(ID,')'),nl(ID),
	atoms_per_state(C,T,ID).

possible_states_interpretations(Size,Size,ID):-
	add_actual_ds(ID),
	!,true.
possible_states_interpretations(C,Size,ID):-
	g_read(bck(0,C),[State,_]),
	atoms_per_state(C,State,ID),
	NC is C + 1,
	possible_states_interpretations(NC,Size,ID).
	

add_actual_ds(ID):-
	agents(A),
	length(A,La),
	runs(R),
	beliefstate(B),
	add_actual_ds(0,La,B,B,R,ID),
	add_not_possible_ds(R,B,A,ID).

add_not_possible_ds(R,[],A,ID):-!,true.
add_not_possible_ds(R,[H|T],A,ID):-
	g_read(bck(R,H),[_,0]),
	add_not_possible_ds(H,A,ID),
	add_not_possible_ds(R,T,A,ID).
add_not_possible_ds(R,[H|T],A,ID):-
	add_not_possible_ds(R,T,A,ID).

add_not_possible_ds(H,[],ID):-!,true.
add_not_possible_ds(H,[A|T],ID):-
	number_atom(H,Catom),
	atom_concat('s',Catom,State),
	atom_concat(State,State,State2),
	atom_concat(State2,A,StateF),
	write(ID,'('),write(ID,StateF),write(ID,')'),nl(ID),
	add_not_possible_ds(H,T,ID).

add_actual_ds(La,La,Ts,B,R,ID):-!,true.
add_actual_ds(Ca,La,[],B,R,ID):-
	NCa is Ca + 1,
	add_actual_ds(NCa,La,B,B,R,ID).
add_actual_ds(Ca,La,[S|Ts],B,R,ID):-
	g_read(tuple(R,S,Ca),PersonalBel),
	agents(List),
	NCa is Ca + 1,
	nth(NCa,List,Atom),

	g_read(bck(R,S),[_,1]),

	add_actual_ds(Atom,S,PersonalBel,B,ID),
	add_actual_ds(Ca,La,Ts,B,R,ID).
add_actual_ds(Ca,La,[S|Ts],B,R,ID):-
	g_read(tuple(R,S,Ca),PersonalBel),
	agents(List),
	NCa is Ca + 1,
	nth(NCa,List,Atom),

	g_read(bck(R,S),[_,0]),

	add_actual_ds2(Atom,S,PersonalBel,B,ID),
	add_actual_ds(Ca,La,Ts,B,R,ID).



add_actual_ds(Ca,S,PersonalBel,[],ID):-!,true.
add_actual_ds(Ca,S,PersonalBel,[H|T],ID):-
	S < H,
	\+ member(H,PersonalBel),
%	agents(List),
%	nth(Ca,List,Atom),
	number_atom(S,Satom),
	atom_concat('s',Satom,State1),
	number_atom(H,Hatom),
	atom_concat('s',Hatom,State2),
	atom_concat(State2,Ca,State3),
	atom_concat(State1,State3,State4),
	write(ID,'('),write(ID,State4),write(ID,')'),nl(ID),
	add_actual_ds(Ca,S,PersonalBel,T,ID).
add_actual_ds(Ca,S,PersonalBel,[H|T],ID):-
	add_actual_ds(Ca,S,PersonalBel,T,ID).

add_actual_ds2(Ca,S,PersonalBel,[],ID):-!,true.
add_actual_ds2(Ca,S,PersonalBel,[H|T],ID):-
	S < H,
%	agents(List),
%	nth(Ca,List,Atom),
	number_atom(S,Satom),
	atom_concat('s',Satom,State1),
	number_atom(H,Hatom),
	atom_concat('s',Hatom,State2),
	atom_concat(State2,Ca,State3),
	atom_concat(State1,State3,State4),
	write(ID,'('),write(ID,State4),write(ID,')'),nl(ID),
	add_actual_ds2(Ca,S,PersonalBel,T,ID).
add_actual_ds2(Ca,S,PersonalBel,[H|T],ID):-
	add_actual_ds2(Ca,S,PersonalBel,T,ID).





	

	
all_atoms_per_all_states([],C,Size,ID2):-
	!,true.
all_atoms_per_all_states([Atom|T],Size,Size,ID2):-
	all_atoms_per_all_states(T,0,Size,ID2),!,true.
all_atoms_per_all_states([Atom|T],C,Size,ID2):-
	number_atom(C,Catom),
	atom_concat('s',Catom,State),
	atom_concat(Atom,State,StateF),
	write(ID2,'('),write(ID2,StateF),write(ID2,')'),nl(ID2),
	NC is C + 1,
	all_atoms_per_all_states([Atom|T],NC,Size,ID2).
	
add_rest_axioms(EF,ID):-
	runs(Run),		
	g_array_size(bck(Run),Size),
	once(add_possible_axioms(EF,ID,Size)),
	add_axioms(EF,ID).	
	
add_possible_axioms([],ID,Size):-
	!,true.	
add_possible_axioms([Action|T],ID,Size):-
	once(add_axiom(Action,ID,0,Size)),
	add_possible_axioms(T,ID,Size).	
	
add_axioms_b([],_).	
add_axioms_b([k(A,L)|T],ID).
	
add_axioms_b([neg(k(A,L))|T],ID).
add_axioms_b([k(A,neg(L))|T],ID).
add_axioms_b([Atom|T],ID):-
	write(ID,'(:derived ('), write(ID,Atom),write(ID,') '),nl(ID),
	write(ID,'(and'),nl(ID), runs(Runs),
	g_array_size(bck(Runs),Size),
	add_axioms_aux_b(Atom,ID,0,Size),
	write(ID,'))'),nl(ID),
	add_axioms_b(T,ID).	
	
add_axioms_aux_b(L,ID,Size,Size):-
	!,true.
add_axioms_aux_b(L,ID,C,Size):-
	agents([H|_]),
	write(ID,'(or '),
	number_atom(C,C1atom),
	atom_concat('s',C1atom,State1),
	atom_concat(L,State1,Res),
	write(ID,'('),write(ID,Res),write(ID,') '),
	write(ID,'('),write(ID,State1),write(ID,State1),write(ID,H),write(ID,'))'),nl(ID),
	NC is C + 1,
	add_axioms_aux_b(L,ID,NC,Size).

add_axioms_c([],_).
add_axioms_c([Atom|T],ID):-
	runs(Runs),
	g_array_size(bck(Runs),Size),
	add_axioms_aux_c(Atom,ID,0,Size),
	add_axioms_c(T,ID).
add_axioms_aux_c(L,ID,Size,Size):-
	!,true.
add_axioms_aux_c(L,ID,C,Size):-
	agents([H|_]),
	number_atom(C,C1atom),
	atom_concat('s',C1atom,State1),
	atom_concat(L,State1,Res),
	NC is C + 1,
	open('EpistemicFormulas',append,IDt),
	write(IDt,'('), write(IDt,Res),write(IDt,') '),nl(IDt),
	close(IDt),
	add_axioms_aux_c(L,ID,NC,Size).	
	

	
	
add_axioms([],_).	
add_axioms([k(A,L)|T],ID).
	
add_axioms([neg(k(A,L))|T],ID).
add_axioms([k(A,neg(L))|T],ID).
add_axioms([Atom|T],ID):-
	write(ID,'(:derived ('), write(ID,Atom),write(ID,') '),nl(ID),
	open('EpistemicFormulas',append,IDt),
	write(IDt,'('), write(IDt,Atom),write(IDt,') '),nl(IDt),
	close(IDt),
	write(ID,'(and'),nl(ID), 
	runs(Runs),
	g_array_size(bck(Runs),Size),
	add_axioms_aux(Atom,ID,0,Size),
	write(ID,'))'),nl(ID),
	add_axioms(T,ID).

add_axioms_aux(L,ID,Size,Size):-
	!,true.
add_axioms_aux(L,ID,C,Size):-
	agents([H|_]),
	write(ID,'(or '),
	number_atom(C,C1atom),
	atom_concat('s',C1atom,State1),
	atom_concat(L,State1,Res),
	write(ID,'('),write(ID,Res),write(ID,') '),
	write(ID,'('),write(ID,State1),write(ID,State1),write(ID,H),write(ID,'))'),nl(ID),
	NC is C + 1,
	open('EpistemicFormulas',append,IDt),
	write(IDt,'('), write(IDt,Res),write(IDt,') '),nl(IDt),
	close(IDt),
	add_axioms_aux(L,ID,NC,Size).
	


add_axioms_d([],_).	
add_axioms_d([neg(k(A,L))|T],ID):-
	sensing_atom(k(A,L),Res,_),write(Res),
	write(ID,'(:derived (not_'), write(ID,Res),write(ID,') '),nl(ID),
	open('EpistemicFormulas',append,IDt),
	write(IDt,'(not_'), write(IDt,Res),write(IDt,') '),nl(IDt),
	close(IDt),
	write(ID,'(and'),nl(ID), 
	runs(Runs),
	g_array_size(bck(Runs),Size),
	add_axioms_aux_d(neg(Res),ID,0,Size),
	write(ID,'))'),nl(ID),
	add_axioms_d(T,ID).
add_axioms_d([k(A,L)|T],ID):-
	sensing_atom(k(A,L),Res,_),write(Res),
	write(ID,'(:derived ('), write(ID,Res),write(ID,') '),nl(ID),
	open('EpistemicFormulas',append,IDt),
	write(IDt,'('), write(IDt,Res),write(IDt,') '),nl(IDt),
	close(IDt),
	write(ID,'(and'),nl(ID), 
	runs(Runs),
	g_array_size(bck(Runs),Size),
	add_axioms_aux_d(Res,ID,0,Size),
	write(ID,'))'),nl(ID),
	add_axioms_d(T,ID).

add_axioms_d([k(A,neg(L))|T],ID).

%add_axioms_d([or(H)|T],ID):-
%	append(H,T,New),write(New),nl,
%	add_axioms_d(New,ID).

add_axioms_d([neg(Atom)|T],ID):-
	sensing_atom(Atom,Atom2,_),													%%%%%ALLAGH
	write(ID,'(:derived (not_'), write(ID,Atom2),write(ID,') '),nl(ID),
	open('EpistemicFormulas',append,IDt),
	write(IDt,'(not_'), write(IDt,Atom2),write(IDt,') '),nl(IDt),
	close(IDt),
	write(ID,'(and'),nl(ID), 
	runs(Runs),
	g_array_size(bck(Runs),Size),
	add_axioms_aux_d(neg(Atom),ID,0,Size),
	write(ID,'))'),nl(ID),
	add_axioms(T,ID).
	
add_axioms_d([Atom|T],ID):-
	write(ID,'(:derived ('), write(ID,Atom),write(ID,') '),nl(ID),
	open('EpistemicFormulas',append,IDt),
	write(IDt,'('), write(IDt,Atom),write(IDt,') '),nl(IDt),
	close(IDt),
	write(ID,'(and'),nl(ID), 
	g_array_size(bck(0),Size),
	add_axioms_aux_d(Atom,ID,0,Size),
	write(ID,'))'),nl(ID),
	add_axioms(T,ID).



add_axioms_aux_d(L,ID,Size,Size):-
	!,true.
add_axioms_aux_d(neg(L),ID,C,Size):-
	agents([H|_]),
	write(ID,'(or '),
	number_atom(C,C1atom),
	atom_concat('s',C1atom,State1),
	atom_concat(L,State1,Res),
	write(ID,'(not('),write(ID,Res),write(ID,')) '),
	write(ID,'('),write(ID,State1),write(ID,State1),write(ID,H),write(ID,'))'),nl(ID),
	NC is C + 1,
	add_axioms_aux_d(neg(L),ID,NC,Size).
add_axioms_aux_d(L,ID,C,Size):-
	agents([H|_]),
	write(ID,'(or '),
	number_atom(C,C1atom),
	atom_concat('s',C1atom,State1),
	atom_concat(L,State1,Res),
	write(ID,'('),write(ID,Res),write(ID,') '),
	write(ID,'('),write(ID,State1),write(ID,State1),write(ID,H),write(ID,'))'),nl(ID),
	NC is C + 1,
	add_axioms_aux_d(L,ID,NC,Size).
	
		
return_all_formulas([],[]).
return_all_formulas([[_,Prec,observe(and(H))]|T],[H,Prec|T2]):-
	return_all_formulas(T,T2).
return_all_formulas([[_,Prec,observe(or(H))]|T],[H,Prec|T2]):-
	return_all_formulas(T,T2).
return_all_formulas([[_,Prec,observe(H)]|T],[H,Prec|T2]):-
	return_all_formulas(T,T2).
return_all_formulas([[_,Prec,action(H1,H2,H3)]|T],[H1,H2,H3,Prec|T2]):-
	return_all_formulas(T,T2).
return_all_formulas([[_,Prec,sense(H)]|T],[H,Prec|T2]):-
	return_all_formulas(T,T2).
return_all_formulas([[_,Prec,sense(_,H)]|T],[H,Prec|T2]):-
	return_all_formulas(T,T2).
		
	
fix_epistemic_formulas(EF,RES):-
	flatten(EF,RemFEF),
\+	flatten(RemFEF,RemFEF),
	fix_epistemic_formulas(RemFEF,RES).
fix_epistemic_formulas(EF,RES):-
	flatten(EF,FlattenEF),
	remove_ands_ors_etc(FlattenEF,RES).	
	

remove_ands_ors_etc([],[]).
remove_ands_ors_etc([k(A,L)|T],[k(A,L)|T2]):-
	remove_ands_ors_etc(T,T2).
remove_ands_ors_etc([neg(k(A,L))|T],[k(A,L)|T2]):-
	remove_ands_ors_etc(T,T2).
remove_ands_ors_etc([and(X)|T],[X|T2]):-
	remove_ands_ors_etc(T,T2).
remove_ands_ors_etc([or(X)|T],[X|T2]):-
	remove_ands_ors_etc(T,T2).
remove_ands_ors_etc([neg(X)|T],[X|T2]):-
	remove_ands_ors_etc(T,T2).
remove_ands_ors_etc([X|T],[X|T2]):-
	remove_ands_ors_etc(T,T2).
%remove_ands_ors_etc([X|T],T2):-
%	remove_ands_ors_etc(T,T2).
remove_unnecessary_precs_2([],[]).
remove_unnecessary_precs_2([empty|T],T2):-
	remove_unnecessary_precs_2(T,T2).
remove_unnecessary_precs_2([or(L)|T],T2):-
	remove_unnecessary_precs_2(T,T2).
remove_unnecessary_precs_2([X|T],[X|T2]):-
	remove_unnecessary_precs_2(T,T2).		



remove_unnecessary_precs([],[]).
remove_unnecessary_precs([empty|T],T2):-
	remove_unnecessary_precs(T,T2).
remove_unnecessary_precs([or([neg(g),g])|T],T2):-
	remove_unnecessary_precs(T,T2).
remove_unnecessary_precs([or(X)|T],[X|T2]):-
	remove_unnecessary_precs(T,T2).
remove_unnecessary_precs([X|T],[X|T2]):-
	remove_unnecessary_precs(T,T2).	
	
	
add_deep_epistemic_formulas(S,R):-
	break_deep_epistemic_formulas(S,R),
	onlytheepistemic(R,R2),
	check_if_more_nested_formulas(R2,R).
add_deep_epistemic_formulas(S,R):-
	break_deep_epistemic_formulas(S,R1),!,
	onlytheepistemic(R1,R2), !,
	\+ check_if_more_nested_formulas(R2,R1),
	add_deep_epistemic_formulas(R1,R).
	



break_deep_epistemic_formulas([],[]):-!.
break_deep_epistemic_formulas([k(A,neg(k(B,L)))|T],[k(A,neg(k(B,L))),k(B,L)|T2]):-
	break_deep_epistemic_formulas(T,T2).	
break_deep_epistemic_formulas([k(A,k(B,L))|T],[k(A,k(B,L)),k(B,L)|T2]):-
	break_deep_epistemic_formulas(T,T2).
break_deep_epistemic_formulas([neg(k(A,L))|T],[k(A,L)|T2]):-
	break_deep_epistemic_formulas(T,T2).
break_deep_epistemic_formulas([H|T],[H|T2]):-
	break_deep_epistemic_formulas(T,T2).

check_if_more_nested_formulas([],R2):-
	!,true.
check_if_more_nested_formulas([k(A,neg(L))|T],R2):-
	member(L,R2),
	check_if_more_nested_formulas(T,R2).
check_if_more_nested_formulas([k(A,L)|T],R2):-
	member(L,R2),
	check_if_more_nested_formulas(T,R2).	
	
onlytheepistemic([],[]):-!.
onlytheepistemic([k(A,L)|T],[k(A,L)|T2]):-
	onlytheepistemic(T,T2),!.
onlytheepistemic([neg(k(A,L))|T],[neg(k(A,L))|T2]):-
	onlytheepistemic(T,T2),!.
onlytheepistemic([H|T],T2):-
	onlytheepistemic(T,T2),!.	
	
	
add_axiom(_,ID,Size,Size):-
	!,true.

add_axiom([k(A,neg(k(B,L2)))],ID,C,Size):-
	number_atom(C,C1atom),
%	add_axiom(k(B,L2),ID,C,Size), 
	sensing_atom(k(B,L2), Done,_),
	atom_concat('not_',Done,Done2),
%	atom_concat('not_',Res,L01),
	sensing_atom(k(A,Done2), Res0,_),
	atom_concat('s',C1atom,State1),
	atom_concat(Res0,State1,Res),
%	write(ID,'(:derived (true_d ?x )'),nl(ID),
	write(ID,'(:derived ('), write(ID,Res),write(ID,') '),nl(ID),
	write(ID,'(and'),nl(ID), 
	atom_concat(Done,State1,Atom),
	write(ID,'(not('),write(ID,Atom),write(ID,'))'),%nl(ID),
%	write(ID,'('),write(ID,Atom),write(ID,')'),%nl(ID),write('hola'),
	open('EpistemicFormulas',append,IDt),
	write(IDt,'('), write(IDt,Res),write(IDt,') '),nl(IDt),
	close(IDt),
 %   	write(ID,'(= ?x '),
%     write(ID,Res),write(ID,') '),
     	nl(ID),
   	%write(ID,'(and '),nl(ID),
  	once(add_axioms_ors(ID,A,neg(Done),C,0,Size)),
  	write(ID,'))'),
 % 	add_axioms([Res0],ID),
	%write(ID,')))'),
%	write(ID,'))'),
	nl(ID),nl(ID),
 	NC is C + 1,
     	add_axiom(k(A,neg(k(B,L2))),ID,NC,Size).

add_axiom(k(A,neg(k(B,L2))),ID,C,Size):-
	number_atom(C,C1atom),
%	add_axiom(k(B,L2),ID,C,Size), 
	sensing_atom(k(B,L2), Done,_),
	atom_concat('not_',Done,Done2),
%	atom_concat('not_',Res,L01),
	sensing_atom(k(A,Done2), Res0,_),
	atom_concat('s',C1atom,State1),
	atom_concat(Res0,State1,Res),
%	write(ID,'(:derived (true_d ?x )'),nl(ID),
	write(ID,'(:derived ('), write(ID,Res),write(ID,') '),nl(ID),
	write(ID,'(and'),nl(ID), 
	atom_concat(Done,State1,Atom),
	write(ID,'(not('),write(ID,Atom),write(ID,'))'),%nl(ID),
%	write(ID,'('),write(ID,Atom),write(ID,')'),%nl(ID),write('hola'),
	open('EpistemicFormulas',append,IDt),
	write(IDt,'('), write(IDt,Res),write(IDt,') '),nl(IDt),
	close(IDt),
 %   	write(ID,'(= ?x '),
%     write(ID,Res),write(ID,') '),
     	nl(ID),
   	%write(ID,'(and '),nl(ID),
  	once(add_axioms_ors(ID,A,neg(Done),C,0,Size)),
  	write(ID,'))'),
 % 	add_axioms([Res0],ID),
	%write(ID,')))'),
%	write(ID,'))'),
	nl(ID),nl(ID),
 	NC is C + 1,
     	add_axiom(k(A,neg(k(B,L2))),ID,NC,Size).	
add_axiom([k(A,k(B,L2))],ID,C,Size):-
	number_atom(C,C1atom),
%	add_axiom(k(B,L2),ID,C,Size), 
	sensing_atom(k(B,L2), Done,_),
%	atom_concat('not_',Res,L01),
	sensing_atom(k(A,Done), Res0,_),
	atom_concat('s',C1atom,State1),
	atom_concat(Res0,State1,Res),
%	write(ID,'(:derived (true_d ?x )'),nl(ID),
	write(ID,'(:derived ('), write(ID,Res),write(ID,') '),nl(ID),
	write(ID,'(and'),nl(ID), 
	atom_concat(Done,State1,Atom),
	write(ID,'('),write(ID,Atom),write(ID,')'),%nl(ID),write('hola'),
	open('EpistemicFormulas',append,IDt),
	write(IDt,'('), write(IDt,Res),write(IDt,') '),nl(IDt),
	close(IDt),
 %   	write(ID,'(= ?x '),
%     write(ID,Res),write(ID,') '),
     	nl(ID),
   	%write(ID,'(and '),nl(ID),
  	once(add_axioms_ors(ID,A,Done,C,0,Size)),
  	write(ID,'))'),
 % 	add_axioms([Res0],ID),
	%write(ID,')))'),
%	write(ID,'))'),
	nl(ID),nl(ID),
 	NC is C + 1,
     	add_axiom(k(A,k(B,L2)),ID,NC,Size).	
add_axiom(k(A,k(B,L2)),ID,C,Size):-
	number_atom(C,C1atom),
%	add_axiom(k(B,L2),ID,C,Size), 
	sensing_atom(k(B,L2), Done,_),
%	atom_concat('not_',Res,L01),
	sensing_atom(k(A,Done), Res0,_),
	atom_concat('s',C1atom,State1),
	atom_concat(Res0,State1,Res),
%	write(ID,'(:derived (true_d ?x )'),nl(ID),
	write(ID,'(:derived ('), write(ID,Res),write(ID,') '),nl(ID),
	write(ID,'(and'),nl(ID), 
	atom_concat(Done,State1,Atom),
	write(ID,'('),write(ID,Atom),write(ID,')'),%nl(ID),write('hola'),
	open('EpistemicFormulas',append,IDt),
	write(IDt,'('), write(IDt,Res),write(IDt,') '),nl(IDt),
	close(IDt),
 %   	write(ID,'(= ?x '),
%     write(ID,Res),write(ID,') '),
     	nl(ID),
   	%write(ID,'(and '),nl(ID),
  	once(add_axioms_ors(ID,A,Done,C,0,Size)),
  	write(ID,'))'),
%  	add_axioms([Res0],ID),
	%write(ID,')))'),
%	write(ID,'))'),
	nl(ID),nl(ID),
 	NC is C + 1,
     	add_axiom(k(A,k(B,L2)),ID,NC,Size).		
add_axiom([k(A,neg(L))],ID,C,Size):-
	number_atom(C,C1atom),
	atom_concat('not_',L,L01),
%	atom_concat(')',L01,L1),
	sensing_atom(k(A,L01), Res0,_),
	atom_concat('s',C1atom,State1),
	atom_concat(Res0,State1,Res),

%	write(ID,'(:derived (true_d ?x )'),nl(ID),
	write(ID,'(:derived ('), write(ID,Res),write(ID,') '),nl(ID),
	write(ID,'(and'),nl(ID), 
	atom_concat(L01,State1,Atom),
	write(ID,'('),write(ID,Atom),write(ID,')'),%nl(ID),
	open('EpistemicFormulas',append,IDt),
	write(IDt,'('), write(IDt,Res),write(IDt,') '),nl(IDt),
	close(IDt),
 %   	write(ID,'(= ?x '),
%     write(ID,Res),write(ID,') '),
     	nl(ID),
   	%write(ID,'(and '),nl(ID),
  	once(add_axioms_ors(ID,A,neg(L),C,0,Size)),
  	write(ID,'))'),
 % 	add_axioms([Res0],ID),
	%write(ID,')))'),
%	write(ID,'))'),
	nl(ID),nl(ID),
 	NC is C + 1,
     	add_axiom(k(A,neg(L)),ID,NC,Size).	
     	
add_axiom(k(A,neg(L)),ID,C,Size):-
	number_atom(C,C1atom),
	atom_concat('not_',L,L01),
	sensing_atom(k(A,L01), Res0,_),
	atom_concat('s',C1atom,State1),
	atom_concat(Res0,State1,Res),
	write(ID,'(:derived ('), write(ID,Res),write(ID,') '),nl(ID),
	write(ID,'(and'),nl(ID), 
	atom_concat(L,State1,Atom),
	write(ID,'(not('),write(ID,Atom),write(ID,'))'),%nl(ID),
	open('EpistemicFormulas',append,IDt),
	write(IDt,'('), write(IDt,Res),write(IDt,') '),nl(IDt),
	close(IDt),
    	nl(ID),
  	once(add_axioms_ors(ID,A,neg(L),C,0,Size)),
  	write(ID,'))'),
 % 	add_axioms([Res0],ID),
%	write(ID,'))'),
	nl(ID),nl(ID),
 	NC is C + 1,
     	add_axiom(k(A,neg(L)),ID,NC,Size).	
	
add_axiom([k(A,L)],ID,C,Size):-
	number_atom(C,C1atom),
	sensing_atom(k(A,L), Res0,_),
	atom_concat('s',C1atom,State1),
	atom_concat(Res0,State1,Res),
	write(ID,'(:derived ('), write(ID,Res),write(ID,') '),nl(ID),
	write(ID,'(and'),nl(ID), 
	atom_concat(L,State1,Atom),
	write(ID,'('),write(ID,Atom),write(ID,')'),%nl(ID),
	open('EpistemicFormulas',append,IDt),
	write(IDt,'('), write(IDt,Res),write(IDt,') '),nl(IDt),
	close(IDt),
     	nl(ID),
  	once(add_axioms_ors(ID,A,L,C,0,Size)),
  	write(ID,'))'),
%  	add_axioms([Res0],ID),
%	write(ID,'))'),
	nl(ID),nl(ID),
 	NC is C + 1,
     	add_axiom(k(A,L),ID,NC,Size).

add_axiom(k(A,L),ID,C,Size):-
	number_atom(C,C1atom),
	sensing_atom(k(A,L), Res0,_),
	atom_concat('s',C1atom,State1),
	atom_concat(Res0,State1,Res),
	write(ID,'(:derived ('), write(ID,Res),write(ID,') '),nl(ID),
	write(ID,'(and'),nl(ID), 
	atom_concat(L,State1,Atom),
	write(ID,'('),write(ID,Atom),write(ID,')'),%nl(ID),
	open('EpistemicFormulas',append,IDt),
	write(IDt,'('), write(IDt,Res),write(IDt,') '),nl(IDt),
	close(IDt),
     	nl(ID),
  	once(add_axioms_ors(ID,A,L,C,0,Size)),
  	write(ID,'))'),
%  	add_axioms([Res0],ID),
%	write(ID,'))'),
	nl(ID),nl(ID),
 	NC is C + 1,
     	add_axiom(k(A,L),ID,NC,Size).
add_axiom(_,_,_,_):-
	!,true. 	
	
return_agent(k(A,L),A).	
return_agent(X,X).

return_lit(k(A,L),L).
return_lit(k(A,neg(L)),L).	
return_lit(X,X).

sensing_atom(k(A,k(B,L)),Res,1 ):-
	sensing_atom(k(B,L),Res0,1),
	atom_concat('k',A,Res1),
	atom_concat(Res1,Res0,Res).	
sensing_atom(k(A,neg(k(B,L))),Res,1 ):-
	sensing_atom(k(B,L),Res0,1),
	atom_concat('not_',Res0,Res1),
	atom_concat('k',A,Res2),
	atom_concat(Res2,Res1,Res).		
sensing_atom(k(A,neg(L)), Res,1):-
	atom_concat('not_',L,Res0),
	atom_concat('k',A,Res1),
	atom_concat(Res1,Res0,Res).
sensing_atom(k(A,L), Res,1):-
	atom_concat('k',A,Res1),
	atom_concat(Res1,L,Res).
sensing_atom(Res, Res, 0).	
	
add_axioms_ors(_,_,_,_,Size,Size):-
	!,true.
	
add_axioms_ors(ID,A,neg(L),C,C2,Size):-
	C =\= C2,
	diz([C,C2]),
	write(ID,'(or '),%nl(ID),
	number_atom(C,Catom),
	number_atom(C2,C2atom),
	atom_concat('s',C2atom,Res0),
	atom_concat(L,Res0,Res0_2),
	write(ID,'(not('),write(ID,Res0_2),write(ID,')) '),%nl(ID),
     	atom_concat('s',Catom,Res1),
     	%%%%%adddiz	
     	atom_concat(Res1,Res0,Res2),
     	atom_concat(Res2,A,Res),
     	write(ID,'('),write(ID,Res),write(ID,') '),%nl(ID),
     	write(ID,') '),nl(ID),%nl(ID),
     	NC2 is C2 + 1,
     	add_axioms_ors(ID,A,neg(L),C,NC2,Size).
add_axioms_ors(ID,A,neg(L),C,C2,Size):-
	C =\= C2,
	write(ID,'(or '),%nl(ID),
	number_atom(C,Catom),
	number_atom(C2,C2atom),
	atom_concat('s',C2atom,Res0),
	atom_concat(L,Res0,Res0_2),
	write(ID,'(not('),write(ID,Res0_2),write(ID,')) '),%nl(ID),
     	atom_concat('s',Catom,Res1),
     	%%%%%adddiz	
     	atom_concat(Res0,Res1,Res2),
     	atom_concat(Res2,A,Res),
     	write(ID,'('),write(ID,Res),write(ID,') '),%nl(ID),
     	write(ID,') '),nl(ID),%nl(ID),
     	NC2 is C2 + 1,
     	add_axioms_ors(ID,A,neg(L),C,NC2,Size).
add_axioms_ors(ID,A,neg(L),C,C2,Size):-
     NC2 is C2 + 1,
     add_axioms_ors(ID,A,neg(L),C,NC2,Size).
     	
     
add_axioms_ors(ID,A,L,C,C2,Size):-
	C =\= C2,
	diz([C,C2]),
	write(ID,'(or '),%nl(ID),
	number_atom(C,Catom),
	number_atom(C2,C2atom),
	atom_concat('s',C2atom,Res0),
	atom_concat(L,Res0,Res0_2),
	write(ID,'('),write(ID,Res0_2),write(ID,') '),%nl(ID),
     	atom_concat('s',Catom,Res1),	
     	atom_concat(Res1,Res0,Res2),
     	atom_concat(Res2,A,Res),
     	write(ID,'('),write(ID,Res),write(ID,') '),%nl(ID),
     	write(ID,') '),nl(ID),%nl(ID),
     	NC2 is C2 + 1,
     	add_axioms_ors(ID,A,L,C,NC2,Size).
add_axioms_ors(ID,A,L,C,C2,Size):-
	C =\= C2,
	write(ID,'(or '),%nl(ID),
	number_atom(C,Catom),
	number_atom(C2,C2atom),
	atom_concat('s',C2atom,Res0),
	atom_concat(L,Res0,Res0_2),
	write(ID,'('),write(ID,Res0_2),write(ID,') '),%nl(ID),
     	atom_concat('s',Catom,Res1),	
     	atom_concat(Res0,Res1,Res2),
     	atom_concat(Res2,A,Res),
     	write(ID,'('),write(ID,Res),write(ID,') '),%nl(ID),
     	write(ID,') '),nl(ID),%nl(ID),
     	NC2 is C2 + 1,
     	add_axioms_ors(ID,A,L,C,NC2,Size).
add_axioms_ors(ID,A,L,C,C2,Size):-
     	NC2 is C2 + 1,
     	add_axioms_ors(ID,A,L,C,NC2,Size).			
	
	
	
	
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%555
read_possibleactions([],_):-!.
read_possibleactions([H|T],ID):-

	determine_action(H,ID),
	nl(ID),nl(ID),
	read_possibleactions(T,ID).
	
determine_action([Name,Prec,observe(Formula)],ID):-
	write(ID,' (:action '),
	write(ID,Name),nl(ID),
	add_precondition_all([Prec],ID),
	write(ID,':effect ( and '),nl(ID),
	runs(Run),		
	agents(Agents),		
	g_array_size(bck(Run),Size),
	observe_agents(ID,Formula,Agents,0,Size,Flag),!,
	nl(ID),write(ID,'))'),nl(ID),
%	add_axiom_precondition(Prec,ID),
	!,true.
		

determine_action([Name,Prec,action(Pr,Del,Add)],ID):-
	write(ID,' (:action '),write(ID,Name),nl(ID),
	%write('cdewcewcwecewcewcewcccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc'),
%	write(ID,Prec),nl(ID),
	add_precondition_all([Prec],ID),
	write(ID,':effect ( and '),nl(ID),
	runs(Run),		
	agents(Agents),		
	g_array_size(bck(Run),Size),
	add_action_effects(ID,Pr,Del,Add,0,Size,Pr,Del,Add),!,
	nl(ID),write(ID,'))'),nl(ID),
%	add_axiom_precondition(Prec,ID),
	!,true.
	

	
determine_action([Name,Prec,sense([Action|T])],ID):-
	write(ID,' (:action '),write(ID,Name),
%	write(ID,' (:action sense_all_'),
%	write(ID,Action),write(ID,T),
	nl(ID),
	add_precondition_all([Prec],ID),
	write(ID,':effect ( and '),nl(ID),
	runs(Run),
	agents(Agents),		
	g_array_size(bck(Run),Size),

	forallatoms(ID,[[Action|T]],Agents,Size),!,
	write(ID,' ))'),nl(ID),!.
%	add_axiom_precondition(Prec,ID),
%	add_possible_axioms([Action|T],ID,Size),!.

determine_action([Name,Prec,sense(Action)],ID):-
	sensing_atom(Action, AAction,Flag),
	return_agent(Action,SensedA),
	write(ID,' (:action '),write(ID,Name),nl(ID),
%	write(ID,AAction),nl(ID),
	add_precondition_all([Prec],ID),
	write(ID,':effect ( and '),nl(ID),
	runs(Run),
	num_agents(NAgents),		
	agents(Agents),		
	g_array_size(bck(Run),Size),
	create_conditionals_forall_agents(SensedA,ID,Action,AAction,Agents,0,Size,0,Size,Flag),!,
	write(ID,' ))'),nl(ID),!.
%	add_axiom_precondition(Prec,ID),
%	add_axiom(Action,ID,0,Size),!.
			
	
	
determine_action([Name,Prec,sense([H1|T1],[H2|T2])],ID):-
	runs(Run), 
	g_array_size(bck(Run),Size),
	write(ID,' (:action '),write(ID,Name),nl(ID),
	add_precondition_all([Prec],ID),
	write(ID,':effect ( and '),nl(ID),
	break_sensing_conc([H1|T1],[H2|T2],ID), 
	write(ID,' ))'),nl(ID),!.
%	add_axiom_precondition(Prec,ID),
%	add_possible_axioms([H2|T2],ID,Size),!.
	
	
determine_action([Name,Prec,sense([Agent],[Action])],ID):-
	write(ID,'Its a group sensing action'),nl(ID),!.	
	
add_precondition_all([or([neg(q),q])],ID):-!,true.	
add_precondition_all([or([neg(g),g])],ID):-!,true.	
add_precondition_all([empty],ID):-!,true.	
add_precondition_all([],ID).
add_precondition_all([H|T],ID):-
	write(ID,':precondition (and '),
	add_precondition([H|T],ID),
	write(ID,')'),nl(ID).

add_precondition([],ID).
add_precondition([H|T],ID):-
	add_precondition(H,ID),
	add_precondition(T,ID).

add_precondition(or([neg(q),q]),ID):-!,true.	
add_precondition(or([neg(g),g]),ID):-!,true.	
add_precondition(empty,ID):-!,true.

add_precondition(or(S),ID):-
	write(ID,'(or '),
	add_disj_precondition(S,ID),
	write(ID,') ').

add_precondition(k(A,L),ID):-
	sensing_atom(k(A,L),Res,_),
	write(ID,'('),write(ID,Res),write(ID,') ').
add_precondition(neg(k(A,L2)),ID):-
	sensing_atom(k(A,L2),L,_),												%%%%%allagh				
	write(ID,' (not_'),write(ID,L),write(ID,') ').
add_precondition(neg(L),ID):-
	write(ID,' (not_'),write(ID,L),write(ID,') ').
add_precondition(Prec,ID):-
	write(ID,' ('),write(ID,Prec),write(ID,') ').
%	add_axioms_d([Prec],ID).	


add_disj_precondition([],_):-!,true.
add_disj_precondition([H|T],ID):-
%	write(ID,'('),
%	write(ID,H),
%	write(ID,') '),
	add_precondition(H,ID),
	add_disj_precondition(T,ID).

observe_conjunction_aux([],State1,ID):-!,true.
observe_conjunction_aux([Formula|T],State1,ID):-
	sensing_atom(Formula,R,1),
	atom_concat(R,State1,Res),
	write(ID,'(not('),
	write(ID,Res),
	write(ID,')) '),
	observe_conjunction_aux(T,State1,ID).


observe_conjunction([],State1,ID):-!,true.
observe_conjunction(Formula,State1,ID):-
	write(ID,'( when (or '),
	observe_conjunction_aux(Formula,State1,ID).

observe_disjunction_aux([],State1,ID):-!,true.
observe_disjunction_aux([Formula|T],State1,ID):-
	sensing_atom(Formula,R,1),
	atom_concat(R,State1,Res),
	write(ID,'(not('),
	write(ID,Res),
	write(ID,')) '),
	observe_disjunction_aux(T,State1,ID).


observe_disjunction([],State1,ID):-!,true.
observe_disjunction(Formula,State1,ID):-
	write(ID,'( when (and '),
	observe_disjunction_aux(Formula,State1,ID).


observe_agents(ID,Formula,Agents,Size,Size,Flag):-
	!,true.
observe_agents(ID,and(Formula),Agents,C,Size,Flag):-
	number_atom(C,Catom),
	atom_concat('s',Catom,State1),
%	write(ID,'( when (or '),
	observe_conjunction(Formula,State1,ID),


%	atom_concat(Formula,State1,StateF),
%	write(ID,'( when (not('),write(ID,StateF),
%	write(ID,')) (and '),
	write(ID,') (and '),
	add_ds_for_observe(ID,C,0,Size,Agents),
	write(ID,'))'),nl(ID),
	NC is C + 1,
	observe_agents(ID,and(Formula),Agents,NC,Size,Flag).	
observe_agents(ID,or(Formula),Agents,C,Size,Flag):-
	number_atom(C,Catom),
	atom_concat('s',Catom,State1),
%	write(ID,'( when (or '),
	observe_disjunction(Formula,State1,ID),


%	atom_concat(Formula,State1,StateF),
%	write(ID,'( when (not('),write(ID,StateF),
%	write(ID,')) (and '),
	write(ID,') (and '),
	add_ds_for_observe(ID,C,0,Size,Agents),
	write(ID,'))'),nl(ID),
	NC is C + 1,
	observe_agents(ID,or(Formula),Agents,NC,Size,Flag).	
observe_agents(ID,Formula,Agents,C,Size,Flag):-
	number_atom(C,Catom),
	atom_concat('s',Catom,State1),
	atom_concat(Formula,State1,StateF),
	write(ID,'( when (not('),write(ID,StateF),
	write(ID,')) (and '),
	add_ds_for_observe(ID,C,0,Size,Agents),
	write(ID,'))'),nl(ID),
	NC is C + 1,
	observe_agents(ID,Formula,Agents,NC,Size,Flag).	
	
	
add_action_effects(ID,[],[],[],_,Size,Pr,Del,Add):-
	!,true.
add_action_effects(ID,[Hp|Tp],[Hd|Td],[Ha|Ta],Size,Size,Pr,Del,Add):-
	add_action_effects(ID,Tp,Td,Ta,0,Size,Pr,Del,Add).
	
add_action_effects(ID,[Hp|Tp],[Hd|Td],[Ha|Ta],C,Size,Pr,Del,Add):-
	number_atom(C,Catom),
	atom_concat('s',Catom,State),
	write(ID,'( when (and '),
	add_action_conditional(ID,Hp,State),
	write(ID,') (and '),
	add_action_conditional_del(ID,Hd,State),
	add_action_conditional(ID,Ha,State),
	write(ID,'))'),nl(ID),
	NC is C + 1,
	add_action_effects(ID,[Hp|Tp],[Hd|Td],[Ha|Ta],NC,Size,Pr,Del,Add).
	
	
forallatoms(_,[[]],_,_):-!.
forallatoms(ID,[[Action|T]],Agents,Size):-
    
	sensing_atom(Action, AAction,Flag),	

	return_agent(Action,SensedA),	
	create_conditionals_forall_agents(SensedA,ID,Action,AAction,Agents,0,Size,0,Size,Flag),
	forallatoms(ID,[T],Agents,Size). 
	
	
create_conditionals_forall_agents(SensedA,ID,Orig,AAction,Agents,Size,Size,_,Size,F):-
	!,true.

create_conditionals_forall_agents(SensedA,ID,Orig,AAction,Agents,C1,Size,Size,Size,F):-
	NC1 is C1 + 1,
	create_conditionals_forall_agents(SensedA,ID,Orig,AAction,Agents,NC1,Size,0,Size,F).
create_conditionals_forall_agents(SensedA,ID,Orig,AAction,Agents,C1,Size,C2,Size,1):-
	C1 =\= C2,
	number_atom(C1,C1atom),
	atom_concat('s',C1atom,State1),
	number_atom(C2,C2atom),
	atom_concat('s',C2atom,State2),
	diz([C1,C2]),
	
	
	check_if_is_is_an_or_formula(ID,C1, C2,Act1,Act2, Orig,AAction, AAction,SensedA,ID,State1,State2,Agents),

%	write(ID,' (and '),
%	create_ds_forallagents(SensedA,ID,State1,State2,Agents),
%	write(ID,'))'),nl(ID),
	NC2 is C2 + 1,
	create_conditionals_forall_agents(SensedA,ID,Orig,AAction,Agents,C1,Size,NC2,Size,1).
create_conditionals_forall_agents(SensedA,ID,Orig,AAction,Agents,C1,Size,C2,Size,1):-
	C1 =\= C2,
	number_atom(C1,C1atom),
	atom_concat('s',C1atom,State1),
	number_atom(C2,C2atom),
	atom_concat('s',C2atom,State2),

	check_if_is_is_an_or_formula(ID,C1, C2,Act1,Act2,Orig, AAction, AAction,SensedA,ID,State2,State1,Agents),

%	write(ID,' (and '),
%	create_ds_forallagents(SensedA,ID,State2,State1,Agents),
%	write(ID,'))'),nl(ID),
	NC2 is C2 + 1,
	create_conditionals_forall_agents(SensedA,ID,Orig,AAction,Agents,C1,Size,NC2,Size,1).
create_conditionals_forall_agents(SensedA,ID,Orig,AAction,Agents,C1,Size,C2,Size,0):-
	C1 =\= C2,
	number_atom(C1,C1atom),
	atom_concat('s',C1atom,State1),
	number_atom(C2,C2atom),
	atom_concat('s',C2atom,State2),
	diz([C1,C2]),

	check_if_is_is_an_or_formula(ID,C1, C2,Act1,Act2,Orig, AAction, AAction,SensedA,ID,State1,State2,Agents),

%	write(ID,' (and '),
%	create_ds_forallagents(SensedA,ID,State1,State2,Agents),
%	create_ds_forallagents(ID,State1,State2,Agents),
%	write(ID,'))'),nl(ID),
	NC2 is C2 + 1,
	create_conditionals_forall_agents(SensedA,ID,Orig,AAction,Agents,C1,Size,NC2,Size,0).
create_conditionals_forall_agents(SensedA,ID,Orig,AAction,Agents,C1,Size,C2,Size,0):-
	C1 =\= C2,
	number_atom(C1,C1atom),
	atom_concat('s',C1atom,State1),
	number_atom(C2,C2atom),
	atom_concat('s',C2atom,State2),

	check_if_is_is_an_or_formula(ID,C1, C2,Act1,Act2,Orig, AAction, AAction,SensedA,ID,State2,State1,Agents),
	

%	write(ID,' (and '),
%	create_ds_forallagents(SensedA,ID,State2,State1,Agents),
%	create_ds_forallagents(ID,State1,State2,Agents),
%	write(ID,'))'),nl(ID),
	NC2 is C2 + 1,
	create_conditionals_forall_agents(SensedA,ID,Orig,AAction,Agents,C1,Size,NC2,Size,0).
create_conditionals_forall_agents(SensedA,ID,Orig,AAction,Agents,C1,Size,C2,Size,F):-
	NC2 is C2 + 1,
	create_conditionals_forall_agents(SensedA,ID,Orig,AAction,Agents,C1,Size,NC2,Size,F).

check_if_is_is_an_or_formula(ID,S1,S2,R1,R2,Orig, or([]), or([X|T]),SensedA,ID,State1,State2,Agents).
check_if_is_is_an_or_formula(ID,C1,C2,R1,R2,Orig, or([X1|T1]), or([X|T]),SensedA,ID,State1,State2,Agents):-
	number_atom(C1,C1atom),
	atom_concat('s',C1atom,S1),
	number_atom(C2,C2atom),
	atom_concat('s',C2atom,S2),

	write(ID,'( when (and '),
	add_or_lits(ID,[X1],S1),
	%write(ID,') '),
	add_neg_or_lits(ID,[X|T],S2),
	write(ID,')'),
	write(ID,' (and '),
	create_ds_forallagents(SensedA,ID,State1,State2,Agents),
	write(ID,'))'),nl(ID),
	check_if_is_is_an_or_formula(ID,S1,S2,R1,R2,Orig, or(T1), or([X|T]),SensedA,ID,State1,State2,Agents).






















%% check_if_is_is_an_or_formula(ID,C1,C2,R1,R2,k(Ag,Lit),A,A,SensedA,ID,State1,State2,Agents):-
%% 	number_atom(C1,C1atom),
%% 	atom_concat('s',C1atom,S1),
%% 	number_atom(C2,C2atom),
%% 	atom_concat('s',C2atom,S2),

%% 	static_atoms(SA),
%% 	member(Lit,SA),
%% 	atom_concat(A,S1,R1),
%% 	atom_concat(A,S2,R2),
%% 	runs(Runs),
%% 	g_read(bck(Runs,C1),[StateS1,_]),
%% 	g_read(bck(Runs,C2),[StateS2,_]),
%% 	member(Lit,StateS1),
%% 	\+ member(Lit,StateS2),
%% 	write(ID,'( when ('),
%% %			add_or_lits(ID,[A],S1),

%% 	write(ID,R1),
%% 	write(ID,') '), 
%% %	write(ID,R2), 
%% %	add_or_lits(ID,[A],S2),
%% %	write(ID,')))'),
%% 	write(ID,' (and '),
%% 	create_ds_forallagents(SensedA,ID,State1,State2,Agents),
%% 	write(ID,'))'),
%% nl(ID).

%% check_if_is_is_an_or_formula(ID,C1,C2,R1,R2,k(Ag,Lit),A,A,SensedA,ID,State1,State2,Agents):-
%% 	number_atom(C1,C1atom),
%% 	atom_concat('s',C1atom,S1),
%% 	number_atom(C2,C2atom),
%% 	atom_concat('s',C2atom,S2),

%% 	static_atoms(SA),
%% 	member(Lit,SA),
%% 	atom_concat(A,S1,R1),
%% 	atom_concat(A,S2,R2),
%% 	runs(Runs),
%% 	g_read(bck(Runs,C1),[StateS1,_]),
%% 	g_read(bck(Runs,C2),[StateS2,_]),
%% 	\+ member(Lit,StateS1),
%% 	member(Lit,StateS2),
%% 	write(ID,'( when ('),
%% %			add_or_lits(ID,[A],S1),

%% 	write(ID,R1),
%% 	write(ID,') '), 
%% %	write(ID,R2), 
%% %	add_or_lits(ID,[A],S2),
%% %	write(ID,')))'),
%% 	write(ID,' (and '),
%% 	create_ds_forallagents(SensedA,ID,State1,State2,Agents),
%% 	write(ID,'))'),
%% 	nl(ID).







































check_if_is_is_an_or_formula(ID,C1,C2,R1,R2,Orig,A,A,SensedA,ID,State1,State2,Agents):-
	number_atom(C1,C1atom),
	atom_concat('s',C1atom,S1),
	number_atom(C2,C2atom),
	atom_concat('s',C2atom,S2),

	static_atoms(SA),
	memberchk(A,SA),
	atom_concat(A,S1,R1),
	atom_concat(A,S2,R2),
	runs(Runs),
	g_read(bck(Runs,C1),[StateS1,_]),%!,
	g_read(bck(Runs,C2),[StateS2,_]),%!,
	memberchk(A,StateS1),
	\+ memberchk(A,StateS2),
%	dstatesused(DSU),!,
%	\+ memberchk([C1,C2],DSU),!,
%	\+ memberchk([C2,C1],DSU),!,
%	append([[C1,C2]],DSU,DSU2),!,
%	retract(dstatesused(_)),!,
%	asserta(dstatesused(DSU2)),!, 
%	write(ID,'( when (and ('),
%			add_or_lits(ID,[A],S1),

%	write(ID,R1),
%	write(ID,') (not('), 
%	write(ID,R2), 
%	add_or_lits(ID,[A],S2),
%	write(ID,')))'),
%	write(ID,' (and '),
	create_ds_forallagents(SensedA,ID,State1,State2,Agents),
%	write(ID,'))'),
nl(ID).

check_if_is_is_an_or_formula(ID,C1,C2,R1,R2,Orig,A,A,SensedA,ID,State1,State2,Agents):-
	number_atom(C1,C1atom),
	atom_concat('s',C1atom,S1),
	number_atom(C2,C2atom),
	atom_concat('s',C2atom,S2),

	static_atoms(SA),
	memberchk(A,SA),
	atom_concat(A,S1,R1),
	atom_concat(A,S2,R2),
	runs(Runs),
	g_read(bck(Runs,C1),[StateS1,_]),%!,
	g_read(bck(Runs,C2),[StateS2,_]),%!,
	\+ memberchk(A,StateS1),
	memberchk(A,StateS2),

%	dstatesused(DSU),!,
%	\+ memberchk([C1,C2],DSU),!,
%	\+ memberchk([C2,C1],DSU),!,
%	append([[C1,C2]],DSU,DSU2),!,
%	retract(dstatesused(_)),!,
%	asserta(dstatesused(DSU2)),!, 
%	write(ID,'( when (and ('),
%			add_or_lits(ID,[A],S1),

%	write(ID,R1),
%	write(ID,') (not('), 
%	write(ID,R2), 
%	add_or_lits(ID,[A],S2),
%	write(ID,')))'),
%	write(ID,' (and '),
	create_ds_forallagents(SensedA,ID,State1,State2,Agents),
%	write(ID,'))'),
	nl(ID).

check_if_is_is_an_or_formula(ID,C1,C2,R1,R2,Orig,A,A,SensedA,ID,State1,State2,Agents):-
	number_atom(C1,C1atom),
	atom_concat('s',C1atom,S1),
	number_atom(C2,C2atom),
	atom_concat('s',C2atom,S2),

	static_atoms(SA),
	member(A,SA).

check_if_is_is_an_or_formula(ID,C1,C2,R1,R2,Orig,A,A,SensedA,ID,State1,State2,Agents):-
	number_atom(C1,C1atom),
	atom_concat('s',C1atom,S1),
	number_atom(C2,C2atom),
	atom_concat('s',C2atom,S2),
	
	atom_concat(A,S1,R1),
	atom_concat(A,S2,R2),
	
	write(ID,'( when (and ('),
%			add_or_lits(ID,[A],S1),

	write(ID,R1),
	write(ID,') (not('), 
	write(ID,R2), 
%	add_or_lits(ID,[A],S2),
	write(ID,')))'),
	write(ID,' (and '),
	create_ds_forallagents(SensedA,ID,State1,State2,Agents),
	write(ID,'))'),nl(ID).


add_or_lits(_,[],_):-!,true.

add_or_lits(ID,[k(A,L)|T],S1):-
	write(ID,'('),
	sensing_atom(k(A,L),Res,1),
	atom_concat(Res,S1,R1),
	write(ID,R1),write(ID,') '),
	add_or_lits(ID,T,S1).

add_or_lits(ID,[neg(X)|T],S1):-
	write(ID,'(not('),
	atom_concat(X,S1,R1),
	write(ID,R1),write(ID,')) '),
	add_or_lits(ID,T,S1).


add_or_lits(ID,[X|T],S1):-
	write(ID,'('),
	atom_concat(X,S1,R1),
	write(ID,R1),write(ID,') '),
	add_or_lits(ID,T,S1).




add_neg_or_lits(_,[],_):-true.

add_neg_or_lits(ID,[k(A,L)|T],S2):-
%	write(ID,'('),
	sensing_atom(k(A,L),Res,1),
	atom_concat(Res,S2,R1),

	write(ID,'(not('),
	write(ID,R1),
	write(ID,')) '),
	add_neg_or_lits(ID,T,S2).

add_neg_or_lits(ID,[neg(X)|T],S2):-
	atom_concat(X,S2,R1),
	write(ID,'('),
		write(ID,R1),
		write(ID,') '),
		add_neg_or_lits(ID,T,S2).

add_neg_or_lits(ID,[X|T],S2):-
	atom_concat(X,S2,R1),
	write(ID,'(not('),
		write(ID,R1),
		write(ID,')) '),
		add_neg_or_lits(ID,T,S2).


	
break_sensing_conc([],[],_).
break_sensing_conc([H1|T1],[H2|T2],ID):-
		break_sensing(H1,H2,ID),
		break_sensing_conc(T1,T2,ID).


break_sensing(_,[],_):-
	dstatesused(DSU),!,				%%%%% THIS WAS ADDED FOR DUPLICATE STATIC ATOMS EFFECTS
	retract(dstatesused(_)),!,
	asserta(dstatesused([])).
break_sensing(A,[H|T],ID):-
	sensing_atom(H, AAction,Flag), 
	runs(Run),			
	g_array_size(bck(Run),Size),
	create_conditionals_forall_agents('q',ID,H,AAction,[A],0,Size,0,Size,Flag),
	break_sensing(A,T,ID).
	
add_dss(ID,[],S1,Size,S2,Size):-
	!,true.
add_dss(ID,[A|T],S1,Size,Size,Size):-
	add_dss(ID,T,0,Size,0,Size).
add_dss(ID,[A|T],Size,Size,S2,Size):-
	NS2 is S2 + 1,
	add_dss(ID,[A|T],0,Size,NS2,Size).
add_dss(ID,[A|T],S1,Size,S2,Size):-
	number_atom(S1,S1atom),
	number_atom(S2,S2atom),
	diz([S1,S2]),
	atom_concat('s',S1atom,State1),
	atom_concat('s',S2atom,State2),
	atom_concat(State1,State2,States),
	atom_concat(States,A,Res),
	write(ID,'('),write(ID,Res),write(ID,')'),nl(ID),
	NS1 is S1 + 1,
	add_dss(ID,[A|T],NS1,Size,S2,Size).
%% add_dss(ID,[A|T],S1,Size,S2,Size):-
%% 	number_atom(S1,S1atom),
%% 	number_atom(S2,S2atom),
%% 	diz(LOL),
%% 	memberchk([S2,S1],LOL),!,
%% 	atom_concat('s',S1atom,State1),
%% 	atom_concat('s',S2atom,State2),
%% 	atom_concat(State2,State1,States),
%% 	atom_concat(States,A,Res),
%% 	write(ID,'('),write(ID,Res),write(ID,')'),nl(ID),
%% 	NS1 is S1 + 1,
%% 	add_dss(ID,[A|T],NS1,Size,S2,Size).
add_dss(ID,[A|T],S1,Size,S2,Size):-
	NS1 is S1 + 1,
	add_dss(ID,[A|T],NS1,Size,S2,Size).
	
	
add_ds_for_observe(ID,C,Size,Size,Agents):-
	!,true.
add_ds_for_observe(ID,C,C1,Size,Agents):-
	diz([C,C1]),
	number_atom(C,Catom),
	number_atom(C1,C1atom),
	atom_concat('s',Catom,State1),
	atom_concat('s',C1atom,State2),
	create_ds_forallagents(ID,State1,State2,Agents),nl(ID),
	NC1 is C1 + 1,
	add_ds_for_observe(ID,C,NC1,Size,Agents).
add_ds_for_observe(ID,C,C1,Size,Agents):-
	number_atom(C,Catom),
	number_atom(C1,C1atom),
	atom_concat('s',Catom,State1),
	atom_concat('s',C1atom,State2),
	create_ds_forallagents(ID,State2,State1,Agents),nl(ID),
	NC1 is C1 + 1,
	add_ds_for_observe(ID,C,NC1,Size,Agents).
	
	
add_action_conditional(ID,[],S):-
	!,true.
	
	
add_action_conditional(ID,[and(H)|T],S):-
	addallconditions(ID,H,S),
	add_action_conditional(ID,T,S).
		
add_action_conditional(ID,[k(A,L)|T],S):-
	sensing_atom(k(A,L),Res,1),
	addallconditions(ID,[Res],S),
	add_action_conditional(ID,T,S).
	
		
add_action_conditional(ID,[neg(H)|T],S):-
	atom_concat(H,S,Ce),
	write(ID,'(not('),write(ID,Ce),write(ID,')) '),
	add_action_conditional(ID,T,S).	
add_action_conditional(ID,[H|T],S):-
	atom_concat(H,S,Ce),
	write(ID,'('),write(ID,Ce),write(ID,') '),
	add_action_conditional(ID,T,S).
	
add_action_conditional_del(ID,[],S):-
	!,true.	
add_action_conditional_del(ID,[H|T],S):-
	atom_concat(H,S,Ce),
	write(ID,'(not('),write(ID,Ce),write(ID,')) '),
	add_action_conditional_del(ID,T,S).

	
addallconditions(ID,[],S).
addallconditions(ID,[H|T],S):-
	add_action_conditional(ID,[H],S),
	addallconditions(ID,T,S).
	
create_ds_forallagents(_,_,_,[]):-!,true.
create_ds_forallagents(ID,State1,State1,[H|T]):-
	atom_concat(State1,State1,DStates1),
	atom_concat(DStates1,H,Res1),
	write(ID,'('), write(ID,Res1),write(ID,') '),
	create_ds_forallagents(ID,State1,State1,T).

create_ds_forallagents(ID,State1,State2,[H|T]):-
	atom_concat(State1,State2,DStates1),
	atom_concat(State2,State1,DStates2),
	atom_concat(DStates1,H,Res1),
	atom_concat(DStates2,H,Res2),
	write(ID,'('), write(ID,Res1),write(ID,') '),
%	write(ID,'('), write(ID,Res2),write(ID,') '),
	create_ds_forallagents(ID,State1,State2,T).
	
create_ds_forallagents(_,_,_,_,[]):-!.
create_ds_forallagents(H,ID,State1,State2,[H|T]):-
	create_ds_forallagents(H,ID,State1,State2,T).
create_ds_forallagents(SensedA,ID,State1,State2,[H|T]):-
	atom_concat(State1,State2,DStates1),
	atom_concat(State2,State1,DStates2),
	atom_concat(DStates1,H,Res1),
	atom_concat(DStates2,H,Res2),
	write(ID,'('), write(ID,Res1),write(ID,') '),
%	write(ID,'('), write(ID,Res2),write(ID,') '),
	create_ds_forallagents(SensedA,ID,State1,State2,T).	
		
	
	
	
