anaction(Do):-
	aux_action(Do,[Con,Del,Add]),write(Con),nl,write(Del),nl,write(Add),nl,
	action(Con,Del,Add).

aux_action([],[[],[],[]]):-!.
aux_action([[Con,Del,Add]|T],[[[Con]|Tc],[[Del]|Td],[[Add]|Ta]]):-
	aux_action(T,[Tc,Td,Ta]).
	

action(Conditions,Del,Add):-
	runs(R),
	g_array_size(bck(R),Size), 
	apply_action(R,Conditions,Del,Add,0,Size),
	agents(Ag),
	fixremainingbels(R,0,Size,Ag),
	NRun is R + 1,
	retract(runs(_)),
	asserta(runs(NRun)),!.


apply_action(_,_,_,_,Size,Size):-true.
apply_action(R,Conditions,Del,Add,C,Size):-			
	action_deladd(R,Conditions,Del,Add,C),
	NC is C + 1,
	apply_action(R,Conditions,Del,Add,NC,Size).


action_deladd(_,[],[],[],_):-!,true.
action_deladd(R,[[Hc]|Tc],[Hd|Td],[Ha|Ta],C):-
	g_read(bck(R,C),[Inter,_]),
	formula(R,Hc,[C]),
	action_del(R,Hd,C,Inter),
	NR is R + 1,
	g_read(bck(NR,C),[NInter,_]),
	action_add(R,Ha,C,NInter)
	;
	g_read(bck(R,C), [I,BB]),
	NR is R + 1,
	g_assign(bck(NR,C),[I,BB]),
	action_deladd(R,Tc,Td,Ta,C).


action_add(R,[],Counter,Result):-
	g_read(bck(R,Counter), [_,B]),
	NR is R + 1,
	g_assign(bck(NR,Counter), [Result,B]),true.
action_add(R,[H|T],C,Inter):-
	\+ member(H,Inter),
	append([H],Inter,NInter),
	action_add(R,T,C,NInter)
	;
	action_add(R,T,C,Inter).


action_del(R,[],Counter,Result):-
	g_read(bck(R,Counter), [_,B]),
	NR is R + 1,
	g_assign(bck(NR,Counter), [Result,B]),true.
action_del(R,[H|T],C,Inter):-
	member(H,Inter),
	remove(H,Inter,NInter),
	action_del(R,T,C,NInter)
	;
	action_del(R,T,C,Inter).

change_state(neg(A),Counter,Inter):-
	remove(A,Inter,Result),
	g_read(bck(Counter), [_,B]),
	g_assign(bck(Counter), [Result,B]),!.

change_state([],_,_):-!.
change_state([H|T],Counter,Inter):-
	change_state(H,Counter,Inter),
	change_state(T,Counter,Inter),!.

change_state(Effects,Counter,Inter):-
	append([Effects],Inter,Result),!,
	g_read(bck(Counter), [_,B]),!,
	g_assign(bck(Counter), [Result,B]),!.
	
	
remove(_,[],[]):-!.
remove(X,[X|T],NInter):-
	remove(X,T,NInter),!.	
remove(X,[H|T],[H|T2]):-
	remove(X,T,T2),!.
