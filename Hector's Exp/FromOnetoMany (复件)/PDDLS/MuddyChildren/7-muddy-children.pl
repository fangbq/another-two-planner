%%
%%
%%	The 7-muddy children problem
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

find_plan:-
	search( [ [or([neg(ma),ma]),0] ] , impl(ma,k(a,ma))).



goal([

or([k(a,ma),k(a,neg(ma))]),
or([k(b,mb),k(b,neg(mb))]),
or([k(c,mc),k(c,neg(mc))]),
or([k(d,md),k(d,neg(md))]),
or([k(e,me),k(e,neg(me))]),
or([k(f,mf),k(f,neg(mf))]),
or([k(g,mg),k(g,neg(mg))])


]).
	
		
num_events(120).
num_agents(7).
agents([a,b,c,d,e,f,g]).%,g,h,i]).		
static_atoms([ma,mb,mc,md,me,mf,mg,p]).
atoms([ma,mb,mc,md,me,mf,mg,p]).

possibleactions([	
%%	[r1a,sense([[r1red, r1blue, r1pink, r1yellow]])],
	[observation,or([neg(g),g]),observe(p)],
	[a_senses,or([neg(g),g]),sense([a],[[mb,mc,md,me,mf,mg]])],
	[b_senses,or([neg(g),g]),sense([b],[[ma,mc,md,me,mf,mg]])],
	[c_senses,or([neg(g),g]),sense([c],[[ma,mb,md,me,mf,mg]])],
	[d_senses,or([neg(g),g]),sense([d],[[ma,mb,mc,me,mf,mg]])],
	[e_senses,or([neg(g),g]),sense([e],[[ma,mb,mc,md,mf,mg]])],
	[f_senses,or([neg(g),g]),sense([f],[[ma,mb,mc,me,md,mg]])],
	[g_senses,or([neg(g),g]),sense([g],[[ma,mb,mc,me,mf,md]])],
%	[h_senses,or([neg(g),g]),sense([h],[[ma,mb,mc,me,mf,mg,md,mi]])],
%	[i_senses,or([neg(g),g]),sense([i],[[ma,mb,mc,me,mf,mg,mh,md]])],
	
	
	
	
	[all_sense,or([neg(g),g]),sense([k(d,md),k(a,ma),k(b,mb),k(c,mc),k(e,me),k(f,mf),k(g,mg)])]
%	[or([neg(g),g]),sense(k(a,ma))],
%	[or([neg(g),g]),sense(k(b,mb))],
%	[or([neg(g),g]),sense(k(c,mc))]
    	]).	

%run:- %observe(or([ma,mb,mc])),	
%	 sense(a,mb),
%	 sense(a,mc),
%	 sense(b,ma),
%	 sense(b,mc),
%	 sense(c,mb),
%	 sense(c,ma),
%	 !.
run:-
	!,true.	
constraint(H):-
	 cformula(or([and([or([ma,mb,mc,md,me,mf,mg]), p]) , and([neg(or([ma,mb,mc,md,me,mf,mg])), neg(p)])]),H),
	 
	 
	 					%	and([p,or([ma,mb,mc])]),H),
	!,true.	
	
axioms_to_add([]).
invariant([]).

p_commonknowledge([

    
    ]).
n_commonknowledge([



    ]).

p_commonknowledge([

    
    ]).
n_commonknowledge([



    ]).

invariant([
 ]).
user_input_states([
	]).

