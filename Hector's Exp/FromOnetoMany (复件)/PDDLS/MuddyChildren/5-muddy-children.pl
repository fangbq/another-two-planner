%%
%%
%%	The 5-muddy children problem
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

find_plan:-
	search( [ [or([neg(ma),ma]),0] ] , impl(ma,k(a,ma))).

p_commonknowledge([

    
    ]).
n_commonknowledge([



    ]).

invariant([
 ]).
user_input_states([
	]).

%static_atoms([]).
static_atoms([ma,mb,mc,md,me]).

goal([
or([k(a,ma),k(a,neg(ma))]),
or([k(b,mb),k(b,neg(mb))]),
or([k(c,mc),k(c,neg(mc))]),
or([k(d,md),k(d,neg(md))]),
or([k(e,me),k(e,neg(me))])
]).
	
		
num_events(120).
num_agents(5).
agents([a,b,c,d,e]).%,e,f]).%,g,h,i]).		
atoms([ma,mb,mc,md,me,p]).%me,mf,p]).%,mg,mh,mi,p]).

possibleactions([	
%%	[r1a,sense([[r1red, r1blue, r1pink, r1yellow]])],
	[observation,or([neg(g),g]),observe(p)],
	[a_senses,or([neg(g),g]),sense([a],[[mb,mc,md,me]])],%,me,mf]])],%mg,mh,mi]])],
	[b_senses,or([neg(g),g]),sense([b],[[ma,mc,md,me]])],%,me,mf]])],%mg,mh,mi]])],
	[c_senses,or([neg(g),g]),sense([c],[[ma,mb,md,me]])],%,me,mf]])],%mg,mh,mi]])],
	[d_senses,or([neg(g),g]),sense([d],[[ma,mb,mc,me]])],%,me,mf]])],%mg,mh,mi]])],
	[e_senses,or([neg(g),g]),sense([e],[[ma,mb,mc,md]])],%mg,mh,mi]])],
%	[f_senses,or([neg(g),g]),sense([f],[[ma,mb,mc,me,md]])],%mg,mh,mi]])],
%	[g_senses,or([neg(g),g]),sense([g],[[ma,mb,mc,me,md,mf]])],
%	[or([neg(g),g]),sense([h],[[ma,mb,mc,me,mf,mg,md,mi]])],
%	[or([neg(g),g]),sense([i],[[ma,mb,mc,me,mf,mg,mh,md]])],
	
	
	
	
	[all_sense,or([neg(g),g]),sense([k(e,me),k(d,md),k(a,ma),k(b,mb),k(c,mc)])]%,k(e,me),k(f,mf)]])]%,k(g,mg),k(h,mh),k(i,mi)]])]
%	[or([neg(g),g]),sense(k(a,ma))],
%	[or([neg(g),g]),sense(k(b,mb))],
%	[or([neg(g),g]),sense(k(c,mc))]
    	]).	

%run:- %observe(or([ma,mb,mc])),	
%	 sense(a,mb),
%	 sense(a,mc),
%	 sense(b,ma),
%	 sense(b,mc),
%	 sense(c,mb),
%	 sense(c,ma),
%	 !.
run:-
	!,true.	
constraint(H):-
	 cformula(or([and([or([ma,mb,mc,md,me]), p]) , and([neg(or([ma,mb,mc,md,me])), neg(p)])]),H),
	 
	 
	 					%	and([p,or([ma,mb,mc])]),H),
	!,true.	
	
axioms_to_add([]).
invariant([]).
p_commonknowledge([

    
    ]).
n_commonknowledge([



    ]).
