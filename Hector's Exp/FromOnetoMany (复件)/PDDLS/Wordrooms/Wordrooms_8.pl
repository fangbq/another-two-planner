%%
%%
%%	Wordrooms: NUmber of words 8
%%
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


goal([
	or([k(a,a_1),k(a,c_1),k(a,e_1),k(a,m_1)]),
    or([k(a,t_2),k(a,r_2),k(a,h_2),k(a,e_2),k(a,o_2)]),
    or([k(a,l_3),k(a,i_3),k(a,a_3),k(a,t_3),k(a,e_3)]),
    or([k(a,a_4),k(a,u_4),k(a,t_4),k(a,i_4),k(a,n_4),k(a,o_4)]),
    or([k(a,s_5),k(a,o_5),k(a,a_5)]),
    or([k(b,a_1),k(b,c_1),k(b,e_1),k(b,m_1)]),
    or([k(b,t_2),k(b,r_2),k(b,h_2),k(b,e_2),k(b,o_2)]),
    or([k(b,l_3),k(b,i_3),k(b,a_3),k(b,t_3),k(b,e_3)]),
    or([k(b,a_4),k(b,u_4),k(b,t_4),k(b,i_4),k(b,n_4),k(b,o_4)]),
    or([k(b,s_5),k(b,o_5),k(b,a_5)]),

    k(a,empty_7),k(a,empty_6),
    k(b,empty_7),k(b,empty_6)

	]).


num_events(1).
num_agents(2).
agents([a,b]).		
atoms([
    a1,a2,a3,a4,a5,a6,a7,a8,
    b1,b2,b3,b4,b5,b6,b7,b8,

    a_1,c_1,e_1,m_1,

    t_2,r_2, h_2,e_2, o_2,

    l_3,i_3,a_3,t_3,e_3,

    a_4,u_4,t_4,i_4,n_4,o_4,

    s_5,o_5,a_5,

    

    
   empty_7,
   empty_6
  
    
    


   ]).
static_atoms([
a_1,c_1,e_1,m_1,

    t_2,r_2, h_2,e_2, o_2,

    l_3,i_3,a_3,t_3,e_3,

    a_4,u_4,t_4,i_4,n_4,o_4,

    s_5,o_5,a_5,

    

    
   empty_7,
   empty_6

    ]).

p_commonknowledge([
    a8,
    b8,
   empty_7,
   empty_6
      

    
    ]).
n_commonknowledge([
    a1,a2,a3,a4,a5,a6,a7,
    b1,b2,b3,b4,b5,b6,b7



    ]).


possibleactions([  

        [a_senses_what_b_knows_about_room_1,empty,%[a8,b8]
            sense([a],[[k(b,a_1),k(b,c_1),k(b,e_1),k(b,m_1)]])
        ],

        [a_senses_what_b_knows_about_room_2,empty,%[a8,b8]
            sense([a],[[k(b,t_2),k(b,r_2),k(b,h_2),k(b,e_2),k(b,o_2)]])
        ],

        [a_senses_what_b_knows_about_room_3,empty,%[a8,b8]
            sense([a],[[k(b,l_3),k(b,i_3),k(b,a_3),k(b,t_3),k(b,e_3)]])
        ],

        [a_senses_what_b_knows_about_room_4,empty,%[a8,b8]
            sense([a],[[k(b,a_4),k(b,u_4),k(b,t_4),k(b,i_4),k(b,o_4)]])
        ],

        [a_senses_what_b_knows_about_room_5,empty,%[a8,b8]
            sense([a],[[k(b,s_5),k(b,o_5),k(b,a_5)]])
        ],

        [a_senses_what_b_knows_about_room_6,empty,%[a8,b8]
            sense([a],[[k(b,empty_6)]])
        ],

        [a_senses_what_b_knows_about_room_7,empty,%[a8,b8]
            sense([a],[[k(b,empty_7)]])
        ],









        [a_senses_in_room_1,[a1],
            sense([a],[[a_1,c_1,e_1,m_1]])],
        [a_senses_in_room_2,[a2],
            sense([a],[[t_2,r_2,h_2,e_2,o_2]])],
        [a_senses_in_room_3,[a3],
            sense([a],[[l_3,i_3,a_3,t_3,e_3]])],
        [a_senses_in_room_4,[a4],
            sense([a],[[a_4,u_4,t_4,i_4,o_4]])],
        [a_senses_in_room_5,[a5],
            sense([a],[[s_5,o_5,a_5]])],
        [a_senses_in_room_6,[a6],
            sense([a],[[empty_6]])],
        [a_senses_in_room_7,[a7],
            sense([a],[[empty_7]])],

        [b_senses_in_room_1,[b1],
            sense([b],[[a_1,c_1,e_1,m_1]])],
        [b_senses_in_room_2,[b2],
            sense([b],[[t_2,r_2,h_2,e_2,o_2]])],
        [b_senses_in_room_3,[b3],
            sense([b],[[l_3,i_3,a_3,t_3,e_3]])],
        [b_senses_in_room_4,[b4],
            sense([b],[[a_4,u_4,t_4,i_4,o_4]])],
        [b_senses_in_room_5,[b5],
            sense([b],[[s_5,o_5,a_5]])],
        [b_senses_in_room_6,[b6],
            sense([b],[[empty_6]])],
        [b_senses_in_room_7,[b7],
            sense([b],[[empty_7]])],











        [a_goes_from_room_8_to_room_1,[a8,neg(b1)],
        action(

            [[a8]],[[a8]],[[a1]]
        )],
        [a_goes_from_room_8_to_room_2,[a8,neg(b2)],
        action(

            [[a8]],[[a8]],[[a2]]
        )],
        [a_goes_from_room_8_to_room_3,[a8,neg(b3)],
        action(

            [[a8]],[[a8]],[[a3]]
        )],
        [a_goes_from_room_8_to_room_4,[a8,neg(b4)],
        action(

            [[a8]],[[a8]],[[a4]]
        )],
        [a_goes_from_room_8_to_room_5,[a8,neg(b5)],
        action(

            [[a8]],[[a8]],[[a5]]
        )],
        [a_goes_from_room_8_to_room_6,[a8,neg(b6)],
        action(

            [[a8]],[[a8]],[[a6]]
        )],
        [a_goes_from_room_8_to_room_7,[a8,neg(b7)],
        action(

            [[a8]],[[a8]],[[a7]]
        )],




        [a_goes_from_room_1_to_8,[a1],action(
            [[a1]],[[a1]],[[a8]]
            )],
        [a_goes_from_room_2_to_8,[a2],action(
            [[a2]],[[a2]],[[a8]]
            )],
        [a_goes_from_room_3_to_8,[a3],action(
            [[a3]],[[a3]],[[a8]]
            )],
        [a_goes_from_room_4_to_8,[a4],action(
            [[a4]],[[a4]],[[a8]]
            )],
        [a_goes_from_room_5_to_8,[a5],action(
            [[a5]],[[a5]],[[a8]]
            )],
        [a_goes_from_room_6_to_8,[a6],action(
            [[a6]],[[a6]],[[a8]]
            )],
        [a_goes_from_room_7_to_8,[a7],action(
            [[a7]],[[a7]],[[a8]]
            )],





        

        [b_goes_from_room_8_to_room_1,[b8],%,neg(a1)],
        action(

            [[b8]],[[b8]],[[b1]]
        )],
        [b_goes_from_room_8_to_room_2,[b8],%,neg(a2)],
        action(

            [[b8]],[[b8]],[[b2]]
        )],
        [b_goes_from_room_8_to_room_3,[b8],%,neg(a3)],
        action(

            [[b8]],[[b8]],[[b3]]
        )],
        [b_goes_from_room_8_to_room_4,[b8],%,neg(a4)],
        action(

            [[b8]],[[b8]],[[b4]]
        )],
        [b_goes_from_room_8_to_room_5,[b8],%,neg(a5)],
        action(

            [[b8]],[[b8]],[[b5]]
        )],
        [b_goes_from_room_8_to_room_6,[b8],%,neg(a6)],
        action(

            [[b8]],[[b8]],[[b6]]
        )],
        [b_goes_from_room_8_to_room_7,[b8],%,neg(a7)],
        action(

            [[b8]],[[b8]],[[b7]]
        )],


        [b_goes_from_room_1_to_8,[b1],action(
            [[b1]],[[b1]],[[b8]]
            )],
        [b_goes_from_room_2_to_8,[b2],action(
            [[b2]],[[b2]],[[b8]]
            )],
        [b_goes_from_room_3_to_8,[b3],action(
            [[b3]],[[b3]],[[b8]]
            )],
        [b_goes_from_room_4_to_8,[b4],action(
            [[b4]],[[b4]],[[b8]]
            )],
        [b_goes_from_room_5_to_8,[b5],action(
            [[b5]],[[b5]],[[b8]]
            )],
        [b_goes_from_room_6_to_8,[b6],action(
            [[b6]],[[b6]],[[b8]]
            )],
        [b_goes_from_room_7_to_8,[b7],action(
            [[b7]],[[b7]],[[b8]]
            )],



        [b_senses_what_a_knows_about_room_1,empty,%[a8,b8]
            sense([b],[[k(a,a_1),k(a,c_1),k(a,e_1),k(a,m_1)]])
        ],

        [b_senses_what_a_knows_about_room_2,empty,%[a8,b8]
            sense([b],[[k(a,t_2),k(a,r_2),k(a,h_2),k(a,e_2),k(a,o_2)]])
        ],

        [b_senses_what_a_knows_about_room_3,empty,%[a8,b8]
            sense([b],[[k(a,l_3),k(a,i_3),k(a,a_3),k(a,t_3),k(a,e_3)]])
        ],

        [b_senses_what_a_knows_about_room_4,empty,%[a8,b8]
            sense([b],[[k(a,a_4),k(a,u_4),k(a,t_4),k(a,i_4),k(a,o_4)]])
        ],

        [b_senses_what_a_knows_about_room_5,empty,%[a8,b8]
            sense([b],[[k(a,s_5),k(a,o_5),k(a,a_5)]])
        ],

        [b_senses_what_a_knows_about_room_6,empty,%[a8,b8]
            sense([b],[[k(a,empty_6)]])
        ],

        [b_senses_what_a_knows_about_room_7,empty,%[a8,b8]
            sense([b],[[k(a,empty_7)]])
        ]

 










          	]).

run:-
	!,true.	



constraint(H).
	



invariant([
 ]).
user_input_states([
    [a8,b8,empty_6,empty_7,a_1,t_2,l_3,a_4,s_5],
    [a8,b8,empty_6,empty_7,c_1,r_2,i_3,u_4,s_5],
    [a8,b8,empty_6,empty_7,e_1,r_2,a_3,t_4,o_5],
    [a8,b8,empty_6,empty_7,a_1,t_2,t_3,i_4,s_5],
    [a8,b8,empty_6,empty_7,c_1,h_2,a_3,o_4,s_5],
    [a8,b8,empty_6,empty_7,m_1,e_2,t_3,i_4,s_5],
    [a8,b8,empty_6,empty_7,a_1,e_2,t_3,i_4,a_5],
    [a8,b8,empty_6,empty_7,c_1,o_2,e_3,u_4,s_5]


    ]).
