%%
%%
%%  Sum 4
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
find_plan:-
  search( [ [or([neg(ma),ma]),0] ] , or([k(a,a1),k(a,a2),k(a,a3),k(a,a4)])).

goal([
   
 or([k(a,a1),k(a,a2),k(a,a3),k(a,a4)])




    ]).


num_events(120).
num_agents(3).
agents([a,b,c]).      
static_atoms([
  a1,b1,c1,
  a2,b2,c2,
  a3,b3,c3,
  a4,b4,c4

   ]).
atoms([
  a1,b1,c1,
  a2,b2,c2,
  a3,b3,c3,
  a4,b4,c4

   ]).

p_commonknowledge([
   
    ]).
n_commonknowledge([
   
    ]).


possibleactions([  


    

      [all_look,or([neg(g),g]),sense([a,b,c],[[b1,b2,b3,b4,c1,c2,c3,c4],[a1,a2,a3,a4,c1,c2,c3,c4],[a1,a2,a3,a4,b1,b2,b3,b4]])],

        
        [all_hear_if_b_knows_his_number_is_1,
        or([neg(g),g]),
        sense([k(b,b1)])],

          [all_hear_if_b_knows_his_number_is_2,
        or([neg(g),g]),
        sense([k(b,b2)])],

          [all_hear_if_b_knows_his_number_is_3,
        or([neg(g),g]),
        sense([k(b,b3)])],

          [all_hear_if_b_knows_his_number_is_4,
        or([neg(g),g]),
        sense([k(b,b4)])],
        
        [all_hear_if_c_knows_his_number_is_1,
        or([neg(g),g]),
        sense([k(c,c1)])],

          [all_hear_if_c_knows_his_number_is_2,
        or([neg(g),g]),
        sense([k(c,c2)])],

          [all_hear_if_c_knows_his_number_is_3,
        or([neg(g),g]),
        sense([k(c,c3)])],

          [all_hear_if_c_knows_his_number_is_4,
        or([neg(g),g]),
        sense([k(c,c4)])],
        
        [all_hear_if_a_knows_his_number_is_1,
        or([neg(g),g]),
        sense([k(a,a1)])],

          [all_hear_if_a_knows_his_number_is_2,
        or([neg(g),g]),
        sense([k(a,a2)])],
          [all_hear_if_a_knows_his_number_is_3,
        or([neg(g),g]),
        sense([k(a,a3)])],
          [all_hear_if_a_knows_his_number_is_4,
        or([neg(g),g]),
        sense([k(a,a4)])]



            ]).


 
run:-
!,true.

constraint(H).



invariant([


    
  ]).


user_input_states([
[a1,b1,c2],
[a1,b2,c3],
[a1,b3,c4],
[a2,b1,c3],
[a2,b2,c4],
[a3,b1,c4],
[a1,b2,c1],
[a1,b3,c2],
[a1,b4,c3],
[a2,b3,c1],
[a2,b4,c2],
[a3,b4,c1],
[a2,b1,c1],
[a3,b2,c1],
[a4,b3,c1],
[a3,b1,c2],
[a4,b2,c2],
[a4,b1,c3]


    ]).

