%%
%%
%%  Sum 7
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


goal([
   
 or([k(a,a1),k(a,a2),k(a,a3),k(a,a4),k(a,a5),k(a,a6),k(a,a7)])


    ]).


num_events(120).
num_agents(3).
agents([a,b,c]).      
static_atoms([
  a1,b1,c1,
  a2,b2,c2,
  a3,b3,c3,
  a4,b4,c4,
  a5,b5,c5,
  a6,b6,c6,
  a7,b7,c7

   ]).


atoms([
  a1,b1,c1,
  a2,b2,c2,
  a3,b3,c3,
  a4,b4,c4,
  a5,b5,c5,
  a6,b6,c6,
  a7,b7,c7

   ]).

p_commonknowledge([
   
    ]).
n_commonknowledge([
   
    ]).


possibleactions([  


    
        [all_sense,empty,sense([a,b,c],[[b1,b2,b3,b4,b5,b6,b7,c1,c2,c3,c4,c5,c6,c7],
          [a1,a2,a3,a4,a5,a6,a7,c1,c2,c3,c4,c5,c6,c7],
                [a1,a2,a3,a4,a5,a6,a7,b1,b2,b3,b4,b5,b6,b7]])],    
        
        [all_hear_if_b_knows_his_number_is_1,
        or([neg(g),g]),
        sense([k(b,b1)])],

          [all_hear_if_b_knows_his_number_is_2,
        or([neg(g),g]),
        sense([k(b,b2)])],

          [all_hear_if_b_knows_his_number_is_3,
        or([neg(g),g]),
        sense([k(b,b3)])],

          [all_hear_if_b_knows_his_number_is_4,
        or([neg(g),g]),
        sense([k(b,b4)])],

        [all_hear_if_b_knows_his_number_is_5,
        or([neg(g),g]),
        sense([k(b,b5)])],

        [all_hear_if_b_knows_his_number_is_6,
        or([neg(g),g]),
        sense([k(b,b6)])],

        [all_hear_if_b_knows_his_number_is_7,
        or([neg(g),g]),
        sense([k(b,b7)])],
        


        [all_hear_if_c_knows_his_number_is_1,
        or([neg(g),g]),
        sense([k(c,c1)])],

          [all_hear_if_c_knows_his_number_is_2,
        or([neg(g),g]),
        sense([k(c,c2)])],

          [all_hear_if_c_knows_his_number_is_3,
        or([neg(g),g]),
        sense([k(c,c3)])],

          [all_hear_if_c_knows_his_number_is_4,
        or([neg(g),g]),
        sense([k(c,c4)])],

          [all_hear_if_c_knows_his_number_is_5,
        or([neg(g),g]),
        sense([k(c,c5)])],
          [all_hear_if_c_knows_his_number_is_6,
        or([neg(g),g]),
        sense([k(c,c6)])],
          [all_hear_if_c_knows_his_number_is_7,
        or([neg(g),g]),
        sense([k(c,c7)])],


        
        [all_hear_if_a_knows_his_number_is_1,
        or([neg(g),g]),
        sense([k(a,a1)])],

          [all_hear_if_a_knows_his_number_is_2,
        or([neg(g),g]),
        sense([k(a,a2)])],
          [all_hear_if_a_knows_his_number_is_3,
        or([neg(g),g]),
        sense([k(a,a3)])],
          [all_hear_if_a_knows_his_number_is_4,
        or([neg(g),g]),
        sense([k(a,a4)])],
          [all_hear_if_a_knows_his_number_is_5,
        or([neg(g),g]),
        sense([k(a,a5)])],
          [all_hear_if_a_knows_his_number_is_6,
        or([neg(g),g]),
        sense([k(a,a6)])],
          [all_hear_if_a_knows_his_number_is_7,
        or([neg(g),g]),
        sense([k(a,a7)])]



            ]).


 
run:-
!,true.

constraint(H).



invariant([


    
  ]).


user_input_states([
[a1,b1,c2],
[a1,b2,c3],
[a1,b3,c4],
[a1,b4,c5],
[a1,b5,c6],
[a1,b6,c7],
[a2,b1,c3],
[a2,b2,c4],
[a2,b3,c5],
[a2,b4,c6],
[a2,b5,c7],
[a3,b1,c4],
[a3,b2,c5],
[a3,b3,c6],
[a3,b4,c7],
[a4,b1,c5],
[a4,b2,c6],
[a4,b3,c7],
[a5,b1,c6],
[a5,b2,c7],
[a6,b1,c7],
[a1,b2,c1],
[a1,b3,c2],
[a1,b4,c3],
[a1,b5,c4],
[a1,b6,c5],
[a1,b7,c6],
[a2,b3,c1],
[a2,b4,c2],
[a2,b5,c3],
[a2,b6,c4],
[a2,b7,c5],
[a3,b4,c1],
[a3,b5,c2],
[a3,b6,c3],
[a3,b7,c4],
[a4,b5,c1],
[a4,b6,c2],
[a4,b7,c3],
[a5,b6,c1],
[a5,b7,c2],
[a6,b7,c1],
[a2,b1,c1],
[a3,b2,c1],
[a4,b3,c1],
[a5,b4,c1],
[a6,b5,c1],
[a7,b6,c1],
[a3,b1,c2],
[a4,b2,c2],
[a5,b3,c2],
[a6,b4,c2],
[a7,b5,c2],
[a4,b1,c3],
[a5,b2,c3],
[a6,b3,c3],
[a7,b4,c3],
[a5,b1,c4],
[a6,b2,c4],
[a7,b3,c4],
[a6,b1,c5],
[a7,b2,c5],
[a7,b1,c6]

    ]).
