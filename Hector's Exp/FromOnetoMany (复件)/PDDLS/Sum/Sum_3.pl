%%
%%
%%  Sum 3
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


goal([
   
 or([k(a,a1),k(a,a2),k(a,a3)])


    ]).


num_events(120).
num_agents(3).
agents([a,b,c]).      
static_atoms([
  a1,b1,c1,
  a2,b2,c2,
  a3,b3,c3

   ]).
atoms([
  a1,b1,c1,
  a2,b2,c2,
  a3,b3,c3

   ]).

p_commonknowledge([
   
    ]).
n_commonknowledge([
   
    ]).


possibleactions([  


    
        [all_sense,empty,sense([a,b,c],[[b1,b2,b3,c1,c2,c3],[a1,a2,a3,c1,c2,c3],
                [a1,a2,a3,b1,b2,b3]])],    
        
          [all_hear_if_b_knows_his_number_1,
        or([neg(g),g]),
        sense([k(b,b1)])],
          [all_hear_if_b_knows_his_number_2,
        or([neg(g),g]),
        sense([k(b,b2)])],
          [all_hear_if_b_knows_his_number_3,
        or([neg(g),g]),
        sense([k(b,b3)])],
        
        [all_hear_if_c_knows_his_number_1,
        or([neg(g),g]),
        sense([k(c,c1)])],
          [all_hear_if_c_knows_his_number_2,
        or([neg(g),g]),
        sense([k(c,c2)])],
          [all_hear_if_c_knows_his_number_3,
        or([neg(g),g]),
        sense([k(c,c3)])],
        
        [all_hear_if_a_knows_his_number_1,
        or([neg(g),g]),
        sense([k(a,a1)])],

          [all_hear_if_a_knows_his_number_2,
        or([neg(g),g]),
        sense([k(a,a2)])],

          [all_hear_if_a_knows_his_number_3,
        or([neg(g),g]),
        sense([k(a,a3)])]



            ]).


 
run:-
!,true.

constraint(H).



invariant([


    
  ]).


user_input_states([
[a1,b1,c2],
[a1,b2,c3],
[a2,b1,c3],
[a1,c1,b2],
[a1,c2,b3],
[a2,c1,b3],
[c1,b1,a2],
[c1,b2,a3],
[c2,b1,a3]
    ]).
