%%
%%
%%  Sum 5
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


goal([
   
 or([k(a,a1),k(a,a2),k(a,a3),k(a,a4),k(a,a5)])


    ]).


static_atoms([
  a1,b1,c1,
  a2,b2,c2,
  a3,b3,c3,
  a4,b4,c4,
  a5,b5,c5
  ]).

num_events(120).
num_agents(3).
agents([a,b,c]).      
atoms([
  a1,b1,c1,
  a2,b2,c2,
  a3,b3,c3,
  a4,b4,c4,
  a5,b5,c5

   ]).

p_commonknowledge([
   
    ]).
n_commonknowledge([
   
    ]).


possibleactions([  
    
        [all_sense,empty,sense([a,b,c],[[b1,b2,b3,b4,b5,c1,c2,c3,c4,c5],[a1,a2,a3,a4,a5,c1,c2,c3,c4,c5],
                [a1,a2,a3,a4,a5,b1,b2,b3,b4,b5]])],    
        
        [all_hear_if_b_knows_his_number_is_1,
        or([neg(g),g]),
        sense([k(b,b1)])],
          [all_hear_if_b_knows_his_number_is_2,
        or([neg(g),g]),
        sense([k(b,b2)])],
          [all_hear_if_b_knows_his_number_is_3,
        or([neg(g),g]),
        sense([k(b,b3)])],
          [all_hear_if_b_knows_his_number_is_4,
        or([neg(g),g]),
        sense([k(b,b4)])],
          [all_hear_if_b_knows_his_number_is_5,
        or([neg(g),g]),
        sense([k(b,b5)])],
        
        [all_hear_if_c_knows_his_number_is_1,
        or([neg(g),g]),
        sense([k(c,c1)])],
          [all_hear_if_c_knows_his_number_is_2,
        or([neg(g),g]),
        sense([k(c,c2)])],
          [all_hear_if_c_knows_his_number_is_3,
        or([neg(g),g]),
        sense([k(c,c3)])],
          [all_hear_if_c_knows_his_number_is_4,
        or([neg(g),g]),
        sense([k(c,c4)])],
          [all_hear_if_c_knows_his_number_is_5,
        or([neg(g),g]),
        sense([k(c,c5)])],
        
        [all_hear_if_a_knows_his_number_is_1,
        or([neg(g),g]),
        sense([k(a,a1)])],
          [all_hear_if_a_knows_his_number_is_2,
        or([neg(g),g]),
        sense([k(a,a2)])],
          [all_hear_if_a_knows_his_number_is_3,
        or([neg(g),g]),
        sense([k(a,a3)])],
          [all_hear_if_a_knows_his_number_is_4,
        or([neg(g),g]),
        sense([k(a,a4)])],
          [all_hear_if_a_knows_his_number_is_5,
        or([neg(g),g]),
        sense([k(a,a5)])]



            ]).


 
run:-
!,true.

constraint(H).



invariant([


    
  ]).


user_input_states([
[a1,b1,c2],
[a1,b2,c3],
[a1,b3,c4],
[a1,b4,c5],
[a2,b1,c3],
[a2,b2,c4],
[a2,b3,c5],
[a3,b1,c4],
[a3,b2,c5],
[a4,b1,c5],
[a1,c1,b2],
[a1,c2,b3],
[a1,c3,b4],
[a1,c4,b5],
[a2,c1,b3],
[a2,c2,b4],
[a2,c3,b5],
[a3,c1,b4],
[a3,c2,b5],
[a4,c1,b5],
[c1,b1,a2],
[c1,b2,a3],
[c1,b3,a4],
[c1,b4,a5],
[c2,b1,a3],
[c2,b2,a4],
[c2,b3,a5],
[c3,b1,a4],
[c3,b2,a5],
[c4,b1,a5]
    ]).
