%%
%%
%%	The 5-muddy children problem
%%	A asks
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


find_plan:-
	search( [ [or([neg(ma),ma]),0] ] , impl(ma,k(a,ma))).

invariant([
 ]).
user_input_states([
	]).

goal([
or([mc,md,k(a,ma),k(a,neg(ma))])



]).
	
		
num_events(120).
num_agents(5).
agents([a,b,c,d,e]).
static_atoms([ma,mb,mc,md,me,p]).		
atoms([ma,mb,mc,md,me,p]).

possibleactions([	
	[observation,or([neg(g),g]),observe(p)],
	[a_senses,or([neg(g),g]),sense([a],[[mb,mc,md,me]])],
	[b_senses,or([neg(g),g]),sense([b],[[ma,mc,md,me]])],
	[c_senses,or([neg(g),g]),sense([c],[[ma,mb,md,me]])],
	[d_senses,or([neg(g),g]),sense([d],[[ma,mb,mc,me]])],
	[e_senses,or([neg(g),g]),sense([e],[[ma,mb,mc,md]])],



	[a_asks_b,empty,sense([k(b,mb)])],
	[a_asks_c,empty,sense([k(c,mc)])],
	[a_asks_d,empty,sense([k(d,md)])],
	[a_asks_e,empty,sense([k(e,me)])]
 	]).	





run:-
	!,true.	







constraint(H):-
	 cformula(or([and([or([ma,mb,mc,md,me]), p]) , and([neg(or([ma,mb,mc,md,me])), neg(p)])]),H),
	 
	!,true.	
	
axioms_to_add([]).
invariant([]).
p_commonknowledge([

    
    ]).
n_commonknowledge([



    ]).
user_input_states([
    ]).
