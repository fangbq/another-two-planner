%%
%%
%%	The 3-muddy children problem
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% To Retrieve plan, run on console:
%%
%% ?-find_plan.
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
user_input_states([]).



find_plan:-
	search( [ [or([neg(ma),ma]),0] ] , impl(ma,k(a,ma))).

user_input_states([
    ]).

goal([

or([mb,mf,k(a,ma),k(a,neg(ma))])


]).
	
		
num_events(120).
num_agents(7).
agents([a,b,c,d,e,f,g]).	
static_atoms([ma,mb,mc,md,me,mf,mg,p]).	
atoms([ma,mb,mc,md,me,mf,mg,p]).

possibleactions([	
	 [observation,or([neg(g),g]),observe(p)],
	 [a_senses,or([neg(g),g]),sense([a],[[mb,mc,md,me,mf,mg]])],
	 [b_senses,or([neg(g),g]),sense([b],[[ma,mc,md,me,mf,mg]])],
	 [c_senses,or([neg(g),g]),sense([c],[[ma,mb,md,me,mf,mg]])],
	 [d_senses,or([neg(g),g]),sense([d],[[ma,mb,mc,me,mf,mg]])],
	 [e_senses,or([neg(g),g]),sense([e],[[ma,mb,mc,md,mf,mg]])],
	 [f_senses,or([neg(g),g]),sense([f],[[ma,mb,mc,me,md,mg]])],
	 [g_senses,or([neg(g),g]),sense([g],[[ma,mb,mc,me,mf,md]])],
	
	[a_asks_b,empty,sense([k(b,mb)])],
	[a_asks_c,empty,sense([k(c,mc)])],
	[a_asks_d,empty,sense([k(d,md)])],
	[a_asks_e,empty,sense([k(e,me)])],
	[a_asks_f,empty,sense([k(f,mf)])],
	[a_asks_g,empty,sense([k(g,mg)])]


    	]).	

run:-
 !,true.
constraint(H):-
	 cformula(or([and([or([ma,mb,mc,md,me,mf,mg]), p]) , and([neg(or([ma,mb,mc,md,me,mf,mg])), neg(p)])]),H),
	 
	!,true.	
	
axioms_to_add([]).
invariant([]).

p_commonknowledge([

    
    ]).
n_commonknowledge([



    ]).

