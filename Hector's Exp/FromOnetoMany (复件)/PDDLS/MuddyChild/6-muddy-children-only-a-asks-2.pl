%%
%%
%%	The 3-muddy children problem
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% To Retrieve plan, run on console:
%%
%% ?-find_plan.
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

find_plan:-
	search( [ [or([neg(ma),ma]),0] ] , impl(ma,k(a,ma))).
invariant([
 ]).
user_input_states([
	]).


goal([
	or([mb,md,k(a,ma),k(a,neg(ma))])]).
	
		
num_events(120).
num_agents(6).
agents([a,b,c,d,e,f]).	
static_atoms([ma,mb,mc,md,me,mf,p]).	
atoms([ma,mb,mc,md,me,mf,p]).

possibleactions([	
 	[observation,or([neg(g),g]),observe(p)],
  	[a_senses,or([neg(g),g]),sense([a],[[mb,mc,md,me,mf]])],
  	[b_senses,or([neg(g),g]),sense([b],[[ma,mc,md,me,mf]])],
  	[c_senses,or([neg(g),g]),sense([c],[[ma,mb,md,me,mf]])],
  	[d_senses,or([neg(g),g]),sense([d],[[ma,mb,mc,me,mf]])],
  	[e_senses,or([neg(g),g]),sense([e],[[ma,mb,mc,md,mf]])],
  	[f_senses,or([neg(g),g]),sense([f],[[ma,mb,mc,me,md]])],
	
	[a_asks_b,empty,sense([k(b,mb)])],
	[a_asks_c,empty,sense([k(c,mc)])],
	[a_asks_d,empty,sense([k(d,md)])],
	[a_asks_e,empty,sense([k(e,me)])],
	[a_asks_f,empty,sense([k(f,mf)])]
	
    	]).	

run:-
	!,true.
constraint(H):-
	 cformula(or([and([or([ma,mb,mc,md,me,mf]), p]) , and([neg(or([ma,mb,mc,md,me,mf])), neg(p)])]),H),
	 
	 	!,true.	
	
invariant([]).

p_commonknowledge([

    
    ]).
n_commonknowledge([



    ]).

user_input_states([
    ]).
