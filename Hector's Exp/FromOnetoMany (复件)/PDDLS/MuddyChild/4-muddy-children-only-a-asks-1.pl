%%
%%
%%	The 4-muddy children problem
%%	A asks
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


find_plan:-
	search( [ [or([neg(ma),ma]),0] ] , impl(ma,k(a,ma))).
invariant([
 ]).
user_input_states([
	]).


goal([
	or([mb,k(a,ma),k(a,neg(ma))])


]).
	
p_commonknowledge([

    
    ]).
n_commonknowledge([



    ]).

p_commonknowledge([

    
    ]).
n_commonknowledge([



    ]).

invariant([
 ]).
user_input_states([
	]).
    		
num_events(120).
num_agents(4).
agents([a,b,c,d]).	
static_atoms([ma,mb,mc,md,p]).	
atoms([ma,mb,mc,md,p]).

possibleactions([	
	[observation,or([neg(g),g]),observe(p)],
	[a_senses,or([neg(g),g]),sense([a],[[mb,mc,md]])],
	[b_senses,or([neg(g),g]),sense([b],[[ma,mc,md]])],
	[c_senses,or([neg(g),g]),sense([c],[[ma,mb,md]])],
	[d_senses,or([neg(g),g]),sense([d],[[ma,mb,mc]])],
	
	[a_asks_b,empty,sense([k(b,mb)])],
	[a_asks_c,empty,sense([k(c,mc)])],
	[a_asks_d,empty,sense([k(d,md)])]	
	
    	]).	


run:-
	!,true.	
constraint(H):-
	 cformula(or([and([or([ma,mb,mc,md]), p]) , and([neg(or([ma,mb,mc,md])), neg(p)])]),H),
	 
	 !,true.	
	
axioms_to_add([]).
invariant([]).

