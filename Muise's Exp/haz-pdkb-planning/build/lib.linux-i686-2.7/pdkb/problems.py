
import os

from krrt.utils import read_file, write_file, run_command
from krrt.planning import parse_output_FF, parse_output_lapkt

from pdkb.rml import parse_rml, Literal
from pdkb.kd45 import PDKB, project
from pdkb.actions import Action


def parse_ma_cond(line):
    derived_cond = line
    pos_der_cond = []
    neg_der_cond = []
    if ' / ' in derived_cond:
        assert derived_cond.split(' / ')[1], "Error: No negative cond? %s" % derived_cond
        for rml_line in derived_cond.split(' / ')[1].split(','):
            neg_der_cond.append(rml_line)
        derived_cond = derived_cond.split(' / ')[0]

    for rml_line in derived_cond.split(','):
        pos_der_cond.append(rml_line)

    return (pos_der_cond, neg_der_cond)

def parse_action(lines, depth, agents, props):
    name = lines.pop(0).split(':')[1]
    derived_cond = lines.pop(0).split(':')[1]

    if derived_cond == 'always':
        act = Action(name, depth, agents, props, True)

    elif derived_cond == 'never':
        act = Action(name, depth, agents, props, False)

    elif derived_cond:
        act = Action(name, depth, agents, props, parse_ma_cond(derived_cond))

    else:
        assert False, "Error for action %s. You need to specify the type of derived condition ('always', 'never', or 'custom')." % name

    num_prec = int(lines.pop(0).split(':')[1])

    for j in range(num_prec):
        act.add_pre(parse_rml(lines.pop(0)))

    num_nondet = int(lines.pop(0).split(':')[1])
    for i_nondet in range(num_nondet):
        act.new_nondet_effect()
        num_eff = int(lines.pop(0).split(':')[1])
        for i_eff in range(num_eff):

            eff_type = lines[0].split(':')[0]
            eff_line = lines.pop(0).split(':')[1]

            if 'obs if' in eff_line:
                cond_ma_cond = parse_ma_cond(eff_line.split(' obs if ')[1])
                eff_line = eff_line.split(' obs if ')[0]
            else:
                cond_ma_cond = False

            condp = PDKB(depth, agents, props)
            condn = PDKB(depth, agents, props)

            if '-->' in eff_line:
                cond_line = eff_line.split(' --> ')[0]
                for rml_line in cond_line.split(' / ')[0].split(','):
                    condp.add_rml(parse_rml(rml_line))
                if '/' in cond_line:
                    for rml_line in cond_line.split(' / ')[1].split(','):
                        condn.add_rml(parse_rml(rml_line))
                lit = parse_rml(eff_line.split(' --> ')[1])
            else:
                lit = parse_rml(eff_line)

            if 'add' == eff_type:
                act.add_pos_effect(condp, condn, lit, cond_ma_cond)
            elif 'del' == eff_type:
                act.add_neg_effect(condp, condn, lit, cond_ma_cond)
            else:
                assert False, "Error: Bad effect, %s" % lines[0]

    return act

def parse_domain(lines):

    assert 'agents:' in lines[0]
    agents = lines[0].split(':')[1].split(',')

    lines.pop(0)

    assert 'propositions:' in lines[0]
    props = map(Literal, lines[0].split(':')[1].split(','))

    lines.pop(0)

    assert 'depth:' in lines[0]
    depth = int(lines[0].split(':')[1])

    lines.pop(0)

    assert 'types:' in lines[0]
    num_types = int(lines.pop(0).split(':')[1])
    types = {}
    for i in range(num_types):
        types[lines[0].split(':')[0]] = lines[0].split(':')[1].split(',')
        lines.pop(0)

    actions = []

    assert 'actions:' in lines[0]
    num_actions = int(lines.pop(0).split(':')[1])

    for i in range(num_actions):
        new_lines = '////' + lines.pop(0)
        while lines and 'name:' != lines[0][:5]:
            new_lines += '////' + lines.pop(0)

        action_lines = [new_lines]
        for t in types:
            new_action_lines = []
            for old_act_line in action_lines:
                if "<%s>" % t not in old_act_line:
                    new_action_lines.append(old_act_line)
                else:
                    for v in types[t]:
                        new_action_lines.append(v.join(old_act_line.split("<%s>" % t)))
            action_lines = new_action_lines
        for act_line in action_lines:
            actions.append(parse_action(act_line.split('////')[1:], depth, agents, props))

    return Domain(agents, props, actions, depth, types)


def parse_problem(lines, domain):

    prob_type = lines.pop(0).split(':')[1]

    assert prob_type in PROBLEM_TYPES.keys(), "Error: Bad problem type: %s" % prob_type

    return PROBLEM_TYPES[prob_type](lines, domain)

def read_pdkbddl_file(fname):

    lines = read_file(fname)

    found = True
    count = 0
    while found:
        count += 1
        if count > 100:
            assert False, "Error: Already attempted at least 100 imports. Did you recursively import something?"

        found = False
        include_indices = []

        for i in range(len(lines)):
            if '{include' in lines[i]:
                include_indices.append(i)
                found = True

        for index in reversed(include_indices):
            new_file = '/'.join(fname.split('/')[:-1]) + '/' + lines[index].split(':')[1][:-1]
            lines = lines[:index] + read_pdkbddl_file(new_file) + lines[index+1:]

    # Strip out the comments and empty lines
    lines = filter(lambda x: x != '', lines)
    lines = filter(lambda x: x[0] != '#', lines)
    lines = map(lambda x: x.split('#')[0], lines)

    return lines

def parse_pdkbddl(pdkbddl_file):

    lines = read_pdkbddl_file(pdkbddl_file)

    prob_index = -1
    for i in range(len(lines)):
        if 'problem:' in lines[i]:
            assert -1 == prob_index
            prob_index = i
    assert prob_index != -1, "Error: No problem type defined"

    return parse_problem(lines[prob_index:], parse_domain(lines[:prob_index]))

class Problem(object):

    def __init__(self):
        self.domain = None

    def parse_preamble(self, lines):
        self.parse_projection(lines)
        self.parse_init(lines)
        self.parse_goal(lines)

    def parse_projection(self, lines):
        assert 'projection:' in lines[0]
        if (1 == len(lines[0].split(':'))) or ('' == lines[0].split(':')[1]):
            self.agent_projection = []
            lines.pop(0)
        else:
            self.agent_projection = list(reversed(lines.pop(0).split(':')[1].split(',')))


    def parse_init(self, lines):

        self.init = PDKB(self.domain.depth, self.domain.agents, self.domain.props)

        assert 'init:' in lines[0], str(lines)

        assume_closed = False
        if 'complete-init:' in lines[0]:
            assume_closed = True

        num_init = int(lines.pop(0).split(':')[1])

        for i in range(num_init):
            init_rmls = [lines.pop(0)]
            for t in self.domain.types:
                new_init_rmls = []
                for old_init_rml in init_rmls:
                    if "<%s>" % t not in old_init_rml:
                        new_init_rmls.append(old_init_rml)
                    else:
                        for v in self.domain.types[t]:
                            new_init_rmls.append(v.join(old_init_rml.split("<%s>" % t)))
                init_rmls = new_init_rmls
            for rml_line in init_rmls:
                self.init.add_rml(parse_rml(rml_line))

        # Close the initial state
        self.init.logically_close()

        if assume_closed:
            self.init.close_omniscience()

        assert self.init.is_consistent(), "Error: Inconsistent initial state"


    def parse_goal(self, lines):

        self.goal = PDKB(self.domain.depth, self.domain.agents, self.domain.props)

        assert 'goal:' in lines[0]
        num_goal = int(lines.pop(0).split(':')[1])

        for i in range(num_goal):
            self.goal.add_rml(parse_rml(lines.pop(0)))



class ValidGeneration(Problem):
    def __init__(self, lines, domain):
        self.domain = domain
        self.parse_preamble(lines)

    def solve(self):

        # Project the initial state
        new_init = PDKB(self.init.depth, self.init.agents, self.init.props)
        rmls = self.init.rmls
        for ag in self.agent_projection:
            rmls = project(rmls, ag)
        new_init.rmls = rmls
        self.init = new_init

        orig_cond_count = 0
        comp_cond_count = 0

        # Expand and project the action's effects with derived conditional effects
        for act in self.domain.actions:

            orig_cond_count += sum([len(act.effs[i][0]) + len(act.effs[i][1]) for i in range(len(act.effs))])
            act.expand()
            comp_cond_count += sum([len(act.effs[i][0]) + len(act.effs[i][1]) for i in range(len(act.effs))])

            for ag in self.agent_projection:
                act.project_effects(ag)

            if self.agent_projection:
                # We project the precondition separately since we are in the
                #  perspective of the final agent
                act.project_pre(self.agent_projection[-1])

        print "\n\nCond effs (orig): %d (%d)" % (comp_cond_count, orig_cond_count)

        # Write the pddl files
        write_file('pdkb-domain.pddl', self.domain.pddl())
        write_file('pdkb-problem.pddl', self.pddl())

        # Solve the problem
        planner_path = os.path.dirname(os.path.abspath(__file__))
      # chosen_planner = 'siw-then-bfsf' # Can use bfs_f, siw, or siw-then-bfsf
        #run_command("%s/planners/%s --domain pdkb-domain.pddl --problem pdkb-problem.pddl --output pdkb-plan.txt" % (planner_path, chosen_planner),
         #          output_file = 'pdkb-plan.out',
         #           MEMLIMIT = 2000,
         #           TIMELIMIT = 1800)
        #self.plan = parse_output_lapkt('pdkb-plan.txt')

        run_command("%s/planners/ff -o pdkb-domain.pddl -f pdkb-problem.pddl" % planner_path,
                  output_file = 'pdkb-plan.txt',
                  MEMLIMIT = 1000,
                  TIMELIMIT = 1800)
        self.plan = parse_output_FF('pdkb-plan.txt')


    def output_solution(self):
        print "\n  --{ Plan }--\n"
        index = 1
        for a in self.plan.actions:
            print "%d. %s" % (index, str(a))
            index += 1

    def pddl(self):
        to_ret =  "(define (problem %s-prob)\n\n" % self.domain.name
        to_ret += "    (:domain %s)\n\n" % self.domain.name
        to_ret += "    (:init\n"
        for (key, rml) in sorted([(str(r), r) for r in self.init]):
            to_ret += "        (%s)\n" % rml.pddl()
        to_ret += "    )\n\n"
        to_ret += "    (:goal (and\n"
        for (key, rml) in sorted([(str(r), r) for r in self.goal]):
            to_ret += "        (%s)\n" % rml.pddl()
        to_ret += "    ))\n"
        to_ret += ')'

        return to_ret

class ValidAssessment(Problem):
    def __init__(self, lines, domain):
        self.domain = domain
        self.parse_preamble(lines)

        self.actions = []

        assert 'plan:' in lines[0]
        num_act = int(lines.pop(0).split(':')[1])

        for i in range(num_act):
            self.actions.append(lines.pop(0))

    def solve(self):

        # Project the initial state
        new_init = PDKB(self.init.depth, self.init.agents, self.init.props)
        rmls = self.init.rmls
        for ag in self.agent_projection:
            rmls = project(rmls, ag)
        new_init.rmls = rmls
        self.init = new_init

        # Expand and project the action's effects with derived conditional effects
        for act in self.domain.actions:
            act.expand()
            for ag in self.agent_projection:
                act.project_effects(ag)

            if self.agent_projection:
                # We project the precondition separately since we are in the
                #  perspective of the final agent
                act.project_pre(self.agent_projection[-1])

        # Write the domain pddl file in case we want to debug
        write_file('pdkb-domain.pddl', self.domain.pddl())

        # Map the actions we need to refer to from the provided plan
        self.act_map = {}
        for act in self.domain.actions:
            self.act_map[act.name] = act
        for a in self.actions:
            assert a in self.act_map

        # Simulate the plan
        current_state = self.init
        self.executable = True
        for a in self.actions:
            if not self.act_map[a].applicable(current_state):
                self.first_fail = a
                self.failed_state = current_state
                self.executable = False
                break
            current_state = self.act_map[a].apply(current_state)[0]

        # Check that the goal holds / store the result
        if self.executable:
            self.final_state = current_state
            self.goal_holds = (self.goal.rmls <= self.final_state.rmls)
        else:
            self.final_state = None
            self.goal_holds = False

    def output_solution(self):
        print
        print "Assessed the following plan:"
        print "\n".join(self.actions)

        print
        print "Executable: %s" % str(self.executable)
        print "Goal holds: %s" % str(self.goal_holds)

        if not self.executable:
            print "First failed action: %s" % self.first_fail
            print "\nFailed state:"
            print self.failed_state

        if self.final_state:
            print "\nFinal state:"
            print self.final_state
        print


class PlausibleGeneration(Problem):
    def __init__(self, lines, domain):
        self.domain = domain

    def solve(self):
        raise NotImplementedError

    def output_solution(self):
        raise NotImplementedError


class PlausibleAssessment(Problem):
    def __init__(self, lines, domain):
        self.domain = domain

    def solve(self):
        raise NotImplementedError

    def output_solution(self):
        raise NotImplementedError



class Domain:

    def __init__(self, agents, props, actions, depth, types, name='pdkb-planning'):
        self.agents = agents
        self.props = props
        self.actions = actions
        self.depth = depth
        self.name = name
        self.types = types

    def pddl(self):

        pdkb = PDKB(self.depth, self.agents, self.props)

        to_ret =  "(define (domain %s)\n\n" % self.name
        to_ret += "    (:requirements :strips :conditional-effects)\n\n"
        to_ret += "    (:predicates\n"

        print "\n\n# Agents: %d" % len(self.agents)
        print "# Props: %d" % len(pdkb.all_rmls)
        print "# Acts: %d" % len(self.actions)
        print "# Effs: %d" % sum([a.num_effs() for a in self.actions])
        print "Depth: %d" % self.depth

        for (key, rml) in sorted([(str(r), r) for r in pdkb.all_rmls]):
            to_ret += "        (%s)\n" % rml.pddl()

        to_ret += "    )\n\n"

        for (key, act) in sorted([(a.name, a) for a in self.actions]):
            to_ret += "%s\n\n" % act.pddl()

        to_ret += ')'

        return to_ret


PROBLEM_TYPES = {'valid_generation': ValidGeneration,
                 'valid_assessment': ValidAssessment,
                 'plausible_generation': PlausibleGeneration,
                 'plausible_assessment': PlausibleAssessment}
