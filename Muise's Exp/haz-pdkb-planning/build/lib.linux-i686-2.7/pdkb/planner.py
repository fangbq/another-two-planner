
import os, sys, time


from actions import *
from problems import *

def cleanup():
    os.system('rm -f pdkb-domain.pddl')
    os.system('rm -f pdkb-problem.pddl')
    os.system('rm -f pdkb-plan.txt')
    os.system('rm -f pdkb-plan.out')
    os.system('rm -f pdkb-plan.out.err')
    os.system('rm -f execution.details')


def solve(pdkbddl_file):

    print

    t_start = time.time()

    print "Parsing problem...",
    problem = parse_pdkbddl(pdkbddl_file)
    print "done!"

    print "Solving problem...",
    problem.solve()
    print "done!"

    print "\nTime: %f s" % (time.time() - t_start)

    problem.output_solution()

    print


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print "\nUsage: python planner.py <pdkbddl file> [--keep-files]\n"
        sys.exit(1)

    solve(sys.argv[1])

    if len(sys.argv) < 3 or '--keep-files' != sys.argv[2]:
        cleanup()
