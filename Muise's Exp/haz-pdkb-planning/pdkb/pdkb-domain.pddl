(define (domain pdkb-planning)

    (:requirements :strips :conditional-effects)

    (:predicates
        (not_at_a_1)
        (not_at_a_2)
        (not_at_b_1)
        (not_at_b_2)
        (not_at_c_1)
        (not_at_c_2)
        (not_at_d_1)
        (not_at_d_2)
        (not_sa)
        (not_sb)
        (not_sc)
        (not_sd)
        (Ba_not_at_a_1)
        (Ba_not_at_a_2)
        (Ba_not_at_b_1)
        (Ba_not_at_b_2)
        (Ba_not_at_c_1)
        (Ba_not_at_c_2)
        (Ba_not_at_d_1)
        (Ba_not_at_d_2)
        (Ba_not_sa)
        (Ba_not_sb)
        (Ba_not_sc)
        (Ba_not_sd)
        (Ba_at_a_1)
        (Ba_at_a_2)
        (Ba_at_b_1)
        (Ba_at_b_2)
        (Ba_at_c_1)
        (Ba_at_c_2)
        (Ba_at_d_1)
        (Ba_at_d_2)
        (Ba_sa)
        (Ba_sb)
        (Ba_sc)
        (Ba_sd)
        (Bb_not_at_a_1)
        (Bb_not_at_a_2)
        (Bb_not_at_b_1)
        (Bb_not_at_b_2)
        (Bb_not_at_c_1)
        (Bb_not_at_c_2)
        (Bb_not_at_d_1)
        (Bb_not_at_d_2)
        (Bb_not_sa)
        (Bb_not_sb)
        (Bb_not_sc)
        (Bb_not_sd)
        (Bb_at_a_1)
        (Bb_at_a_2)
        (Bb_at_b_1)
        (Bb_at_b_2)
        (Bb_at_c_1)
        (Bb_at_c_2)
        (Bb_at_d_1)
        (Bb_at_d_2)
        (Bb_sa)
        (Bb_sb)
        (Bb_sc)
        (Bb_sd)
        (Bc_not_at_a_1)
        (Bc_not_at_a_2)
        (Bc_not_at_b_1)
        (Bc_not_at_b_2)
        (Bc_not_at_c_1)
        (Bc_not_at_c_2)
        (Bc_not_at_d_1)
        (Bc_not_at_d_2)
        (Bc_not_sa)
        (Bc_not_sb)
        (Bc_not_sc)
        (Bc_not_sd)
        (Bc_at_a_1)
        (Bc_at_a_2)
        (Bc_at_b_1)
        (Bc_at_b_2)
        (Bc_at_c_1)
        (Bc_at_c_2)
        (Bc_at_d_1)
        (Bc_at_d_2)
        (Bc_sa)
        (Bc_sb)
        (Bc_sc)
        (Bc_sd)
        (Bd_not_at_a_1)
        (Bd_not_at_a_2)
        (Bd_not_at_b_1)
        (Bd_not_at_b_2)
        (Bd_not_at_c_1)
        (Bd_not_at_c_2)
        (Bd_not_at_d_1)
        (Bd_not_at_d_2)
        (Bd_not_sa)
        (Bd_not_sb)
        (Bd_not_sc)
        (Bd_not_sd)
        (Bd_at_a_1)
        (Bd_at_a_2)
        (Bd_at_b_1)
        (Bd_at_b_2)
        (Bd_at_c_1)
        (Bd_at_c_2)
        (Bd_at_d_1)
        (Bd_at_d_2)
        (Bd_sa)
        (Bd_sb)
        (Bd_sc)
        (Bd_sd)
        (Pa_not_at_a_1)
        (Pa_not_at_a_2)
        (Pa_not_at_b_1)
        (Pa_not_at_b_2)
        (Pa_not_at_c_1)
        (Pa_not_at_c_2)
        (Pa_not_at_d_1)
        (Pa_not_at_d_2)
        (Pa_not_sa)
        (Pa_not_sb)
        (Pa_not_sc)
        (Pa_not_sd)
        (Pa_at_a_1)
        (Pa_at_a_2)
        (Pa_at_b_1)
        (Pa_at_b_2)
        (Pa_at_c_1)
        (Pa_at_c_2)
        (Pa_at_d_1)
        (Pa_at_d_2)
        (Pa_sa)
        (Pa_sb)
        (Pa_sc)
        (Pa_sd)
        (Pb_not_at_a_1)
        (Pb_not_at_a_2)
        (Pb_not_at_b_1)
        (Pb_not_at_b_2)
        (Pb_not_at_c_1)
        (Pb_not_at_c_2)
        (Pb_not_at_d_1)
        (Pb_not_at_d_2)
        (Pb_not_sa)
        (Pb_not_sb)
        (Pb_not_sc)
        (Pb_not_sd)
        (Pb_at_a_1)
        (Pb_at_a_2)
        (Pb_at_b_1)
        (Pb_at_b_2)
        (Pb_at_c_1)
        (Pb_at_c_2)
        (Pb_at_d_1)
        (Pb_at_d_2)
        (Pb_sa)
        (Pb_sb)
        (Pb_sc)
        (Pb_sd)
        (Pc_not_at_a_1)
        (Pc_not_at_a_2)
        (Pc_not_at_b_1)
        (Pc_not_at_b_2)
        (Pc_not_at_c_1)
        (Pc_not_at_c_2)
        (Pc_not_at_d_1)
        (Pc_not_at_d_2)
        (Pc_not_sa)
        (Pc_not_sb)
        (Pc_not_sc)
        (Pc_not_sd)
        (Pc_at_a_1)
        (Pc_at_a_2)
        (Pc_at_b_1)
        (Pc_at_b_2)
        (Pc_at_c_1)
        (Pc_at_c_2)
        (Pc_at_d_1)
        (Pc_at_d_2)
        (Pc_sa)
        (Pc_sb)
        (Pc_sc)
        (Pc_sd)
        (Pd_not_at_a_1)
        (Pd_not_at_a_2)
        (Pd_not_at_b_1)
        (Pd_not_at_b_2)
        (Pd_not_at_c_1)
        (Pd_not_at_c_2)
        (Pd_not_at_d_1)
        (Pd_not_at_d_2)
        (Pd_not_sa)
        (Pd_not_sb)
        (Pd_not_sc)
        (Pd_not_sd)
        (Pd_at_a_1)
        (Pd_at_a_2)
        (Pd_at_b_1)
        (Pd_at_b_2)
        (Pd_at_c_1)
        (Pd_at_c_2)
        (Pd_at_d_1)
        (Pd_at_d_2)
        (Pd_sa)
        (Pd_sb)
        (Pd_sc)
        (Pd_sd)
        (at_a_1)
        (at_a_2)
        (at_b_1)
        (at_b_2)
        (at_c_1)
        (at_c_2)
        (at_d_1)
        (at_d_2)
        (sa)
        (sb)
        (sc)
        (sd)
    )

    (:action left_a
        :precondition (and (at_a_2))
        :effect (and
                    ; #11184: <==closure== 52768 (pos)
                    (Pb_not_at_a_2)

                    ; #15897: <==commonly_known== 34056 (pos)
                    (Bd_at_a_1)

                    ; #20399: <==commonly_known== 62903 (pos)
                    (Ba_not_at_a_2)

                    ; #27165: <==commonly_known== 34056 (pos)
                    (Ba_at_a_1)

                    ; #31691: <==commonly_known== 34056 (pos)
                    (Bb_at_a_1)

                    ; #31966: <==closure== 39587 (pos)
                    (Pc_not_at_a_2)

                    ; #34056: origin
                    (at_a_1)

                    ; #37199: <==closure== 15897 (pos)
                    (Pd_at_a_1)

                    ; #37553: <==commonly_known== 34056 (pos)
                    (Bc_at_a_1)

                    ; #38307: <==closure== 20399 (pos)
                    (Pa_not_at_a_2)

                    ; #39587: <==commonly_known== 62903 (pos)
                    (Bc_not_at_a_2)

                    ; #40264: <==closure== 27165 (pos)
                    (Pa_at_a_1)

                    ; #43771: <==closure== 79442 (pos)
                    (Pd_not_at_a_2)

                    ; #46601: <==closure== 37553 (pos)
                    (Pc_at_a_1)

                    ; #52768: <==commonly_known== 62903 (pos)
                    (Bb_not_at_a_2)

                    ; #58615: <==closure== 31691 (pos)
                    (Pb_at_a_1)

                    ; #62903: origin
                    (not_at_a_2)

                    ; #79442: <==commonly_known== 62903 (pos)
                    (Bd_not_at_a_2)

                    ; #11184: <==negation-removal== 31691 (pos)
                    (not (Pb_not_at_a_1))

                    ; #15897: <==negation-removal== 43771 (pos)
                    (not (Bd_at_a_2))

                    ; #20399: <==negation-removal== 40264 (pos)
                    (not (Ba_not_at_a_1))

                    ; #27165: <==negation-removal== 38307 (pos)
                    (not (Ba_at_a_2))

                    ; #31691: <==negation-removal== 11184 (pos)
                    (not (Bb_at_a_2))

                    ; #31966: <==negation-removal== 37553 (pos)
                    (not (Pc_not_at_a_1))

                    ; #34056: <==negation-removal== 62903 (pos)
                    (not (at_a_2))

                    ; #37199: <==negation-removal== 79442 (pos)
                    (not (Pd_at_a_2))

                    ; #37553: <==negation-removal== 31966 (pos)
                    (not (Bc_at_a_2))

                    ; #38307: <==negation-removal== 27165 (pos)
                    (not (Pa_not_at_a_1))

                    ; #39587: <==negation-removal== 46601 (pos)
                    (not (Bc_not_at_a_1))

                    ; #40264: <==negation-removal== 20399 (pos)
                    (not (Pa_at_a_2))

                    ; #43771: <==negation-removal== 15897 (pos)
                    (not (Pd_not_at_a_1))

                    ; #46601: <==negation-removal== 39587 (pos)
                    (not (Pc_at_a_2))

                    ; #52768: <==negation-removal== 58615 (pos)
                    (not (Bb_not_at_a_1))

                    ; #58615: <==negation-removal== 52768 (pos)
                    (not (Pb_at_a_2))

                    ; #62903: <==negation-removal== 34056 (pos)
                    (not (not_at_a_1))

                    ; #79442: <==negation-removal== 37199 (pos)
                    (not (Bd_not_at_a_1))))

    (:action left_b
        :precondition (and (at_b_2))
        :effect (and
                    ; #16599: <==closure== 57471 (pos)
                    (Pd_not_at_b_2)

                    ; #17475: <==closure== 52677 (pos)
                    (Pb_not_at_b_2)

                    ; #20054: <==closure== 41916 (pos)
                    (Pb_at_b_1)

                    ; #20588: origin
                    (not_at_b_2)

                    ; #22708: <==closure== 52010 (pos)
                    (Pa_at_b_1)

                    ; #28634: <==commonly_known== 88685 (pos)
                    (Bd_at_b_1)

                    ; #32144: <==closure== 28634 (pos)
                    (Pd_at_b_1)

                    ; #33049: <==commonly_known== 20588 (pos)
                    (Ba_not_at_b_2)

                    ; #41916: <==commonly_known== 88685 (pos)
                    (Bb_at_b_1)

                    ; #52010: <==commonly_known== 88685 (pos)
                    (Ba_at_b_1)

                    ; #52268: <==closure== 33049 (pos)
                    (Pa_not_at_b_2)

                    ; #52677: <==commonly_known== 20588 (pos)
                    (Bb_not_at_b_2)

                    ; #53431: <==commonly_known== 88685 (pos)
                    (Bc_at_b_1)

                    ; #57471: <==commonly_known== 20588 (pos)
                    (Bd_not_at_b_2)

                    ; #59321: <==commonly_known== 20588 (pos)
                    (Bc_not_at_b_2)

                    ; #76824: <==closure== 59321 (pos)
                    (Pc_not_at_b_2)

                    ; #80999: <==closure== 53431 (pos)
                    (Pc_at_b_1)

                    ; #88685: origin
                    (at_b_1)

                    ; #16599: <==negation-removal== 28634 (pos)
                    (not (Pd_not_at_b_1))

                    ; #17475: <==negation-removal== 41916 (pos)
                    (not (Pb_not_at_b_1))

                    ; #20054: <==negation-removal== 52677 (pos)
                    (not (Pb_at_b_2))

                    ; #20588: <==negation-removal== 88685 (pos)
                    (not (not_at_b_1))

                    ; #22708: <==negation-removal== 33049 (pos)
                    (not (Pa_at_b_2))

                    ; #28634: <==negation-removal== 16599 (pos)
                    (not (Bd_at_b_2))

                    ; #32144: <==negation-removal== 57471 (pos)
                    (not (Pd_at_b_2))

                    ; #33049: <==negation-removal== 22708 (pos)
                    (not (Ba_not_at_b_1))

                    ; #41916: <==negation-removal== 17475 (pos)
                    (not (Bb_at_b_2))

                    ; #52010: <==negation-removal== 52268 (pos)
                    (not (Ba_at_b_2))

                    ; #52268: <==negation-removal== 52010 (pos)
                    (not (Pa_not_at_b_1))

                    ; #52677: <==negation-removal== 20054 (pos)
                    (not (Bb_not_at_b_1))

                    ; #53431: <==negation-removal== 76824 (pos)
                    (not (Bc_at_b_2))

                    ; #57471: <==negation-removal== 32144 (pos)
                    (not (Bd_not_at_b_1))

                    ; #59321: <==negation-removal== 80999 (pos)
                    (not (Bc_not_at_b_1))

                    ; #76824: <==negation-removal== 53431 (pos)
                    (not (Pc_not_at_b_1))

                    ; #80999: <==negation-removal== 59321 (pos)
                    (not (Pc_at_b_2))

                    ; #88685: <==negation-removal== 20588 (pos)
                    (not (at_b_2))))

    (:action left_c
        :precondition (and (at_c_2))
        :effect (and
                    ; #13981: <==commonly_known== 90599 (pos)
                    (Ba_at_c_1)

                    ; #16217: <==commonly_known== 79543 (pos)
                    (Ba_not_at_c_2)

                    ; #25492: <==closure== 72323 (pos)
                    (Pd_at_c_1)

                    ; #29317: <==closure== 84366 (pos)
                    (Pd_not_at_c_2)

                    ; #34623: <==closure== 43110 (pos)
                    (Pb_at_c_1)

                    ; #38264: <==closure== 59334 (pos)
                    (Pb_not_at_c_2)

                    ; #43110: <==commonly_known== 90599 (pos)
                    (Bb_at_c_1)

                    ; #59334: <==commonly_known== 79543 (pos)
                    (Bb_not_at_c_2)

                    ; #72323: <==commonly_known== 90599 (pos)
                    (Bd_at_c_1)

                    ; #79543: origin
                    (not_at_c_2)

                    ; #83687: <==closure== 85931 (pos)
                    (Pc_not_at_c_2)

                    ; #84366: <==commonly_known== 79543 (pos)
                    (Bd_not_at_c_2)

                    ; #85562: <==closure== 13981 (pos)
                    (Pa_at_c_1)

                    ; #85931: <==commonly_known== 79543 (pos)
                    (Bc_not_at_c_2)

                    ; #86483: <==commonly_known== 90599 (pos)
                    (Bc_at_c_1)

                    ; #88892: <==closure== 86483 (pos)
                    (Pc_at_c_1)

                    ; #89561: <==closure== 16217 (pos)
                    (Pa_not_at_c_2)

                    ; #90599: origin
                    (at_c_1)

                    ; #13981: <==negation-removal== 89561 (pos)
                    (not (Ba_at_c_2))

                    ; #16217: <==negation-removal== 85562 (pos)
                    (not (Ba_not_at_c_1))

                    ; #25492: <==negation-removal== 84366 (pos)
                    (not (Pd_at_c_2))

                    ; #29317: <==negation-removal== 72323 (pos)
                    (not (Pd_not_at_c_1))

                    ; #34623: <==negation-removal== 59334 (pos)
                    (not (Pb_at_c_2))

                    ; #38264: <==negation-removal== 43110 (pos)
                    (not (Pb_not_at_c_1))

                    ; #43110: <==negation-removal== 38264 (pos)
                    (not (Bb_at_c_2))

                    ; #59334: <==negation-removal== 34623 (pos)
                    (not (Bb_not_at_c_1))

                    ; #72323: <==negation-removal== 29317 (pos)
                    (not (Bd_at_c_2))

                    ; #79543: <==negation-removal== 90599 (pos)
                    (not (not_at_c_1))

                    ; #83687: <==negation-removal== 86483 (pos)
                    (not (Pc_not_at_c_1))

                    ; #84366: <==negation-removal== 25492 (pos)
                    (not (Bd_not_at_c_1))

                    ; #85562: <==negation-removal== 16217 (pos)
                    (not (Pa_at_c_2))

                    ; #85931: <==negation-removal== 88892 (pos)
                    (not (Bc_not_at_c_1))

                    ; #86483: <==negation-removal== 83687 (pos)
                    (not (Bc_at_c_2))

                    ; #88892: <==negation-removal== 85931 (pos)
                    (not (Pc_at_c_2))

                    ; #89561: <==negation-removal== 13981 (pos)
                    (not (Pa_not_at_c_1))

                    ; #90599: <==negation-removal== 79543 (pos)
                    (not (at_c_2))))

    (:action left_d
        :precondition (and (at_d_2))
        :effect (and
                    ; #16673: <==commonly_known== 67317 (pos)
                    (Ba_not_at_d_2)

                    ; #16977: <==closure== 49399 (pos)
                    (Pb_not_at_d_2)

                    ; #20280: <==closure== 56594 (pos)
                    (Pa_at_d_1)

                    ; #20738: <==closure== 16673 (pos)
                    (Pa_not_at_d_2)

                    ; #23370: <==closure== 71683 (pos)
                    (Pc_not_at_d_2)

                    ; #25282: <==closure== 70068 (pos)
                    (Pc_at_d_1)

                    ; #25930: <==closure== 40573 (pos)
                    (Pb_at_d_1)

                    ; #40573: <==commonly_known== 42574 (pos)
                    (Bb_at_d_1)

                    ; #40604: <==commonly_known== 42574 (pos)
                    (Bd_at_d_1)

                    ; #42574: origin
                    (at_d_1)

                    ; #48011: <==closure== 40604 (pos)
                    (Pd_at_d_1)

                    ; #49399: <==commonly_known== 67317 (pos)
                    (Bb_not_at_d_2)

                    ; #49763: <==closure== 61720 (pos)
                    (Pd_not_at_d_2)

                    ; #56594: <==commonly_known== 42574 (pos)
                    (Ba_at_d_1)

                    ; #61720: <==commonly_known== 67317 (pos)
                    (Bd_not_at_d_2)

                    ; #67317: origin
                    (not_at_d_2)

                    ; #70068: <==commonly_known== 42574 (pos)
                    (Bc_at_d_1)

                    ; #71683: <==commonly_known== 67317 (pos)
                    (Bc_not_at_d_2)

                    ; #16673: <==negation-removal== 20280 (pos)
                    (not (Ba_not_at_d_1))

                    ; #16977: <==negation-removal== 40573 (pos)
                    (not (Pb_not_at_d_1))

                    ; #20280: <==negation-removal== 16673 (pos)
                    (not (Pa_at_d_2))

                    ; #20738: <==negation-removal== 56594 (pos)
                    (not (Pa_not_at_d_1))

                    ; #23370: <==negation-removal== 70068 (pos)
                    (not (Pc_not_at_d_1))

                    ; #25282: <==negation-removal== 71683 (pos)
                    (not (Pc_at_d_2))

                    ; #25930: <==negation-removal== 49399 (pos)
                    (not (Pb_at_d_2))

                    ; #40573: <==negation-removal== 16977 (pos)
                    (not (Bb_at_d_2))

                    ; #40604: <==negation-removal== 49763 (pos)
                    (not (Bd_at_d_2))

                    ; #42574: <==negation-removal== 67317 (pos)
                    (not (at_d_2))

                    ; #48011: <==negation-removal== 61720 (pos)
                    (not (Pd_at_d_2))

                    ; #49399: <==negation-removal== 25930 (pos)
                    (not (Bb_not_at_d_1))

                    ; #49763: <==negation-removal== 40604 (pos)
                    (not (Pd_not_at_d_1))

                    ; #56594: <==negation-removal== 20738 (pos)
                    (not (Ba_at_d_2))

                    ; #61720: <==negation-removal== 48011 (pos)
                    (not (Bd_not_at_d_1))

                    ; #67317: <==negation-removal== 42574 (pos)
                    (not (not_at_d_1))

                    ; #70068: <==negation-removal== 23370 (pos)
                    (not (Bc_at_d_2))

                    ; #71683: <==negation-removal== 25282 (pos)
                    (not (Bc_not_at_d_1))))

    (:action right_a
        :precondition (and (at_a_1))
        :effect (and
                    ; #11184: <==closure== 52768 (pos)
                    (Pb_not_at_a_1)

                    ; #15897: <==commonly_known== 34056 (pos)
                    (Bd_at_a_2)

                    ; #20399: <==commonly_known== 62903 (pos)
                    (Ba_not_at_a_1)

                    ; #27165: <==commonly_known== 34056 (pos)
                    (Ba_at_a_2)

                    ; #31691: <==commonly_known== 34056 (pos)
                    (Bb_at_a_2)

                    ; #31966: <==closure== 39587 (pos)
                    (Pc_not_at_a_1)

                    ; #34056: origin
                    (at_a_2)

                    ; #37199: <==closure== 15897 (pos)
                    (Pd_at_a_2)

                    ; #37553: <==commonly_known== 34056 (pos)
                    (Bc_at_a_2)

                    ; #38307: <==closure== 20399 (pos)
                    (Pa_not_at_a_1)

                    ; #39587: <==commonly_known== 62903 (pos)
                    (Bc_not_at_a_1)

                    ; #40264: <==closure== 27165 (pos)
                    (Pa_at_a_2)

                    ; #43771: <==closure== 79442 (pos)
                    (Pd_not_at_a_1)

                    ; #46601: <==closure== 37553 (pos)
                    (Pc_at_a_2)

                    ; #52768: <==commonly_known== 62903 (pos)
                    (Bb_not_at_a_1)

                    ; #58615: <==closure== 31691 (pos)
                    (Pb_at_a_2)

                    ; #62903: origin
                    (not_at_a_1)

                    ; #79442: <==commonly_known== 62903 (pos)
                    (Bd_not_at_a_1)

                    ; #11184: <==negation-removal== 31691 (pos)
                    (not (Pb_not_at_a_2))

                    ; #15897: <==negation-removal== 43771 (pos)
                    (not (Bd_at_a_1))

                    ; #20399: <==negation-removal== 40264 (pos)
                    (not (Ba_not_at_a_2))

                    ; #27165: <==negation-removal== 38307 (pos)
                    (not (Ba_at_a_1))

                    ; #31691: <==negation-removal== 11184 (pos)
                    (not (Bb_at_a_1))

                    ; #31966: <==negation-removal== 37553 (pos)
                    (not (Pc_not_at_a_2))

                    ; #34056: <==negation-removal== 62903 (pos)
                    (not (at_a_1))

                    ; #37199: <==negation-removal== 79442 (pos)
                    (not (Pd_at_a_1))

                    ; #37553: <==negation-removal== 31966 (pos)
                    (not (Bc_at_a_1))

                    ; #38307: <==negation-removal== 27165 (pos)
                    (not (Pa_not_at_a_2))

                    ; #39587: <==negation-removal== 46601 (pos)
                    (not (Bc_not_at_a_2))

                    ; #40264: <==negation-removal== 20399 (pos)
                    (not (Pa_at_a_1))

                    ; #43771: <==negation-removal== 15897 (pos)
                    (not (Pd_not_at_a_2))

                    ; #46601: <==negation-removal== 39587 (pos)
                    (not (Pc_at_a_1))

                    ; #52768: <==negation-removal== 58615 (pos)
                    (not (Bb_not_at_a_2))

                    ; #58615: <==negation-removal== 52768 (pos)
                    (not (Pb_at_a_1))

                    ; #62903: <==negation-removal== 34056 (pos)
                    (not (not_at_a_2))

                    ; #79442: <==negation-removal== 37199 (pos)
                    (not (Bd_not_at_a_2))))

    (:action right_b
        :precondition (and (at_b_1))
        :effect (and
                    ; #16599: <==closure== 57471 (pos)
                    (Pd_not_at_b_1)

                    ; #17475: <==closure== 52677 (pos)
                    (Pb_not_at_b_1)

                    ; #20054: <==closure== 41916 (pos)
                    (Pb_at_b_2)

                    ; #20588: origin
                    (not_at_b_1)

                    ; #22708: <==closure== 52010 (pos)
                    (Pa_at_b_2)

                    ; #28634: <==commonly_known== 88685 (pos)
                    (Bd_at_b_2)

                    ; #32144: <==closure== 28634 (pos)
                    (Pd_at_b_2)

                    ; #33049: <==commonly_known== 20588 (pos)
                    (Ba_not_at_b_1)

                    ; #41916: <==commonly_known== 88685 (pos)
                    (Bb_at_b_2)

                    ; #52010: <==commonly_known== 88685 (pos)
                    (Ba_at_b_2)

                    ; #52268: <==closure== 33049 (pos)
                    (Pa_not_at_b_1)

                    ; #52677: <==commonly_known== 20588 (pos)
                    (Bb_not_at_b_1)

                    ; #53431: <==commonly_known== 88685 (pos)
                    (Bc_at_b_2)

                    ; #57471: <==commonly_known== 20588 (pos)
                    (Bd_not_at_b_1)

                    ; #59321: <==commonly_known== 20588 (pos)
                    (Bc_not_at_b_1)

                    ; #76824: <==closure== 59321 (pos)
                    (Pc_not_at_b_1)

                    ; #80999: <==closure== 53431 (pos)
                    (Pc_at_b_2)

                    ; #88685: origin
                    (at_b_2)

                    ; #16599: <==negation-removal== 28634 (pos)
                    (not (Pd_not_at_b_2))

                    ; #17475: <==negation-removal== 41916 (pos)
                    (not (Pb_not_at_b_2))

                    ; #20054: <==negation-removal== 52677 (pos)
                    (not (Pb_at_b_1))

                    ; #20588: <==negation-removal== 88685 (pos)
                    (not (not_at_b_2))

                    ; #22708: <==negation-removal== 33049 (pos)
                    (not (Pa_at_b_1))

                    ; #28634: <==negation-removal== 16599 (pos)
                    (not (Bd_at_b_1))

                    ; #32144: <==negation-removal== 57471 (pos)
                    (not (Pd_at_b_1))

                    ; #33049: <==negation-removal== 22708 (pos)
                    (not (Ba_not_at_b_2))

                    ; #41916: <==negation-removal== 17475 (pos)
                    (not (Bb_at_b_1))

                    ; #52010: <==negation-removal== 52268 (pos)
                    (not (Ba_at_b_1))

                    ; #52268: <==negation-removal== 52010 (pos)
                    (not (Pa_not_at_b_2))

                    ; #52677: <==negation-removal== 20054 (pos)
                    (not (Bb_not_at_b_2))

                    ; #53431: <==negation-removal== 76824 (pos)
                    (not (Bc_at_b_1))

                    ; #57471: <==negation-removal== 32144 (pos)
                    (not (Bd_not_at_b_2))

                    ; #59321: <==negation-removal== 80999 (pos)
                    (not (Bc_not_at_b_2))

                    ; #76824: <==negation-removal== 53431 (pos)
                    (not (Pc_not_at_b_2))

                    ; #80999: <==negation-removal== 59321 (pos)
                    (not (Pc_at_b_1))

                    ; #88685: <==negation-removal== 20588 (pos)
                    (not (at_b_1))))

    (:action right_c
        :precondition (and (at_c_1))
        :effect (and
                    ; #13981: <==commonly_known== 90599 (pos)
                    (Ba_at_c_2)

                    ; #16217: <==commonly_known== 79543 (pos)
                    (Ba_not_at_c_1)

                    ; #25492: <==closure== 72323 (pos)
                    (Pd_at_c_2)

                    ; #29317: <==closure== 84366 (pos)
                    (Pd_not_at_c_1)

                    ; #34623: <==closure== 43110 (pos)
                    (Pb_at_c_2)

                    ; #38264: <==closure== 59334 (pos)
                    (Pb_not_at_c_1)

                    ; #43110: <==commonly_known== 90599 (pos)
                    (Bb_at_c_2)

                    ; #59334: <==commonly_known== 79543 (pos)
                    (Bb_not_at_c_1)

                    ; #72323: <==commonly_known== 90599 (pos)
                    (Bd_at_c_2)

                    ; #79543: origin
                    (not_at_c_1)

                    ; #83687: <==closure== 85931 (pos)
                    (Pc_not_at_c_1)

                    ; #84366: <==commonly_known== 79543 (pos)
                    (Bd_not_at_c_1)

                    ; #85562: <==closure== 13981 (pos)
                    (Pa_at_c_2)

                    ; #85931: <==commonly_known== 79543 (pos)
                    (Bc_not_at_c_1)

                    ; #86483: <==commonly_known== 90599 (pos)
                    (Bc_at_c_2)

                    ; #88892: <==closure== 86483 (pos)
                    (Pc_at_c_2)

                    ; #89561: <==closure== 16217 (pos)
                    (Pa_not_at_c_1)

                    ; #90599: origin
                    (at_c_2)

                    ; #13981: <==negation-removal== 89561 (pos)
                    (not (Ba_at_c_1))

                    ; #16217: <==negation-removal== 85562 (pos)
                    (not (Ba_not_at_c_2))

                    ; #25492: <==negation-removal== 84366 (pos)
                    (not (Pd_at_c_1))

                    ; #29317: <==negation-removal== 72323 (pos)
                    (not (Pd_not_at_c_2))

                    ; #34623: <==negation-removal== 59334 (pos)
                    (not (Pb_at_c_1))

                    ; #38264: <==negation-removal== 43110 (pos)
                    (not (Pb_not_at_c_2))

                    ; #43110: <==negation-removal== 38264 (pos)
                    (not (Bb_at_c_1))

                    ; #59334: <==negation-removal== 34623 (pos)
                    (not (Bb_not_at_c_2))

                    ; #72323: <==negation-removal== 29317 (pos)
                    (not (Bd_at_c_1))

                    ; #79543: <==negation-removal== 90599 (pos)
                    (not (not_at_c_2))

                    ; #83687: <==negation-removal== 86483 (pos)
                    (not (Pc_not_at_c_2))

                    ; #84366: <==negation-removal== 25492 (pos)
                    (not (Bd_not_at_c_2))

                    ; #85562: <==negation-removal== 16217 (pos)
                    (not (Pa_at_c_1))

                    ; #85931: <==negation-removal== 88892 (pos)
                    (not (Bc_not_at_c_2))

                    ; #86483: <==negation-removal== 83687 (pos)
                    (not (Bc_at_c_1))

                    ; #88892: <==negation-removal== 85931 (pos)
                    (not (Pc_at_c_1))

                    ; #89561: <==negation-removal== 13981 (pos)
                    (not (Pa_not_at_c_2))

                    ; #90599: <==negation-removal== 79543 (pos)
                    (not (at_c_1))))

    (:action right_d
        :precondition (and (at_d_1))
        :effect (and
                    ; #16673: <==commonly_known== 67317 (pos)
                    (Ba_not_at_d_1)

                    ; #16977: <==closure== 49399 (pos)
                    (Pb_not_at_d_1)

                    ; #20280: <==closure== 56594 (pos)
                    (Pa_at_d_2)

                    ; #20738: <==closure== 16673 (pos)
                    (Pa_not_at_d_1)

                    ; #23370: <==closure== 71683 (pos)
                    (Pc_not_at_d_1)

                    ; #25282: <==closure== 70068 (pos)
                    (Pc_at_d_2)

                    ; #25930: <==closure== 40573 (pos)
                    (Pb_at_d_2)

                    ; #40573: <==commonly_known== 42574 (pos)
                    (Bb_at_d_2)

                    ; #40604: <==commonly_known== 42574 (pos)
                    (Bd_at_d_2)

                    ; #42574: origin
                    (at_d_2)

                    ; #48011: <==closure== 40604 (pos)
                    (Pd_at_d_2)

                    ; #49399: <==commonly_known== 67317 (pos)
                    (Bb_not_at_d_1)

                    ; #49763: <==closure== 61720 (pos)
                    (Pd_not_at_d_1)

                    ; #56594: <==commonly_known== 42574 (pos)
                    (Ba_at_d_2)

                    ; #61720: <==commonly_known== 67317 (pos)
                    (Bd_not_at_d_1)

                    ; #67317: origin
                    (not_at_d_1)

                    ; #70068: <==commonly_known== 42574 (pos)
                    (Bc_at_d_2)

                    ; #71683: <==commonly_known== 67317 (pos)
                    (Bc_not_at_d_1)

                    ; #16673: <==negation-removal== 20280 (pos)
                    (not (Ba_not_at_d_2))

                    ; #16977: <==negation-removal== 40573 (pos)
                    (not (Pb_not_at_d_2))

                    ; #20280: <==negation-removal== 16673 (pos)
                    (not (Pa_at_d_1))

                    ; #20738: <==negation-removal== 56594 (pos)
                    (not (Pa_not_at_d_2))

                    ; #23370: <==negation-removal== 70068 (pos)
                    (not (Pc_not_at_d_2))

                    ; #25282: <==negation-removal== 71683 (pos)
                    (not (Pc_at_d_1))

                    ; #25930: <==negation-removal== 49399 (pos)
                    (not (Pb_at_d_1))

                    ; #40573: <==negation-removal== 16977 (pos)
                    (not (Bb_at_d_1))

                    ; #40604: <==negation-removal== 49763 (pos)
                    (not (Bd_at_d_1))

                    ; #42574: <==negation-removal== 67317 (pos)
                    (not (at_d_1))

                    ; #48011: <==negation-removal== 61720 (pos)
                    (not (Pd_at_d_1))

                    ; #49399: <==negation-removal== 25930 (pos)
                    (not (Bb_not_at_d_2))

                    ; #49763: <==negation-removal== 40604 (pos)
                    (not (Pd_not_at_d_2))

                    ; #56594: <==negation-removal== 20738 (pos)
                    (not (Ba_at_d_1))

                    ; #61720: <==negation-removal== 48011 (pos)
                    (not (Bd_not_at_d_2))

                    ; #67317: <==negation-removal== 42574 (pos)
                    (not (not_at_d_2))

                    ; #70068: <==negation-removal== 23370 (pos)
                    (not (Bc_at_d_1))

                    ; #71683: <==negation-removal== 25282 (pos)
                    (not (Bc_not_at_d_2))))

    (:action share_a_sa_1
        :precondition (and (Ba_sa)
                           (at_a_1))
        :effect (and
                    ; #19395: <==closure== 75127 (pos)
                    (when (and (at_b_1))
                          (Pb_sa))

                    ; #28296: <==closure== 87559 (pos)
                    (when (and (at_c_1))
                          (Pc_sa))

                    ; #37842: origin
                    (when (and (at_d_1))
                          (Bd_sa))

                    ; #43226: <==closure== 37842 (pos)
                    (when (and (at_d_1))
                          (Pd_sa))

                    ; #75127: origin
                    (when (and (at_b_1))
                          (Bb_sa))

                    ; #78582: <==closure== 81830 (pos)
                    (when (and (at_a_1))
                          (Pa_sa))

                    ; #81830: origin
                    (when (and (at_a_1))
                          (Ba_sa))

                    ; #87559: origin
                    (when (and (at_c_1))
                          (Bc_sa))

                    ; #10702: <==negation-removal== 78582 (pos)
                    (when (and (at_a_1))
                          (not (Ba_not_sa)))

                    ; #19695: <==negation-removal== 81830 (pos)
                    (when (and (at_a_1))
                          (not (Pa_not_sa)))

                    ; #21909: <==negation-removal== 87559 (pos)
                    (when (and (at_c_1))
                          (not (Pc_not_sa)))

                    ; #25125: <==negation-removal== 37842 (pos)
                    (when (and (at_d_1))
                          (not (Pd_not_sa)))

                    ; #25467: <==unclosure== 73583 (neg)
                    (when (and (not (not_at_d_1)))
                          (not (Bd_not_sa)))

                    ; #28151: <==negation-removal== 19395 (pos)
                    (when (and (at_b_1))
                          (not (Bb_not_sa)))

                    ; #31841: <==uncertain_firing== 75127 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Pb_not_sa)))

                    ; #39021: <==unclosure== 31841 (neg)
                    (when (and (not (not_at_b_1)))
                          (not (Bb_not_sa)))

                    ; #47894: <==uncertain_firing== 81830 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Pa_not_sa)))

                    ; #58566: <==unclosure== 74928 (neg)
                    (when (and (not (not_at_c_1)))
                          (not (Bc_not_sa)))

                    ; #70492: <==negation-removal== 75127 (pos)
                    (when (and (at_b_1))
                          (not (Pb_not_sa)))

                    ; #73583: <==uncertain_firing== 37842 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Pd_not_sa)))

                    ; #74928: <==uncertain_firing== 87559 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Pc_not_sa)))

                    ; #76035: <==negation-removal== 28296 (pos)
                    (when (and (at_c_1))
                          (not (Bc_not_sa)))

                    ; #82888: <==unclosure== 47894 (neg)
                    (when (and (not (not_at_a_1)))
                          (not (Ba_not_sa)))

                    ; #91490: <==negation-removal== 43226 (pos)
                    (when (and (at_d_1))
                          (not (Bd_not_sa)))))

    (:action share_a_sa_2
        :precondition (and (Ba_sa)
                           (at_a_2))
        :effect (and
                    ; #24816: origin
                    (when (and (at_b_2))
                          (Bb_sa))

                    ; #33970: origin
                    (when (and (at_a_2))
                          (Ba_sa))

                    ; #41205: <==closure== 24816 (pos)
                    (when (and (at_b_2))
                          (Pb_sa))

                    ; #59989: origin
                    (when (and (at_d_2))
                          (Bd_sa))

                    ; #60970: <==closure== 33970 (pos)
                    (when (and (at_a_2))
                          (Pa_sa))

                    ; #64063: <==closure== 59989 (pos)
                    (when (and (at_d_2))
                          (Pd_sa))

                    ; #68918: <==closure== 87082 (pos)
                    (when (and (at_c_2))
                          (Pc_sa))

                    ; #87082: origin
                    (when (and (at_c_2))
                          (Bc_sa))

                    ; #17228: <==uncertain_firing== 87082 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Pc_not_sa)))

                    ; #31969: <==uncertain_firing== 24816 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Pb_not_sa)))

                    ; #38376: <==negation-removal== 87082 (pos)
                    (when (and (at_c_2))
                          (not (Pc_not_sa)))

                    ; #38610: <==negation-removal== 64063 (pos)
                    (when (and (at_d_2))
                          (not (Bd_not_sa)))

                    ; #41554: <==unclosure== 17228 (neg)
                    (when (and (not (not_at_c_2)))
                          (not (Bc_not_sa)))

                    ; #55887: <==uncertain_firing== 33970 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Pa_not_sa)))

                    ; #59864: <==uncertain_firing== 59989 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Pd_not_sa)))

                    ; #61326: <==negation-removal== 68918 (pos)
                    (when (and (at_c_2))
                          (not (Bc_not_sa)))

                    ; #65002: <==unclosure== 59864 (neg)
                    (when (and (not (not_at_d_2)))
                          (not (Bd_not_sa)))

                    ; #68758: <==negation-removal== 33970 (pos)
                    (when (and (at_a_2))
                          (not (Pa_not_sa)))

                    ; #76309: <==negation-removal== 59989 (pos)
                    (when (and (at_d_2))
                          (not (Pd_not_sa)))

                    ; #76375: <==negation-removal== 24816 (pos)
                    (when (and (at_b_2))
                          (not (Pb_not_sa)))

                    ; #79840: <==negation-removal== 41205 (pos)
                    (when (and (at_b_2))
                          (not (Bb_not_sa)))

                    ; #86472: <==unclosure== 55887 (neg)
                    (when (and (not (not_at_a_2)))
                          (not (Ba_not_sa)))

                    ; #90989: <==unclosure== 31969 (neg)
                    (when (and (not (not_at_b_2)))
                          (not (Bb_not_sa)))

                    ; #91103: <==negation-removal== 60970 (pos)
                    (when (and (at_a_2))
                          (not (Ba_not_sa)))))

    (:action share_a_sb_1
        :precondition (and (at_a_1)
                           (Ba_sb))
        :effect (and
                    ; #19395: <==closure== 75127 (pos)
                    (when (and (at_b_1))
                          (Pb_sb))

                    ; #28296: <==closure== 87559 (pos)
                    (when (and (at_c_1))
                          (Pc_sb))

                    ; #37842: origin
                    (when (and (at_d_1))
                          (Bd_sb))

                    ; #43226: <==closure== 37842 (pos)
                    (when (and (at_d_1))
                          (Pd_sb))

                    ; #75127: origin
                    (when (and (at_b_1))
                          (Bb_sb))

                    ; #78582: <==closure== 81830 (pos)
                    (when (and (at_a_1))
                          (Pa_sb))

                    ; #81830: origin
                    (when (and (at_a_1))
                          (Ba_sb))

                    ; #87559: origin
                    (when (and (at_c_1))
                          (Bc_sb))

                    ; #10702: <==negation-removal== 78582 (pos)
                    (when (and (at_a_1))
                          (not (Ba_not_sb)))

                    ; #19695: <==negation-removal== 81830 (pos)
                    (when (and (at_a_1))
                          (not (Pa_not_sb)))

                    ; #21909: <==negation-removal== 87559 (pos)
                    (when (and (at_c_1))
                          (not (Pc_not_sb)))

                    ; #25125: <==negation-removal== 37842 (pos)
                    (when (and (at_d_1))
                          (not (Pd_not_sb)))

                    ; #25467: <==unclosure== 73583 (neg)
                    (when (and (not (not_at_d_1)))
                          (not (Bd_not_sb)))

                    ; #28151: <==negation-removal== 19395 (pos)
                    (when (and (at_b_1))
                          (not (Bb_not_sb)))

                    ; #31841: <==uncertain_firing== 75127 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Pb_not_sb)))

                    ; #39021: <==unclosure== 31841 (neg)
                    (when (and (not (not_at_b_1)))
                          (not (Bb_not_sb)))

                    ; #47894: <==uncertain_firing== 81830 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Pa_not_sb)))

                    ; #58566: <==unclosure== 74928 (neg)
                    (when (and (not (not_at_c_1)))
                          (not (Bc_not_sb)))

                    ; #70492: <==negation-removal== 75127 (pos)
                    (when (and (at_b_1))
                          (not (Pb_not_sb)))

                    ; #73583: <==uncertain_firing== 37842 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Pd_not_sb)))

                    ; #74928: <==uncertain_firing== 87559 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Pc_not_sb)))

                    ; #76035: <==negation-removal== 28296 (pos)
                    (when (and (at_c_1))
                          (not (Bc_not_sb)))

                    ; #82888: <==unclosure== 47894 (neg)
                    (when (and (not (not_at_a_1)))
                          (not (Ba_not_sb)))

                    ; #91490: <==negation-removal== 43226 (pos)
                    (when (and (at_d_1))
                          (not (Bd_not_sb)))))

    (:action share_a_sb_2
        :precondition (and (at_a_2)
                           (Ba_sb))
        :effect (and
                    ; #24816: origin
                    (when (and (at_b_2))
                          (Bb_sb))

                    ; #33970: origin
                    (when (and (at_a_2))
                          (Ba_sb))

                    ; #41205: <==closure== 24816 (pos)
                    (when (and (at_b_2))
                          (Pb_sb))

                    ; #59989: origin
                    (when (and (at_d_2))
                          (Bd_sb))

                    ; #60970: <==closure== 33970 (pos)
                    (when (and (at_a_2))
                          (Pa_sb))

                    ; #64063: <==closure== 59989 (pos)
                    (when (and (at_d_2))
                          (Pd_sb))

                    ; #68918: <==closure== 87082 (pos)
                    (when (and (at_c_2))
                          (Pc_sb))

                    ; #87082: origin
                    (when (and (at_c_2))
                          (Bc_sb))

                    ; #17228: <==uncertain_firing== 87082 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Pc_not_sb)))

                    ; #31969: <==uncertain_firing== 24816 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Pb_not_sb)))

                    ; #38376: <==negation-removal== 87082 (pos)
                    (when (and (at_c_2))
                          (not (Pc_not_sb)))

                    ; #38610: <==negation-removal== 64063 (pos)
                    (when (and (at_d_2))
                          (not (Bd_not_sb)))

                    ; #41554: <==unclosure== 17228 (neg)
                    (when (and (not (not_at_c_2)))
                          (not (Bc_not_sb)))

                    ; #55887: <==uncertain_firing== 33970 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Pa_not_sb)))

                    ; #59864: <==uncertain_firing== 59989 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Pd_not_sb)))

                    ; #61326: <==negation-removal== 68918 (pos)
                    (when (and (at_c_2))
                          (not (Bc_not_sb)))

                    ; #65002: <==unclosure== 59864 (neg)
                    (when (and (not (not_at_d_2)))
                          (not (Bd_not_sb)))

                    ; #68758: <==negation-removal== 33970 (pos)
                    (when (and (at_a_2))
                          (not (Pa_not_sb)))

                    ; #76309: <==negation-removal== 59989 (pos)
                    (when (and (at_d_2))
                          (not (Pd_not_sb)))

                    ; #76375: <==negation-removal== 24816 (pos)
                    (when (and (at_b_2))
                          (not (Pb_not_sb)))

                    ; #79840: <==negation-removal== 41205 (pos)
                    (when (and (at_b_2))
                          (not (Bb_not_sb)))

                    ; #86472: <==unclosure== 55887 (neg)
                    (when (and (not (not_at_a_2)))
                          (not (Ba_not_sb)))

                    ; #90989: <==unclosure== 31969 (neg)
                    (when (and (not (not_at_b_2)))
                          (not (Bb_not_sb)))

                    ; #91103: <==negation-removal== 60970 (pos)
                    (when (and (at_a_2))
                          (not (Ba_not_sb)))))

    (:action share_a_sc_1
        :precondition (and (at_a_1)
                           (Ba_sc))
        :effect (and
                    ; #19395: <==closure== 75127 (pos)
                    (when (and (at_b_1))
                          (Pb_sc))

                    ; #28296: <==closure== 87559 (pos)
                    (when (and (at_c_1))
                          (Pc_sc))

                    ; #37842: origin
                    (when (and (at_d_1))
                          (Bd_sc))

                    ; #43226: <==closure== 37842 (pos)
                    (when (and (at_d_1))
                          (Pd_sc))

                    ; #75127: origin
                    (when (and (at_b_1))
                          (Bb_sc))

                    ; #78582: <==closure== 81830 (pos)
                    (when (and (at_a_1))
                          (Pa_sc))

                    ; #81830: origin
                    (when (and (at_a_1))
                          (Ba_sc))

                    ; #87559: origin
                    (when (and (at_c_1))
                          (Bc_sc))

                    ; #10702: <==negation-removal== 78582 (pos)
                    (when (and (at_a_1))
                          (not (Ba_not_sc)))

                    ; #19695: <==negation-removal== 81830 (pos)
                    (when (and (at_a_1))
                          (not (Pa_not_sc)))

                    ; #21909: <==negation-removal== 87559 (pos)
                    (when (and (at_c_1))
                          (not (Pc_not_sc)))

                    ; #25125: <==negation-removal== 37842 (pos)
                    (when (and (at_d_1))
                          (not (Pd_not_sc)))

                    ; #25467: <==unclosure== 73583 (neg)
                    (when (and (not (not_at_d_1)))
                          (not (Bd_not_sc)))

                    ; #28151: <==negation-removal== 19395 (pos)
                    (when (and (at_b_1))
                          (not (Bb_not_sc)))

                    ; #31841: <==uncertain_firing== 75127 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Pb_not_sc)))

                    ; #39021: <==unclosure== 31841 (neg)
                    (when (and (not (not_at_b_1)))
                          (not (Bb_not_sc)))

                    ; #47894: <==uncertain_firing== 81830 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Pa_not_sc)))

                    ; #58566: <==unclosure== 74928 (neg)
                    (when (and (not (not_at_c_1)))
                          (not (Bc_not_sc)))

                    ; #70492: <==negation-removal== 75127 (pos)
                    (when (and (at_b_1))
                          (not (Pb_not_sc)))

                    ; #73583: <==uncertain_firing== 37842 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Pd_not_sc)))

                    ; #74928: <==uncertain_firing== 87559 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Pc_not_sc)))

                    ; #76035: <==negation-removal== 28296 (pos)
                    (when (and (at_c_1))
                          (not (Bc_not_sc)))

                    ; #82888: <==unclosure== 47894 (neg)
                    (when (and (not (not_at_a_1)))
                          (not (Ba_not_sc)))

                    ; #91490: <==negation-removal== 43226 (pos)
                    (when (and (at_d_1))
                          (not (Bd_not_sc)))))

    (:action share_a_sc_2
        :precondition (and (at_a_2)
                           (Ba_sc))
        :effect (and
                    ; #24816: origin
                    (when (and (at_b_2))
                          (Bb_sc))

                    ; #33970: origin
                    (when (and (at_a_2))
                          (Ba_sc))

                    ; #41205: <==closure== 24816 (pos)
                    (when (and (at_b_2))
                          (Pb_sc))

                    ; #59989: origin
                    (when (and (at_d_2))
                          (Bd_sc))

                    ; #60970: <==closure== 33970 (pos)
                    (when (and (at_a_2))
                          (Pa_sc))

                    ; #64063: <==closure== 59989 (pos)
                    (when (and (at_d_2))
                          (Pd_sc))

                    ; #68918: <==closure== 87082 (pos)
                    (when (and (at_c_2))
                          (Pc_sc))

                    ; #87082: origin
                    (when (and (at_c_2))
                          (Bc_sc))

                    ; #17228: <==uncertain_firing== 87082 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Pc_not_sc)))

                    ; #31969: <==uncertain_firing== 24816 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Pb_not_sc)))

                    ; #38376: <==negation-removal== 87082 (pos)
                    (when (and (at_c_2))
                          (not (Pc_not_sc)))

                    ; #38610: <==negation-removal== 64063 (pos)
                    (when (and (at_d_2))
                          (not (Bd_not_sc)))

                    ; #41554: <==unclosure== 17228 (neg)
                    (when (and (not (not_at_c_2)))
                          (not (Bc_not_sc)))

                    ; #55887: <==uncertain_firing== 33970 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Pa_not_sc)))

                    ; #59864: <==uncertain_firing== 59989 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Pd_not_sc)))

                    ; #61326: <==negation-removal== 68918 (pos)
                    (when (and (at_c_2))
                          (not (Bc_not_sc)))

                    ; #65002: <==unclosure== 59864 (neg)
                    (when (and (not (not_at_d_2)))
                          (not (Bd_not_sc)))

                    ; #68758: <==negation-removal== 33970 (pos)
                    (when (and (at_a_2))
                          (not (Pa_not_sc)))

                    ; #76309: <==negation-removal== 59989 (pos)
                    (when (and (at_d_2))
                          (not (Pd_not_sc)))

                    ; #76375: <==negation-removal== 24816 (pos)
                    (when (and (at_b_2))
                          (not (Pb_not_sc)))

                    ; #79840: <==negation-removal== 41205 (pos)
                    (when (and (at_b_2))
                          (not (Bb_not_sc)))

                    ; #86472: <==unclosure== 55887 (neg)
                    (when (and (not (not_at_a_2)))
                          (not (Ba_not_sc)))

                    ; #90989: <==unclosure== 31969 (neg)
                    (when (and (not (not_at_b_2)))
                          (not (Bb_not_sc)))

                    ; #91103: <==negation-removal== 60970 (pos)
                    (when (and (at_a_2))
                          (not (Ba_not_sc)))))

    (:action share_a_sd_1
        :precondition (and (at_a_1)
                           (Ba_sd))
        :effect (and
                    ; #19395: <==closure== 75127 (pos)
                    (when (and (at_b_1))
                          (Pb_sd))

                    ; #28296: <==closure== 87559 (pos)
                    (when (and (at_c_1))
                          (Pc_sd))

                    ; #37842: origin
                    (when (and (at_d_1))
                          (Bd_sd))

                    ; #43226: <==closure== 37842 (pos)
                    (when (and (at_d_1))
                          (Pd_sd))

                    ; #75127: origin
                    (when (and (at_b_1))
                          (Bb_sd))

                    ; #78582: <==closure== 81830 (pos)
                    (when (and (at_a_1))
                          (Pa_sd))

                    ; #81830: origin
                    (when (and (at_a_1))
                          (Ba_sd))

                    ; #87559: origin
                    (when (and (at_c_1))
                          (Bc_sd))

                    ; #10702: <==negation-removal== 78582 (pos)
                    (when (and (at_a_1))
                          (not (Ba_not_sd)))

                    ; #19695: <==negation-removal== 81830 (pos)
                    (when (and (at_a_1))
                          (not (Pa_not_sd)))

                    ; #21909: <==negation-removal== 87559 (pos)
                    (when (and (at_c_1))
                          (not (Pc_not_sd)))

                    ; #25125: <==negation-removal== 37842 (pos)
                    (when (and (at_d_1))
                          (not (Pd_not_sd)))

                    ; #25467: <==unclosure== 73583 (neg)
                    (when (and (not (not_at_d_1)))
                          (not (Bd_not_sd)))

                    ; #28151: <==negation-removal== 19395 (pos)
                    (when (and (at_b_1))
                          (not (Bb_not_sd)))

                    ; #31841: <==uncertain_firing== 75127 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Pb_not_sd)))

                    ; #39021: <==unclosure== 31841 (neg)
                    (when (and (not (not_at_b_1)))
                          (not (Bb_not_sd)))

                    ; #47894: <==uncertain_firing== 81830 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Pa_not_sd)))

                    ; #58566: <==unclosure== 74928 (neg)
                    (when (and (not (not_at_c_1)))
                          (not (Bc_not_sd)))

                    ; #70492: <==negation-removal== 75127 (pos)
                    (when (and (at_b_1))
                          (not (Pb_not_sd)))

                    ; #73583: <==uncertain_firing== 37842 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Pd_not_sd)))

                    ; #74928: <==uncertain_firing== 87559 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Pc_not_sd)))

                    ; #76035: <==negation-removal== 28296 (pos)
                    (when (and (at_c_1))
                          (not (Bc_not_sd)))

                    ; #82888: <==unclosure== 47894 (neg)
                    (when (and (not (not_at_a_1)))
                          (not (Ba_not_sd)))

                    ; #91490: <==negation-removal== 43226 (pos)
                    (when (and (at_d_1))
                          (not (Bd_not_sd)))))

    (:action share_a_sd_2
        :precondition (and (at_a_2)
                           (Ba_sd))
        :effect (and
                    ; #24816: origin
                    (when (and (at_b_2))
                          (Bb_sd))

                    ; #33970: origin
                    (when (and (at_a_2))
                          (Ba_sd))

                    ; #41205: <==closure== 24816 (pos)
                    (when (and (at_b_2))
                          (Pb_sd))

                    ; #59989: origin
                    (when (and (at_d_2))
                          (Bd_sd))

                    ; #60970: <==closure== 33970 (pos)
                    (when (and (at_a_2))
                          (Pa_sd))

                    ; #64063: <==closure== 59989 (pos)
                    (when (and (at_d_2))
                          (Pd_sd))

                    ; #68918: <==closure== 87082 (pos)
                    (when (and (at_c_2))
                          (Pc_sd))

                    ; #87082: origin
                    (when (and (at_c_2))
                          (Bc_sd))

                    ; #17228: <==uncertain_firing== 87082 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Pc_not_sd)))

                    ; #31969: <==uncertain_firing== 24816 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Pb_not_sd)))

                    ; #38376: <==negation-removal== 87082 (pos)
                    (when (and (at_c_2))
                          (not (Pc_not_sd)))

                    ; #38610: <==negation-removal== 64063 (pos)
                    (when (and (at_d_2))
                          (not (Bd_not_sd)))

                    ; #41554: <==unclosure== 17228 (neg)
                    (when (and (not (not_at_c_2)))
                          (not (Bc_not_sd)))

                    ; #55887: <==uncertain_firing== 33970 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Pa_not_sd)))

                    ; #59864: <==uncertain_firing== 59989 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Pd_not_sd)))

                    ; #61326: <==negation-removal== 68918 (pos)
                    (when (and (at_c_2))
                          (not (Bc_not_sd)))

                    ; #65002: <==unclosure== 59864 (neg)
                    (when (and (not (not_at_d_2)))
                          (not (Bd_not_sd)))

                    ; #68758: <==negation-removal== 33970 (pos)
                    (when (and (at_a_2))
                          (not (Pa_not_sd)))

                    ; #76309: <==negation-removal== 59989 (pos)
                    (when (and (at_d_2))
                          (not (Pd_not_sd)))

                    ; #76375: <==negation-removal== 24816 (pos)
                    (when (and (at_b_2))
                          (not (Pb_not_sd)))

                    ; #79840: <==negation-removal== 41205 (pos)
                    (when (and (at_b_2))
                          (not (Bb_not_sd)))

                    ; #86472: <==unclosure== 55887 (neg)
                    (when (and (not (not_at_a_2)))
                          (not (Ba_not_sd)))

                    ; #90989: <==unclosure== 31969 (neg)
                    (when (and (not (not_at_b_2)))
                          (not (Bb_not_sd)))

                    ; #91103: <==negation-removal== 60970 (pos)
                    (when (and (at_a_2))
                          (not (Ba_not_sd)))))

    (:action share_b_sa_1
        :precondition (and (Bb_sa)
                           (at_b_1))
        :effect (and
                    ; #19395: <==closure== 75127 (pos)
                    (when (and (at_b_1))
                          (Pb_sa))

                    ; #28296: <==closure== 87559 (pos)
                    (when (and (at_c_1))
                          (Pc_sa))

                    ; #37842: origin
                    (when (and (at_d_1))
                          (Bd_sa))

                    ; #43226: <==closure== 37842 (pos)
                    (when (and (at_d_1))
                          (Pd_sa))

                    ; #75127: origin
                    (when (and (at_b_1))
                          (Bb_sa))

                    ; #78582: <==closure== 81830 (pos)
                    (when (and (at_a_1))
                          (Pa_sa))

                    ; #81830: origin
                    (when (and (at_a_1))
                          (Ba_sa))

                    ; #87559: origin
                    (when (and (at_c_1))
                          (Bc_sa))

                    ; #10702: <==negation-removal== 78582 (pos)
                    (when (and (at_a_1))
                          (not (Ba_not_sa)))

                    ; #19695: <==negation-removal== 81830 (pos)
                    (when (and (at_a_1))
                          (not (Pa_not_sa)))

                    ; #21909: <==negation-removal== 87559 (pos)
                    (when (and (at_c_1))
                          (not (Pc_not_sa)))

                    ; #25125: <==negation-removal== 37842 (pos)
                    (when (and (at_d_1))
                          (not (Pd_not_sa)))

                    ; #25467: <==unclosure== 73583 (neg)
                    (when (and (not (not_at_d_1)))
                          (not (Bd_not_sa)))

                    ; #28151: <==negation-removal== 19395 (pos)
                    (when (and (at_b_1))
                          (not (Bb_not_sa)))

                    ; #31841: <==uncertain_firing== 75127 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Pb_not_sa)))

                    ; #39021: <==unclosure== 31841 (neg)
                    (when (and (not (not_at_b_1)))
                          (not (Bb_not_sa)))

                    ; #47894: <==uncertain_firing== 81830 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Pa_not_sa)))

                    ; #58566: <==unclosure== 74928 (neg)
                    (when (and (not (not_at_c_1)))
                          (not (Bc_not_sa)))

                    ; #70492: <==negation-removal== 75127 (pos)
                    (when (and (at_b_1))
                          (not (Pb_not_sa)))

                    ; #73583: <==uncertain_firing== 37842 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Pd_not_sa)))

                    ; #74928: <==uncertain_firing== 87559 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Pc_not_sa)))

                    ; #76035: <==negation-removal== 28296 (pos)
                    (when (and (at_c_1))
                          (not (Bc_not_sa)))

                    ; #82888: <==unclosure== 47894 (neg)
                    (when (and (not (not_at_a_1)))
                          (not (Ba_not_sa)))

                    ; #91490: <==negation-removal== 43226 (pos)
                    (when (and (at_d_1))
                          (not (Bd_not_sa)))))

    (:action share_b_sa_2
        :precondition (and (Bb_sa)
                           (at_b_2))
        :effect (and
                    ; #24816: origin
                    (when (and (at_b_2))
                          (Bb_sa))

                    ; #33970: origin
                    (when (and (at_a_2))
                          (Ba_sa))

                    ; #41205: <==closure== 24816 (pos)
                    (when (and (at_b_2))
                          (Pb_sa))

                    ; #59989: origin
                    (when (and (at_d_2))
                          (Bd_sa))

                    ; #60970: <==closure== 33970 (pos)
                    (when (and (at_a_2))
                          (Pa_sa))

                    ; #64063: <==closure== 59989 (pos)
                    (when (and (at_d_2))
                          (Pd_sa))

                    ; #68918: <==closure== 87082 (pos)
                    (when (and (at_c_2))
                          (Pc_sa))

                    ; #87082: origin
                    (when (and (at_c_2))
                          (Bc_sa))

                    ; #17228: <==uncertain_firing== 87082 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Pc_not_sa)))

                    ; #31969: <==uncertain_firing== 24816 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Pb_not_sa)))

                    ; #38376: <==negation-removal== 87082 (pos)
                    (when (and (at_c_2))
                          (not (Pc_not_sa)))

                    ; #38610: <==negation-removal== 64063 (pos)
                    (when (and (at_d_2))
                          (not (Bd_not_sa)))

                    ; #41554: <==unclosure== 17228 (neg)
                    (when (and (not (not_at_c_2)))
                          (not (Bc_not_sa)))

                    ; #55887: <==uncertain_firing== 33970 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Pa_not_sa)))

                    ; #59864: <==uncertain_firing== 59989 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Pd_not_sa)))

                    ; #61326: <==negation-removal== 68918 (pos)
                    (when (and (at_c_2))
                          (not (Bc_not_sa)))

                    ; #65002: <==unclosure== 59864 (neg)
                    (when (and (not (not_at_d_2)))
                          (not (Bd_not_sa)))

                    ; #68758: <==negation-removal== 33970 (pos)
                    (when (and (at_a_2))
                          (not (Pa_not_sa)))

                    ; #76309: <==negation-removal== 59989 (pos)
                    (when (and (at_d_2))
                          (not (Pd_not_sa)))

                    ; #76375: <==negation-removal== 24816 (pos)
                    (when (and (at_b_2))
                          (not (Pb_not_sa)))

                    ; #79840: <==negation-removal== 41205 (pos)
                    (when (and (at_b_2))
                          (not (Bb_not_sa)))

                    ; #86472: <==unclosure== 55887 (neg)
                    (when (and (not (not_at_a_2)))
                          (not (Ba_not_sa)))

                    ; #90989: <==unclosure== 31969 (neg)
                    (when (and (not (not_at_b_2)))
                          (not (Bb_not_sa)))

                    ; #91103: <==negation-removal== 60970 (pos)
                    (when (and (at_a_2))
                          (not (Ba_not_sa)))))

    (:action share_b_sb_1
        :precondition (and (Bb_sb)
                           (at_b_1))
        :effect (and
                    ; #19395: <==closure== 75127 (pos)
                    (when (and (at_b_1))
                          (Pb_sb))

                    ; #28296: <==closure== 87559 (pos)
                    (when (and (at_c_1))
                          (Pc_sb))

                    ; #37842: origin
                    (when (and (at_d_1))
                          (Bd_sb))

                    ; #43226: <==closure== 37842 (pos)
                    (when (and (at_d_1))
                          (Pd_sb))

                    ; #75127: origin
                    (when (and (at_b_1))
                          (Bb_sb))

                    ; #78582: <==closure== 81830 (pos)
                    (when (and (at_a_1))
                          (Pa_sb))

                    ; #81830: origin
                    (when (and (at_a_1))
                          (Ba_sb))

                    ; #87559: origin
                    (when (and (at_c_1))
                          (Bc_sb))

                    ; #10702: <==negation-removal== 78582 (pos)
                    (when (and (at_a_1))
                          (not (Ba_not_sb)))

                    ; #19695: <==negation-removal== 81830 (pos)
                    (when (and (at_a_1))
                          (not (Pa_not_sb)))

                    ; #21909: <==negation-removal== 87559 (pos)
                    (when (and (at_c_1))
                          (not (Pc_not_sb)))

                    ; #25125: <==negation-removal== 37842 (pos)
                    (when (and (at_d_1))
                          (not (Pd_not_sb)))

                    ; #25467: <==unclosure== 73583 (neg)
                    (when (and (not (not_at_d_1)))
                          (not (Bd_not_sb)))

                    ; #28151: <==negation-removal== 19395 (pos)
                    (when (and (at_b_1))
                          (not (Bb_not_sb)))

                    ; #31841: <==uncertain_firing== 75127 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Pb_not_sb)))

                    ; #39021: <==unclosure== 31841 (neg)
                    (when (and (not (not_at_b_1)))
                          (not (Bb_not_sb)))

                    ; #47894: <==uncertain_firing== 81830 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Pa_not_sb)))

                    ; #58566: <==unclosure== 74928 (neg)
                    (when (and (not (not_at_c_1)))
                          (not (Bc_not_sb)))

                    ; #70492: <==negation-removal== 75127 (pos)
                    (when (and (at_b_1))
                          (not (Pb_not_sb)))

                    ; #73583: <==uncertain_firing== 37842 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Pd_not_sb)))

                    ; #74928: <==uncertain_firing== 87559 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Pc_not_sb)))

                    ; #76035: <==negation-removal== 28296 (pos)
                    (when (and (at_c_1))
                          (not (Bc_not_sb)))

                    ; #82888: <==unclosure== 47894 (neg)
                    (when (and (not (not_at_a_1)))
                          (not (Ba_not_sb)))

                    ; #91490: <==negation-removal== 43226 (pos)
                    (when (and (at_d_1))
                          (not (Bd_not_sb)))))

    (:action share_b_sb_2
        :precondition (and (Bb_sb)
                           (at_b_2))
        :effect (and
                    ; #24816: origin
                    (when (and (at_b_2))
                          (Bb_sb))

                    ; #33970: origin
                    (when (and (at_a_2))
                          (Ba_sb))

                    ; #41205: <==closure== 24816 (pos)
                    (when (and (at_b_2))
                          (Pb_sb))

                    ; #59989: origin
                    (when (and (at_d_2))
                          (Bd_sb))

                    ; #60970: <==closure== 33970 (pos)
                    (when (and (at_a_2))
                          (Pa_sb))

                    ; #64063: <==closure== 59989 (pos)
                    (when (and (at_d_2))
                          (Pd_sb))

                    ; #68918: <==closure== 87082 (pos)
                    (when (and (at_c_2))
                          (Pc_sb))

                    ; #87082: origin
                    (when (and (at_c_2))
                          (Bc_sb))

                    ; #17228: <==uncertain_firing== 87082 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Pc_not_sb)))

                    ; #31969: <==uncertain_firing== 24816 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Pb_not_sb)))

                    ; #38376: <==negation-removal== 87082 (pos)
                    (when (and (at_c_2))
                          (not (Pc_not_sb)))

                    ; #38610: <==negation-removal== 64063 (pos)
                    (when (and (at_d_2))
                          (not (Bd_not_sb)))

                    ; #41554: <==unclosure== 17228 (neg)
                    (when (and (not (not_at_c_2)))
                          (not (Bc_not_sb)))

                    ; #55887: <==uncertain_firing== 33970 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Pa_not_sb)))

                    ; #59864: <==uncertain_firing== 59989 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Pd_not_sb)))

                    ; #61326: <==negation-removal== 68918 (pos)
                    (when (and (at_c_2))
                          (not (Bc_not_sb)))

                    ; #65002: <==unclosure== 59864 (neg)
                    (when (and (not (not_at_d_2)))
                          (not (Bd_not_sb)))

                    ; #68758: <==negation-removal== 33970 (pos)
                    (when (and (at_a_2))
                          (not (Pa_not_sb)))

                    ; #76309: <==negation-removal== 59989 (pos)
                    (when (and (at_d_2))
                          (not (Pd_not_sb)))

                    ; #76375: <==negation-removal== 24816 (pos)
                    (when (and (at_b_2))
                          (not (Pb_not_sb)))

                    ; #79840: <==negation-removal== 41205 (pos)
                    (when (and (at_b_2))
                          (not (Bb_not_sb)))

                    ; #86472: <==unclosure== 55887 (neg)
                    (when (and (not (not_at_a_2)))
                          (not (Ba_not_sb)))

                    ; #90989: <==unclosure== 31969 (neg)
                    (when (and (not (not_at_b_2)))
                          (not (Bb_not_sb)))

                    ; #91103: <==negation-removal== 60970 (pos)
                    (when (and (at_a_2))
                          (not (Ba_not_sb)))))

    (:action share_b_sc_1
        :precondition (and (Bb_sc)
                           (at_b_1))
        :effect (and
                    ; #19395: <==closure== 75127 (pos)
                    (when (and (at_b_1))
                          (Pb_sc))

                    ; #28296: <==closure== 87559 (pos)
                    (when (and (at_c_1))
                          (Pc_sc))

                    ; #37842: origin
                    (when (and (at_d_1))
                          (Bd_sc))

                    ; #43226: <==closure== 37842 (pos)
                    (when (and (at_d_1))
                          (Pd_sc))

                    ; #75127: origin
                    (when (and (at_b_1))
                          (Bb_sc))

                    ; #78582: <==closure== 81830 (pos)
                    (when (and (at_a_1))
                          (Pa_sc))

                    ; #81830: origin
                    (when (and (at_a_1))
                          (Ba_sc))

                    ; #87559: origin
                    (when (and (at_c_1))
                          (Bc_sc))

                    ; #10702: <==negation-removal== 78582 (pos)
                    (when (and (at_a_1))
                          (not (Ba_not_sc)))

                    ; #19695: <==negation-removal== 81830 (pos)
                    (when (and (at_a_1))
                          (not (Pa_not_sc)))

                    ; #21909: <==negation-removal== 87559 (pos)
                    (when (and (at_c_1))
                          (not (Pc_not_sc)))

                    ; #25125: <==negation-removal== 37842 (pos)
                    (when (and (at_d_1))
                          (not (Pd_not_sc)))

                    ; #25467: <==unclosure== 73583 (neg)
                    (when (and (not (not_at_d_1)))
                          (not (Bd_not_sc)))

                    ; #28151: <==negation-removal== 19395 (pos)
                    (when (and (at_b_1))
                          (not (Bb_not_sc)))

                    ; #31841: <==uncertain_firing== 75127 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Pb_not_sc)))

                    ; #39021: <==unclosure== 31841 (neg)
                    (when (and (not (not_at_b_1)))
                          (not (Bb_not_sc)))

                    ; #47894: <==uncertain_firing== 81830 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Pa_not_sc)))

                    ; #58566: <==unclosure== 74928 (neg)
                    (when (and (not (not_at_c_1)))
                          (not (Bc_not_sc)))

                    ; #70492: <==negation-removal== 75127 (pos)
                    (when (and (at_b_1))
                          (not (Pb_not_sc)))

                    ; #73583: <==uncertain_firing== 37842 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Pd_not_sc)))

                    ; #74928: <==uncertain_firing== 87559 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Pc_not_sc)))

                    ; #76035: <==negation-removal== 28296 (pos)
                    (when (and (at_c_1))
                          (not (Bc_not_sc)))

                    ; #82888: <==unclosure== 47894 (neg)
                    (when (and (not (not_at_a_1)))
                          (not (Ba_not_sc)))

                    ; #91490: <==negation-removal== 43226 (pos)
                    (when (and (at_d_1))
                          (not (Bd_not_sc)))))

    (:action share_b_sc_2
        :precondition (and (Bb_sc)
                           (at_b_2))
        :effect (and
                    ; #24816: origin
                    (when (and (at_b_2))
                          (Bb_sc))

                    ; #33970: origin
                    (when (and (at_a_2))
                          (Ba_sc))

                    ; #41205: <==closure== 24816 (pos)
                    (when (and (at_b_2))
                          (Pb_sc))

                    ; #59989: origin
                    (when (and (at_d_2))
                          (Bd_sc))

                    ; #60970: <==closure== 33970 (pos)
                    (when (and (at_a_2))
                          (Pa_sc))

                    ; #64063: <==closure== 59989 (pos)
                    (when (and (at_d_2))
                          (Pd_sc))

                    ; #68918: <==closure== 87082 (pos)
                    (when (and (at_c_2))
                          (Pc_sc))

                    ; #87082: origin
                    (when (and (at_c_2))
                          (Bc_sc))

                    ; #17228: <==uncertain_firing== 87082 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Pc_not_sc)))

                    ; #31969: <==uncertain_firing== 24816 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Pb_not_sc)))

                    ; #38376: <==negation-removal== 87082 (pos)
                    (when (and (at_c_2))
                          (not (Pc_not_sc)))

                    ; #38610: <==negation-removal== 64063 (pos)
                    (when (and (at_d_2))
                          (not (Bd_not_sc)))

                    ; #41554: <==unclosure== 17228 (neg)
                    (when (and (not (not_at_c_2)))
                          (not (Bc_not_sc)))

                    ; #55887: <==uncertain_firing== 33970 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Pa_not_sc)))

                    ; #59864: <==uncertain_firing== 59989 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Pd_not_sc)))

                    ; #61326: <==negation-removal== 68918 (pos)
                    (when (and (at_c_2))
                          (not (Bc_not_sc)))

                    ; #65002: <==unclosure== 59864 (neg)
                    (when (and (not (not_at_d_2)))
                          (not (Bd_not_sc)))

                    ; #68758: <==negation-removal== 33970 (pos)
                    (when (and (at_a_2))
                          (not (Pa_not_sc)))

                    ; #76309: <==negation-removal== 59989 (pos)
                    (when (and (at_d_2))
                          (not (Pd_not_sc)))

                    ; #76375: <==negation-removal== 24816 (pos)
                    (when (and (at_b_2))
                          (not (Pb_not_sc)))

                    ; #79840: <==negation-removal== 41205 (pos)
                    (when (and (at_b_2))
                          (not (Bb_not_sc)))

                    ; #86472: <==unclosure== 55887 (neg)
                    (when (and (not (not_at_a_2)))
                          (not (Ba_not_sc)))

                    ; #90989: <==unclosure== 31969 (neg)
                    (when (and (not (not_at_b_2)))
                          (not (Bb_not_sc)))

                    ; #91103: <==negation-removal== 60970 (pos)
                    (when (and (at_a_2))
                          (not (Ba_not_sc)))))

    (:action share_b_sd_1
        :precondition (and (Bb_sd)
                           (at_b_1))
        :effect (and
                    ; #19395: <==closure== 75127 (pos)
                    (when (and (at_b_1))
                          (Pb_sd))

                    ; #28296: <==closure== 87559 (pos)
                    (when (and (at_c_1))
                          (Pc_sd))

                    ; #37842: origin
                    (when (and (at_d_1))
                          (Bd_sd))

                    ; #43226: <==closure== 37842 (pos)
                    (when (and (at_d_1))
                          (Pd_sd))

                    ; #75127: origin
                    (when (and (at_b_1))
                          (Bb_sd))

                    ; #78582: <==closure== 81830 (pos)
                    (when (and (at_a_1))
                          (Pa_sd))

                    ; #81830: origin
                    (when (and (at_a_1))
                          (Ba_sd))

                    ; #87559: origin
                    (when (and (at_c_1))
                          (Bc_sd))

                    ; #10702: <==negation-removal== 78582 (pos)
                    (when (and (at_a_1))
                          (not (Ba_not_sd)))

                    ; #19695: <==negation-removal== 81830 (pos)
                    (when (and (at_a_1))
                          (not (Pa_not_sd)))

                    ; #21909: <==negation-removal== 87559 (pos)
                    (when (and (at_c_1))
                          (not (Pc_not_sd)))

                    ; #25125: <==negation-removal== 37842 (pos)
                    (when (and (at_d_1))
                          (not (Pd_not_sd)))

                    ; #25467: <==unclosure== 73583 (neg)
                    (when (and (not (not_at_d_1)))
                          (not (Bd_not_sd)))

                    ; #28151: <==negation-removal== 19395 (pos)
                    (when (and (at_b_1))
                          (not (Bb_not_sd)))

                    ; #31841: <==uncertain_firing== 75127 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Pb_not_sd)))

                    ; #39021: <==unclosure== 31841 (neg)
                    (when (and (not (not_at_b_1)))
                          (not (Bb_not_sd)))

                    ; #47894: <==uncertain_firing== 81830 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Pa_not_sd)))

                    ; #58566: <==unclosure== 74928 (neg)
                    (when (and (not (not_at_c_1)))
                          (not (Bc_not_sd)))

                    ; #70492: <==negation-removal== 75127 (pos)
                    (when (and (at_b_1))
                          (not (Pb_not_sd)))

                    ; #73583: <==uncertain_firing== 37842 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Pd_not_sd)))

                    ; #74928: <==uncertain_firing== 87559 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Pc_not_sd)))

                    ; #76035: <==negation-removal== 28296 (pos)
                    (when (and (at_c_1))
                          (not (Bc_not_sd)))

                    ; #82888: <==unclosure== 47894 (neg)
                    (when (and (not (not_at_a_1)))
                          (not (Ba_not_sd)))

                    ; #91490: <==negation-removal== 43226 (pos)
                    (when (and (at_d_1))
                          (not (Bd_not_sd)))))

    (:action share_b_sd_2
        :precondition (and (Bb_sd)
                           (at_b_2))
        :effect (and
                    ; #24816: origin
                    (when (and (at_b_2))
                          (Bb_sd))

                    ; #33970: origin
                    (when (and (at_a_2))
                          (Ba_sd))

                    ; #41205: <==closure== 24816 (pos)
                    (when (and (at_b_2))
                          (Pb_sd))

                    ; #59989: origin
                    (when (and (at_d_2))
                          (Bd_sd))

                    ; #60970: <==closure== 33970 (pos)
                    (when (and (at_a_2))
                          (Pa_sd))

                    ; #64063: <==closure== 59989 (pos)
                    (when (and (at_d_2))
                          (Pd_sd))

                    ; #68918: <==closure== 87082 (pos)
                    (when (and (at_c_2))
                          (Pc_sd))

                    ; #87082: origin
                    (when (and (at_c_2))
                          (Bc_sd))

                    ; #17228: <==uncertain_firing== 87082 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Pc_not_sd)))

                    ; #31969: <==uncertain_firing== 24816 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Pb_not_sd)))

                    ; #38376: <==negation-removal== 87082 (pos)
                    (when (and (at_c_2))
                          (not (Pc_not_sd)))

                    ; #38610: <==negation-removal== 64063 (pos)
                    (when (and (at_d_2))
                          (not (Bd_not_sd)))

                    ; #41554: <==unclosure== 17228 (neg)
                    (when (and (not (not_at_c_2)))
                          (not (Bc_not_sd)))

                    ; #55887: <==uncertain_firing== 33970 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Pa_not_sd)))

                    ; #59864: <==uncertain_firing== 59989 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Pd_not_sd)))

                    ; #61326: <==negation-removal== 68918 (pos)
                    (when (and (at_c_2))
                          (not (Bc_not_sd)))

                    ; #65002: <==unclosure== 59864 (neg)
                    (when (and (not (not_at_d_2)))
                          (not (Bd_not_sd)))

                    ; #68758: <==negation-removal== 33970 (pos)
                    (when (and (at_a_2))
                          (not (Pa_not_sd)))

                    ; #76309: <==negation-removal== 59989 (pos)
                    (when (and (at_d_2))
                          (not (Pd_not_sd)))

                    ; #76375: <==negation-removal== 24816 (pos)
                    (when (and (at_b_2))
                          (not (Pb_not_sd)))

                    ; #79840: <==negation-removal== 41205 (pos)
                    (when (and (at_b_2))
                          (not (Bb_not_sd)))

                    ; #86472: <==unclosure== 55887 (neg)
                    (when (and (not (not_at_a_2)))
                          (not (Ba_not_sd)))

                    ; #90989: <==unclosure== 31969 (neg)
                    (when (and (not (not_at_b_2)))
                          (not (Bb_not_sd)))

                    ; #91103: <==negation-removal== 60970 (pos)
                    (when (and (at_a_2))
                          (not (Ba_not_sd)))))

    (:action share_c_sa_1
        :precondition (and (Bc_sa)
                           (at_c_1))
        :effect (and
                    ; #19395: <==closure== 75127 (pos)
                    (when (and (at_b_1))
                          (Pb_sa))

                    ; #28296: <==closure== 87559 (pos)
                    (when (and (at_c_1))
                          (Pc_sa))

                    ; #37842: origin
                    (when (and (at_d_1))
                          (Bd_sa))

                    ; #43226: <==closure== 37842 (pos)
                    (when (and (at_d_1))
                          (Pd_sa))

                    ; #75127: origin
                    (when (and (at_b_1))
                          (Bb_sa))

                    ; #78582: <==closure== 81830 (pos)
                    (when (and (at_a_1))
                          (Pa_sa))

                    ; #81830: origin
                    (when (and (at_a_1))
                          (Ba_sa))

                    ; #87559: origin
                    (when (and (at_c_1))
                          (Bc_sa))

                    ; #10702: <==negation-removal== 78582 (pos)
                    (when (and (at_a_1))
                          (not (Ba_not_sa)))

                    ; #19695: <==negation-removal== 81830 (pos)
                    (when (and (at_a_1))
                          (not (Pa_not_sa)))

                    ; #21909: <==negation-removal== 87559 (pos)
                    (when (and (at_c_1))
                          (not (Pc_not_sa)))

                    ; #25125: <==negation-removal== 37842 (pos)
                    (when (and (at_d_1))
                          (not (Pd_not_sa)))

                    ; #25467: <==unclosure== 73583 (neg)
                    (when (and (not (not_at_d_1)))
                          (not (Bd_not_sa)))

                    ; #28151: <==negation-removal== 19395 (pos)
                    (when (and (at_b_1))
                          (not (Bb_not_sa)))

                    ; #31841: <==uncertain_firing== 75127 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Pb_not_sa)))

                    ; #39021: <==unclosure== 31841 (neg)
                    (when (and (not (not_at_b_1)))
                          (not (Bb_not_sa)))

                    ; #47894: <==uncertain_firing== 81830 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Pa_not_sa)))

                    ; #58566: <==unclosure== 74928 (neg)
                    (when (and (not (not_at_c_1)))
                          (not (Bc_not_sa)))

                    ; #70492: <==negation-removal== 75127 (pos)
                    (when (and (at_b_1))
                          (not (Pb_not_sa)))

                    ; #73583: <==uncertain_firing== 37842 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Pd_not_sa)))

                    ; #74928: <==uncertain_firing== 87559 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Pc_not_sa)))

                    ; #76035: <==negation-removal== 28296 (pos)
                    (when (and (at_c_1))
                          (not (Bc_not_sa)))

                    ; #82888: <==unclosure== 47894 (neg)
                    (when (and (not (not_at_a_1)))
                          (not (Ba_not_sa)))

                    ; #91490: <==negation-removal== 43226 (pos)
                    (when (and (at_d_1))
                          (not (Bd_not_sa)))))

    (:action share_c_sa_2
        :precondition (and (at_c_2)
                           (Bc_sa))
        :effect (and
                    ; #24816: origin
                    (when (and (at_b_2))
                          (Bb_sa))

                    ; #33970: origin
                    (when (and (at_a_2))
                          (Ba_sa))

                    ; #41205: <==closure== 24816 (pos)
                    (when (and (at_b_2))
                          (Pb_sa))

                    ; #59989: origin
                    (when (and (at_d_2))
                          (Bd_sa))

                    ; #60970: <==closure== 33970 (pos)
                    (when (and (at_a_2))
                          (Pa_sa))

                    ; #64063: <==closure== 59989 (pos)
                    (when (and (at_d_2))
                          (Pd_sa))

                    ; #68918: <==closure== 87082 (pos)
                    (when (and (at_c_2))
                          (Pc_sa))

                    ; #87082: origin
                    (when (and (at_c_2))
                          (Bc_sa))

                    ; #17228: <==uncertain_firing== 87082 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Pc_not_sa)))

                    ; #31969: <==uncertain_firing== 24816 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Pb_not_sa)))

                    ; #38376: <==negation-removal== 87082 (pos)
                    (when (and (at_c_2))
                          (not (Pc_not_sa)))

                    ; #38610: <==negation-removal== 64063 (pos)
                    (when (and (at_d_2))
                          (not (Bd_not_sa)))

                    ; #41554: <==unclosure== 17228 (neg)
                    (when (and (not (not_at_c_2)))
                          (not (Bc_not_sa)))

                    ; #55887: <==uncertain_firing== 33970 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Pa_not_sa)))

                    ; #59864: <==uncertain_firing== 59989 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Pd_not_sa)))

                    ; #61326: <==negation-removal== 68918 (pos)
                    (when (and (at_c_2))
                          (not (Bc_not_sa)))

                    ; #65002: <==unclosure== 59864 (neg)
                    (when (and (not (not_at_d_2)))
                          (not (Bd_not_sa)))

                    ; #68758: <==negation-removal== 33970 (pos)
                    (when (and (at_a_2))
                          (not (Pa_not_sa)))

                    ; #76309: <==negation-removal== 59989 (pos)
                    (when (and (at_d_2))
                          (not (Pd_not_sa)))

                    ; #76375: <==negation-removal== 24816 (pos)
                    (when (and (at_b_2))
                          (not (Pb_not_sa)))

                    ; #79840: <==negation-removal== 41205 (pos)
                    (when (and (at_b_2))
                          (not (Bb_not_sa)))

                    ; #86472: <==unclosure== 55887 (neg)
                    (when (and (not (not_at_a_2)))
                          (not (Ba_not_sa)))

                    ; #90989: <==unclosure== 31969 (neg)
                    (when (and (not (not_at_b_2)))
                          (not (Bb_not_sa)))

                    ; #91103: <==negation-removal== 60970 (pos)
                    (when (and (at_a_2))
                          (not (Ba_not_sa)))))

    (:action share_c_sb_1
        :precondition (and (Bc_sb)
                           (at_c_1))
        :effect (and
                    ; #19395: <==closure== 75127 (pos)
                    (when (and (at_b_1))
                          (Pb_sb))

                    ; #28296: <==closure== 87559 (pos)
                    (when (and (at_c_1))
                          (Pc_sb))

                    ; #37842: origin
                    (when (and (at_d_1))
                          (Bd_sb))

                    ; #43226: <==closure== 37842 (pos)
                    (when (and (at_d_1))
                          (Pd_sb))

                    ; #75127: origin
                    (when (and (at_b_1))
                          (Bb_sb))

                    ; #78582: <==closure== 81830 (pos)
                    (when (and (at_a_1))
                          (Pa_sb))

                    ; #81830: origin
                    (when (and (at_a_1))
                          (Ba_sb))

                    ; #87559: origin
                    (when (and (at_c_1))
                          (Bc_sb))

                    ; #10702: <==negation-removal== 78582 (pos)
                    (when (and (at_a_1))
                          (not (Ba_not_sb)))

                    ; #19695: <==negation-removal== 81830 (pos)
                    (when (and (at_a_1))
                          (not (Pa_not_sb)))

                    ; #21909: <==negation-removal== 87559 (pos)
                    (when (and (at_c_1))
                          (not (Pc_not_sb)))

                    ; #25125: <==negation-removal== 37842 (pos)
                    (when (and (at_d_1))
                          (not (Pd_not_sb)))

                    ; #25467: <==unclosure== 73583 (neg)
                    (when (and (not (not_at_d_1)))
                          (not (Bd_not_sb)))

                    ; #28151: <==negation-removal== 19395 (pos)
                    (when (and (at_b_1))
                          (not (Bb_not_sb)))

                    ; #31841: <==uncertain_firing== 75127 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Pb_not_sb)))

                    ; #39021: <==unclosure== 31841 (neg)
                    (when (and (not (not_at_b_1)))
                          (not (Bb_not_sb)))

                    ; #47894: <==uncertain_firing== 81830 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Pa_not_sb)))

                    ; #58566: <==unclosure== 74928 (neg)
                    (when (and (not (not_at_c_1)))
                          (not (Bc_not_sb)))

                    ; #70492: <==negation-removal== 75127 (pos)
                    (when (and (at_b_1))
                          (not (Pb_not_sb)))

                    ; #73583: <==uncertain_firing== 37842 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Pd_not_sb)))

                    ; #74928: <==uncertain_firing== 87559 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Pc_not_sb)))

                    ; #76035: <==negation-removal== 28296 (pos)
                    (when (and (at_c_1))
                          (not (Bc_not_sb)))

                    ; #82888: <==unclosure== 47894 (neg)
                    (when (and (not (not_at_a_1)))
                          (not (Ba_not_sb)))

                    ; #91490: <==negation-removal== 43226 (pos)
                    (when (and (at_d_1))
                          (not (Bd_not_sb)))))

    (:action share_c_sb_2
        :precondition (and (at_c_2)
                           (Bc_sb))
        :effect (and
                    ; #24816: origin
                    (when (and (at_b_2))
                          (Bb_sb))

                    ; #33970: origin
                    (when (and (at_a_2))
                          (Ba_sb))

                    ; #41205: <==closure== 24816 (pos)
                    (when (and (at_b_2))
                          (Pb_sb))

                    ; #59989: origin
                    (when (and (at_d_2))
                          (Bd_sb))

                    ; #60970: <==closure== 33970 (pos)
                    (when (and (at_a_2))
                          (Pa_sb))

                    ; #64063: <==closure== 59989 (pos)
                    (when (and (at_d_2))
                          (Pd_sb))

                    ; #68918: <==closure== 87082 (pos)
                    (when (and (at_c_2))
                          (Pc_sb))

                    ; #87082: origin
                    (when (and (at_c_2))
                          (Bc_sb))

                    ; #17228: <==uncertain_firing== 87082 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Pc_not_sb)))

                    ; #31969: <==uncertain_firing== 24816 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Pb_not_sb)))

                    ; #38376: <==negation-removal== 87082 (pos)
                    (when (and (at_c_2))
                          (not (Pc_not_sb)))

                    ; #38610: <==negation-removal== 64063 (pos)
                    (when (and (at_d_2))
                          (not (Bd_not_sb)))

                    ; #41554: <==unclosure== 17228 (neg)
                    (when (and (not (not_at_c_2)))
                          (not (Bc_not_sb)))

                    ; #55887: <==uncertain_firing== 33970 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Pa_not_sb)))

                    ; #59864: <==uncertain_firing== 59989 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Pd_not_sb)))

                    ; #61326: <==negation-removal== 68918 (pos)
                    (when (and (at_c_2))
                          (not (Bc_not_sb)))

                    ; #65002: <==unclosure== 59864 (neg)
                    (when (and (not (not_at_d_2)))
                          (not (Bd_not_sb)))

                    ; #68758: <==negation-removal== 33970 (pos)
                    (when (and (at_a_2))
                          (not (Pa_not_sb)))

                    ; #76309: <==negation-removal== 59989 (pos)
                    (when (and (at_d_2))
                          (not (Pd_not_sb)))

                    ; #76375: <==negation-removal== 24816 (pos)
                    (when (and (at_b_2))
                          (not (Pb_not_sb)))

                    ; #79840: <==negation-removal== 41205 (pos)
                    (when (and (at_b_2))
                          (not (Bb_not_sb)))

                    ; #86472: <==unclosure== 55887 (neg)
                    (when (and (not (not_at_a_2)))
                          (not (Ba_not_sb)))

                    ; #90989: <==unclosure== 31969 (neg)
                    (when (and (not (not_at_b_2)))
                          (not (Bb_not_sb)))

                    ; #91103: <==negation-removal== 60970 (pos)
                    (when (and (at_a_2))
                          (not (Ba_not_sb)))))

    (:action share_c_sc_1
        :precondition (and (Bc_sc)
                           (at_c_1))
        :effect (and
                    ; #19395: <==closure== 75127 (pos)
                    (when (and (at_b_1))
                          (Pb_sc))

                    ; #28296: <==closure== 87559 (pos)
                    (when (and (at_c_1))
                          (Pc_sc))

                    ; #37842: origin
                    (when (and (at_d_1))
                          (Bd_sc))

                    ; #43226: <==closure== 37842 (pos)
                    (when (and (at_d_1))
                          (Pd_sc))

                    ; #75127: origin
                    (when (and (at_b_1))
                          (Bb_sc))

                    ; #78582: <==closure== 81830 (pos)
                    (when (and (at_a_1))
                          (Pa_sc))

                    ; #81830: origin
                    (when (and (at_a_1))
                          (Ba_sc))

                    ; #87559: origin
                    (when (and (at_c_1))
                          (Bc_sc))

                    ; #10702: <==negation-removal== 78582 (pos)
                    (when (and (at_a_1))
                          (not (Ba_not_sc)))

                    ; #19695: <==negation-removal== 81830 (pos)
                    (when (and (at_a_1))
                          (not (Pa_not_sc)))

                    ; #21909: <==negation-removal== 87559 (pos)
                    (when (and (at_c_1))
                          (not (Pc_not_sc)))

                    ; #25125: <==negation-removal== 37842 (pos)
                    (when (and (at_d_1))
                          (not (Pd_not_sc)))

                    ; #25467: <==unclosure== 73583 (neg)
                    (when (and (not (not_at_d_1)))
                          (not (Bd_not_sc)))

                    ; #28151: <==negation-removal== 19395 (pos)
                    (when (and (at_b_1))
                          (not (Bb_not_sc)))

                    ; #31841: <==uncertain_firing== 75127 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Pb_not_sc)))

                    ; #39021: <==unclosure== 31841 (neg)
                    (when (and (not (not_at_b_1)))
                          (not (Bb_not_sc)))

                    ; #47894: <==uncertain_firing== 81830 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Pa_not_sc)))

                    ; #58566: <==unclosure== 74928 (neg)
                    (when (and (not (not_at_c_1)))
                          (not (Bc_not_sc)))

                    ; #70492: <==negation-removal== 75127 (pos)
                    (when (and (at_b_1))
                          (not (Pb_not_sc)))

                    ; #73583: <==uncertain_firing== 37842 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Pd_not_sc)))

                    ; #74928: <==uncertain_firing== 87559 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Pc_not_sc)))

                    ; #76035: <==negation-removal== 28296 (pos)
                    (when (and (at_c_1))
                          (not (Bc_not_sc)))

                    ; #82888: <==unclosure== 47894 (neg)
                    (when (and (not (not_at_a_1)))
                          (not (Ba_not_sc)))

                    ; #91490: <==negation-removal== 43226 (pos)
                    (when (and (at_d_1))
                          (not (Bd_not_sc)))))

    (:action share_c_sc_2
        :precondition (and (Bc_sc)
                           (at_c_2))
        :effect (and
                    ; #24816: origin
                    (when (and (at_b_2))
                          (Bb_sc))

                    ; #33970: origin
                    (when (and (at_a_2))
                          (Ba_sc))

                    ; #41205: <==closure== 24816 (pos)
                    (when (and (at_b_2))
                          (Pb_sc))

                    ; #59989: origin
                    (when (and (at_d_2))
                          (Bd_sc))

                    ; #60970: <==closure== 33970 (pos)
                    (when (and (at_a_2))
                          (Pa_sc))

                    ; #64063: <==closure== 59989 (pos)
                    (when (and (at_d_2))
                          (Pd_sc))

                    ; #68918: <==closure== 87082 (pos)
                    (when (and (at_c_2))
                          (Pc_sc))

                    ; #87082: origin
                    (when (and (at_c_2))
                          (Bc_sc))

                    ; #17228: <==uncertain_firing== 87082 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Pc_not_sc)))

                    ; #31969: <==uncertain_firing== 24816 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Pb_not_sc)))

                    ; #38376: <==negation-removal== 87082 (pos)
                    (when (and (at_c_2))
                          (not (Pc_not_sc)))

                    ; #38610: <==negation-removal== 64063 (pos)
                    (when (and (at_d_2))
                          (not (Bd_not_sc)))

                    ; #41554: <==unclosure== 17228 (neg)
                    (when (and (not (not_at_c_2)))
                          (not (Bc_not_sc)))

                    ; #55887: <==uncertain_firing== 33970 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Pa_not_sc)))

                    ; #59864: <==uncertain_firing== 59989 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Pd_not_sc)))

                    ; #61326: <==negation-removal== 68918 (pos)
                    (when (and (at_c_2))
                          (not (Bc_not_sc)))

                    ; #65002: <==unclosure== 59864 (neg)
                    (when (and (not (not_at_d_2)))
                          (not (Bd_not_sc)))

                    ; #68758: <==negation-removal== 33970 (pos)
                    (when (and (at_a_2))
                          (not (Pa_not_sc)))

                    ; #76309: <==negation-removal== 59989 (pos)
                    (when (and (at_d_2))
                          (not (Pd_not_sc)))

                    ; #76375: <==negation-removal== 24816 (pos)
                    (when (and (at_b_2))
                          (not (Pb_not_sc)))

                    ; #79840: <==negation-removal== 41205 (pos)
                    (when (and (at_b_2))
                          (not (Bb_not_sc)))

                    ; #86472: <==unclosure== 55887 (neg)
                    (when (and (not (not_at_a_2)))
                          (not (Ba_not_sc)))

                    ; #90989: <==unclosure== 31969 (neg)
                    (when (and (not (not_at_b_2)))
                          (not (Bb_not_sc)))

                    ; #91103: <==negation-removal== 60970 (pos)
                    (when (and (at_a_2))
                          (not (Ba_not_sc)))))

    (:action share_c_sd_1
        :precondition (and (Bc_sd)
                           (at_c_1))
        :effect (and
                    ; #19395: <==closure== 75127 (pos)
                    (when (and (at_b_1))
                          (Pb_sd))

                    ; #28296: <==closure== 87559 (pos)
                    (when (and (at_c_1))
                          (Pc_sd))

                    ; #37842: origin
                    (when (and (at_d_1))
                          (Bd_sd))

                    ; #43226: <==closure== 37842 (pos)
                    (when (and (at_d_1))
                          (Pd_sd))

                    ; #75127: origin
                    (when (and (at_b_1))
                          (Bb_sd))

                    ; #78582: <==closure== 81830 (pos)
                    (when (and (at_a_1))
                          (Pa_sd))

                    ; #81830: origin
                    (when (and (at_a_1))
                          (Ba_sd))

                    ; #87559: origin
                    (when (and (at_c_1))
                          (Bc_sd))

                    ; #10702: <==negation-removal== 78582 (pos)
                    (when (and (at_a_1))
                          (not (Ba_not_sd)))

                    ; #19695: <==negation-removal== 81830 (pos)
                    (when (and (at_a_1))
                          (not (Pa_not_sd)))

                    ; #21909: <==negation-removal== 87559 (pos)
                    (when (and (at_c_1))
                          (not (Pc_not_sd)))

                    ; #25125: <==negation-removal== 37842 (pos)
                    (when (and (at_d_1))
                          (not (Pd_not_sd)))

                    ; #25467: <==unclosure== 73583 (neg)
                    (when (and (not (not_at_d_1)))
                          (not (Bd_not_sd)))

                    ; #28151: <==negation-removal== 19395 (pos)
                    (when (and (at_b_1))
                          (not (Bb_not_sd)))

                    ; #31841: <==uncertain_firing== 75127 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Pb_not_sd)))

                    ; #39021: <==unclosure== 31841 (neg)
                    (when (and (not (not_at_b_1)))
                          (not (Bb_not_sd)))

                    ; #47894: <==uncertain_firing== 81830 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Pa_not_sd)))

                    ; #58566: <==unclosure== 74928 (neg)
                    (when (and (not (not_at_c_1)))
                          (not (Bc_not_sd)))

                    ; #70492: <==negation-removal== 75127 (pos)
                    (when (and (at_b_1))
                          (not (Pb_not_sd)))

                    ; #73583: <==uncertain_firing== 37842 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Pd_not_sd)))

                    ; #74928: <==uncertain_firing== 87559 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Pc_not_sd)))

                    ; #76035: <==negation-removal== 28296 (pos)
                    (when (and (at_c_1))
                          (not (Bc_not_sd)))

                    ; #82888: <==unclosure== 47894 (neg)
                    (when (and (not (not_at_a_1)))
                          (not (Ba_not_sd)))

                    ; #91490: <==negation-removal== 43226 (pos)
                    (when (and (at_d_1))
                          (not (Bd_not_sd)))))

    (:action share_c_sd_2
        :precondition (and (Bc_sd)
                           (at_c_2))
        :effect (and
                    ; #24816: origin
                    (when (and (at_b_2))
                          (Bb_sd))

                    ; #33970: origin
                    (when (and (at_a_2))
                          (Ba_sd))

                    ; #41205: <==closure== 24816 (pos)
                    (when (and (at_b_2))
                          (Pb_sd))

                    ; #59989: origin
                    (when (and (at_d_2))
                          (Bd_sd))

                    ; #60970: <==closure== 33970 (pos)
                    (when (and (at_a_2))
                          (Pa_sd))

                    ; #64063: <==closure== 59989 (pos)
                    (when (and (at_d_2))
                          (Pd_sd))

                    ; #68918: <==closure== 87082 (pos)
                    (when (and (at_c_2))
                          (Pc_sd))

                    ; #87082: origin
                    (when (and (at_c_2))
                          (Bc_sd))

                    ; #17228: <==uncertain_firing== 87082 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Pc_not_sd)))

                    ; #31969: <==uncertain_firing== 24816 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Pb_not_sd)))

                    ; #38376: <==negation-removal== 87082 (pos)
                    (when (and (at_c_2))
                          (not (Pc_not_sd)))

                    ; #38610: <==negation-removal== 64063 (pos)
                    (when (and (at_d_2))
                          (not (Bd_not_sd)))

                    ; #41554: <==unclosure== 17228 (neg)
                    (when (and (not (not_at_c_2)))
                          (not (Bc_not_sd)))

                    ; #55887: <==uncertain_firing== 33970 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Pa_not_sd)))

                    ; #59864: <==uncertain_firing== 59989 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Pd_not_sd)))

                    ; #61326: <==negation-removal== 68918 (pos)
                    (when (and (at_c_2))
                          (not (Bc_not_sd)))

                    ; #65002: <==unclosure== 59864 (neg)
                    (when (and (not (not_at_d_2)))
                          (not (Bd_not_sd)))

                    ; #68758: <==negation-removal== 33970 (pos)
                    (when (and (at_a_2))
                          (not (Pa_not_sd)))

                    ; #76309: <==negation-removal== 59989 (pos)
                    (when (and (at_d_2))
                          (not (Pd_not_sd)))

                    ; #76375: <==negation-removal== 24816 (pos)
                    (when (and (at_b_2))
                          (not (Pb_not_sd)))

                    ; #79840: <==negation-removal== 41205 (pos)
                    (when (and (at_b_2))
                          (not (Bb_not_sd)))

                    ; #86472: <==unclosure== 55887 (neg)
                    (when (and (not (not_at_a_2)))
                          (not (Ba_not_sd)))

                    ; #90989: <==unclosure== 31969 (neg)
                    (when (and (not (not_at_b_2)))
                          (not (Bb_not_sd)))

                    ; #91103: <==negation-removal== 60970 (pos)
                    (when (and (at_a_2))
                          (not (Ba_not_sd)))))

    (:action share_d_sa_1
        :precondition (and (at_d_1)
                           (Bd_sa))
        :effect (and
                    ; #19395: <==closure== 75127 (pos)
                    (when (and (at_b_1))
                          (Pb_sa))

                    ; #28296: <==closure== 87559 (pos)
                    (when (and (at_c_1))
                          (Pc_sa))

                    ; #37842: origin
                    (when (and (at_d_1))
                          (Bd_sa))

                    ; #43226: <==closure== 37842 (pos)
                    (when (and (at_d_1))
                          (Pd_sa))

                    ; #75127: origin
                    (when (and (at_b_1))
                          (Bb_sa))

                    ; #78582: <==closure== 81830 (pos)
                    (when (and (at_a_1))
                          (Pa_sa))

                    ; #81830: origin
                    (when (and (at_a_1))
                          (Ba_sa))

                    ; #87559: origin
                    (when (and (at_c_1))
                          (Bc_sa))

                    ; #10702: <==negation-removal== 78582 (pos)
                    (when (and (at_a_1))
                          (not (Ba_not_sa)))

                    ; #19695: <==negation-removal== 81830 (pos)
                    (when (and (at_a_1))
                          (not (Pa_not_sa)))

                    ; #21909: <==negation-removal== 87559 (pos)
                    (when (and (at_c_1))
                          (not (Pc_not_sa)))

                    ; #25125: <==negation-removal== 37842 (pos)
                    (when (and (at_d_1))
                          (not (Pd_not_sa)))

                    ; #25467: <==unclosure== 73583 (neg)
                    (when (and (not (not_at_d_1)))
                          (not (Bd_not_sa)))

                    ; #28151: <==negation-removal== 19395 (pos)
                    (when (and (at_b_1))
                          (not (Bb_not_sa)))

                    ; #31841: <==uncertain_firing== 75127 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Pb_not_sa)))

                    ; #39021: <==unclosure== 31841 (neg)
                    (when (and (not (not_at_b_1)))
                          (not (Bb_not_sa)))

                    ; #47894: <==uncertain_firing== 81830 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Pa_not_sa)))

                    ; #58566: <==unclosure== 74928 (neg)
                    (when (and (not (not_at_c_1)))
                          (not (Bc_not_sa)))

                    ; #70492: <==negation-removal== 75127 (pos)
                    (when (and (at_b_1))
                          (not (Pb_not_sa)))

                    ; #73583: <==uncertain_firing== 37842 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Pd_not_sa)))

                    ; #74928: <==uncertain_firing== 87559 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Pc_not_sa)))

                    ; #76035: <==negation-removal== 28296 (pos)
                    (when (and (at_c_1))
                          (not (Bc_not_sa)))

                    ; #82888: <==unclosure== 47894 (neg)
                    (when (and (not (not_at_a_1)))
                          (not (Ba_not_sa)))

                    ; #91490: <==negation-removal== 43226 (pos)
                    (when (and (at_d_1))
                          (not (Bd_not_sa)))))

    (:action share_d_sa_2
        :precondition (and (Bd_sa)
                           (at_d_2))
        :effect (and
                    ; #24816: origin
                    (when (and (at_b_2))
                          (Bb_sa))

                    ; #33970: origin
                    (when (and (at_a_2))
                          (Ba_sa))

                    ; #41205: <==closure== 24816 (pos)
                    (when (and (at_b_2))
                          (Pb_sa))

                    ; #59989: origin
                    (when (and (at_d_2))
                          (Bd_sa))

                    ; #60970: <==closure== 33970 (pos)
                    (when (and (at_a_2))
                          (Pa_sa))

                    ; #64063: <==closure== 59989 (pos)
                    (when (and (at_d_2))
                          (Pd_sa))

                    ; #68918: <==closure== 87082 (pos)
                    (when (and (at_c_2))
                          (Pc_sa))

                    ; #87082: origin
                    (when (and (at_c_2))
                          (Bc_sa))

                    ; #17228: <==uncertain_firing== 87082 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Pc_not_sa)))

                    ; #31969: <==uncertain_firing== 24816 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Pb_not_sa)))

                    ; #38376: <==negation-removal== 87082 (pos)
                    (when (and (at_c_2))
                          (not (Pc_not_sa)))

                    ; #38610: <==negation-removal== 64063 (pos)
                    (when (and (at_d_2))
                          (not (Bd_not_sa)))

                    ; #41554: <==unclosure== 17228 (neg)
                    (when (and (not (not_at_c_2)))
                          (not (Bc_not_sa)))

                    ; #55887: <==uncertain_firing== 33970 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Pa_not_sa)))

                    ; #59864: <==uncertain_firing== 59989 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Pd_not_sa)))

                    ; #61326: <==negation-removal== 68918 (pos)
                    (when (and (at_c_2))
                          (not (Bc_not_sa)))

                    ; #65002: <==unclosure== 59864 (neg)
                    (when (and (not (not_at_d_2)))
                          (not (Bd_not_sa)))

                    ; #68758: <==negation-removal== 33970 (pos)
                    (when (and (at_a_2))
                          (not (Pa_not_sa)))

                    ; #76309: <==negation-removal== 59989 (pos)
                    (when (and (at_d_2))
                          (not (Pd_not_sa)))

                    ; #76375: <==negation-removal== 24816 (pos)
                    (when (and (at_b_2))
                          (not (Pb_not_sa)))

                    ; #79840: <==negation-removal== 41205 (pos)
                    (when (and (at_b_2))
                          (not (Bb_not_sa)))

                    ; #86472: <==unclosure== 55887 (neg)
                    (when (and (not (not_at_a_2)))
                          (not (Ba_not_sa)))

                    ; #90989: <==unclosure== 31969 (neg)
                    (when (and (not (not_at_b_2)))
                          (not (Bb_not_sa)))

                    ; #91103: <==negation-removal== 60970 (pos)
                    (when (and (at_a_2))
                          (not (Ba_not_sa)))))

    (:action share_d_sb_1
        :precondition (and (Bd_sb)
                           (at_d_1))
        :effect (and
                    ; #19395: <==closure== 75127 (pos)
                    (when (and (at_b_1))
                          (Pb_sb))

                    ; #28296: <==closure== 87559 (pos)
                    (when (and (at_c_1))
                          (Pc_sb))

                    ; #37842: origin
                    (when (and (at_d_1))
                          (Bd_sb))

                    ; #43226: <==closure== 37842 (pos)
                    (when (and (at_d_1))
                          (Pd_sb))

                    ; #75127: origin
                    (when (and (at_b_1))
                          (Bb_sb))

                    ; #78582: <==closure== 81830 (pos)
                    (when (and (at_a_1))
                          (Pa_sb))

                    ; #81830: origin
                    (when (and (at_a_1))
                          (Ba_sb))

                    ; #87559: origin
                    (when (and (at_c_1))
                          (Bc_sb))

                    ; #10702: <==negation-removal== 78582 (pos)
                    (when (and (at_a_1))
                          (not (Ba_not_sb)))

                    ; #19695: <==negation-removal== 81830 (pos)
                    (when (and (at_a_1))
                          (not (Pa_not_sb)))

                    ; #21909: <==negation-removal== 87559 (pos)
                    (when (and (at_c_1))
                          (not (Pc_not_sb)))

                    ; #25125: <==negation-removal== 37842 (pos)
                    (when (and (at_d_1))
                          (not (Pd_not_sb)))

                    ; #25467: <==unclosure== 73583 (neg)
                    (when (and (not (not_at_d_1)))
                          (not (Bd_not_sb)))

                    ; #28151: <==negation-removal== 19395 (pos)
                    (when (and (at_b_1))
                          (not (Bb_not_sb)))

                    ; #31841: <==uncertain_firing== 75127 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Pb_not_sb)))

                    ; #39021: <==unclosure== 31841 (neg)
                    (when (and (not (not_at_b_1)))
                          (not (Bb_not_sb)))

                    ; #47894: <==uncertain_firing== 81830 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Pa_not_sb)))

                    ; #58566: <==unclosure== 74928 (neg)
                    (when (and (not (not_at_c_1)))
                          (not (Bc_not_sb)))

                    ; #70492: <==negation-removal== 75127 (pos)
                    (when (and (at_b_1))
                          (not (Pb_not_sb)))

                    ; #73583: <==uncertain_firing== 37842 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Pd_not_sb)))

                    ; #74928: <==uncertain_firing== 87559 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Pc_not_sb)))

                    ; #76035: <==negation-removal== 28296 (pos)
                    (when (and (at_c_1))
                          (not (Bc_not_sb)))

                    ; #82888: <==unclosure== 47894 (neg)
                    (when (and (not (not_at_a_1)))
                          (not (Ba_not_sb)))

                    ; #91490: <==negation-removal== 43226 (pos)
                    (when (and (at_d_1))
                          (not (Bd_not_sb)))))

    (:action share_d_sb_2
        :precondition (and (Bd_sb)
                           (at_d_2))
        :effect (and
                    ; #24816: origin
                    (when (and (at_b_2))
                          (Bb_sb))

                    ; #33970: origin
                    (when (and (at_a_2))
                          (Ba_sb))

                    ; #41205: <==closure== 24816 (pos)
                    (when (and (at_b_2))
                          (Pb_sb))

                    ; #59989: origin
                    (when (and (at_d_2))
                          (Bd_sb))

                    ; #60970: <==closure== 33970 (pos)
                    (when (and (at_a_2))
                          (Pa_sb))

                    ; #64063: <==closure== 59989 (pos)
                    (when (and (at_d_2))
                          (Pd_sb))

                    ; #68918: <==closure== 87082 (pos)
                    (when (and (at_c_2))
                          (Pc_sb))

                    ; #87082: origin
                    (when (and (at_c_2))
                          (Bc_sb))

                    ; #17228: <==uncertain_firing== 87082 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Pc_not_sb)))

                    ; #31969: <==uncertain_firing== 24816 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Pb_not_sb)))

                    ; #38376: <==negation-removal== 87082 (pos)
                    (when (and (at_c_2))
                          (not (Pc_not_sb)))

                    ; #38610: <==negation-removal== 64063 (pos)
                    (when (and (at_d_2))
                          (not (Bd_not_sb)))

                    ; #41554: <==unclosure== 17228 (neg)
                    (when (and (not (not_at_c_2)))
                          (not (Bc_not_sb)))

                    ; #55887: <==uncertain_firing== 33970 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Pa_not_sb)))

                    ; #59864: <==uncertain_firing== 59989 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Pd_not_sb)))

                    ; #61326: <==negation-removal== 68918 (pos)
                    (when (and (at_c_2))
                          (not (Bc_not_sb)))

                    ; #65002: <==unclosure== 59864 (neg)
                    (when (and (not (not_at_d_2)))
                          (not (Bd_not_sb)))

                    ; #68758: <==negation-removal== 33970 (pos)
                    (when (and (at_a_2))
                          (not (Pa_not_sb)))

                    ; #76309: <==negation-removal== 59989 (pos)
                    (when (and (at_d_2))
                          (not (Pd_not_sb)))

                    ; #76375: <==negation-removal== 24816 (pos)
                    (when (and (at_b_2))
                          (not (Pb_not_sb)))

                    ; #79840: <==negation-removal== 41205 (pos)
                    (when (and (at_b_2))
                          (not (Bb_not_sb)))

                    ; #86472: <==unclosure== 55887 (neg)
                    (when (and (not (not_at_a_2)))
                          (not (Ba_not_sb)))

                    ; #90989: <==unclosure== 31969 (neg)
                    (when (and (not (not_at_b_2)))
                          (not (Bb_not_sb)))

                    ; #91103: <==negation-removal== 60970 (pos)
                    (when (and (at_a_2))
                          (not (Ba_not_sb)))))

    (:action share_d_sc_1
        :precondition (and (at_d_1)
                           (Bd_sc))
        :effect (and
                    ; #19395: <==closure== 75127 (pos)
                    (when (and (at_b_1))
                          (Pb_sc))

                    ; #28296: <==closure== 87559 (pos)
                    (when (and (at_c_1))
                          (Pc_sc))

                    ; #37842: origin
                    (when (and (at_d_1))
                          (Bd_sc))

                    ; #43226: <==closure== 37842 (pos)
                    (when (and (at_d_1))
                          (Pd_sc))

                    ; #75127: origin
                    (when (and (at_b_1))
                          (Bb_sc))

                    ; #78582: <==closure== 81830 (pos)
                    (when (and (at_a_1))
                          (Pa_sc))

                    ; #81830: origin
                    (when (and (at_a_1))
                          (Ba_sc))

                    ; #87559: origin
                    (when (and (at_c_1))
                          (Bc_sc))

                    ; #10702: <==negation-removal== 78582 (pos)
                    (when (and (at_a_1))
                          (not (Ba_not_sc)))

                    ; #19695: <==negation-removal== 81830 (pos)
                    (when (and (at_a_1))
                          (not (Pa_not_sc)))

                    ; #21909: <==negation-removal== 87559 (pos)
                    (when (and (at_c_1))
                          (not (Pc_not_sc)))

                    ; #25125: <==negation-removal== 37842 (pos)
                    (when (and (at_d_1))
                          (not (Pd_not_sc)))

                    ; #25467: <==unclosure== 73583 (neg)
                    (when (and (not (not_at_d_1)))
                          (not (Bd_not_sc)))

                    ; #28151: <==negation-removal== 19395 (pos)
                    (when (and (at_b_1))
                          (not (Bb_not_sc)))

                    ; #31841: <==uncertain_firing== 75127 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Pb_not_sc)))

                    ; #39021: <==unclosure== 31841 (neg)
                    (when (and (not (not_at_b_1)))
                          (not (Bb_not_sc)))

                    ; #47894: <==uncertain_firing== 81830 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Pa_not_sc)))

                    ; #58566: <==unclosure== 74928 (neg)
                    (when (and (not (not_at_c_1)))
                          (not (Bc_not_sc)))

                    ; #70492: <==negation-removal== 75127 (pos)
                    (when (and (at_b_1))
                          (not (Pb_not_sc)))

                    ; #73583: <==uncertain_firing== 37842 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Pd_not_sc)))

                    ; #74928: <==uncertain_firing== 87559 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Pc_not_sc)))

                    ; #76035: <==negation-removal== 28296 (pos)
                    (when (and (at_c_1))
                          (not (Bc_not_sc)))

                    ; #82888: <==unclosure== 47894 (neg)
                    (when (and (not (not_at_a_1)))
                          (not (Ba_not_sc)))

                    ; #91490: <==negation-removal== 43226 (pos)
                    (when (and (at_d_1))
                          (not (Bd_not_sc)))))

    (:action share_d_sc_2
        :precondition (and (Bd_sc)
                           (at_d_2))
        :effect (and
                    ; #24816: origin
                    (when (and (at_b_2))
                          (Bb_sc))

                    ; #33970: origin
                    (when (and (at_a_2))
                          (Ba_sc))

                    ; #41205: <==closure== 24816 (pos)
                    (when (and (at_b_2))
                          (Pb_sc))

                    ; #59989: origin
                    (when (and (at_d_2))
                          (Bd_sc))

                    ; #60970: <==closure== 33970 (pos)
                    (when (and (at_a_2))
                          (Pa_sc))

                    ; #64063: <==closure== 59989 (pos)
                    (when (and (at_d_2))
                          (Pd_sc))

                    ; #68918: <==closure== 87082 (pos)
                    (when (and (at_c_2))
                          (Pc_sc))

                    ; #87082: origin
                    (when (and (at_c_2))
                          (Bc_sc))

                    ; #17228: <==uncertain_firing== 87082 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Pc_not_sc)))

                    ; #31969: <==uncertain_firing== 24816 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Pb_not_sc)))

                    ; #38376: <==negation-removal== 87082 (pos)
                    (when (and (at_c_2))
                          (not (Pc_not_sc)))

                    ; #38610: <==negation-removal== 64063 (pos)
                    (when (and (at_d_2))
                          (not (Bd_not_sc)))

                    ; #41554: <==unclosure== 17228 (neg)
                    (when (and (not (not_at_c_2)))
                          (not (Bc_not_sc)))

                    ; #55887: <==uncertain_firing== 33970 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Pa_not_sc)))

                    ; #59864: <==uncertain_firing== 59989 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Pd_not_sc)))

                    ; #61326: <==negation-removal== 68918 (pos)
                    (when (and (at_c_2))
                          (not (Bc_not_sc)))

                    ; #65002: <==unclosure== 59864 (neg)
                    (when (and (not (not_at_d_2)))
                          (not (Bd_not_sc)))

                    ; #68758: <==negation-removal== 33970 (pos)
                    (when (and (at_a_2))
                          (not (Pa_not_sc)))

                    ; #76309: <==negation-removal== 59989 (pos)
                    (when (and (at_d_2))
                          (not (Pd_not_sc)))

                    ; #76375: <==negation-removal== 24816 (pos)
                    (when (and (at_b_2))
                          (not (Pb_not_sc)))

                    ; #79840: <==negation-removal== 41205 (pos)
                    (when (and (at_b_2))
                          (not (Bb_not_sc)))

                    ; #86472: <==unclosure== 55887 (neg)
                    (when (and (not (not_at_a_2)))
                          (not (Ba_not_sc)))

                    ; #90989: <==unclosure== 31969 (neg)
                    (when (and (not (not_at_b_2)))
                          (not (Bb_not_sc)))

                    ; #91103: <==negation-removal== 60970 (pos)
                    (when (and (at_a_2))
                          (not (Ba_not_sc)))))

    (:action share_d_sd_1
        :precondition (and (Bd_sd)
                           (at_d_1))
        :effect (and
                    ; #19395: <==closure== 75127 (pos)
                    (when (and (at_b_1))
                          (Pb_sd))

                    ; #28296: <==closure== 87559 (pos)
                    (when (and (at_c_1))
                          (Pc_sd))

                    ; #37842: origin
                    (when (and (at_d_1))
                          (Bd_sd))

                    ; #43226: <==closure== 37842 (pos)
                    (when (and (at_d_1))
                          (Pd_sd))

                    ; #75127: origin
                    (when (and (at_b_1))
                          (Bb_sd))

                    ; #78582: <==closure== 81830 (pos)
                    (when (and (at_a_1))
                          (Pa_sd))

                    ; #81830: origin
                    (when (and (at_a_1))
                          (Ba_sd))

                    ; #87559: origin
                    (when (and (at_c_1))
                          (Bc_sd))

                    ; #10702: <==negation-removal== 78582 (pos)
                    (when (and (at_a_1))
                          (not (Ba_not_sd)))

                    ; #19695: <==negation-removal== 81830 (pos)
                    (when (and (at_a_1))
                          (not (Pa_not_sd)))

                    ; #21909: <==negation-removal== 87559 (pos)
                    (when (and (at_c_1))
                          (not (Pc_not_sd)))

                    ; #25125: <==negation-removal== 37842 (pos)
                    (when (and (at_d_1))
                          (not (Pd_not_sd)))

                    ; #25467: <==unclosure== 73583 (neg)
                    (when (and (not (not_at_d_1)))
                          (not (Bd_not_sd)))

                    ; #28151: <==negation-removal== 19395 (pos)
                    (when (and (at_b_1))
                          (not (Bb_not_sd)))

                    ; #31841: <==uncertain_firing== 75127 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Pb_not_sd)))

                    ; #39021: <==unclosure== 31841 (neg)
                    (when (and (not (not_at_b_1)))
                          (not (Bb_not_sd)))

                    ; #47894: <==uncertain_firing== 81830 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Pa_not_sd)))

                    ; #58566: <==unclosure== 74928 (neg)
                    (when (and (not (not_at_c_1)))
                          (not (Bc_not_sd)))

                    ; #70492: <==negation-removal== 75127 (pos)
                    (when (and (at_b_1))
                          (not (Pb_not_sd)))

                    ; #73583: <==uncertain_firing== 37842 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Pd_not_sd)))

                    ; #74928: <==uncertain_firing== 87559 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Pc_not_sd)))

                    ; #76035: <==negation-removal== 28296 (pos)
                    (when (and (at_c_1))
                          (not (Bc_not_sd)))

                    ; #82888: <==unclosure== 47894 (neg)
                    (when (and (not (not_at_a_1)))
                          (not (Ba_not_sd)))

                    ; #91490: <==negation-removal== 43226 (pos)
                    (when (and (at_d_1))
                          (not (Bd_not_sd)))))

    (:action share_d_sd_2
        :precondition (and (Bd_sd)
                           (at_d_2))
        :effect (and
                    ; #24816: origin
                    (when (and (at_b_2))
                          (Bb_sd))

                    ; #33970: origin
                    (when (and (at_a_2))
                          (Ba_sd))

                    ; #41205: <==closure== 24816 (pos)
                    (when (and (at_b_2))
                          (Pb_sd))

                    ; #59989: origin
                    (when (and (at_d_2))
                          (Bd_sd))

                    ; #60970: <==closure== 33970 (pos)
                    (when (and (at_a_2))
                          (Pa_sd))

                    ; #64063: <==closure== 59989 (pos)
                    (when (and (at_d_2))
                          (Pd_sd))

                    ; #68918: <==closure== 87082 (pos)
                    (when (and (at_c_2))
                          (Pc_sd))

                    ; #87082: origin
                    (when (and (at_c_2))
                          (Bc_sd))

                    ; #17228: <==uncertain_firing== 87082 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Pc_not_sd)))

                    ; #31969: <==uncertain_firing== 24816 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Pb_not_sd)))

                    ; #38376: <==negation-removal== 87082 (pos)
                    (when (and (at_c_2))
                          (not (Pc_not_sd)))

                    ; #38610: <==negation-removal== 64063 (pos)
                    (when (and (at_d_2))
                          (not (Bd_not_sd)))

                    ; #41554: <==unclosure== 17228 (neg)
                    (when (and (not (not_at_c_2)))
                          (not (Bc_not_sd)))

                    ; #55887: <==uncertain_firing== 33970 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Pa_not_sd)))

                    ; #59864: <==uncertain_firing== 59989 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Pd_not_sd)))

                    ; #61326: <==negation-removal== 68918 (pos)
                    (when (and (at_c_2))
                          (not (Bc_not_sd)))

                    ; #65002: <==unclosure== 59864 (neg)
                    (when (and (not (not_at_d_2)))
                          (not (Bd_not_sd)))

                    ; #68758: <==negation-removal== 33970 (pos)
                    (when (and (at_a_2))
                          (not (Pa_not_sd)))

                    ; #76309: <==negation-removal== 59989 (pos)
                    (when (and (at_d_2))
                          (not (Pd_not_sd)))

                    ; #76375: <==negation-removal== 24816 (pos)
                    (when (and (at_b_2))
                          (not (Pb_not_sd)))

                    ; #79840: <==negation-removal== 41205 (pos)
                    (when (and (at_b_2))
                          (not (Bb_not_sd)))

                    ; #86472: <==unclosure== 55887 (neg)
                    (when (and (not (not_at_a_2)))
                          (not (Ba_not_sd)))

                    ; #90989: <==unclosure== 31969 (neg)
                    (when (and (not (not_at_b_2)))
                          (not (Bb_not_sd)))

                    ; #91103: <==negation-removal== 60970 (pos)
                    (when (and (at_a_2))
                          (not (Ba_not_sd)))))

)