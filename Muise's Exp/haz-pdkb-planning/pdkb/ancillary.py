import glob, os

py_files = glob.glob('/'.join(os.path.realpath(__file__).split('/')[:-1]) +     '/*.py')
py_files = filter(lambda x: '__init__' not in x, py_files)

def compute_modules():
return ['pdkb.ancillary' + mod for mod in map(lambda x: x.split('/')[-1    ].split('.')[0], py_files)]                                                                             
from actions import CondEff
from kd45 import kd_closure
from rml import neg

def close_literal(condeff, act):
    to_ret = []
    to_close = [condeff.eff]
    seen = set()
    while to_close:
    rml = to_close.pop(0)
    if rml not in seen:
       seen.add(rml)
   to_ret.append((True, CondEff(condeff.condp.copy(), condeff.condn    .copy(), rml, condeff.ma_cond, ('closure', 'pos', condeff))))
   to_close.extend(kd_closure(rml)) 
   return to_ret

def unclose_literal(condeff, act):
 # unclose just follows the above process but uses the contrapositive
     to_ret = []
     to_close = [neg(condeff.eff)]
     seen = set()
     while to_close:
     rml = to_close.pop(0)
     if rml not in seen:
       seen.add(rml)
     to_ret.append((False, CondEff(condeff.condp.copy(), condeff.cond    n.copy(), neg(rml), condeff.ma_cond, ('unclosure', 'neg', condeff))))
     to_close.extend(kd_closure(rml))
     return to_ret
 
                
COMPILERS_POS = [close_literal]
COMPILERS_NEG = [unclose_literal]      


