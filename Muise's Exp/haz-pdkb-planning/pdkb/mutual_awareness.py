
from kd45 import PDKB
from actions import CondEff
from rml import Belief, neg, parse_rml

def commonly_known_effect(condeff, agents, depth, props, posneg, schema, negated=False):

    if (schema is False) or (condeff.depth >= depth):
        return []

    condeffs = []

    if schema is True:
        schema = [[],[]]

    if condeff.ma_cond:
        schema[0].extend(condeff.ma_cond[0])
        schema[1].extend(condeff.ma_cond[1])

    for ag in agents:

        # We can't remove belief we don't have.
        # E.g., (-) p -> B1 q and (-) p -> !B1 q together state that
        #       when p holds, we're not sure if 1 believes q or not.
        #       When introspecting for agent 1, it doesn't make sense
        #       to try and consider (+) B1 p -> !B1 !B1 q as that would
        #       make (+) B1 p -> B1 q, which when projected would be
        #       (+) p -> q. Definitely an undesired result.
        #if ag == condeff.eff.agent and isinstance(condeff.eff, Not) and negated:
        if ag == condeff.eff.agent and negated:
            #print "Warning: Ignoring the commonly known extension for the following conditional effect:\n%s" % str(condeff)
            continue

        condp = PDKB(depth, agents, props)
        condn = PDKB(depth, agents, props)

        for rml in condeff.condp:
            condp.add_rml(Belief(ag, rml).merge_modalities())
        for rml in condeff.condn:
            condp.add_rml(neg(Belief(ag, rml)).merge_modalities())

        for rmlscheme in schema[0]:
            rml = parse_rml(ag.join(rmlscheme.split('?')))
            condp.add_rml(Belief(ag, rml).merge_modalities())
        for rmlscheme in schema[1]:
            rml = parse_rml(ag.join(rmlscheme.split('?')))
            condp.add_rml(neg(Belief(ag, rml)).merge_modalities())

        new_eff = Belief(ag, condeff.eff).merge_modalities()

        if negated:
            new_eff = neg(new_eff)

        # Only add the new conditional effect if it is different (this may not
        #  be the case if there was some introspection removed)
        new_cond = CondEff(condp, condn, new_eff, condeff.ma_cond, ('commonly_known', posneg, condeff))
        if new_cond != condeff:
            condeffs.append((True, new_cond))

    return condeffs

def positive_mutual_awareness(condeff, act):
    return commonly_known_effect(condeff, act.agents, act.depth, act.props, 'pos', act.derived_cond)

def negative_mutual_awareness(condeff, act):
    return commonly_known_effect(condeff, act.agents, act.depth, act.props, 'neg', act.derived_cond, True)

COMPILERS_POS = [positive_mutual_awareness]
COMPILERS_NEG = [negative_mutual_awareness]
