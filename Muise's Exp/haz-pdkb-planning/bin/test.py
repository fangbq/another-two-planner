

import sys, os

from pdkb.test.rand_pdkbs import test0, test1, test2, test3, test4, test5
from pdkb.test.utils import parse_test_pdkb, test_random_pdkb

if __name__ == '__main__':
    if len(sys.argv) > 2:
        assert 'test' == sys.argv[1]
        [test0, test1, test2, test3, test4, test5][int(sys.argv[2])]()
    elif len(sys.argv) > 1:
        parse_test_pdkb(sys.argv[1])
    else:
        test_random_pdkb(3, 2, 'pq', 5)
