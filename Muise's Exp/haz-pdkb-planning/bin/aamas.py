
import sys, os

from pdkb.test.aamas import doit, checkit

if __name__ == '__main__':

    if len(sys.argv) != 2 or sys.argv[1] not in ['run', 'assess']:
        print "\n Usage: python aamas.py [run|assess]\n"
        sys.exit(1)

    if 'run' == sys.argv[1]:
        doit()

    elif 'assess' == sys.argv[1]:
        checkit()
