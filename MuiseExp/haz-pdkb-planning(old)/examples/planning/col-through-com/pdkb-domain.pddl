(define (domain pdkb-planning)

    (:requirements :strips :conditional-effects)

    (:predicates
        (not_at_a_1)
        (not_at_a_2)
        (not_at_a_3)
        (not_at_a_4)
        (not_at_b_1)
        (not_at_b_2)
        (not_at_b_3)
        (not_at_b_4)
        (not_blue)
        (not_red)
        (Ba_not_at_a_1)
        (Ba_not_at_a_2)
        (Ba_not_at_a_3)
        (Ba_not_at_a_4)
        (Ba_not_at_b_1)
        (Ba_not_at_b_2)
        (Ba_not_at_b_3)
        (Ba_not_at_b_4)
        (Ba_not_blue)
        (Ba_not_red)
        (Ba_at_a_1)
        (Ba_at_a_2)
        (Ba_at_a_3)
        (Ba_at_a_4)
        (Ba_at_b_1)
        (Ba_at_b_2)
        (Ba_at_b_3)
        (Ba_at_b_4)
        (Ba_blue)
        (Ba_red)
        (Bb_not_at_a_1)
        (Bb_not_at_a_2)
        (Bb_not_at_a_3)
        (Bb_not_at_a_4)
        (Bb_not_at_b_1)
        (Bb_not_at_b_2)
        (Bb_not_at_b_3)
        (Bb_not_at_b_4)
        (Bb_not_blue)
        (Bb_not_red)
        (Bb_at_a_1)
        (Bb_at_a_2)
        (Bb_at_a_3)
        (Bb_at_a_4)
        (Bb_at_b_1)
        (Bb_at_b_2)
        (Bb_at_b_3)
        (Bb_at_b_4)
        (Bb_blue)
        (Bb_red)
        (Pa_not_at_a_1)
        (Pa_not_at_a_2)
        (Pa_not_at_a_3)
        (Pa_not_at_a_4)
        (Pa_not_at_b_1)
        (Pa_not_at_b_2)
        (Pa_not_at_b_3)
        (Pa_not_at_b_4)
        (Pa_not_blue)
        (Pa_not_red)
        (Pa_at_a_1)
        (Pa_at_a_2)
        (Pa_at_a_3)
        (Pa_at_a_4)
        (Pa_at_b_1)
        (Pa_at_b_2)
        (Pa_at_b_3)
        (Pa_at_b_4)
        (Pa_blue)
        (Pa_red)
        (Pb_not_at_a_1)
        (Pb_not_at_a_2)
        (Pb_not_at_a_3)
        (Pb_not_at_a_4)
        (Pb_not_at_b_1)
        (Pb_not_at_b_2)
        (Pb_not_at_b_3)
        (Pb_not_at_b_4)
        (Pb_not_blue)
        (Pb_not_red)
        (Pb_at_a_1)
        (Pb_at_a_2)
        (Pb_at_a_3)
        (Pb_at_a_4)
        (Pb_at_b_1)
        (Pb_at_b_2)
        (Pb_at_b_3)
        (Pb_at_b_4)
        (Pb_blue)
        (Pb_red)
        (at_a_1)
        (at_a_2)
        (at_a_3)
        (at_a_4)
        (at_b_1)
        (at_b_2)
        (at_b_3)
        (at_b_4)
        (blue)
        (red)
    )

    (:action left_a
        :precondition (and (not_at_a_1))
        :effect (and
                    ; #10784: <==commonly_known== 25056 (pos)
                    (when (and (Bb_at_a_4))
                          (Bb_not_at_a_4))

                    ; #12260: <==commonly_known== 25056 (neg)
                    (when (and (Bb_at_a_4))
                          (Pb_at_a_3))

                    ; #12946: <==commonly_known== 53310 (neg)
                    (when (and (Bb_at_a_3))
                          (Pb_at_a_2))

                    ; #14447: <==commonly_known== 22650 (neg)
                    (when (and (Pa_at_a_4))
                          (Pa_not_at_a_4))

                    ; #18479: <==commonly_known== 41460 (neg)
                    (when (and (Bb_at_a_3))
                          (Pb_not_at_a_3))

                    ; #18745: <==commonly_known== 10289 (neg)
                    (when (and (Pb_at_a_2))
                          (Pb_at_a_1))

                    ; #21057: <==commonly_known== 61663 (neg)
                    (when (and (Pb_at_a_2))
                          (Pb_not_at_a_2))

                    ; #25056: origin
                    (when (and (at_a_4))
                          (not_at_a_4))

                    ; #25556: <==commonly_known== 87141 (neg)
                    (when (and (Ba_at_a_2))
                          (Pa_not_at_a_2))

                    ; #25996: <==commonly_known== 84239 (pos)
                    (when (and (Ba_at_a_4))
                          (Ba_at_a_3))

                    ; #27130: <==commonly_known== 73442 (neg)
                    (when (and (Pa_at_a_3))
                          (Pa_at_a_2))

                    ; #27860: <==commonly_known== 25056 (pos)
                    (when (and (Ba_at_a_4))
                          (Ba_not_at_a_4))

                    ; #30366: <==commonly_known== 41460 (pos)
                    (when (and (Bb_at_a_3))
                          (Bb_at_a_2))

                    ; #39573: <==commonly_known== 74587 (neg)
                    (when (and (Ba_at_a_2))
                          (Pa_at_a_1))

                    ; #41460: origin
                    (when (and (at_a_3))
                          (at_a_2))

                    ; #42899: <==commonly_known== 87141 (pos)
                    (when (and (Bb_at_a_2))
                          (Bb_at_a_1))

                    ; #47376: <==commonly_known== 53310 (pos)
                    (when (and (Ba_at_a_3))
                          (Ba_not_at_a_3))

                    ; #50336: <==commonly_known== 73442 (neg)
                    (when (and (Pb_at_a_3))
                          (Pb_at_a_2))

                    ; #50747: <==commonly_known== 41460 (pos)
                    (when (and (Ba_at_a_3))
                          (Ba_at_a_2))

                    ; #53310: origin
                    (when (and (at_a_3))
                          (not_at_a_3))

                    ; #53776: <==commonly_known== 84239 (neg)
                    (when (and (Ba_at_a_4))
                          (Pa_not_at_a_4))

                    ; #55059: <==commonly_known== 61663 (neg)
                    (when (and (Pa_at_a_2))
                          (Pa_not_at_a_2))

                    ; #56621: <==commonly_known== 41460 (neg)
                    (when (and (Ba_at_a_3))
                          (Pa_not_at_a_3))

                    ; #57307: <==commonly_known== 73283 (neg)
                    (when (and (Pb_at_a_3))
                          (Pb_not_at_a_3))

                    ; #62836: <==commonly_known== 87141 (pos)
                    (when (and (Ba_at_a_2))
                          (Ba_at_a_1))

                    ; #64976: <==commonly_known== 10289 (neg)
                    (when (and (Pa_at_a_2))
                          (Pa_at_a_1))

                    ; #66652: <==commonly_known== 84239 (neg)
                    (when (and (Bb_at_a_4))
                          (Pb_not_at_a_4))

                    ; #66814: <==commonly_known== 73283 (neg)
                    (when (and (Pa_at_a_3))
                          (Pa_not_at_a_3))

                    ; #68352: <==commonly_known== 74587 (pos)
                    (when (and (Ba_at_a_2))
                          (Ba_not_at_a_2))

                    ; #68413: <==commonly_known== 22650 (neg)
                    (when (and (Pb_at_a_4))
                          (Pb_not_at_a_4))

                    ; #70378: <==commonly_known== 53310 (neg)
                    (when (and (Ba_at_a_3))
                          (Pa_at_a_2))

                    ; #74349: <==commonly_known== 84239 (pos)
                    (when (and (Bb_at_a_4))
                          (Bb_at_a_3))

                    ; #74587: origin
                    (when (and (at_a_2))
                          (not_at_a_2))

                    ; #75428: <==commonly_known== 25056 (neg)
                    (when (and (Ba_at_a_4))
                          (Pa_at_a_3))

                    ; #76708: <==commonly_known== 83441 (neg)
                    (when (and (Pb_at_a_4))
                          (Pb_at_a_3))

                    ; #83021: <==commonly_known== 87141 (neg)
                    (when (and (Bb_at_a_2))
                          (Pb_not_at_a_2))

                    ; #84239: origin
                    (when (and (at_a_4))
                          (at_a_3))

                    ; #84875: <==commonly_known== 53310 (pos)
                    (when (and (Bb_at_a_3))
                          (Bb_not_at_a_3))

                    ; #85085: <==commonly_known== 74587 (pos)
                    (when (and (Bb_at_a_2))
                          (Bb_not_at_a_2))

                    ; #87141: origin
                    (when (and (at_a_2))
                          (at_a_1))

                    ; #87741: <==commonly_known== 74587 (neg)
                    (when (and (Bb_at_a_2))
                          (Pb_at_a_1))

                    ; #88751: <==commonly_known== 83441 (neg)
                    (when (and (Pa_at_a_4))
                          (Pa_at_a_3))

                    ; #10289: <==uncertain_firing== 87141 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (not_at_a_1)))

                    ; #10309: <==negation-removal== 88751 (pos)
                    (when (and (Pa_at_a_4))
                          (not (Ba_not_at_a_3)))

                    ; #10509: <==uncertain_firing== 88751 (pos)
                    (when (and (not (Ba_not_at_a_4)))
                          (not (Ba_not_at_a_3)))

                    ; #10784: <==negation-removal== 12260 (pos)
                    (when (and (Bb_at_a_4))
                          (not (Bb_not_at_a_3)))

                    ; #11460: <==negation-removal== 57307 (pos)
                    (when (and (Pb_at_a_3))
                          (not (Bb_at_a_3)))

                    ; #12260: <==negation-removal== 10784 (pos)
                    (when (and (Bb_at_a_4))
                          (not (Pb_at_a_4)))

                    ; #12946: <==negation-removal== 84875 (pos)
                    (when (and (Bb_at_a_3))
                          (not (Pb_at_a_3)))

                    ; #13007: <==negation-removal== 64976 (pos)
                    (when (and (Pa_at_a_2))
                          (not (Ba_not_at_a_1)))

                    ; #13800: <==uncertain_firing== 18479 (pos)
                    (when (and (not (Pb_not_at_a_3)))
                          (not (Bb_at_a_3)))

                    ; #18479: <==negation-removal== 30366 (pos)
                    (when (and (Bb_at_a_3))
                          (not (Pb_not_at_a_2)))

                    ; #19328: <==uncertain_firing== 75428 (pos)
                    (when (and (not (Pa_not_at_a_4)))
                          (not (Ba_not_at_a_3)))

                    ; #19998: <==uncertain_firing== 83021 (pos)
                    (when (and (not (Pb_not_at_a_2)))
                          (not (Bb_at_a_2)))

                    ; #21002: <==uncertain_firing== 50336 (pos)
                    (when (and (not (Bb_not_at_a_3)))
                          (not (Bb_not_at_a_2)))

                    ; #21855: <==uncertain_firing== 55059 (pos)
                    (when (and (not (Ba_not_at_a_2)))
                          (not (Ba_at_a_2)))

                    ; #22502: <==uncertain_firing== 25556 (pos)
                    (when (and (not (Pa_not_at_a_2)))
                          (not (Ba_at_a_2)))

                    ; #22650: <==uncertain_firing== 25056 (pos)
                    (when (and (not (not_at_a_4)))
                          (not (at_a_4)))

                    ; #24863: <==uncertain_firing== 70378 (pos)
                    (when (and (not (Pa_not_at_a_3)))
                          (not (Ba_not_at_a_2)))

                    ; #25056: <==negation-removal== 84239 (pos)
                    (when (and (at_a_4))
                          (not (not_at_a_3)))

                    ; #25556: <==negation-removal== 62836 (pos)
                    (when (and (Ba_at_a_2))
                          (not (Pa_not_at_a_1)))

                    ; #25885: <==negation-removal== 27130 (pos)
                    (when (and (Pa_at_a_3))
                          (not (Ba_not_at_a_2)))

                    ; #25996: <==negation-removal== 53776 (pos)
                    (when (and (Ba_at_a_4))
                          (not (Ba_at_a_4)))

                    ; #26066: <==negation-removal== 76708 (pos)
                    (when (and (Pb_at_a_4))
                          (not (Bb_not_at_a_3)))

                    ; #27860: <==negation-removal== 75428 (pos)
                    (when (and (Ba_at_a_4))
                          (not (Ba_not_at_a_3)))

                    ; #28945: <==uncertain_firing== 66814 (pos)
                    (when (and (not (Ba_not_at_a_3)))
                          (not (Ba_at_a_3)))

                    ; #30366: <==negation-removal== 18479 (pos)
                    (when (and (Bb_at_a_3))
                          (not (Bb_at_a_3)))

                    ; #35325: <==uncertain_firing== 12260 (pos)
                    (when (and (not (Pb_not_at_a_4)))
                          (not (Bb_not_at_a_3)))

                    ; #36257: <==uncertain_firing== 68352 (pos)
                    (when (and (not (Pa_not_at_a_2)))
                          (not (Pa_at_a_2)))

                    ; #38445: <==negation-removal== 68413 (pos)
                    (when (and (Pb_at_a_4))
                          (not (Bb_at_a_4)))

                    ; #38633: <==uncertain_firing== 47376 (pos)
                    (when (and (not (Pa_not_at_a_3)))
                          (not (Pa_at_a_3)))

                    ; #39573: <==negation-removal== 68352 (pos)
                    (when (and (Ba_at_a_2))
                          (not (Pa_at_a_2)))

                    ; #41460: <==negation-removal== 53310 (pos)
                    (when (and (at_a_3))
                          (not (at_a_3)))

                    ; #41529: <==uncertain_firing== 62836 (pos)
                    (when (and (not (Pa_not_at_a_2)))
                          (not (Pa_not_at_a_1)))

                    ; #41657: <==uncertain_firing== 66652 (pos)
                    (when (and (not (Pb_not_at_a_4)))
                          (not (Bb_at_a_4)))

                    ; #42899: <==negation-removal== 83021 (pos)
                    (when (and (Bb_at_a_2))
                          (not (Bb_at_a_2)))

                    ; #45184: <==uncertain_firing== 39573 (pos)
                    (when (and (not (Pa_not_at_a_2)))
                          (not (Ba_not_at_a_1)))

                    ; #46500: <==uncertain_firing== 74349 (pos)
                    (when (and (not (Pb_not_at_a_4)))
                          (not (Pb_not_at_a_3)))

                    ; #47376: <==negation-removal== 70378 (pos)
                    (when (and (Ba_at_a_3))
                          (not (Ba_not_at_a_2)))

                    ; #48487: <==uncertain_firing== 85085 (pos)
                    (when (and (not (Pb_not_at_a_2)))
                          (not (Pb_at_a_2)))

                    ; #48890: <==uncertain_firing== 25996 (pos)
                    (when (and (not (Pa_not_at_a_4)))
                          (not (Pa_not_at_a_3)))

                    ; #50747: <==negation-removal== 56621 (pos)
                    (when (and (Ba_at_a_3))
                          (not (Ba_at_a_3)))

                    ; #51740: <==uncertain_firing== 56621 (pos)
                    (when (and (not (Pa_not_at_a_3)))
                          (not (Ba_at_a_3)))

                    ; #53310: <==negation-removal== 41460 (pos)
                    (when (and (at_a_3))
                          (not (not_at_a_2)))

                    ; #53776: <==negation-removal== 25996 (pos)
                    (when (and (Ba_at_a_4))
                          (not (Pa_not_at_a_3)))

                    ; #55695: <==uncertain_firing== 50747 (pos)
                    (when (and (not (Pa_not_at_a_3)))
                          (not (Pa_not_at_a_2)))

                    ; #55980: <==uncertain_firing== 10784 (pos)
                    (when (and (not (Pb_not_at_a_4)))
                          (not (Pb_at_a_4)))

                    ; #56621: <==negation-removal== 50747 (pos)
                    (when (and (Ba_at_a_3))
                          (not (Pa_not_at_a_2)))

                    ; #56675: <==uncertain_firing== 76708 (pos)
                    (when (and (not (Bb_not_at_a_4)))
                          (not (Bb_not_at_a_3)))

                    ; #58859: <==uncertain_firing== 21057 (pos)
                    (when (and (not (Bb_not_at_a_2)))
                          (not (Bb_at_a_2)))

                    ; #59654: <==uncertain_firing== 64976 (pos)
                    (when (and (not (Ba_not_at_a_2)))
                          (not (Ba_not_at_a_1)))

                    ; #61663: <==uncertain_firing== 74587 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (at_a_2)))

                    ; #62787: <==negation-removal== 21057 (pos)
                    (when (and (Pb_at_a_2))
                          (not (Bb_at_a_2)))

                    ; #62836: <==negation-removal== 25556 (pos)
                    (when (and (Ba_at_a_2))
                          (not (Ba_at_a_2)))

                    ; #65061: <==uncertain_firing== 27860 (pos)
                    (when (and (not (Pa_not_at_a_4)))
                          (not (Pa_at_a_4)))

                    ; #66652: <==negation-removal== 74349 (pos)
                    (when (and (Bb_at_a_4))
                          (not (Pb_not_at_a_3)))

                    ; #68352: <==negation-removal== 39573 (pos)
                    (when (and (Ba_at_a_2))
                          (not (Ba_not_at_a_1)))

                    ; #70378: <==negation-removal== 47376 (pos)
                    (when (and (Ba_at_a_3))
                          (not (Pa_at_a_3)))

                    ; #72232: <==uncertain_firing== 30366 (pos)
                    (when (and (not (Pb_not_at_a_3)))
                          (not (Pb_not_at_a_2)))

                    ; #72525: <==uncertain_firing== 14447 (pos)
                    (when (and (not (Ba_not_at_a_4)))
                          (not (Ba_at_a_4)))

                    ; #73283: <==uncertain_firing== 53310 (pos)
                    (when (and (not (not_at_a_3)))
                          (not (at_a_3)))

                    ; #73442: <==uncertain_firing== 41460 (pos)
                    (when (and (not (not_at_a_3)))
                          (not (not_at_a_2)))

                    ; #73632: <==uncertain_firing== 42899 (pos)
                    (when (and (not (Pb_not_at_a_2)))
                          (not (Pb_not_at_a_1)))

                    ; #74349: <==negation-removal== 66652 (pos)
                    (when (and (Bb_at_a_4))
                          (not (Bb_at_a_4)))

                    ; #74587: <==negation-removal== 87141 (pos)
                    (when (and (at_a_2))
                          (not (not_at_a_1)))

                    ; #74963: <==uncertain_firing== 18745 (pos)
                    (when (and (not (Bb_not_at_a_2)))
                          (not (Bb_not_at_a_1)))

                    ; #75395: <==uncertain_firing== 53776 (pos)
                    (when (and (not (Pa_not_at_a_4)))
                          (not (Ba_at_a_4)))

                    ; #75428: <==negation-removal== 27860 (pos)
                    (when (and (Ba_at_a_4))
                          (not (Pa_at_a_4)))

                    ; #76226: <==negation-removal== 14447 (pos)
                    (when (and (Pa_at_a_4))
                          (not (Ba_at_a_4)))

                    ; #76242: <==uncertain_firing== 84875 (pos)
                    (when (and (not (Pb_not_at_a_3)))
                          (not (Pb_at_a_3)))

                    ; #76580: <==uncertain_firing== 87741 (pos)
                    (when (and (not (Pb_not_at_a_2)))
                          (not (Bb_not_at_a_1)))

                    ; #76691: <==negation-removal== 55059 (pos)
                    (when (and (Pa_at_a_2))
                          (not (Ba_at_a_2)))

                    ; #79068: <==negation-removal== 18745 (pos)
                    (when (and (Pb_at_a_2))
                          (not (Bb_not_at_a_1)))

                    ; #82237: <==uncertain_firing== 57307 (pos)
                    (when (and (not (Bb_not_at_a_3)))
                          (not (Bb_at_a_3)))

                    ; #83021: <==negation-removal== 42899 (pos)
                    (when (and (Bb_at_a_2))
                          (not (Pb_not_at_a_1)))

                    ; #83441: <==uncertain_firing== 84239 (pos)
                    (when (and (not (not_at_a_4)))
                          (not (not_at_a_3)))

                    ; #83555: <==uncertain_firing== 12946 (pos)
                    (when (and (not (Pb_not_at_a_3)))
                          (not (Bb_not_at_a_2)))

                    ; #84239: <==negation-removal== 25056 (pos)
                    (when (and (at_a_4))
                          (not (at_a_4)))

                    ; #84573: <==uncertain_firing== 68413 (pos)
                    (when (and (not (Bb_not_at_a_4)))
                          (not (Bb_at_a_4)))

                    ; #84875: <==negation-removal== 12946 (pos)
                    (when (and (Bb_at_a_3))
                          (not (Bb_not_at_a_2)))

                    ; #85085: <==negation-removal== 87741 (pos)
                    (when (and (Bb_at_a_2))
                          (not (Bb_not_at_a_1)))

                    ; #85701: <==negation-removal== 50336 (pos)
                    (when (and (Pb_at_a_3))
                          (not (Bb_not_at_a_2)))

                    ; #86190: <==uncertain_firing== 27130 (pos)
                    (when (and (not (Ba_not_at_a_3)))
                          (not (Ba_not_at_a_2)))

                    ; #87141: <==negation-removal== 74587 (pos)
                    (when (and (at_a_2))
                          (not (at_a_2)))

                    ; #87741: <==negation-removal== 85085 (pos)
                    (when (and (Bb_at_a_2))
                          (not (Pb_at_a_2)))

                    ; #91130: <==negation-removal== 66814 (pos)
                    (when (and (Pa_at_a_3))
                          (not (Ba_at_a_3)))))

    (:action left_b
        :precondition (and (not_at_b_1))
        :effect (and
                    ; #10023: <==commonly_known== 65747 (neg)
                    (when (and (Bb_at_b_3))
                          (Pb_at_b_2))

                    ; #14227: <==commonly_known== 67107 (neg)
                    (when (and (Pa_at_b_3))
                          (Pa_at_b_2))

                    ; #18178: <==commonly_known== 61516 (neg)
                    (when (and (Pb_at_b_2))
                          (Pb_not_at_b_2))

                    ; #18185: origin
                    (when (and (at_b_4))
                          (at_b_3))

                    ; #21532: <==commonly_known== 78697 (pos)
                    (when (and (Bb_at_b_3))
                          (Bb_at_b_2))

                    ; #24379: <==commonly_known== 65747 (neg)
                    (when (and (Ba_at_b_3))
                          (Pa_at_b_2))

                    ; #26802: <==commonly_known== 78697 (neg)
                    (when (and (Bb_at_b_3))
                          (Pb_not_at_b_3))

                    ; #31610: <==commonly_known== 61516 (neg)
                    (when (and (Pa_at_b_2))
                          (Pa_not_at_b_2))

                    ; #32532: <==commonly_known== 80594 (neg)
                    (when (and (Ba_at_b_2))
                          (Pa_not_at_b_2))

                    ; #33573: <==commonly_known== 51315 (neg)
                    (when (and (Pa_at_b_4))
                          (Pa_not_at_b_4))

                    ; #33960: <==commonly_known== 85936 (neg)
                    (when (and (Bb_at_b_2))
                          (Pb_at_b_1))

                    ; #35878: <==commonly_known== 80594 (pos)
                    (when (and (Ba_at_b_2))
                          (Ba_at_b_1))

                    ; #37798: <==commonly_known== 68990 (neg)
                    (when (and (Bb_at_b_4))
                          (Pb_at_b_3))

                    ; #42844: <==commonly_known== 78697 (pos)
                    (when (and (Ba_at_b_3))
                          (Ba_at_b_2))

                    ; #45844: <==commonly_known== 85936 (pos)
                    (when (and (Ba_at_b_2))
                          (Ba_not_at_b_2))

                    ; #46522: <==commonly_known== 80594 (neg)
                    (when (and (Bb_at_b_2))
                          (Pb_not_at_b_2))

                    ; #48741: <==commonly_known== 50592 (neg)
                    (when (and (Pa_at_b_2))
                          (Pa_at_b_1))

                    ; #51520: <==commonly_known== 65747 (pos)
                    (when (and (Bb_at_b_3))
                          (Bb_not_at_b_3))

                    ; #55027: <==commonly_known== 48003 (neg)
                    (when (and (Pa_at_b_3))
                          (Pa_not_at_b_3))

                    ; #56128: <==commonly_known== 68990 (pos)
                    (when (and (Ba_at_b_4))
                          (Ba_not_at_b_4))

                    ; #61763: <==commonly_known== 48003 (neg)
                    (when (and (Pb_at_b_3))
                          (Pb_not_at_b_3))

                    ; #63883: <==commonly_known== 85936 (neg)
                    (when (and (Ba_at_b_2))
                          (Pa_at_b_1))

                    ; #65747: origin
                    (when (and (at_b_3))
                          (not_at_b_3))

                    ; #68990: origin
                    (when (and (at_b_4))
                          (not_at_b_4))

                    ; #70573: <==commonly_known== 68990 (pos)
                    (when (and (Bb_at_b_4))
                          (Bb_not_at_b_4))

                    ; #70979: <==commonly_known== 50592 (neg)
                    (when (and (Pb_at_b_2))
                          (Pb_at_b_1))

                    ; #71271: <==commonly_known== 67107 (neg)
                    (when (and (Pb_at_b_3))
                          (Pb_at_b_2))

                    ; #71366: <==commonly_known== 51315 (neg)
                    (when (and (Pb_at_b_4))
                          (Pb_not_at_b_4))

                    ; #71377: <==commonly_known== 18185 (pos)
                    (when (and (Bb_at_b_4))
                          (Bb_at_b_3))

                    ; #72065: <==commonly_known== 18185 (neg)
                    (when (and (Bb_at_b_4))
                          (Pb_not_at_b_4))

                    ; #74025: <==commonly_known== 85381 (neg)
                    (when (and (Pa_at_b_4))
                          (Pa_at_b_3))

                    ; #77895: <==commonly_known== 18185 (pos)
                    (when (and (Ba_at_b_4))
                          (Ba_at_b_3))

                    ; #78697: origin
                    (when (and (at_b_3))
                          (at_b_2))

                    ; #79009: <==commonly_known== 85936 (pos)
                    (when (and (Bb_at_b_2))
                          (Bb_not_at_b_2))

                    ; #80594: origin
                    (when (and (at_b_2))
                          (at_b_1))

                    ; #81346: <==commonly_known== 78697 (neg)
                    (when (and (Ba_at_b_3))
                          (Pa_not_at_b_3))

                    ; #82211: <==commonly_known== 80594 (pos)
                    (when (and (Bb_at_b_2))
                          (Bb_at_b_1))

                    ; #83041: <==commonly_known== 18185 (neg)
                    (when (and (Ba_at_b_4))
                          (Pa_not_at_b_4))

                    ; #85834: <==commonly_known== 65747 (pos)
                    (when (and (Ba_at_b_3))
                          (Ba_not_at_b_3))

                    ; #85936: origin
                    (when (and (at_b_2))
                          (not_at_b_2))

                    ; #87796: <==commonly_known== 85381 (neg)
                    (when (and (Pb_at_b_4))
                          (Pb_at_b_3))

                    ; #89602: <==commonly_known== 68990 (neg)
                    (when (and (Ba_at_b_4))
                          (Pa_at_b_3))

                    ; #10014: <==negation-removal== 87796 (pos)
                    (when (and (Pb_at_b_4))
                          (not (Bb_not_at_b_3)))

                    ; #10022: <==uncertain_firing== 77895 (pos)
                    (when (and (not (Pa_not_at_b_4)))
                          (not (Pa_not_at_b_3)))

                    ; #10023: <==negation-removal== 51520 (pos)
                    (when (and (Bb_at_b_3))
                          (not (Pb_at_b_3)))

                    ; #11827: <==uncertain_firing== 72065 (pos)
                    (when (and (not (Pb_not_at_b_4)))
                          (not (Bb_at_b_4)))

                    ; #13715: <==negation-removal== 61763 (pos)
                    (when (and (Pb_at_b_3))
                          (not (Bb_at_b_3)))

                    ; #16922: <==uncertain_firing== 83041 (pos)
                    (when (and (not (Pa_not_at_b_4)))
                          (not (Ba_at_b_4)))

                    ; #18185: <==negation-removal== 68990 (pos)
                    (when (and (at_b_4))
                          (not (at_b_4)))

                    ; #21532: <==negation-removal== 26802 (pos)
                    (when (and (Bb_at_b_3))
                          (not (Bb_at_b_3)))

                    ; #24379: <==negation-removal== 85834 (pos)
                    (when (and (Ba_at_b_3))
                          (not (Pa_at_b_3)))

                    ; #24943: <==uncertain_firing== 70979 (pos)
                    (when (and (not (Bb_not_at_b_2)))
                          (not (Bb_not_at_b_1)))

                    ; #26038: <==negation-removal== 31610 (pos)
                    (when (and (Pa_at_b_2))
                          (not (Ba_at_b_2)))

                    ; #26802: <==negation-removal== 21532 (pos)
                    (when (and (Bb_at_b_3))
                          (not (Pb_not_at_b_2)))

                    ; #28433: <==uncertain_firing== 81346 (pos)
                    (when (and (not (Pa_not_at_b_3)))
                          (not (Ba_at_b_3)))

                    ; #29978: <==negation-removal== 14227 (pos)
                    (when (and (Pa_at_b_3))
                          (not (Ba_not_at_b_2)))

                    ; #31707: <==negation-removal== 18178 (pos)
                    (when (and (Pb_at_b_2))
                          (not (Bb_at_b_2)))

                    ; #32532: <==negation-removal== 35878 (pos)
                    (when (and (Ba_at_b_2))
                          (not (Pa_not_at_b_1)))

                    ; #33960: <==negation-removal== 79009 (pos)
                    (when (and (Bb_at_b_2))
                          (not (Pb_at_b_2)))

                    ; #35878: <==negation-removal== 32532 (pos)
                    (when (and (Ba_at_b_2))
                          (not (Ba_at_b_2)))

                    ; #37780: <==uncertain_firing== 82211 (pos)
                    (when (and (not (Pb_not_at_b_2)))
                          (not (Pb_not_at_b_1)))

                    ; #37798: <==negation-removal== 70573 (pos)
                    (when (and (Bb_at_b_4))
                          (not (Pb_at_b_4)))

                    ; #38283: <==negation-removal== 71271 (pos)
                    (when (and (Pb_at_b_3))
                          (not (Bb_not_at_b_2)))

                    ; #40663: <==uncertain_firing== 79009 (pos)
                    (when (and (not (Pb_not_at_b_2)))
                          (not (Pb_at_b_2)))

                    ; #40828: <==uncertain_firing== 74025 (pos)
                    (when (and (not (Ba_not_at_b_4)))
                          (not (Ba_not_at_b_3)))

                    ; #40976: <==uncertain_firing== 37798 (pos)
                    (when (and (not (Pb_not_at_b_4)))
                          (not (Bb_not_at_b_3)))

                    ; #41203: <==uncertain_firing== 45844 (pos)
                    (when (and (not (Pa_not_at_b_2)))
                          (not (Pa_at_b_2)))

                    ; #41367: <==uncertain_firing== 24379 (pos)
                    (when (and (not (Pa_not_at_b_3)))
                          (not (Ba_not_at_b_2)))

                    ; #41771: <==uncertain_firing== 31610 (pos)
                    (when (and (not (Ba_not_at_b_2)))
                          (not (Ba_at_b_2)))

                    ; #42844: <==negation-removal== 81346 (pos)
                    (when (and (Ba_at_b_3))
                          (not (Ba_at_b_3)))

                    ; #42966: <==negation-removal== 71366 (pos)
                    (when (and (Pb_at_b_4))
                          (not (Bb_at_b_4)))

                    ; #45844: <==negation-removal== 63883 (pos)
                    (when (and (Ba_at_b_2))
                          (not (Ba_not_at_b_1)))

                    ; #46522: <==negation-removal== 82211 (pos)
                    (when (and (Bb_at_b_2))
                          (not (Pb_not_at_b_1)))

                    ; #46645: <==uncertain_firing== 42844 (pos)
                    (when (and (not (Pa_not_at_b_3)))
                          (not (Pa_not_at_b_2)))

                    ; #48003: <==uncertain_firing== 65747 (pos)
                    (when (and (not (not_at_b_3)))
                          (not (at_b_3)))

                    ; #49099: <==uncertain_firing== 33573 (pos)
                    (when (and (not (Ba_not_at_b_4)))
                          (not (Ba_at_b_4)))

                    ; #50592: <==uncertain_firing== 80594 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (not_at_b_1)))

                    ; #51315: <==uncertain_firing== 68990 (pos)
                    (when (and (not (not_at_b_4)))
                          (not (at_b_4)))

                    ; #51520: <==negation-removal== 10023 (pos)
                    (when (and (Bb_at_b_3))
                          (not (Bb_not_at_b_2)))

                    ; #52095: <==uncertain_firing== 33960 (pos)
                    (when (and (not (Pb_not_at_b_2)))
                          (not (Bb_not_at_b_1)))

                    ; #53584: <==uncertain_firing== 56128 (pos)
                    (when (and (not (Pa_not_at_b_4)))
                          (not (Pa_at_b_4)))

                    ; #54996: <==uncertain_firing== 71271 (pos)
                    (when (and (not (Bb_not_at_b_3)))
                          (not (Bb_not_at_b_2)))

                    ; #55105: <==negation-removal== 74025 (pos)
                    (when (and (Pa_at_b_4))
                          (not (Ba_not_at_b_3)))

                    ; #55720: <==uncertain_firing== 46522 (pos)
                    (when (and (not (Pb_not_at_b_2)))
                          (not (Bb_at_b_2)))

                    ; #56090: <==negation-removal== 48741 (pos)
                    (when (and (Pa_at_b_2))
                          (not (Ba_not_at_b_1)))

                    ; #56128: <==negation-removal== 89602 (pos)
                    (when (and (Ba_at_b_4))
                          (not (Ba_not_at_b_3)))

                    ; #58651: <==uncertain_firing== 70573 (pos)
                    (when (and (not (Pb_not_at_b_4)))
                          (not (Pb_at_b_4)))

                    ; #59675: <==uncertain_firing== 63883 (pos)
                    (when (and (not (Pa_not_at_b_2)))
                          (not (Ba_not_at_b_1)))

                    ; #61516: <==uncertain_firing== 85936 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (at_b_2)))

                    ; #63883: <==negation-removal== 45844 (pos)
                    (when (and (Ba_at_b_2))
                          (not (Pa_at_b_2)))

                    ; #65188: <==uncertain_firing== 71366 (pos)
                    (when (and (not (Bb_not_at_b_4)))
                          (not (Bb_at_b_4)))

                    ; #65240: <==negation-removal== 33573 (pos)
                    (when (and (Pa_at_b_4))
                          (not (Ba_at_b_4)))

                    ; #65747: <==negation-removal== 78697 (pos)
                    (when (and (at_b_3))
                          (not (not_at_b_2)))

                    ; #66774: <==uncertain_firing== 61763 (pos)
                    (when (and (not (Bb_not_at_b_3)))
                          (not (Bb_at_b_3)))

                    ; #67107: <==uncertain_firing== 78697 (pos)
                    (when (and (not (not_at_b_3)))
                          (not (not_at_b_2)))

                    ; #68990: <==negation-removal== 18185 (pos)
                    (when (and (at_b_4))
                          (not (not_at_b_3)))

                    ; #69710: <==uncertain_firing== 14227 (pos)
                    (when (and (not (Ba_not_at_b_3)))
                          (not (Ba_not_at_b_2)))

                    ; #70573: <==negation-removal== 37798 (pos)
                    (when (and (Bb_at_b_4))
                          (not (Bb_not_at_b_3)))

                    ; #70869: <==uncertain_firing== 71377 (pos)
                    (when (and (not (Pb_not_at_b_4)))
                          (not (Pb_not_at_b_3)))

                    ; #71377: <==negation-removal== 72065 (pos)
                    (when (and (Bb_at_b_4))
                          (not (Bb_at_b_4)))

                    ; #71475: <==uncertain_firing== 55027 (pos)
                    (when (and (not (Ba_not_at_b_3)))
                          (not (Ba_at_b_3)))

                    ; #72065: <==negation-removal== 71377 (pos)
                    (when (and (Bb_at_b_4))
                          (not (Pb_not_at_b_3)))

                    ; #72932: <==uncertain_firing== 21532 (pos)
                    (when (and (not (Pb_not_at_b_3)))
                          (not (Pb_not_at_b_2)))

                    ; #73767: <==uncertain_firing== 26802 (pos)
                    (when (and (not (Pb_not_at_b_3)))
                          (not (Bb_at_b_3)))

                    ; #77744: <==uncertain_firing== 35878 (pos)
                    (when (and (not (Pa_not_at_b_2)))
                          (not (Pa_not_at_b_1)))

                    ; #77895: <==negation-removal== 83041 (pos)
                    (when (and (Ba_at_b_4))
                          (not (Ba_at_b_4)))

                    ; #78697: <==negation-removal== 65747 (pos)
                    (when (and (at_b_3))
                          (not (at_b_3)))

                    ; #79009: <==negation-removal== 33960 (pos)
                    (when (and (Bb_at_b_2))
                          (not (Bb_not_at_b_1)))

                    ; #79830: <==uncertain_firing== 48741 (pos)
                    (when (and (not (Ba_not_at_b_2)))
                          (not (Ba_not_at_b_1)))

                    ; #80155: <==negation-removal== 70979 (pos)
                    (when (and (Pb_at_b_2))
                          (not (Bb_not_at_b_1)))

                    ; #80594: <==negation-removal== 85936 (pos)
                    (when (and (at_b_2))
                          (not (at_b_2)))

                    ; #81346: <==negation-removal== 42844 (pos)
                    (when (and (Ba_at_b_3))
                          (not (Pa_not_at_b_2)))

                    ; #82211: <==negation-removal== 46522 (pos)
                    (when (and (Bb_at_b_2))
                          (not (Bb_at_b_2)))

                    ; #82478: <==uncertain_firing== 10023 (pos)
                    (when (and (not (Pb_not_at_b_3)))
                          (not (Bb_not_at_b_2)))

                    ; #82568: <==negation-removal== 55027 (pos)
                    (when (and (Pa_at_b_3))
                          (not (Ba_at_b_3)))

                    ; #83041: <==negation-removal== 77895 (pos)
                    (when (and (Ba_at_b_4))
                          (not (Pa_not_at_b_3)))

                    ; #85381: <==uncertain_firing== 18185 (pos)
                    (when (and (not (not_at_b_4)))
                          (not (not_at_b_3)))

                    ; #85834: <==negation-removal== 24379 (pos)
                    (when (and (Ba_at_b_3))
                          (not (Ba_not_at_b_2)))

                    ; #85936: <==negation-removal== 80594 (pos)
                    (when (and (at_b_2))
                          (not (not_at_b_1)))

                    ; #88766: <==uncertain_firing== 85834 (pos)
                    (when (and (not (Pa_not_at_b_3)))
                          (not (Pa_at_b_3)))

                    ; #89602: <==negation-removal== 56128 (pos)
                    (when (and (Ba_at_b_4))
                          (not (Pa_at_b_4)))

                    ; #91835: <==uncertain_firing== 89602 (pos)
                    (when (and (not (Pa_not_at_b_4)))
                          (not (Ba_not_at_b_3)))

                    ; #92048: <==uncertain_firing== 51520 (pos)
                    (when (and (not (Pb_not_at_b_3)))
                          (not (Pb_at_b_3)))

                    ; #94051: <==uncertain_firing== 87796 (pos)
                    (when (and (not (Bb_not_at_b_4)))
                          (not (Bb_not_at_b_3)))

                    ; #97409: <==uncertain_firing== 18178 (pos)
                    (when (and (not (Bb_not_at_b_2)))
                          (not (Bb_at_b_2)))

                    ; #98763: <==uncertain_firing== 32532 (pos)
                    (when (and (not (Pa_not_at_b_2)))
                          (not (Ba_at_b_2)))))

    (:action right_a
        :precondition (and (not_at_a_4))
        :effect (and
                    ; #11148: origin
                    (when (and (at_a_1))
                          (not_at_a_1))

                    ; #12946: <==commonly_known== 53310 (neg)
                    (when (and (Bb_at_a_3))
                          (Pb_at_a_4))

                    ; #17900: <==commonly_known== 11148 (pos)
                    (when (and (Bb_at_a_1))
                          (Bb_not_at_a_1))

                    ; #18479: <==commonly_known== 41460 (neg)
                    (when (and (Bb_at_a_3))
                          (Pb_not_at_a_3))

                    ; #18745: <==commonly_known== 10289 (neg)
                    (when (and (Pb_at_a_2))
                          (Pb_at_a_3))

                    ; #21057: <==commonly_known== 61663 (neg)
                    (when (and (Pb_at_a_2))
                          (Pb_not_at_a_2))

                    ; #21198: <==commonly_known== 11148 (neg)
                    (when (and (Bb_at_a_1))
                          (Pb_at_a_2))

                    ; #21902: <==commonly_known== 11148 (pos)
                    (when (and (Ba_at_a_1))
                          (Ba_not_at_a_1))

                    ; #21932: <==commonly_known== 16185 (neg)
                    (when (and (Pb_at_a_1))
                          (Pb_not_at_a_1))

                    ; #25146: <==commonly_known== 16185 (neg)
                    (when (and (Pa_at_a_1))
                          (Pa_not_at_a_1))

                    ; #25556: <==commonly_known== 87141 (neg)
                    (when (and (Ba_at_a_2))
                          (Pa_not_at_a_2))

                    ; #27130: <==commonly_known== 73442 (neg)
                    (when (and (Pa_at_a_3))
                          (Pa_at_a_4))

                    ; #30193: <==commonly_known== 11148 (neg)
                    (when (and (Ba_at_a_1))
                          (Pa_at_a_2))

                    ; #30366: <==commonly_known== 41460 (pos)
                    (when (and (Bb_at_a_3))
                          (Bb_at_a_4))

                    ; #37931: origin
                    (when (and (at_a_1))
                          (at_a_2))

                    ; #39573: <==commonly_known== 74587 (neg)
                    (when (and (Ba_at_a_2))
                          (Pa_at_a_3))

                    ; #40116: <==commonly_known== 17595 (neg)
                    (when (and (Pb_at_a_1))
                          (Pb_at_a_2))

                    ; #40143: <==commonly_known== 37931 (pos)
                    (when (and (Bb_at_a_1))
                          (Bb_at_a_2))

                    ; #41460: origin
                    (when (and (at_a_3))
                          (at_a_4))

                    ; #42899: <==commonly_known== 87141 (pos)
                    (when (and (Bb_at_a_2))
                          (Bb_at_a_3))

                    ; #47376: <==commonly_known== 53310 (pos)
                    (when (and (Ba_at_a_3))
                          (Ba_not_at_a_3))

                    ; #50336: <==commonly_known== 73442 (neg)
                    (when (and (Pb_at_a_3))
                          (Pb_at_a_4))

                    ; #50747: <==commonly_known== 41460 (pos)
                    (when (and (Ba_at_a_3))
                          (Ba_at_a_4))

                    ; #53310: origin
                    (when (and (at_a_3))
                          (not_at_a_3))

                    ; #55051: <==commonly_known== 37931 (neg)
                    (when (and (Ba_at_a_1))
                          (Pa_not_at_a_1))

                    ; #55059: <==commonly_known== 61663 (neg)
                    (when (and (Pa_at_a_2))
                          (Pa_not_at_a_2))

                    ; #56621: <==commonly_known== 41460 (neg)
                    (when (and (Ba_at_a_3))
                          (Pa_not_at_a_3))

                    ; #57307: <==commonly_known== 73283 (neg)
                    (when (and (Pb_at_a_3))
                          (Pb_not_at_a_3))

                    ; #62485: <==commonly_known== 17595 (neg)
                    (when (and (Pa_at_a_1))
                          (Pa_at_a_2))

                    ; #62836: <==commonly_known== 87141 (pos)
                    (when (and (Ba_at_a_2))
                          (Ba_at_a_3))

                    ; #62960: <==commonly_known== 37931 (pos)
                    (when (and (Ba_at_a_1))
                          (Ba_at_a_2))

                    ; #64976: <==commonly_known== 10289 (neg)
                    (when (and (Pa_at_a_2))
                          (Pa_at_a_3))

                    ; #66814: <==commonly_known== 73283 (neg)
                    (when (and (Pa_at_a_3))
                          (Pa_not_at_a_3))

                    ; #68352: <==commonly_known== 74587 (pos)
                    (when (and (Ba_at_a_2))
                          (Ba_not_at_a_2))

                    ; #70378: <==commonly_known== 53310 (neg)
                    (when (and (Ba_at_a_3))
                          (Pa_at_a_4))

                    ; #70429: <==commonly_known== 37931 (neg)
                    (when (and (Bb_at_a_1))
                          (Pb_not_at_a_1))

                    ; #74587: origin
                    (when (and (at_a_2))
                          (not_at_a_2))

                    ; #83021: <==commonly_known== 87141 (neg)
                    (when (and (Bb_at_a_2))
                          (Pb_not_at_a_2))

                    ; #84875: <==commonly_known== 53310 (pos)
                    (when (and (Bb_at_a_3))
                          (Bb_not_at_a_3))

                    ; #85085: <==commonly_known== 74587 (pos)
                    (when (and (Bb_at_a_2))
                          (Bb_not_at_a_2))

                    ; #87141: origin
                    (when (and (at_a_2))
                          (at_a_3))

                    ; #87741: <==commonly_known== 74587 (neg)
                    (when (and (Bb_at_a_2))
                          (Pb_at_a_3))

                    ; #10289: <==uncertain_firing== 87141 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (not_at_a_3)))

                    ; #11148: <==negation-removal== 37931 (pos)
                    (when (and (at_a_1))
                          (not (not_at_a_2)))

                    ; #11460: <==negation-removal== 57307 (pos)
                    (when (and (Pb_at_a_3))
                          (not (Bb_at_a_3)))

                    ; #12946: <==negation-removal== 84875 (pos)
                    (when (and (Bb_at_a_3))
                          (not (Pb_at_a_3)))

                    ; #13007: <==negation-removal== 64976 (pos)
                    (when (and (Pa_at_a_2))
                          (not (Ba_not_at_a_3)))

                    ; #13800: <==uncertain_firing== 18479 (pos)
                    (when (and (not (Pb_not_at_a_3)))
                          (not (Bb_at_a_3)))

                    ; #16185: <==uncertain_firing== 11148 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (at_a_1)))

                    ; #17595: <==uncertain_firing== 37931 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (not_at_a_2)))

                    ; #17900: <==negation-removal== 21198 (pos)
                    (when (and (Bb_at_a_1))
                          (not (Bb_not_at_a_2)))

                    ; #18479: <==negation-removal== 30366 (pos)
                    (when (and (Bb_at_a_3))
                          (not (Pb_not_at_a_4)))

                    ; #19998: <==uncertain_firing== 83021 (pos)
                    (when (and (not (Pb_not_at_a_2)))
                          (not (Bb_at_a_2)))

                    ; #21002: <==uncertain_firing== 50336 (pos)
                    (when (and (not (Bb_not_at_a_3)))
                          (not (Bb_not_at_a_4)))

                    ; #21198: <==negation-removal== 17900 (pos)
                    (when (and (Bb_at_a_1))
                          (not (Pb_at_a_1)))

                    ; #21855: <==uncertain_firing== 55059 (pos)
                    (when (and (not (Ba_not_at_a_2)))
                          (not (Ba_at_a_2)))

                    ; #21902: <==negation-removal== 30193 (pos)
                    (when (and (Ba_at_a_1))
                          (not (Ba_not_at_a_2)))

                    ; #21925: <==uncertain_firing== 40143 (pos)
                    (when (and (not (Pb_not_at_a_1)))
                          (not (Pb_not_at_a_2)))

                    ; #22258: <==negation-removal== 21932 (pos)
                    (when (and (Pb_at_a_1))
                          (not (Bb_at_a_1)))

                    ; #22502: <==uncertain_firing== 25556 (pos)
                    (when (and (not (Pa_not_at_a_2)))
                          (not (Ba_at_a_2)))

                    ; #24863: <==uncertain_firing== 70378 (pos)
                    (when (and (not (Pa_not_at_a_3)))
                          (not (Ba_not_at_a_4)))

                    ; #25556: <==negation-removal== 62836 (pos)
                    (when (and (Ba_at_a_2))
                          (not (Pa_not_at_a_3)))

                    ; #25655: <==uncertain_firing== 21902 (pos)
                    (when (and (not (Pa_not_at_a_1)))
                          (not (Pa_at_a_1)))

                    ; #25885: <==negation-removal== 27130 (pos)
                    (when (and (Pa_at_a_3))
                          (not (Ba_not_at_a_4)))

                    ; #28945: <==uncertain_firing== 66814 (pos)
                    (when (and (not (Ba_not_at_a_3)))
                          (not (Ba_at_a_3)))

                    ; #30193: <==negation-removal== 21902 (pos)
                    (when (and (Ba_at_a_1))
                          (not (Pa_at_a_1)))

                    ; #30366: <==negation-removal== 18479 (pos)
                    (when (and (Bb_at_a_3))
                          (not (Bb_at_a_3)))

                    ; #36257: <==uncertain_firing== 68352 (pos)
                    (when (and (not (Pa_not_at_a_2)))
                          (not (Pa_at_a_2)))

                    ; #36308: <==uncertain_firing== 62485 (pos)
                    (when (and (not (Ba_not_at_a_1)))
                          (not (Ba_not_at_a_2)))

                    ; #37931: <==negation-removal== 11148 (pos)
                    (when (and (at_a_1))
                          (not (at_a_1)))

                    ; #38633: <==uncertain_firing== 47376 (pos)
                    (when (and (not (Pa_not_at_a_3)))
                          (not (Pa_at_a_3)))

                    ; #39573: <==negation-removal== 68352 (pos)
                    (when (and (Ba_at_a_2))
                          (not (Pa_at_a_2)))

                    ; #40143: <==negation-removal== 70429 (pos)
                    (when (and (Bb_at_a_1))
                          (not (Bb_at_a_1)))

                    ; #41460: <==negation-removal== 53310 (pos)
                    (when (and (at_a_3))
                          (not (at_a_3)))

                    ; #41529: <==uncertain_firing== 62836 (pos)
                    (when (and (not (Pa_not_at_a_2)))
                          (not (Pa_not_at_a_3)))

                    ; #41840: <==uncertain_firing== 21198 (pos)
                    (when (and (not (Pb_not_at_a_1)))
                          (not (Bb_not_at_a_2)))

                    ; #42899: <==negation-removal== 83021 (pos)
                    (when (and (Bb_at_a_2))
                          (not (Bb_at_a_2)))

                    ; #45184: <==uncertain_firing== 39573 (pos)
                    (when (and (not (Pa_not_at_a_2)))
                          (not (Ba_not_at_a_3)))

                    ; #46154: <==uncertain_firing== 40116 (pos)
                    (when (and (not (Bb_not_at_a_1)))
                          (not (Bb_not_at_a_2)))

                    ; #47093: <==uncertain_firing== 70429 (pos)
                    (when (and (not (Pb_not_at_a_1)))
                          (not (Bb_at_a_1)))

                    ; #47376: <==negation-removal== 70378 (pos)
                    (when (and (Ba_at_a_3))
                          (not (Ba_not_at_a_4)))

                    ; #48477: <==uncertain_firing== 62960 (pos)
                    (when (and (not (Pa_not_at_a_1)))
                          (not (Pa_not_at_a_2)))

                    ; #48487: <==uncertain_firing== 85085 (pos)
                    (when (and (not (Pb_not_at_a_2)))
                          (not (Pb_at_a_2)))

                    ; #49296: <==uncertain_firing== 55051 (pos)
                    (when (and (not (Pa_not_at_a_1)))
                          (not (Ba_at_a_1)))

                    ; #50747: <==negation-removal== 56621 (pos)
                    (when (and (Ba_at_a_3))
                          (not (Ba_at_a_3)))

                    ; #51740: <==uncertain_firing== 56621 (pos)
                    (when (and (not (Pa_not_at_a_3)))
                          (not (Ba_at_a_3)))

                    ; #51890: <==uncertain_firing== 25146 (pos)
                    (when (and (not (Ba_not_at_a_1)))
                          (not (Ba_at_a_1)))

                    ; #53310: <==negation-removal== 41460 (pos)
                    (when (and (at_a_3))
                          (not (not_at_a_4)))

                    ; #55051: <==negation-removal== 62960 (pos)
                    (when (and (Ba_at_a_1))
                          (not (Pa_not_at_a_2)))

                    ; #55695: <==uncertain_firing== 50747 (pos)
                    (when (and (not (Pa_not_at_a_3)))
                          (not (Pa_not_at_a_4)))

                    ; #56008: <==uncertain_firing== 21932 (pos)
                    (when (and (not (Bb_not_at_a_1)))
                          (not (Bb_at_a_1)))

                    ; #56621: <==negation-removal== 50747 (pos)
                    (when (and (Ba_at_a_3))
                          (not (Pa_not_at_a_4)))

                    ; #57713: <==uncertain_firing== 30193 (pos)
                    (when (and (not (Pa_not_at_a_1)))
                          (not (Ba_not_at_a_2)))

                    ; #58859: <==uncertain_firing== 21057 (pos)
                    (when (and (not (Bb_not_at_a_2)))
                          (not (Bb_at_a_2)))

                    ; #59654: <==uncertain_firing== 64976 (pos)
                    (when (and (not (Ba_not_at_a_2)))
                          (not (Ba_not_at_a_3)))

                    ; #61663: <==uncertain_firing== 74587 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (at_a_2)))

                    ; #62787: <==negation-removal== 21057 (pos)
                    (when (and (Pb_at_a_2))
                          (not (Bb_at_a_2)))

                    ; #62836: <==negation-removal== 25556 (pos)
                    (when (and (Ba_at_a_2))
                          (not (Ba_at_a_2)))

                    ; #62960: <==negation-removal== 55051 (pos)
                    (when (and (Ba_at_a_1))
                          (not (Ba_at_a_1)))

                    ; #64643: <==negation-removal== 25146 (pos)
                    (when (and (Pa_at_a_1))
                          (not (Ba_at_a_1)))

                    ; #68352: <==negation-removal== 39573 (pos)
                    (when (and (Ba_at_a_2))
                          (not (Ba_not_at_a_3)))

                    ; #70378: <==negation-removal== 47376 (pos)
                    (when (and (Ba_at_a_3))
                          (not (Pa_at_a_3)))

                    ; #70429: <==negation-removal== 40143 (pos)
                    (when (and (Bb_at_a_1))
                          (not (Pb_not_at_a_2)))

                    ; #72232: <==uncertain_firing== 30366 (pos)
                    (when (and (not (Pb_not_at_a_3)))
                          (not (Pb_not_at_a_4)))

                    ; #72834: <==negation-removal== 40116 (pos)
                    (when (and (Pb_at_a_1))
                          (not (Bb_not_at_a_2)))

                    ; #73283: <==uncertain_firing== 53310 (pos)
                    (when (and (not (not_at_a_3)))
                          (not (at_a_3)))

                    ; #73442: <==uncertain_firing== 41460 (pos)
                    (when (and (not (not_at_a_3)))
                          (not (not_at_a_4)))

                    ; #73632: <==uncertain_firing== 42899 (pos)
                    (when (and (not (Pb_not_at_a_2)))
                          (not (Pb_not_at_a_3)))

                    ; #74579: <==uncertain_firing== 17900 (pos)
                    (when (and (not (Pb_not_at_a_1)))
                          (not (Pb_at_a_1)))

                    ; #74587: <==negation-removal== 87141 (pos)
                    (when (and (at_a_2))
                          (not (not_at_a_3)))

                    ; #74963: <==uncertain_firing== 18745 (pos)
                    (when (and (not (Bb_not_at_a_2)))
                          (not (Bb_not_at_a_3)))

                    ; #76242: <==uncertain_firing== 84875 (pos)
                    (when (and (not (Pb_not_at_a_3)))
                          (not (Pb_at_a_3)))

                    ; #76580: <==uncertain_firing== 87741 (pos)
                    (when (and (not (Pb_not_at_a_2)))
                          (not (Bb_not_at_a_3)))

                    ; #76691: <==negation-removal== 55059 (pos)
                    (when (and (Pa_at_a_2))
                          (not (Ba_at_a_2)))

                    ; #79068: <==negation-removal== 18745 (pos)
                    (when (and (Pb_at_a_2))
                          (not (Bb_not_at_a_3)))

                    ; #82237: <==uncertain_firing== 57307 (pos)
                    (when (and (not (Bb_not_at_a_3)))
                          (not (Bb_at_a_3)))

                    ; #83021: <==negation-removal== 42899 (pos)
                    (when (and (Bb_at_a_2))
                          (not (Pb_not_at_a_3)))

                    ; #83555: <==uncertain_firing== 12946 (pos)
                    (when (and (not (Pb_not_at_a_3)))
                          (not (Bb_not_at_a_4)))

                    ; #84875: <==negation-removal== 12946 (pos)
                    (when (and (Bb_at_a_3))
                          (not (Bb_not_at_a_4)))

                    ; #85085: <==negation-removal== 87741 (pos)
                    (when (and (Bb_at_a_2))
                          (not (Bb_not_at_a_3)))

                    ; #85701: <==negation-removal== 50336 (pos)
                    (when (and (Pb_at_a_3))
                          (not (Bb_not_at_a_4)))

                    ; #86190: <==uncertain_firing== 27130 (pos)
                    (when (and (not (Ba_not_at_a_3)))
                          (not (Ba_not_at_a_4)))

                    ; #87141: <==negation-removal== 74587 (pos)
                    (when (and (at_a_2))
                          (not (at_a_2)))

                    ; #87741: <==negation-removal== 85085 (pos)
                    (when (and (Bb_at_a_2))
                          (not (Pb_at_a_2)))

                    ; #90239: <==negation-removal== 62485 (pos)
                    (when (and (Pa_at_a_1))
                          (not (Ba_not_at_a_2)))

                    ; #91130: <==negation-removal== 66814 (pos)
                    (when (and (Pa_at_a_3))
                          (not (Ba_at_a_3)))))

    (:action right_b
        :precondition (and (not_at_b_4))
        :effect (and
                    ; #10023: <==commonly_known== 65747 (neg)
                    (when (and (Bb_at_b_3))
                          (Pb_at_b_4))

                    ; #14227: <==commonly_known== 67107 (neg)
                    (when (and (Pa_at_b_3))
                          (Pa_at_b_4))

                    ; #15674: <==commonly_known== 56280 (neg)
                    (when (and (Ba_at_b_1))
                          (Pa_at_b_2))

                    ; #18178: <==commonly_known== 61516 (neg)
                    (when (and (Pb_at_b_2))
                          (Pb_not_at_b_2))

                    ; #18460: <==commonly_known== 62457 (neg)
                    (when (and (Bb_at_b_1))
                          (Pb_not_at_b_1))

                    ; #19405: <==commonly_known== 62457 (neg)
                    (when (and (Ba_at_b_1))
                          (Pa_not_at_b_1))

                    ; #21532: <==commonly_known== 78697 (pos)
                    (when (and (Bb_at_b_3))
                          (Bb_at_b_4))

                    ; #24379: <==commonly_known== 65747 (neg)
                    (when (and (Ba_at_b_3))
                          (Pa_at_b_4))

                    ; #24591: <==commonly_known== 62457 (pos)
                    (when (and (Bb_at_b_1))
                          (Bb_at_b_2))

                    ; #26802: <==commonly_known== 78697 (neg)
                    (when (and (Bb_at_b_3))
                          (Pb_not_at_b_3))

                    ; #31610: <==commonly_known== 61516 (neg)
                    (when (and (Pa_at_b_2))
                          (Pa_not_at_b_2))

                    ; #32532: <==commonly_known== 80594 (neg)
                    (when (and (Ba_at_b_2))
                          (Pa_not_at_b_2))

                    ; #33960: <==commonly_known== 85936 (neg)
                    (when (and (Bb_at_b_2))
                          (Pb_at_b_3))

                    ; #35878: <==commonly_known== 80594 (pos)
                    (when (and (Ba_at_b_2))
                          (Ba_at_b_3))

                    ; #42844: <==commonly_known== 78697 (pos)
                    (when (and (Ba_at_b_3))
                          (Ba_at_b_4))

                    ; #45844: <==commonly_known== 85936 (pos)
                    (when (and (Ba_at_b_2))
                          (Ba_not_at_b_2))

                    ; #46522: <==commonly_known== 80594 (neg)
                    (when (and (Bb_at_b_2))
                          (Pb_not_at_b_2))

                    ; #48741: <==commonly_known== 50592 (neg)
                    (when (and (Pa_at_b_2))
                          (Pa_at_b_3))

                    ; #51520: <==commonly_known== 65747 (pos)
                    (when (and (Bb_at_b_3))
                          (Bb_not_at_b_3))

                    ; #53619: <==commonly_known== 59708 (neg)
                    (when (and (Pa_at_b_1))
                          (Pa_at_b_2))

                    ; #55027: <==commonly_known== 48003 (neg)
                    (when (and (Pa_at_b_3))
                          (Pa_not_at_b_3))

                    ; #56280: origin
                    (when (and (at_b_1))
                          (not_at_b_1))

                    ; #59248: <==commonly_known== 59708 (neg)
                    (when (and (Pb_at_b_1))
                          (Pb_at_b_2))

                    ; #61763: <==commonly_known== 48003 (neg)
                    (when (and (Pb_at_b_3))
                          (Pb_not_at_b_3))

                    ; #62457: origin
                    (when (and (at_b_1))
                          (at_b_2))

                    ; #63883: <==commonly_known== 85936 (neg)
                    (when (and (Ba_at_b_2))
                          (Pa_at_b_3))

                    ; #65747: origin
                    (when (and (at_b_3))
                          (not_at_b_3))

                    ; #67536: <==commonly_known== 56280 (pos)
                    (when (and (Bb_at_b_1))
                          (Bb_not_at_b_1))

                    ; #70979: <==commonly_known== 50592 (neg)
                    (when (and (Pb_at_b_2))
                          (Pb_at_b_3))

                    ; #71271: <==commonly_known== 67107 (neg)
                    (when (and (Pb_at_b_3))
                          (Pb_at_b_4))

                    ; #78697: origin
                    (when (and (at_b_3))
                          (at_b_4))

                    ; #79009: <==commonly_known== 85936 (pos)
                    (when (and (Bb_at_b_2))
                          (Bb_not_at_b_2))

                    ; #79523: <==commonly_known== 56280 (neg)
                    (when (and (Bb_at_b_1))
                          (Pb_at_b_2))

                    ; #80240: <==commonly_known== 26649 (neg)
                    (when (and (Pb_at_b_1))
                          (Pb_not_at_b_1))

                    ; #80594: origin
                    (when (and (at_b_2))
                          (at_b_3))

                    ; #81346: <==commonly_known== 78697 (neg)
                    (when (and (Ba_at_b_3))
                          (Pa_not_at_b_3))

                    ; #82211: <==commonly_known== 80594 (pos)
                    (when (and (Bb_at_b_2))
                          (Bb_at_b_3))

                    ; #85834: <==commonly_known== 65747 (pos)
                    (when (and (Ba_at_b_3))
                          (Ba_not_at_b_3))

                    ; #85936: origin
                    (when (and (at_b_2))
                          (not_at_b_2))

                    ; #89773: <==commonly_known== 56280 (pos)
                    (when (and (Ba_at_b_1))
                          (Ba_not_at_b_1))

                    ; #90480: <==commonly_known== 26649 (neg)
                    (when (and (Pa_at_b_1))
                          (Pa_not_at_b_1))

                    ; #90633: <==commonly_known== 62457 (pos)
                    (when (and (Ba_at_b_1))
                          (Ba_at_b_2))

                    ; #10023: <==negation-removal== 51520 (pos)
                    (when (and (Bb_at_b_3))
                          (not (Pb_at_b_3)))

                    ; #11875: <==uncertain_firing== 15674 (pos)
                    (when (and (not (Pa_not_at_b_1)))
                          (not (Ba_not_at_b_2)))

                    ; #13715: <==negation-removal== 61763 (pos)
                    (when (and (Pb_at_b_3))
                          (not (Bb_at_b_3)))

                    ; #15674: <==negation-removal== 89773 (pos)
                    (when (and (Ba_at_b_1))
                          (not (Pa_at_b_1)))

                    ; #18460: <==negation-removal== 24591 (pos)
                    (when (and (Bb_at_b_1))
                          (not (Pb_not_at_b_2)))

                    ; #19405: <==negation-removal== 90633 (pos)
                    (when (and (Ba_at_b_1))
                          (not (Pa_not_at_b_2)))

                    ; #19706: <==uncertain_firing== 59248 (pos)
                    (when (and (not (Bb_not_at_b_1)))
                          (not (Bb_not_at_b_2)))

                    ; #21532: <==negation-removal== 26802 (pos)
                    (when (and (Bb_at_b_3))
                          (not (Bb_at_b_3)))

                    ; #23800: <==negation-removal== 90480 (pos)
                    (when (and (Pa_at_b_1))
                          (not (Ba_at_b_1)))

                    ; #24379: <==negation-removal== 85834 (pos)
                    (when (and (Ba_at_b_3))
                          (not (Pa_at_b_3)))

                    ; #24591: <==negation-removal== 18460 (pos)
                    (when (and (Bb_at_b_1))
                          (not (Bb_at_b_1)))

                    ; #24943: <==uncertain_firing== 70979 (pos)
                    (when (and (not (Bb_not_at_b_2)))
                          (not (Bb_not_at_b_3)))

                    ; #26038: <==negation-removal== 31610 (pos)
                    (when (and (Pa_at_b_2))
                          (not (Ba_at_b_2)))

                    ; #26308: <==uncertain_firing== 90480 (pos)
                    (when (and (not (Ba_not_at_b_1)))
                          (not (Ba_at_b_1)))

                    ; #26649: <==uncertain_firing== 56280 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (at_b_1)))

                    ; #26802: <==negation-removal== 21532 (pos)
                    (when (and (Bb_at_b_3))
                          (not (Pb_not_at_b_4)))

                    ; #28433: <==uncertain_firing== 81346 (pos)
                    (when (and (not (Pa_not_at_b_3)))
                          (not (Ba_at_b_3)))

                    ; #29978: <==negation-removal== 14227 (pos)
                    (when (and (Pa_at_b_3))
                          (not (Ba_not_at_b_4)))

                    ; #31632: <==uncertain_firing== 79523 (pos)
                    (when (and (not (Pb_not_at_b_1)))
                          (not (Bb_not_at_b_2)))

                    ; #31707: <==negation-removal== 18178 (pos)
                    (when (and (Pb_at_b_2))
                          (not (Bb_at_b_2)))

                    ; #32532: <==negation-removal== 35878 (pos)
                    (when (and (Ba_at_b_2))
                          (not (Pa_not_at_b_3)))

                    ; #33960: <==negation-removal== 79009 (pos)
                    (when (and (Bb_at_b_2))
                          (not (Pb_at_b_2)))

                    ; #35878: <==negation-removal== 32532 (pos)
                    (when (and (Ba_at_b_2))
                          (not (Ba_at_b_2)))

                    ; #37780: <==uncertain_firing== 82211 (pos)
                    (when (and (not (Pb_not_at_b_2)))
                          (not (Pb_not_at_b_3)))

                    ; #38283: <==negation-removal== 71271 (pos)
                    (when (and (Pb_at_b_3))
                          (not (Bb_not_at_b_4)))

                    ; #38735: <==uncertain_firing== 89773 (pos)
                    (when (and (not (Pa_not_at_b_1)))
                          (not (Pa_at_b_1)))

                    ; #40663: <==uncertain_firing== 79009 (pos)
                    (when (and (not (Pb_not_at_b_2)))
                          (not (Pb_at_b_2)))

                    ; #40695: <==negation-removal== 53619 (pos)
                    (when (and (Pa_at_b_1))
                          (not (Ba_not_at_b_2)))

                    ; #41203: <==uncertain_firing== 45844 (pos)
                    (when (and (not (Pa_not_at_b_2)))
                          (not (Pa_at_b_2)))

                    ; #41367: <==uncertain_firing== 24379 (pos)
                    (when (and (not (Pa_not_at_b_3)))
                          (not (Ba_not_at_b_4)))

                    ; #41771: <==uncertain_firing== 31610 (pos)
                    (when (and (not (Ba_not_at_b_2)))
                          (not (Ba_at_b_2)))

                    ; #42096: <==uncertain_firing== 90633 (pos)
                    (when (and (not (Pa_not_at_b_1)))
                          (not (Pa_not_at_b_2)))

                    ; #42844: <==negation-removal== 81346 (pos)
                    (when (and (Ba_at_b_3))
                          (not (Ba_at_b_3)))

                    ; #45844: <==negation-removal== 63883 (pos)
                    (when (and (Ba_at_b_2))
                          (not (Ba_not_at_b_3)))

                    ; #46522: <==negation-removal== 82211 (pos)
                    (when (and (Bb_at_b_2))
                          (not (Pb_not_at_b_3)))

                    ; #46645: <==uncertain_firing== 42844 (pos)
                    (when (and (not (Pa_not_at_b_3)))
                          (not (Pa_not_at_b_4)))

                    ; #48003: <==uncertain_firing== 65747 (pos)
                    (when (and (not (not_at_b_3)))
                          (not (at_b_3)))

                    ; #48292: <==uncertain_firing== 18460 (pos)
                    (when (and (not (Pb_not_at_b_1)))
                          (not (Bb_at_b_1)))

                    ; #49820: <==uncertain_firing== 80240 (pos)
                    (when (and (not (Bb_not_at_b_1)))
                          (not (Bb_at_b_1)))

                    ; #50592: <==uncertain_firing== 80594 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (not_at_b_3)))

                    ; #51520: <==negation-removal== 10023 (pos)
                    (when (and (Bb_at_b_3))
                          (not (Bb_not_at_b_4)))

                    ; #52095: <==uncertain_firing== 33960 (pos)
                    (when (and (not (Pb_not_at_b_2)))
                          (not (Bb_not_at_b_3)))

                    ; #54996: <==uncertain_firing== 71271 (pos)
                    (when (and (not (Bb_not_at_b_3)))
                          (not (Bb_not_at_b_4)))

                    ; #55720: <==uncertain_firing== 46522 (pos)
                    (when (and (not (Pb_not_at_b_2)))
                          (not (Bb_at_b_2)))

                    ; #56090: <==negation-removal== 48741 (pos)
                    (when (and (Pa_at_b_2))
                          (not (Ba_not_at_b_3)))

                    ; #56280: <==negation-removal== 62457 (pos)
                    (when (and (at_b_1))
                          (not (not_at_b_2)))

                    ; #56755: <==negation-removal== 59248 (pos)
                    (when (and (Pb_at_b_1))
                          (not (Bb_not_at_b_2)))

                    ; #59541: <==uncertain_firing== 53619 (pos)
                    (when (and (not (Ba_not_at_b_1)))
                          (not (Ba_not_at_b_2)))

                    ; #59675: <==uncertain_firing== 63883 (pos)
                    (when (and (not (Pa_not_at_b_2)))
                          (not (Ba_not_at_b_3)))

                    ; #59708: <==uncertain_firing== 62457 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (not_at_b_2)))

                    ; #61516: <==uncertain_firing== 85936 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (at_b_2)))

                    ; #62457: <==negation-removal== 56280 (pos)
                    (when (and (at_b_1))
                          (not (at_b_1)))

                    ; #62646: <==uncertain_firing== 67536 (pos)
                    (when (and (not (Pb_not_at_b_1)))
                          (not (Pb_at_b_1)))

                    ; #63445: <==uncertain_firing== 24591 (pos)
                    (when (and (not (Pb_not_at_b_1)))
                          (not (Pb_not_at_b_2)))

                    ; #63773: <==uncertain_firing== 19405 (pos)
                    (when (and (not (Pa_not_at_b_1)))
                          (not (Ba_at_b_1)))

                    ; #63883: <==negation-removal== 45844 (pos)
                    (when (and (Ba_at_b_2))
                          (not (Pa_at_b_2)))

                    ; #65747: <==negation-removal== 78697 (pos)
                    (when (and (at_b_3))
                          (not (not_at_b_4)))

                    ; #66774: <==uncertain_firing== 61763 (pos)
                    (when (and (not (Bb_not_at_b_3)))
                          (not (Bb_at_b_3)))

                    ; #67107: <==uncertain_firing== 78697 (pos)
                    (when (and (not (not_at_b_3)))
                          (not (not_at_b_4)))

                    ; #67536: <==negation-removal== 79523 (pos)
                    (when (and (Bb_at_b_1))
                          (not (Bb_not_at_b_2)))

                    ; #69710: <==uncertain_firing== 14227 (pos)
                    (when (and (not (Ba_not_at_b_3)))
                          (not (Ba_not_at_b_4)))

                    ; #71475: <==uncertain_firing== 55027 (pos)
                    (when (and (not (Ba_not_at_b_3)))
                          (not (Ba_at_b_3)))

                    ; #72932: <==uncertain_firing== 21532 (pos)
                    (when (and (not (Pb_not_at_b_3)))
                          (not (Pb_not_at_b_4)))

                    ; #73767: <==uncertain_firing== 26802 (pos)
                    (when (and (not (Pb_not_at_b_3)))
                          (not (Bb_at_b_3)))

                    ; #77744: <==uncertain_firing== 35878 (pos)
                    (when (and (not (Pa_not_at_b_2)))
                          (not (Pa_not_at_b_3)))

                    ; #78697: <==negation-removal== 65747 (pos)
                    (when (and (at_b_3))
                          (not (at_b_3)))

                    ; #79009: <==negation-removal== 33960 (pos)
                    (when (and (Bb_at_b_2))
                          (not (Bb_not_at_b_3)))

                    ; #79523: <==negation-removal== 67536 (pos)
                    (when (and (Bb_at_b_1))
                          (not (Pb_at_b_1)))

                    ; #79830: <==uncertain_firing== 48741 (pos)
                    (when (and (not (Ba_not_at_b_2)))
                          (not (Ba_not_at_b_3)))

                    ; #80155: <==negation-removal== 70979 (pos)
                    (when (and (Pb_at_b_2))
                          (not (Bb_not_at_b_3)))

                    ; #80594: <==negation-removal== 85936 (pos)
                    (when (and (at_b_2))
                          (not (at_b_2)))

                    ; #81346: <==negation-removal== 42844 (pos)
                    (when (and (Ba_at_b_3))
                          (not (Pa_not_at_b_4)))

                    ; #82211: <==negation-removal== 46522 (pos)
                    (when (and (Bb_at_b_2))
                          (not (Bb_at_b_2)))

                    ; #82478: <==uncertain_firing== 10023 (pos)
                    (when (and (not (Pb_not_at_b_3)))
                          (not (Bb_not_at_b_4)))

                    ; #82568: <==negation-removal== 55027 (pos)
                    (when (and (Pa_at_b_3))
                          (not (Ba_at_b_3)))

                    ; #84567: <==negation-removal== 80240 (pos)
                    (when (and (Pb_at_b_1))
                          (not (Bb_at_b_1)))

                    ; #85834: <==negation-removal== 24379 (pos)
                    (when (and (Ba_at_b_3))
                          (not (Ba_not_at_b_4)))

                    ; #85936: <==negation-removal== 80594 (pos)
                    (when (and (at_b_2))
                          (not (not_at_b_3)))

                    ; #88766: <==uncertain_firing== 85834 (pos)
                    (when (and (not (Pa_not_at_b_3)))
                          (not (Pa_at_b_3)))

                    ; #89773: <==negation-removal== 15674 (pos)
                    (when (and (Ba_at_b_1))
                          (not (Ba_not_at_b_2)))

                    ; #90633: <==negation-removal== 19405 (pos)
                    (when (and (Ba_at_b_1))
                          (not (Ba_at_b_1)))

                    ; #92048: <==uncertain_firing== 51520 (pos)
                    (when (and (not (Pb_not_at_b_3)))
                          (not (Pb_at_b_3)))

                    ; #97409: <==uncertain_firing== 18178 (pos)
                    (when (and (not (Bb_not_at_b_2)))
                          (not (Bb_at_b_2)))

                    ; #98763: <==uncertain_firing== 32532 (pos)
                    (when (and (not (Pa_not_at_b_2)))
                          (not (Ba_at_b_2)))))

    (:action sense_a_at_1
        :precondition (and (at_a_1))
        :effect (and
                    ; #54436: <==closure== 71933 (pos)
                    (Pa_red)

                    ; #71933: origin
                    (Ba_red)

                    ; #17208: <==negation-removal== 54436 (pos)
                    (not (Ba_not_red))

                    ; #41086: <==negation-removal== 71933 (pos)
                    (not (Pa_not_red))))

    (:action sense_b_at_3
        :precondition (and (at_b_3))
        :effect (and
                    ; #56040: origin
                    (Bb_blue)

                    ; #74757: <==closure== 56040 (pos)
                    (Pb_blue)

                    ; #18287: <==negation-removal== 56040 (pos)
                    (not (Pb_not_blue))

                    ; #29328: <==negation-removal== 74757 (pos)
                    (not (Bb_not_blue))))

)