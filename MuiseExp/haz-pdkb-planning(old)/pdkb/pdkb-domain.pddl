(define (domain pdkb-planning)

    (:requirements :strips :conditional-effects)

    (:predicates
        (not_at_1)
        (not_at_2)
        (not_at_3)
        (not_at_4)
        (not_q)
        (Ba_not_at_1)
        (Ba_not_at_2)
        (Ba_not_at_3)
        (Ba_not_at_4)
        (Ba_not_q)
        (Ba_at_1)
        (Ba_at_2)
        (Ba_at_3)
        (Ba_at_4)
        (Ba_q)
        (Bb_not_at_1)
        (Bb_not_at_2)
        (Bb_not_at_3)
        (Bb_not_at_4)
        (Bb_not_q)
        (Bb_at_1)
        (Bb_at_2)
        (Bb_at_3)
        (Bb_at_4)
        (Bb_q)
        (Bc_not_at_1)
        (Bc_not_at_2)
        (Bc_not_at_3)
        (Bc_not_at_4)
        (Bc_not_q)
        (Bc_at_1)
        (Bc_at_2)
        (Bc_at_3)
        (Bc_at_4)
        (Bc_q)
        (Pa_not_at_1)
        (Pa_not_at_2)
        (Pa_not_at_3)
        (Pa_not_at_4)
        (Pa_not_q)
        (Pa_at_1)
        (Pa_at_2)
        (Pa_at_3)
        (Pa_at_4)
        (Pa_q)
        (Pb_not_at_1)
        (Pb_not_at_2)
        (Pb_not_at_3)
        (Pb_not_at_4)
        (Pb_not_q)
        (Pb_at_1)
        (Pb_at_2)
        (Pb_at_3)
        (Pb_at_4)
        (Pb_q)
        (Pc_not_at_1)
        (Pc_not_at_2)
        (Pc_not_at_3)
        (Pc_not_at_4)
        (Pc_not_q)
        (Pc_at_1)
        (Pc_at_2)
        (Pc_at_3)
        (Pc_at_4)
        (Pc_q)
        (at_1)
        (at_2)
        (at_3)
        (at_4)
        (q)
    )

    (:action left
        :precondition (and )
        :effect (and
                    ; #12632: <==commonly_known== 43865 (pos)
                    (when (and (Ba_at_3))
                          (Ba_at_2))

                    ; #12906: <==closure== 54985 (pos)
                    (when (and (Ba_at_2))
                          (Pa_not_at_2))

                    ; #14824: <==commonly_known== 37462 (neg)
                    (when (and (Pa_at_3))
                          (Pa_at_2))

                    ; #16502: <==commonly_known== 43865 (pos)
                    (when (and (Bc_at_3))
                          (Bc_at_2))

                    ; #16548: <==closure== 86435 (pos)
                    (when (and (Bc_at_2))
                          (Pc_at_1))

                    ; #17760: <==commonly_known== 55386 (pos)
                    (when (and (Bc_at_4))
                          (Bc_at_3))

                    ; #18712: <==commonly_known== 39606 (neg)
                    (when (and (Pc_at_4))
                          (Pc_at_3))

                    ; #20642: <==closure== 62907 (pos)
                    (when (and (Bc_at_4))
                          (Pc_not_at_4))

                    ; #21910: <==commonly_known== 81434 (pos)
                    (when (and (Bb_at_2))
                          (Bb_not_at_2))

                    ; #22085: <==commonly_known== 90398 (pos)
                    (when (and (Ba_at_2))
                          (Ba_at_1))

                    ; #22902: <==commonly_known== 63250 (neg)
                    (when (and (Pa_at_4))
                          (Pa_not_at_4))

                    ; #23131: <==commonly_known== 90398 (pos)
                    (when (and (Bb_at_2))
                          (Bb_at_1))

                    ; #23519: <==commonly_known== 41087 (neg)
                    (when (and (Pc_at_3))
                          (Pc_not_at_3))

                    ; #26451: <==closure== 73330 (pos)
                    (when (and (Ba_at_4))
                          (Pa_not_at_4))

                    ; #26725: <==commonly_known== 41087 (neg)
                    (when (and (Pb_at_3))
                          (Pb_not_at_3))

                    ; #28719: <==commonly_known== 37462 (neg)
                    (when (and (Pc_at_3))
                          (Pc_at_2))

                    ; #30129: <==commonly_known== 55386 (pos)
                    (when (and (Ba_at_4))
                          (Ba_at_3))

                    ; #30164: <==closure== 46782 (pos)
                    (when (and (Bc_at_2))
                          (Pc_not_at_2))

                    ; #30752: <==commonly_known== 88292 (neg)
                    (when (and (Pb_at_2))
                          (Pb_not_at_2))

                    ; #31100: <==commonly_known== 88386 (pos)
                    (when (and (Bb_at_3))
                          (Bb_not_at_3))

                    ; #32080: <==closure== 55538 (pos)
                    (when (and (Bb_at_3))
                          (Pb_at_2))

                    ; #32514: <==commonly_known== 41087 (neg)
                    (when (and (Pa_at_3))
                          (Pa_not_at_3))

                    ; #32777: <==commonly_known== 53188 (neg)
                    (when (and (Pa_at_2))
                          (Pa_at_1))

                    ; #43865: origin
                    (when (and (at_3))
                          (at_2))

                    ; #46782: <==commonly_known== 81434 (pos)
                    (when (and (Bc_at_2))
                          (Bc_not_at_2))

                    ; #50579: <==commonly_known== 88386 (pos)
                    (when (and (Bc_at_3))
                          (Bc_not_at_3))

                    ; #52583: <==commonly_known== 39606 (neg)
                    (when (and (Pa_at_4))
                          (Pa_at_3))

                    ; #53357: <==commonly_known== 63250 (neg)
                    (when (and (Pc_at_4))
                          (Pc_not_at_4))

                    ; #54985: <==commonly_known== 81434 (pos)
                    (when (and (Ba_at_2))
                          (Ba_not_at_2))

                    ; #55386: origin
                    (when (and (at_4))
                          (at_3))

                    ; #55538: <==commonly_known== 43865 (pos)
                    (when (and (Bb_at_3))
                          (Bb_at_2))

                    ; #56987: <==closure== 87211 (pos)
                    (when (and (Ba_at_3))
                          (Pa_not_at_3))

                    ; #57134: <==closure== 23131 (pos)
                    (when (and (Bb_at_2))
                          (Pb_at_1))

                    ; #61343: <==commonly_known== 53188 (neg)
                    (when (and (Pc_at_2))
                          (Pc_at_1))

                    ; #62525: <==commonly_known== 63250 (neg)
                    (when (and (Pb_at_4))
                          (Pb_not_at_4))

                    ; #62907: <==commonly_known== 64264 (pos)
                    (when (and (Bc_at_4))
                          (Bc_not_at_4))

                    ; #63485: <==commonly_known== 88292 (neg)
                    (when (and (Pc_at_2))
                          (Pc_not_at_2))

                    ; #64264: origin
                    (when (and (at_4))
                          (not_at_4))

                    ; #65520: <==commonly_known== 39606 (neg)
                    (when (and (Pb_at_4))
                          (Pb_at_3))

                    ; #66569: <==closure== 30129 (pos)
                    (when (and (Ba_at_4))
                          (Pa_at_3))

                    ; #69012: <==commonly_known== 64264 (pos)
                    (when (and (Bb_at_4))
                          (Bb_not_at_4))

                    ; #70544: <==commonly_known== 37462 (neg)
                    (when (and (Pb_at_3))
                          (Pb_at_2))

                    ; #71130: <==closure== 21910 (pos)
                    (when (and (Bb_at_2))
                          (Pb_not_at_2))

                    ; #71595: <==closure== 22085 (pos)
                    (when (and (Ba_at_2))
                          (Pa_at_1))

                    ; #73330: <==commonly_known== 64264 (pos)
                    (when (and (Ba_at_4))
                          (Ba_not_at_4))

                    ; #76461: <==closure== 16502 (pos)
                    (when (and (Bc_at_3))
                          (Pc_at_2))

                    ; #76751: <==commonly_known== 88292 (neg)
                    (when (and (Pa_at_2))
                          (Pa_not_at_2))

                    ; #77680: <==closure== 31100 (pos)
                    (when (and (Bb_at_3))
                          (Pb_not_at_3))

                    ; #79206: <==closure== 50579 (pos)
                    (when (and (Bc_at_3))
                          (Pc_not_at_3))

                    ; #81434: origin
                    (when (and (at_2))
                          (not_at_2))

                    ; #81627: <==closure== 12632 (pos)
                    (when (and (Ba_at_3))
                          (Pa_at_2))

                    ; #84749: <==commonly_known== 53188 (neg)
                    (when (and (Pb_at_2))
                          (Pb_at_1))

                    ; #86435: <==commonly_known== 90398 (pos)
                    (when (and (Bc_at_2))
                          (Bc_at_1))

                    ; #87211: <==commonly_known== 88386 (pos)
                    (when (and (Ba_at_3))
                          (Ba_not_at_3))

                    ; #87478: <==closure== 69012 (pos)
                    (when (and (Bb_at_4))
                          (Pb_not_at_4))

                    ; #88386: origin
                    (when (and (at_3))
                          (not_at_3))

                    ; #88681: <==commonly_known== 55386 (pos)
                    (when (and (Bb_at_4))
                          (Bb_at_3))

                    ; #90398: origin
                    (when (and (at_2))
                          (at_1))

                    ; #90553: <==closure== 17760 (pos)
                    (when (and (Bc_at_4))
                          (Pc_at_3))

                    ; #92095: <==closure== 88681 (pos)
                    (when (and (Bb_at_4))
                          (Pb_at_3))

                    ; #10980: <==uncertain_firing== 30752 (pos)
                    (when (and (not (Bb_not_at_2)))
                          (not (Bb_at_2)))

                    ; #12632: <==negation-removal== 56987 (pos)
                    (when (and (Ba_at_3))
                          (not (Ba_at_3)))

                    ; #12906: <==negation-removal== 22085 (pos)
                    (when (and (Ba_at_2))
                          (not (Pa_not_at_1)))

                    ; #13773: <==negation-removal== 70544 (pos)
                    (when (and (Pb_at_3))
                          (not (Bb_not_at_2)))

                    ; #15253: <==unclosure== 76891 (neg)
                    (when (and (not (Pc_not_at_4)))
                          (not (Bc_at_4)))

                    ; #15855: <==uncertain_firing== 53357 (pos)
                    (when (and (not (Bc_not_at_4)))
                          (not (Bc_at_4)))

                    ; #15876: <==negation-removal== 23519 (pos)
                    (when (and (Pc_at_3))
                          (not (Bc_at_3)))

                    ; #15909: <==uncertain_firing== 14824 (pos)
                    (when (and (not (Ba_not_at_3)))
                          (not (Ba_not_at_2)))

                    ; #16502: <==negation-removal== 79206 (pos)
                    (when (and (Bc_at_3))
                          (not (Bc_at_3)))

                    ; #16548: <==negation-removal== 46782 (pos)
                    (when (and (Bc_at_2))
                          (not (Pc_at_2)))

                    ; #17760: <==negation-removal== 20642 (pos)
                    (when (and (Bc_at_4))
                          (not (Bc_at_4)))

                    ; #18036: <==uncertain_firing== 88681 (pos)
                    (when (and (not (Pb_not_at_4)))
                          (not (Pb_not_at_3)))

                    ; #20237: <==uncertain_firing== 32777 (pos)
                    (when (and (not (Ba_not_at_2)))
                          (not (Ba_not_at_1)))

                    ; #20450: <==negation-removal== 14824 (pos)
                    (when (and (Pa_at_3))
                          (not (Ba_not_at_2)))

                    ; #20642: <==negation-removal== 17760 (pos)
                    (when (and (Bc_at_4))
                          (not (Pc_not_at_3)))

                    ; #21166: <==unclosure== 54535 (neg)
                    (when (and (not (Pb_not_at_2)))
                          (not (Bb_at_2)))

                    ; #21910: <==negation-removal== 57134 (pos)
                    (when (and (Bb_at_2))
                          (not (Bb_not_at_1)))

                    ; #22085: <==negation-removal== 12906 (pos)
                    (when (and (Ba_at_2))
                          (not (Ba_at_2)))

                    ; #22901: <==negation-removal== 62525 (pos)
                    (when (and (Pb_at_4))
                          (not (Bb_at_4)))

                    ; #23131: <==negation-removal== 71130 (pos)
                    (when (and (Bb_at_2))
                          (not (Bb_at_2)))

                    ; #23938: <==uncertain_firing== 22085 (pos)
                    (when (and (not (Pa_not_at_2)))
                          (not (Pa_not_at_1)))

                    ; #24055: <==unclosure== 64786 (neg)
                    (when (and (not (Pc_not_at_2)))
                          (not (Bc_not_at_1)))

                    ; #25375: <==unclosure== 23938 (neg)
                    (when (and (not (Pa_not_at_2)))
                          (not (Ba_not_at_1)))

                    ; #25645: <==uncertain_firing== 63485 (pos)
                    (when (and (not (Bc_not_at_2)))
                          (not (Bc_at_2)))

                    ; #26451: <==negation-removal== 30129 (pos)
                    (when (and (Ba_at_4))
                          (not (Pa_not_at_3)))

                    ; #26487: <==unclosure== 57035 (neg)
                    (when (and (not (Pa_not_at_4)))
                          (not (Ba_at_4)))

                    ; #27160: <==unclosure== 79237 (neg)
                    (when (and (not (Pa_not_at_4)))
                          (not (Ba_not_at_3)))

                    ; #30129: <==negation-removal== 26451 (pos)
                    (when (and (Ba_at_4))
                          (not (Ba_at_4)))

                    ; #30164: <==negation-removal== 86435 (pos)
                    (when (and (Bc_at_2))
                          (not (Pc_not_at_1)))

                    ; #31100: <==negation-removal== 32080 (pos)
                    (when (and (Bb_at_3))
                          (not (Bb_not_at_2)))

                    ; #32080: <==negation-removal== 31100 (pos)
                    (when (and (Bb_at_3))
                          (not (Pb_at_3)))

                    ; #33116: <==unclosure== 82752 (neg)
                    (when (and (not (Pb_not_at_3)))
                          (not (Bb_at_3)))

                    ; #33194: <==uncertain_firing== 18712 (pos)
                    (when (and (not (Bc_not_at_4)))
                          (not (Bc_not_at_3)))

                    ; #34397: <==unclosure== 60944 (neg)
                    (when (and (not (Pb_not_at_2)))
                          (not (Bb_not_at_1)))

                    ; #36069: <==negation-removal== 84749 (pos)
                    (when (and (Pb_at_2))
                          (not (Bb_not_at_1)))

                    ; #37462: <==uncertain_firing== 43865 (pos)
                    (when (and (not (not_at_3)))
                          (not (not_at_2)))

                    ; #38850: <==negation-removal== 52583 (pos)
                    (when (and (Pa_at_4))
                          (not (Ba_not_at_3)))

                    ; #39606: <==uncertain_firing== 55386 (pos)
                    (when (and (not (not_at_4)))
                          (not (not_at_3)))

                    ; #40121: <==uncertain_firing== 46782 (pos)
                    (when (and (not (Pc_not_at_2)))
                          (not (Pc_at_2)))

                    ; #40549: <==unclosure== 59243 (neg)
                    (when (and (not (Pa_not_at_2)))
                          (not (Ba_at_2)))

                    ; #41087: <==uncertain_firing== 88386 (pos)
                    (when (and (not (not_at_3)))
                          (not (at_3)))

                    ; #41407: <==uncertain_firing== 87211 (pos)
                    (when (and (not (Pa_not_at_3)))
                          (not (Pa_at_3)))

                    ; #42972: <==uncertain_firing== 28719 (pos)
                    (when (and (not (Bc_not_at_3)))
                          (not (Bc_not_at_2)))

                    ; #43337: <==uncertain_firing== 50579 (pos)
                    (when (and (not (Pc_not_at_3)))
                          (not (Pc_at_3)))

                    ; #43814: <==unclosure== 96670 (neg)
                    (when (and (not (Pa_not_at_3)))
                          (not (Ba_not_at_2)))

                    ; #43865: <==negation-removal== 88386 (pos)
                    (when (and (at_3))
                          (not (at_3)))

                    ; #44403: <==uncertain_firing== 23519 (pos)
                    (when (and (not (Bc_not_at_3)))
                          (not (Bc_at_3)))

                    ; #46782: <==negation-removal== 16548 (pos)
                    (when (and (Bc_at_2))
                          (not (Bc_not_at_1)))

                    ; #46837: <==uncertain_firing== 62525 (pos)
                    (when (and (not (Bb_not_at_4)))
                          (not (Bb_at_4)))

                    ; #48907: <==negation-removal== 32777 (pos)
                    (when (and (Pa_at_2))
                          (not (Ba_not_at_1)))

                    ; #50579: <==negation-removal== 76461 (pos)
                    (when (and (Bc_at_3))
                          (not (Bc_not_at_2)))

                    ; #52324: <==negation-removal== 65520 (pos)
                    (when (and (Pb_at_4))
                          (not (Bb_not_at_3)))

                    ; #53188: <==uncertain_firing== 90398 (pos)
                    (when (and (not (not_at_2)))
                          (not (not_at_1)))

                    ; #53335: <==uncertain_firing== 84749 (pos)
                    (when (and (not (Bb_not_at_2)))
                          (not (Bb_not_at_1)))

                    ; #54535: <==uncertain_firing== 21910 (pos)
                    (when (and (not (Pb_not_at_2)))
                          (not (Pb_at_2)))

                    ; #54985: <==negation-removal== 71595 (pos)
                    (when (and (Ba_at_2))
                          (not (Ba_not_at_1)))

                    ; #55386: <==negation-removal== 64264 (pos)
                    (when (and (at_4))
                          (not (at_4)))

                    ; #55538: <==negation-removal== 77680 (pos)
                    (when (and (Bb_at_3))
                          (not (Bb_at_3)))

                    ; #56679: <==unclosure== 81545 (neg)
                    (when (and (not (Pb_not_at_4)))
                          (not (Bb_at_4)))

                    ; #56987: <==negation-removal== 12632 (pos)
                    (when (and (Ba_at_3))
                          (not (Pa_not_at_2)))

                    ; #57035: <==uncertain_firing== 73330 (pos)
                    (when (and (not (Pa_not_at_4)))
                          (not (Pa_at_4)))

                    ; #57134: <==negation-removal== 21910 (pos)
                    (when (and (Bb_at_2))
                          (not (Pb_at_2)))

                    ; #58080: <==negation-removal== 32514 (pos)
                    (when (and (Pa_at_3))
                          (not (Ba_at_3)))

                    ; #59082: <==negation-removal== 30752 (pos)
                    (when (and (Pb_at_2))
                          (not (Bb_at_2)))

                    ; #59243: <==uncertain_firing== 54985 (pos)
                    (when (and (not (Pa_not_at_2)))
                          (not (Pa_at_2)))

                    ; #59933: <==negation-removal== 53357 (pos)
                    (when (and (Pc_at_4))
                          (not (Bc_at_4)))

                    ; #60343: <==negation-removal== 28719 (pos)
                    (when (and (Pc_at_3))
                          (not (Bc_not_at_2)))

                    ; #60944: <==uncertain_firing== 23131 (pos)
                    (when (and (not (Pb_not_at_2)))
                          (not (Pb_not_at_1)))

                    ; #62779: <==unclosure== 41407 (neg)
                    (when (and (not (Pa_not_at_3)))
                          (not (Ba_at_3)))

                    ; #62907: <==negation-removal== 90553 (pos)
                    (when (and (Bc_at_4))
                          (not (Bc_not_at_3)))

                    ; #63250: <==uncertain_firing== 64264 (pos)
                    (when (and (not (not_at_4)))
                          (not (at_4)))

                    ; #64264: <==negation-removal== 55386 (pos)
                    (when (and (at_4))
                          (not (not_at_3)))

                    ; #64786: <==uncertain_firing== 86435 (pos)
                    (when (and (not (Pc_not_at_2)))
                          (not (Pc_not_at_1)))

                    ; #65466: <==negation-removal== 63485 (pos)
                    (when (and (Pc_at_2))
                          (not (Bc_at_2)))

                    ; #65976: <==negation-removal== 26725 (pos)
                    (when (and (Pb_at_3))
                          (not (Bb_at_3)))

                    ; #66569: <==negation-removal== 73330 (pos)
                    (when (and (Ba_at_4))
                          (not (Pa_at_4)))

                    ; #67414: <==unclosure== 69737 (neg)
                    (when (and (not (Pb_not_at_3)))
                          (not (Bb_not_at_2)))

                    ; #67764: <==uncertain_firing== 70544 (pos)
                    (when (and (not (Bb_not_at_3)))
                          (not (Bb_not_at_2)))

                    ; #67825: <==negation-removal== 22902 (pos)
                    (when (and (Pa_at_4))
                          (not (Ba_at_4)))

                    ; #67903: <==uncertain_firing== 22902 (pos)
                    (when (and (not (Ba_not_at_4)))
                          (not (Ba_at_4)))

                    ; #69012: <==negation-removal== 92095 (pos)
                    (when (and (Bb_at_4))
                          (not (Bb_not_at_3)))

                    ; #69737: <==uncertain_firing== 55538 (pos)
                    (when (and (not (Pb_not_at_3)))
                          (not (Pb_not_at_2)))

                    ; #70401: <==negation-removal== 61343 (pos)
                    (when (and (Pc_at_2))
                          (not (Bc_not_at_1)))

                    ; #71130: <==negation-removal== 23131 (pos)
                    (when (and (Bb_at_2))
                          (not (Pb_not_at_1)))

                    ; #71595: <==negation-removal== 54985 (pos)
                    (when (and (Ba_at_2))
                          (not (Pa_at_2)))

                    ; #72173: <==uncertain_firing== 76751 (pos)
                    (when (and (not (Ba_not_at_2)))
                          (not (Ba_at_2)))

                    ; #73330: <==negation-removal== 66569 (pos)
                    (when (and (Ba_at_4))
                          (not (Ba_not_at_3)))

                    ; #76461: <==negation-removal== 50579 (pos)
                    (when (and (Bc_at_3))
                          (not (Pc_at_3)))

                    ; #76730: <==uncertain_firing== 26725 (pos)
                    (when (and (not (Bb_not_at_3)))
                          (not (Bb_at_3)))

                    ; #76891: <==uncertain_firing== 62907 (pos)
                    (when (and (not (Pc_not_at_4)))
                          (not (Pc_at_4)))

                    ; #77680: <==negation-removal== 55538 (pos)
                    (when (and (Bb_at_3))
                          (not (Pb_not_at_2)))

                    ; #77842: <==uncertain_firing== 16502 (pos)
                    (when (and (not (Pc_not_at_3)))
                          (not (Pc_not_at_2)))

                    ; #77865: <==uncertain_firing== 61343 (pos)
                    (when (and (not (Bc_not_at_2)))
                          (not (Bc_not_at_1)))

                    ; #79206: <==negation-removal== 16502 (pos)
                    (when (and (Bc_at_3))
                          (not (Pc_not_at_2)))

                    ; #79237: <==uncertain_firing== 30129 (pos)
                    (when (and (not (Pa_not_at_4)))
                          (not (Pa_not_at_3)))

                    ; #81434: <==negation-removal== 90398 (pos)
                    (when (and (at_2))
                          (not (not_at_1)))

                    ; #81526: <==unclosure== 81986 (neg)
                    (when (and (not (Pc_not_at_4)))
                          (not (Bc_not_at_3)))

                    ; #81545: <==uncertain_firing== 69012 (pos)
                    (when (and (not (Pb_not_at_4)))
                          (not (Pb_at_4)))

                    ; #81627: <==negation-removal== 87211 (pos)
                    (when (and (Ba_at_3))
                          (not (Pa_at_3)))

                    ; #81986: <==uncertain_firing== 17760 (pos)
                    (when (and (not (Pc_not_at_4)))
                          (not (Pc_not_at_3)))

                    ; #82752: <==uncertain_firing== 31100 (pos)
                    (when (and (not (Pb_not_at_3)))
                          (not (Pb_at_3)))

                    ; #84673: <==uncertain_firing== 52583 (pos)
                    (when (and (not (Ba_not_at_4)))
                          (not (Ba_not_at_3)))

                    ; #86435: <==negation-removal== 30164 (pos)
                    (when (and (Bc_at_2))
                          (not (Bc_at_2)))

                    ; #87211: <==negation-removal== 81627 (pos)
                    (when (and (Ba_at_3))
                          (not (Ba_not_at_2)))

                    ; #87478: <==negation-removal== 88681 (pos)
                    (when (and (Bb_at_4))
                          (not (Pb_not_at_3)))

                    ; #88286: <==uncertain_firing== 32514 (pos)
                    (when (and (not (Ba_not_at_3)))
                          (not (Ba_at_3)))

                    ; #88292: <==uncertain_firing== 81434 (pos)
                    (when (and (not (not_at_2)))
                          (not (at_2)))

                    ; #88386: <==negation-removal== 43865 (pos)
                    (when (and (at_3))
                          (not (not_at_2)))

                    ; #88681: <==negation-removal== 87478 (pos)
                    (when (and (Bb_at_4))
                          (not (Bb_at_4)))

                    ; #88816: <==uncertain_firing== 65520 (pos)
                    (when (and (not (Bb_not_at_4)))
                          (not (Bb_not_at_3)))

                    ; #89556: <==negation-removal== 18712 (pos)
                    (when (and (Pc_at_4))
                          (not (Bc_not_at_3)))

                    ; #90398: <==negation-removal== 81434 (pos)
                    (when (and (at_2))
                          (not (at_2)))

                    ; #90545: <==unclosure== 77842 (neg)
                    (when (and (not (Pc_not_at_3)))
                          (not (Bc_not_at_2)))

                    ; #90553: <==negation-removal== 62907 (pos)
                    (when (and (Bc_at_4))
                          (not (Pc_at_4)))

                    ; #91074: <==unclosure== 40121 (neg)
                    (when (and (not (Pc_not_at_2)))
                          (not (Bc_at_2)))

                    ; #91397: <==unclosure== 43337 (neg)
                    (when (and (not (Pc_not_at_3)))
                          (not (Bc_at_3)))

                    ; #91436: <==negation-removal== 76751 (pos)
                    (when (and (Pa_at_2))
                          (not (Ba_at_2)))

                    ; #92095: <==negation-removal== 69012 (pos)
                    (when (and (Bb_at_4))
                          (not (Pb_at_4)))

                    ; #96252: <==unclosure== 18036 (neg)
                    (when (and (not (Pb_not_at_4)))
                          (not (Bb_not_at_3)))

                    ; #96670: <==uncertain_firing== 12632 (pos)
                    (when (and (not (Pa_not_at_3)))
                          (not (Pa_not_at_2)))))

    (:action right
        :precondition (and )
        :effect (and
                    ; #12632: <==commonly_known== 43865 (pos)
                    (when (and (Ba_at_3))
                          (Ba_at_4))

                    ; #12906: <==closure== 54985 (pos)
                    (when (and (Ba_at_2))
                          (Pa_not_at_2))

                    ; #13756: <==commonly_known== 65811 (pos)
                    (when (and (Ba_at_1))
                          (Ba_at_2))

                    ; #14824: <==commonly_known== 37462 (neg)
                    (when (and (Pa_at_3))
                          (Pa_at_4))

                    ; #15333: <==commonly_known== 61206 (pos)
                    (when (and (Ba_at_1))
                          (Ba_not_at_1))

                    ; #16502: <==commonly_known== 43865 (pos)
                    (when (and (Bc_at_3))
                          (Bc_at_4))

                    ; #16548: <==closure== 86435 (pos)
                    (when (and (Bc_at_2))
                          (Pc_at_3))

                    ; #17657: <==closure== 81078 (pos)
                    (when (and (Bc_at_1))
                          (Pc_at_2))

                    ; #19547: <==commonly_known== 84114 (neg)
                    (when (and (Pc_at_1))
                          (Pc_not_at_1))

                    ; #21910: <==commonly_known== 81434 (pos)
                    (when (and (Bb_at_2))
                          (Bb_not_at_2))

                    ; #22085: <==commonly_known== 90398 (pos)
                    (when (and (Ba_at_2))
                          (Ba_at_3))

                    ; #23131: <==commonly_known== 90398 (pos)
                    (when (and (Bb_at_2))
                          (Bb_at_3))

                    ; #23519: <==commonly_known== 41087 (neg)
                    (when (and (Pc_at_3))
                          (Pc_not_at_3))

                    ; #23650: <==commonly_known== 61206 (pos)
                    (when (and (Bc_at_1))
                          (Bc_not_at_1))

                    ; #23793: <==closure== 55278 (pos)
                    (when (and (Bb_at_1))
                          (Pb_at_2))

                    ; #26725: <==commonly_known== 41087 (neg)
                    (when (and (Pb_at_3))
                          (Pb_not_at_3))

                    ; #28719: <==commonly_known== 37462 (neg)
                    (when (and (Pc_at_3))
                          (Pc_at_4))

                    ; #30164: <==closure== 46782 (pos)
                    (when (and (Bc_at_2))
                          (Pc_not_at_2))

                    ; #30752: <==commonly_known== 88292 (neg)
                    (when (and (Pb_at_2))
                          (Pb_not_at_2))

                    ; #31100: <==commonly_known== 88386 (pos)
                    (when (and (Bb_at_3))
                          (Bb_not_at_3))

                    ; #32080: <==closure== 55538 (pos)
                    (when (and (Bb_at_3))
                          (Pb_at_4))

                    ; #32514: <==commonly_known== 41087 (neg)
                    (when (and (Pa_at_3))
                          (Pa_not_at_3))

                    ; #32777: <==commonly_known== 53188 (neg)
                    (when (and (Pa_at_2))
                          (Pa_at_3))

                    ; #34618: <==commonly_known== 84114 (neg)
                    (when (and (Pa_at_1))
                          (Pa_not_at_1))

                    ; #43865: origin
                    (when (and (at_3))
                          (at_4))

                    ; #45409: <==commonly_known== 91391 (neg)
                    (when (and (Pc_at_1))
                          (Pc_at_2))

                    ; #46782: <==commonly_known== 81434 (pos)
                    (when (and (Bc_at_2))
                          (Bc_not_at_2))

                    ; #50579: <==commonly_known== 88386 (pos)
                    (when (and (Bc_at_3))
                          (Bc_not_at_3))

                    ; #50589: <==commonly_known== 91391 (neg)
                    (when (and (Pa_at_1))
                          (Pa_at_2))

                    ; #50765: <==commonly_known== 61206 (pos)
                    (when (and (Bb_at_1))
                          (Bb_not_at_1))

                    ; #53763: <==closure== 50765 (pos)
                    (when (and (Bb_at_1))
                          (Pb_not_at_1))

                    ; #54985: <==commonly_known== 81434 (pos)
                    (when (and (Ba_at_2))
                          (Ba_not_at_2))

                    ; #55278: <==commonly_known== 65811 (pos)
                    (when (and (Bb_at_1))
                          (Bb_at_2))

                    ; #55538: <==commonly_known== 43865 (pos)
                    (when (and (Bb_at_3))
                          (Bb_at_4))

                    ; #56987: <==closure== 87211 (pos)
                    (when (and (Ba_at_3))
                          (Pa_not_at_3))

                    ; #57134: <==closure== 23131 (pos)
                    (when (and (Bb_at_2))
                          (Pb_at_3))

                    ; #59846: <==closure== 15333 (pos)
                    (when (and (Ba_at_1))
                          (Pa_not_at_1))

                    ; #60275: <==commonly_known== 84114 (neg)
                    (when (and (Pb_at_1))
                          (Pb_not_at_1))

                    ; #61206: origin
                    (when (and (at_1))
                          (not_at_1))

                    ; #61343: <==commonly_known== 53188 (neg)
                    (when (and (Pc_at_2))
                          (Pc_at_3))

                    ; #61405: <==commonly_known== 91391 (neg)
                    (when (and (Pb_at_1))
                          (Pb_at_2))

                    ; #63485: <==commonly_known== 88292 (neg)
                    (when (and (Pc_at_2))
                          (Pc_not_at_2))

                    ; #65811: origin
                    (when (and (at_1))
                          (at_2))

                    ; #70544: <==commonly_known== 37462 (neg)
                    (when (and (Pb_at_3))
                          (Pb_at_4))

                    ; #71130: <==closure== 21910 (pos)
                    (when (and (Bb_at_2))
                          (Pb_not_at_2))

                    ; #71595: <==closure== 22085 (pos)
                    (when (and (Ba_at_2))
                          (Pa_at_3))

                    ; #75357: <==closure== 23650 (pos)
                    (when (and (Bc_at_1))
                          (Pc_not_at_1))

                    ; #76461: <==closure== 16502 (pos)
                    (when (and (Bc_at_3))
                          (Pc_at_4))

                    ; #76751: <==commonly_known== 88292 (neg)
                    (when (and (Pa_at_2))
                          (Pa_not_at_2))

                    ; #77680: <==closure== 31100 (pos)
                    (when (and (Bb_at_3))
                          (Pb_not_at_3))

                    ; #79206: <==closure== 50579 (pos)
                    (when (and (Bc_at_3))
                          (Pc_not_at_3))

                    ; #81078: <==commonly_known== 65811 (pos)
                    (when (and (Bc_at_1))
                          (Bc_at_2))

                    ; #81434: origin
                    (when (and (at_2))
                          (not_at_2))

                    ; #81627: <==closure== 12632 (pos)
                    (when (and (Ba_at_3))
                          (Pa_at_4))

                    ; #84749: <==commonly_known== 53188 (neg)
                    (when (and (Pb_at_2))
                          (Pb_at_3))

                    ; #86435: <==commonly_known== 90398 (pos)
                    (when (and (Bc_at_2))
                          (Bc_at_3))

                    ; #87211: <==commonly_known== 88386 (pos)
                    (when (and (Ba_at_3))
                          (Ba_not_at_3))

                    ; #87938: <==closure== 13756 (pos)
                    (when (and (Ba_at_1))
                          (Pa_at_2))

                    ; #88386: origin
                    (when (and (at_3))
                          (not_at_3))

                    ; #90398: origin
                    (when (and (at_2))
                          (at_3))

                    ; #10980: <==uncertain_firing== 30752 (pos)
                    (when (and (not (Bb_not_at_2)))
                          (not (Bb_at_2)))

                    ; #12632: <==negation-removal== 56987 (pos)
                    (when (and (Ba_at_3))
                          (not (Ba_at_3)))

                    ; #12906: <==negation-removal== 22085 (pos)
                    (when (and (Ba_at_2))
                          (not (Pa_not_at_3)))

                    ; #13756: <==negation-removal== 59846 (pos)
                    (when (and (Ba_at_1))
                          (not (Ba_at_1)))

                    ; #13773: <==negation-removal== 70544 (pos)
                    (when (and (Pb_at_3))
                          (not (Bb_not_at_4)))

                    ; #14226: <==negation-removal== 60275 (pos)
                    (when (and (Pb_at_1))
                          (not (Bb_at_1)))

                    ; #15333: <==negation-removal== 87938 (pos)
                    (when (and (Ba_at_1))
                          (not (Ba_not_at_2)))

                    ; #15876: <==negation-removal== 23519 (pos)
                    (when (and (Pc_at_3))
                          (not (Bc_at_3)))

                    ; #15909: <==uncertain_firing== 14824 (pos)
                    (when (and (not (Ba_not_at_3)))
                          (not (Ba_not_at_4)))

                    ; #16502: <==negation-removal== 79206 (pos)
                    (when (and (Bc_at_3))
                          (not (Bc_at_3)))

                    ; #16548: <==negation-removal== 46782 (pos)
                    (when (and (Bc_at_2))
                          (not (Pc_at_2)))

                    ; #17212: <==uncertain_firing== 50589 (pos)
                    (when (and (not (Ba_not_at_1)))
                          (not (Ba_not_at_2)))

                    ; #17657: <==negation-removal== 23650 (pos)
                    (when (and (Bc_at_1))
                          (not (Pc_at_1)))

                    ; #19921: <==uncertain_firing== 55278 (pos)
                    (when (and (not (Pb_not_at_1)))
                          (not (Pb_not_at_2)))

                    ; #20237: <==uncertain_firing== 32777 (pos)
                    (when (and (not (Ba_not_at_2)))
                          (not (Ba_not_at_3)))

                    ; #20450: <==negation-removal== 14824 (pos)
                    (when (and (Pa_at_3))
                          (not (Ba_not_at_4)))

                    ; #20796: <==negation-removal== 50589 (pos)
                    (when (and (Pa_at_1))
                          (not (Ba_not_at_2)))

                    ; #21166: <==unclosure== 54535 (neg)
                    (when (and (not (Pb_not_at_2)))
                          (not (Bb_at_2)))

                    ; #21910: <==negation-removal== 57134 (pos)
                    (when (and (Bb_at_2))
                          (not (Bb_not_at_3)))

                    ; #22085: <==negation-removal== 12906 (pos)
                    (when (and (Ba_at_2))
                          (not (Ba_at_2)))

                    ; #23131: <==negation-removal== 71130 (pos)
                    (when (and (Bb_at_2))
                          (not (Bb_at_2)))

                    ; #23270: <==negation-removal== 45409 (pos)
                    (when (and (Pc_at_1))
                          (not (Bc_not_at_2)))

                    ; #23650: <==negation-removal== 17657 (pos)
                    (when (and (Bc_at_1))
                          (not (Bc_not_at_2)))

                    ; #23793: <==negation-removal== 50765 (pos)
                    (when (and (Bb_at_1))
                          (not (Pb_at_1)))

                    ; #23938: <==uncertain_firing== 22085 (pos)
                    (when (and (not (Pa_not_at_2)))
                          (not (Pa_not_at_3)))

                    ; #24055: <==unclosure== 64786 (neg)
                    (when (and (not (Pc_not_at_2)))
                          (not (Bc_not_at_3)))

                    ; #24501: <==uncertain_firing== 13756 (pos)
                    (when (and (not (Pa_not_at_1)))
                          (not (Pa_not_at_2)))

                    ; #25375: <==unclosure== 23938 (neg)
                    (when (and (not (Pa_not_at_2)))
                          (not (Ba_not_at_3)))

                    ; #25645: <==uncertain_firing== 63485 (pos)
                    (when (and (not (Bc_not_at_2)))
                          (not (Bc_at_2)))

                    ; #26931: <==unclosure== 97113 (neg)
                    (when (and (not (Pa_not_at_1)))
                          (not (Ba_at_1)))

                    ; #26995: <==uncertain_firing== 60275 (pos)
                    (when (and (not (Bb_not_at_1)))
                          (not (Bb_at_1)))

                    ; #27729: <==unclosure== 24501 (neg)
                    (when (and (not (Pa_not_at_1)))
                          (not (Ba_not_at_2)))

                    ; #30164: <==negation-removal== 86435 (pos)
                    (when (and (Bc_at_2))
                          (not (Pc_not_at_3)))

                    ; #31100: <==negation-removal== 32080 (pos)
                    (when (and (Bb_at_3))
                          (not (Bb_not_at_4)))

                    ; #31979: <==uncertain_firing== 45409 (pos)
                    (when (and (not (Bc_not_at_1)))
                          (not (Bc_not_at_2)))

                    ; #32080: <==negation-removal== 31100 (pos)
                    (when (and (Bb_at_3))
                          (not (Pb_at_3)))

                    ; #33116: <==unclosure== 82752 (neg)
                    (when (and (not (Pb_not_at_3)))
                          (not (Bb_at_3)))

                    ; #34397: <==unclosure== 60944 (neg)
                    (when (and (not (Pb_not_at_2)))
                          (not (Bb_not_at_3)))

                    ; #36069: <==negation-removal== 84749 (pos)
                    (when (and (Pb_at_2))
                          (not (Bb_not_at_3)))

                    ; #37462: <==uncertain_firing== 43865 (pos)
                    (when (and (not (not_at_3)))
                          (not (not_at_4)))

                    ; #37976: <==negation-removal== 19547 (pos)
                    (when (and (Pc_at_1))
                          (not (Bc_at_1)))

                    ; #40121: <==uncertain_firing== 46782 (pos)
                    (when (and (not (Pc_not_at_2)))
                          (not (Pc_at_2)))

                    ; #40549: <==unclosure== 59243 (neg)
                    (when (and (not (Pa_not_at_2)))
                          (not (Ba_at_2)))

                    ; #41087: <==uncertain_firing== 88386 (pos)
                    (when (and (not (not_at_3)))
                          (not (at_3)))

                    ; #41407: <==uncertain_firing== 87211 (pos)
                    (when (and (not (Pa_not_at_3)))
                          (not (Pa_at_3)))

                    ; #42972: <==uncertain_firing== 28719 (pos)
                    (when (and (not (Bc_not_at_3)))
                          (not (Bc_not_at_4)))

                    ; #43337: <==uncertain_firing== 50579 (pos)
                    (when (and (not (Pc_not_at_3)))
                          (not (Pc_at_3)))

                    ; #43814: <==unclosure== 96670 (neg)
                    (when (and (not (Pa_not_at_3)))
                          (not (Ba_not_at_4)))

                    ; #43865: <==negation-removal== 88386 (pos)
                    (when (and (at_3))
                          (not (at_3)))

                    ; #44403: <==uncertain_firing== 23519 (pos)
                    (when (and (not (Bc_not_at_3)))
                          (not (Bc_at_3)))

                    ; #45214: <==uncertain_firing== 23650 (pos)
                    (when (and (not (Pc_not_at_1)))
                          (not (Pc_at_1)))

                    ; #46246: <==negation-removal== 61405 (pos)
                    (when (and (Pb_at_1))
                          (not (Bb_not_at_2)))

                    ; #46782: <==negation-removal== 16548 (pos)
                    (when (and (Bc_at_2))
                          (not (Bc_not_at_3)))

                    ; #47341: <==uncertain_firing== 34618 (pos)
                    (when (and (not (Ba_not_at_1)))
                          (not (Ba_at_1)))

                    ; #48907: <==negation-removal== 32777 (pos)
                    (when (and (Pa_at_2))
                          (not (Ba_not_at_3)))

                    ; #50579: <==negation-removal== 76461 (pos)
                    (when (and (Bc_at_3))
                          (not (Bc_not_at_4)))

                    ; #50765: <==negation-removal== 23793 (pos)
                    (when (and (Bb_at_1))
                          (not (Bb_not_at_2)))

                    ; #53188: <==uncertain_firing== 90398 (pos)
                    (when (and (not (not_at_2)))
                          (not (not_at_3)))

                    ; #53335: <==uncertain_firing== 84749 (pos)
                    (when (and (not (Bb_not_at_2)))
                          (not (Bb_not_at_3)))

                    ; #53763: <==negation-removal== 55278 (pos)
                    (when (and (Bb_at_1))
                          (not (Pb_not_at_2)))

                    ; #54535: <==uncertain_firing== 21910 (pos)
                    (when (and (not (Pb_not_at_2)))
                          (not (Pb_at_2)))

                    ; #54985: <==negation-removal== 71595 (pos)
                    (when (and (Ba_at_2))
                          (not (Ba_not_at_3)))

                    ; #55278: <==negation-removal== 53763 (pos)
                    (when (and (Bb_at_1))
                          (not (Bb_at_1)))

                    ; #55538: <==negation-removal== 77680 (pos)
                    (when (and (Bb_at_3))
                          (not (Bb_at_3)))

                    ; #56987: <==negation-removal== 12632 (pos)
                    (when (and (Ba_at_3))
                          (not (Pa_not_at_4)))

                    ; #57021: <==uncertain_firing== 81078 (pos)
                    (when (and (not (Pc_not_at_1)))
                          (not (Pc_not_at_2)))

                    ; #57134: <==negation-removal== 21910 (pos)
                    (when (and (Bb_at_2))
                          (not (Pb_at_2)))

                    ; #58080: <==negation-removal== 32514 (pos)
                    (when (and (Pa_at_3))
                          (not (Ba_at_3)))

                    ; #59082: <==negation-removal== 30752 (pos)
                    (when (and (Pb_at_2))
                          (not (Bb_at_2)))

                    ; #59243: <==uncertain_firing== 54985 (pos)
                    (when (and (not (Pa_not_at_2)))
                          (not (Pa_at_2)))

                    ; #59846: <==negation-removal== 13756 (pos)
                    (when (and (Ba_at_1))
                          (not (Pa_not_at_2)))

                    ; #60308: <==unclosure== 70110 (neg)
                    (when (and (not (Pb_not_at_1)))
                          (not (Bb_at_1)))

                    ; #60343: <==negation-removal== 28719 (pos)
                    (when (and (Pc_at_3))
                          (not (Bc_not_at_4)))

                    ; #60944: <==uncertain_firing== 23131 (pos)
                    (when (and (not (Pb_not_at_2)))
                          (not (Pb_not_at_3)))

                    ; #61206: <==negation-removal== 65811 (pos)
                    (when (and (at_1))
                          (not (not_at_2)))

                    ; #61420: <==uncertain_firing== 19547 (pos)
                    (when (and (not (Bc_not_at_1)))
                          (not (Bc_at_1)))

                    ; #62779: <==unclosure== 41407 (neg)
                    (when (and (not (Pa_not_at_3)))
                          (not (Ba_at_3)))

                    ; #64786: <==uncertain_firing== 86435 (pos)
                    (when (and (not (Pc_not_at_2)))
                          (not (Pc_not_at_3)))

                    ; #65466: <==negation-removal== 63485 (pos)
                    (when (and (Pc_at_2))
                          (not (Bc_at_2)))

                    ; #65811: <==negation-removal== 61206 (pos)
                    (when (and (at_1))
                          (not (at_1)))

                    ; #65976: <==negation-removal== 26725 (pos)
                    (when (and (Pb_at_3))
                          (not (Bb_at_3)))

                    ; #67414: <==unclosure== 69737 (neg)
                    (when (and (not (Pb_not_at_3)))
                          (not (Bb_not_at_4)))

                    ; #67764: <==uncertain_firing== 70544 (pos)
                    (when (and (not (Bb_not_at_3)))
                          (not (Bb_not_at_4)))

                    ; #69737: <==uncertain_firing== 55538 (pos)
                    (when (and (not (Pb_not_at_3)))
                          (not (Pb_not_at_4)))

                    ; #70110: <==uncertain_firing== 50765 (pos)
                    (when (and (not (Pb_not_at_1)))
                          (not (Pb_at_1)))

                    ; #70401: <==negation-removal== 61343 (pos)
                    (when (and (Pc_at_2))
                          (not (Bc_not_at_3)))

                    ; #70729: <==unclosure== 45214 (neg)
                    (when (and (not (Pc_not_at_1)))
                          (not (Bc_at_1)))

                    ; #71130: <==negation-removal== 23131 (pos)
                    (when (and (Bb_at_2))
                          (not (Pb_not_at_3)))

                    ; #71595: <==negation-removal== 54985 (pos)
                    (when (and (Ba_at_2))
                          (not (Pa_at_2)))

                    ; #72096: <==unclosure== 19921 (neg)
                    (when (and (not (Pb_not_at_1)))
                          (not (Bb_not_at_2)))

                    ; #72173: <==uncertain_firing== 76751 (pos)
                    (when (and (not (Ba_not_at_2)))
                          (not (Ba_at_2)))

                    ; #75357: <==negation-removal== 81078 (pos)
                    (when (and (Bc_at_1))
                          (not (Pc_not_at_2)))

                    ; #76461: <==negation-removal== 50579 (pos)
                    (when (and (Bc_at_3))
                          (not (Pc_at_3)))

                    ; #76730: <==uncertain_firing== 26725 (pos)
                    (when (and (not (Bb_not_at_3)))
                          (not (Bb_at_3)))

                    ; #77680: <==negation-removal== 55538 (pos)
                    (when (and (Bb_at_3))
                          (not (Pb_not_at_4)))

                    ; #77842: <==uncertain_firing== 16502 (pos)
                    (when (and (not (Pc_not_at_3)))
                          (not (Pc_not_at_4)))

                    ; #77865: <==uncertain_firing== 61343 (pos)
                    (when (and (not (Bc_not_at_2)))
                          (not (Bc_not_at_3)))

                    ; #79206: <==uncertain_firing== 61405 (pos)
                    (when (and (not (Bb_not_at_1)))
                          (not (Bb_not_at_2)))

                    ; #79206: <==negation-removal== 16502 (pos)
                    (when (and (Bc_at_3))
                          (not (Pc_not_at_4)))

                    ; #81078: <==negation-removal== 75357 (pos)
                    (when (and (Bc_at_1))
                          (not (Bc_at_1)))

                    ; #81434: <==negation-removal== 90398 (pos)
                    (when (and (at_2))
                          (not (not_at_3)))

                    ; #81627: <==negation-removal== 87211 (pos)
                    (when (and (Ba_at_3))
                          (not (Pa_at_3)))

                    ; #82401: <==unclosure== 57021 (neg)
                    (when (and (not (Pc_not_at_1)))
                          (not (Bc_not_at_2)))

                    ; #82752: <==uncertain_firing== 31100 (pos)
                    (when (and (not (Pb_not_at_3)))
                          (not (Pb_at_3)))

                    ; #84114: <==uncertain_firing== 61206 (pos)
                    (when (and (not (not_at_1)))
                          (not (at_1)))

                    ; #86435: <==negation-removal== 30164 (pos)
                    (when (and (Bc_at_2))
                          (not (Bc_at_2)))

                    ; #87211: <==negation-removal== 81627 (pos)
                    (when (and (Ba_at_3))
                          (not (Ba_not_at_4)))

                    ; #87938: <==negation-removal== 15333 (pos)
                    (when (and (Ba_at_1))
                          (not (Pa_at_1)))

                    ; #87993: <==negation-removal== 34618 (pos)
                    (when (and (Pa_at_1))
                          (not (Ba_at_1)))

                    ; #88286: <==uncertain_firing== 32514 (pos)
                    (when (and (not (Ba_not_at_3)))
                          (not (Ba_at_3)))

                    ; #88292: <==uncertain_firing== 81434 (pos)
                    (when (and (not (not_at_2)))
                          (not (at_2)))

                    ; #88386: <==negation-removal== 43865 (pos)
                    (when (and (at_3))
                          (not (not_at_4)))

                    ; #90398: <==negation-removal== 81434 (pos)
                    (when (and (at_2))
                          (not (at_2)))

                    ; #90545: <==unclosure== 77842 (neg)
                    (when (and (not (Pc_not_at_3)))
                          (not (Bc_not_at_4)))

                    ; #91074: <==unclosure== 40121 (neg)
                    (when (and (not (Pc_not_at_2)))
                          (not (Bc_at_2)))

                    ; #91391: <==uncertain_firing== 65811 (pos)
                    (when (and (not (not_at_1)))
                          (not (not_at_2)))

                    ; #91397: <==unclosure== 43337 (neg)
                    (when (and (not (Pc_not_at_3)))
                          (not (Bc_at_3)))

                    ; #91436: <==negation-removal== 76751 (pos)
                    (when (and (Pa_at_2))
                          (not (Ba_at_2)))

                    ; #96670: <==uncertain_firing== 12632 (pos)
                    (when (and (not (Pa_not_at_3)))
                          (not (Pa_not_at_4)))

                    ; #97113: <==uncertain_firing== 15333 (pos)
                    (when (and (not (Pa_not_at_1)))
                          (not (Pa_at_1)))))

    (:action sense
        :precondition (and (at_2))
        :effect (and
                    ; #62813: <==closure== 65537 (pos)
                    (Pa_q)

                    ; #65537: origin
                    (Ba_q)

                    ; #71693: <==negation-removal== 62813 (pos)
                    (not (Ba_not_q))

                    ; #84264: <==negation-removal== 65537 (pos)
                    (not (Pa_not_q))))

    (:action shout-1
        :precondition (and (Ba_q)
                           (at_1))
        :effect (and
                    ; #56318: origin
                    (Bb_q)

                    ; #66346: <==closure== 56318 (pos)
                    (Pb_q)

                    ; #24014: <==negation-removal== 56318 (pos)
                    (not (Pb_not_q))

                    ; #26522: <==negation-removal== 66346 (pos)
                    (not (Bb_not_q))))

    (:action shout-2
        :precondition (and (at_2)
                           (Ba_q))
        :effect (and
                    ; #20027: origin
                    (Bc_q)

                    ; #32911: <==closure== 20027 (pos)
                    (Pc_q)

                    ; #56318: origin
                    (Bb_q)

                    ; #66346: <==closure== 56318 (pos)
                    (Pb_q)

                    ; #24014: <==negation-removal== 56318 (pos)
                    (not (Pb_not_q))

                    ; #26522: <==negation-removal== 66346 (pos)
                    (not (Bb_not_q))

                    ; #38692: <==negation-removal== 20027 (pos)
                    (not (Pc_not_q))

                    ; #54948: <==negation-removal== 32911 (pos)
                    (not (Bc_not_q))))

    (:action shout-3
        :precondition (and (at_3)
                           (Ba_q))
        :effect (and
                    ; #20027: origin
                    (Bc_q)

                    ; #32911: <==closure== 20027 (pos)
                    (Pc_q)

                    ; #56318: origin
                    (Bb_q)

                    ; #66346: <==closure== 56318 (pos)
                    (Pb_q)

                    ; #24014: <==negation-removal== 56318 (pos)
                    (not (Pb_not_q))

                    ; #26522: <==negation-removal== 66346 (pos)
                    (not (Bb_not_q))

                    ; #38692: <==negation-removal== 20027 (pos)
                    (not (Pc_not_q))

                    ; #54948: <==negation-removal== 32911 (pos)
                    (not (Bc_not_q))))

    (:action shout-4
        :precondition (and (Ba_q)
                           (at_4))
        :effect (and
                    ; #20027: origin
                    (Bc_q)

                    ; #32911: <==closure== 20027 (pos)
                    (Pc_q)

                    ; #38692: <==negation-removal== 20027 (pos)
                    (not (Pc_not_q))

                    ; #54948: <==negation-removal== 32911 (pos)
                    (not (Bc_not_q))))

)