(define (domain pdkb-planning)

    (:requirements :strips :conditional-effects)

    (:predicates
        (not_at_a_1)
        (not_at_a_2)
        (not_at_b_1)
        (not_at_b_2)
        (not_at_c_1)
        (not_at_c_2)
        (not_at_d_1)
        (not_at_d_2)
        (not_sa)
        (not_sb)
        (not_sc)
        (not_sd)
        (Ba_not_at_a_1)
        (Ba_not_at_a_2)
        (Ba_not_at_b_1)
        (Ba_not_at_b_2)
        (Ba_not_at_c_1)
        (Ba_not_at_c_2)
        (Ba_not_at_d_1)
        (Ba_not_at_d_2)
        (Ba_not_sa)
        (Ba_not_sb)
        (Ba_not_sc)
        (Ba_not_sd)
        (Ba_at_a_1)
        (Ba_at_a_2)
        (Ba_at_b_1)
        (Ba_at_b_2)
        (Ba_at_c_1)
        (Ba_at_c_2)
        (Ba_at_d_1)
        (Ba_at_d_2)
        (Ba_sa)
        (Ba_sb)
        (Ba_sc)
        (Ba_sd)
        (Bb_not_at_a_1)
        (Bb_not_at_a_2)
        (Bb_not_at_b_1)
        (Bb_not_at_b_2)
        (Bb_not_at_c_1)
        (Bb_not_at_c_2)
        (Bb_not_at_d_1)
        (Bb_not_at_d_2)
        (Bb_not_sa)
        (Bb_not_sb)
        (Bb_not_sc)
        (Bb_not_sd)
        (Bb_at_a_1)
        (Bb_at_a_2)
        (Bb_at_b_1)
        (Bb_at_b_2)
        (Bb_at_c_1)
        (Bb_at_c_2)
        (Bb_at_d_1)
        (Bb_at_d_2)
        (Bb_sa)
        (Bb_sb)
        (Bb_sc)
        (Bb_sd)
        (Bc_not_at_a_1)
        (Bc_not_at_a_2)
        (Bc_not_at_b_1)
        (Bc_not_at_b_2)
        (Bc_not_at_c_1)
        (Bc_not_at_c_2)
        (Bc_not_at_d_1)
        (Bc_not_at_d_2)
        (Bc_not_sa)
        (Bc_not_sb)
        (Bc_not_sc)
        (Bc_not_sd)
        (Bc_at_a_1)
        (Bc_at_a_2)
        (Bc_at_b_1)
        (Bc_at_b_2)
        (Bc_at_c_1)
        (Bc_at_c_2)
        (Bc_at_d_1)
        (Bc_at_d_2)
        (Bc_sa)
        (Bc_sb)
        (Bc_sc)
        (Bc_sd)
        (Bd_not_at_a_1)
        (Bd_not_at_a_2)
        (Bd_not_at_b_1)
        (Bd_not_at_b_2)
        (Bd_not_at_c_1)
        (Bd_not_at_c_2)
        (Bd_not_at_d_1)
        (Bd_not_at_d_2)
        (Bd_not_sa)
        (Bd_not_sb)
        (Bd_not_sc)
        (Bd_not_sd)
        (Bd_at_a_1)
        (Bd_at_a_2)
        (Bd_at_b_1)
        (Bd_at_b_2)
        (Bd_at_c_1)
        (Bd_at_c_2)
        (Bd_at_d_1)
        (Bd_at_d_2)
        (Bd_sa)
        (Bd_sb)
        (Bd_sc)
        (Bd_sd)
        (Pa_not_at_a_1)
        (Pa_not_at_a_2)
        (Pa_not_at_b_1)
        (Pa_not_at_b_2)
        (Pa_not_at_c_1)
        (Pa_not_at_c_2)
        (Pa_not_at_d_1)
        (Pa_not_at_d_2)
        (Pa_not_sa)
        (Pa_not_sb)
        (Pa_not_sc)
        (Pa_not_sd)
        (Pa_at_a_1)
        (Pa_at_a_2)
        (Pa_at_b_1)
        (Pa_at_b_2)
        (Pa_at_c_1)
        (Pa_at_c_2)
        (Pa_at_d_1)
        (Pa_at_d_2)
        (Pa_sa)
        (Pa_sb)
        (Pa_sc)
        (Pa_sd)
        (Pb_not_at_a_1)
        (Pb_not_at_a_2)
        (Pb_not_at_b_1)
        (Pb_not_at_b_2)
        (Pb_not_at_c_1)
        (Pb_not_at_c_2)
        (Pb_not_at_d_1)
        (Pb_not_at_d_2)
        (Pb_not_sa)
        (Pb_not_sb)
        (Pb_not_sc)
        (Pb_not_sd)
        (Pb_at_a_1)
        (Pb_at_a_2)
        (Pb_at_b_1)
        (Pb_at_b_2)
        (Pb_at_c_1)
        (Pb_at_c_2)
        (Pb_at_d_1)
        (Pb_at_d_2)
        (Pb_sa)
        (Pb_sb)
        (Pb_sc)
        (Pb_sd)
        (Pc_not_at_a_1)
        (Pc_not_at_a_2)
        (Pc_not_at_b_1)
        (Pc_not_at_b_2)
        (Pc_not_at_c_1)
        (Pc_not_at_c_2)
        (Pc_not_at_d_1)
        (Pc_not_at_d_2)
        (Pc_not_sa)
        (Pc_not_sb)
        (Pc_not_sc)
        (Pc_not_sd)
        (Pc_at_a_1)
        (Pc_at_a_2)
        (Pc_at_b_1)
        (Pc_at_b_2)
        (Pc_at_c_1)
        (Pc_at_c_2)
        (Pc_at_d_1)
        (Pc_at_d_2)
        (Pc_sa)
        (Pc_sb)
        (Pc_sc)
        (Pc_sd)
        (Pd_not_at_a_1)
        (Pd_not_at_a_2)
        (Pd_not_at_b_1)
        (Pd_not_at_b_2)
        (Pd_not_at_c_1)
        (Pd_not_at_c_2)
        (Pd_not_at_d_1)
        (Pd_not_at_d_2)
        (Pd_not_sa)
        (Pd_not_sb)
        (Pd_not_sc)
        (Pd_not_sd)
        (Pd_at_a_1)
        (Pd_at_a_2)
        (Pd_at_b_1)
        (Pd_at_b_2)
        (Pd_at_c_1)
        (Pd_at_c_2)
        (Pd_at_d_1)
        (Pd_at_d_2)
        (Pd_sa)
        (Pd_sb)
        (Pd_sc)
        (Pd_sd)
        (at_a_1)
        (at_a_2)
        (at_b_1)
        (at_b_2)
        (at_c_1)
        (at_c_2)
        (at_d_1)
        (at_d_2)
        (sa)
        (sb)
        (sc)
        (sd)
    )

    (:action left_a
        :precondition (and (at_a_2))
        :effect (and
                    ; #10198: <==closure== 83587 (pos)
                    (Pc_not_at_a_2)

                    ; #11666: <==closure== 91206 (pos)
                    (Pb_at_a_1)

                    ; #13391: <==closure== 16925 (pos)
                    (Pd_not_at_a_2)

                    ; #13397: <==closure== 24541 (pos)
                    (Pc_at_a_1)

                    ; #15573: <==commonly_known== 24820 (pos)
                    (Bd_at_a_1)

                    ; #16476: <==closure== 18731 (pos)
                    (Pa_not_at_a_2)

                    ; #16897: <==closure== 23000 (pos)
                    (Pa_at_a_1)

                    ; #16925: <==commonly_known== 96397 (pos)
                    (Bd_not_at_a_2)

                    ; #17262: <==closure== 68167 (pos)
                    (Pb_not_at_a_2)

                    ; #18731: <==commonly_known== 96397 (pos)
                    (Ba_not_at_a_2)

                    ; #23000: <==commonly_known== 24820 (pos)
                    (Ba_at_a_1)

                    ; #24541: <==commonly_known== 24820 (pos)
                    (Bc_at_a_1)

                    ; #24820: origin
                    (at_a_1)

                    ; #54160: <==closure== 15573 (pos)
                    (Pd_at_a_1)

                    ; #68167: <==commonly_known== 96397 (pos)
                    (Bb_not_at_a_2)

                    ; #83587: <==commonly_known== 96397 (pos)
                    (Bc_not_at_a_2)

                    ; #91206: <==commonly_known== 24820 (pos)
                    (Bb_at_a_1)

                    ; #96397: origin
                    (not_at_a_2)

                    ; #10208: <==negation-removal== 24541 (pos)
                    (not (Pc_not_at_a_1))

                    ; #11666: <==negation-removal== 68167 (pos)
                    (not (Pb_at_a_2))

                    ; #13391: <==negation-removal== 15573 (pos)
                    (not (Pd_not_at_a_1))

                    ; #13397: <==negation-removal== 83587 (pos)
                    (not (Pc_at_a_2))

                    ; #15573: <==negation-removal== 13391 (pos)
                    (not (Bd_at_a_2))

                    ; #16486: <==negation-removal== 23000 (pos)
                    (not (Pa_not_at_a_1))

                    ; #16897: <==negation-removal== 18731 (pos)
                    (not (Pa_at_a_2))

                    ; #16925: <==negation-removal== 54160 (pos)
                    (not (Bd_not_at_a_1))

                    ; #17262: <==negation-removal== 91206 (pos)
                    (not (Pb_not_at_a_1))

                    ; #18731: <==negation-removal== 16897 (pos)
                    (not (Ba_not_at_a_1))

                    ; #23000: <==negation-removal== 16476 (pos)
                    (not (Ba_at_a_2))

                    ; #24520: <==negation-removal== 96397 (pos)
                    (not (at_a_2))

                    ; #24541: <==negation-removal== 10198 (pos)
                    (not (Bc_at_a_2))

                    ; #54160: <==negation-removal== 16925 (pos)
                    (not (Pd_at_a_2))

                    ; #68167: <==negation-removal== 11666 (pos)
                    (not (Bb_not_at_a_1))

                    ; #83487: <==negation-removal== 13397 (pos)
                    (not (Bc_not_at_a_1))

                    ; #91206: <==negation-removal== 17262 (pos)
                    (not (Bb_at_a_2))

                    ; #96397: <==negation-removal== 24820 (pos)
                    (not (not_at_a_1))))

    (:action left_b
        :precondition (and (at_b_2))
        :effect (and
                    ; #10878: <==closure== 68888 (pos)
                    (Pa_not_at_b_2)

                    ; #11783: <==commonly_known== 19252 (pos)
                    (Bc_not_at_b_2)

                    ; #13574: <==commonly_known== 87061 (pos)
                    (Bd_at_b_1)

                    ; #17577: <==closure== 17591 (pos)
                    (Pd_not_at_b_2)

                    ; #17591: <==commonly_known== 19252 (pos)
                    (Bd_not_at_b_2)

                    ; #18597: <==closure== 40984 (pos)
                    (Pc_at_b_1)

                    ; #18656: <==closure== 11783 (pos)
                    (Pc_not_at_b_2)

                    ; #19252: origin
                    (not_at_b_2)

                    ; #19311: <==closure== 13574 (pos)
                    (Pd_at_b_1)

                    ; #19734: <==closure== 64225 (pos)
                    (Pb_not_at_b_2)

                    ; #19985: <==commonly_known== 87061 (pos)
                    (Bb_at_b_1)

                    ; #28184: <==closure== 85642 (pos)
                    (Pa_at_b_1)

                    ; #40984: <==commonly_known== 87061 (pos)
                    (Bc_at_b_1)

                    ; #64225: <==commonly_known== 19252 (pos)
                    (Bb_not_at_b_2)

                    ; #68888: <==commonly_known== 19252 (pos)
                    (Ba_not_at_b_2)

                    ; #85642: <==commonly_known== 87061 (pos)
                    (Ba_at_b_1)

                    ; #87061: origin
                    (at_b_1)

                    ; #98376: <==closure== 19985 (pos)
                    (Pb_at_b_1)

                    ; #10868: <==negation-removal== 85642 (pos)
                    (not (Pa_not_at_b_1))

                    ; #11783: <==negation-removal== 18597 (pos)
                    (not (Bc_not_at_b_1))

                    ; #13574: <==negation-removal== 17577 (pos)
                    (not (Bd_at_b_2))

                    ; #17577: <==negation-removal== 13574 (pos)
                    (not (Pd_not_at_b_1))

                    ; #17591: <==negation-removal== 19311 (pos)
                    (not (Bd_not_at_b_1))

                    ; #18597: <==negation-removal== 11783 (pos)
                    (not (Pc_at_b_2))

                    ; #18656: <==negation-removal== 40984 (pos)
                    (not (Pc_not_at_b_1))

                    ; #19252: <==negation-removal== 87061 (pos)
                    (not (not_at_b_1))

                    ; #19311: <==negation-removal== 17591 (pos)
                    (not (Pd_at_b_2))

                    ; #19724: <==negation-removal== 19985 (pos)
                    (not (Pb_not_at_b_1))

                    ; #20085: <==negation-removal== 19734 (pos)
                    (not (Bb_at_b_2))

                    ; #28184: <==negation-removal== 68888 (pos)
                    (not (Pa_at_b_2))

                    ; #40984: <==negation-removal== 18656 (pos)
                    (not (Bc_at_b_2))

                    ; #64225: <==negation-removal== 98376 (pos)
                    (not (Bb_not_at_b_1))

                    ; #68888: <==negation-removal== 28184 (pos)
                    (not (Ba_not_at_b_1))

                    ; #85642: <==negation-removal== 10878 (pos)
                    (not (Ba_at_b_2))

                    ; #87061: <==negation-removal== 19252 (pos)
                    (not (at_b_2))

                    ; #98376: <==negation-removal== 64225 (pos)
                    (not (Pb_at_b_2))))

    (:action left_c
        :precondition (and (at_c_2))
        :effect (and
                    ; #11400: <==commonly_known== 16938 (pos)
                    (Bd_at_c_1)

                    ; #12212: <==closure== 15135 (pos)
                    (Pd_not_at_c_2)

                    ; #13836: <==closure== 32783 (pos)
                    (Pb_not_at_c_2)

                    ; #15135: <==commonly_known== 79537 (pos)
                    (Bd_not_at_c_2)

                    ; #15285: <==commonly_known== 79537 (pos)
                    (Ba_not_at_c_2)

                    ; #16626: <==commonly_known== 79537 (pos)
                    (Bc_not_at_c_2)

                    ; #16938: origin
                    (at_c_1)

                    ; #17919: <==commonly_known== 16938 (pos)
                    (Bc_at_c_1)

                    ; #18313: <==closure== 11400 (pos)
                    (Pd_at_c_1)

                    ; #19235: <==commonly_known== 16938 (pos)
                    (Bb_at_c_1)

                    ; #31096: <==closure== 17919 (pos)
                    (Pc_at_c_1)

                    ; #32783: <==commonly_known== 79537 (pos)
                    (Bb_not_at_c_2)

                    ; #59459: <==closure== 15285 (pos)
                    (Pa_not_at_c_2)

                    ; #71880: <==closure== 16626 (pos)
                    (Pc_not_at_c_2)

                    ; #76740: <==closure== 19235 (pos)
                    (Pb_at_c_1)

                    ; #76808: <==commonly_known== 16938 (pos)
                    (Ba_at_c_1)

                    ; #79537: origin
                    (not_at_c_2)

                    ; #83401: <==closure== 76808 (pos)
                    (Pa_at_c_1)

                    ; #11400: <==negation-removal== 12212 (pos)
                    (not (Bd_at_c_2))

                    ; #12242: <==negation-removal== 11400 (pos)
                    (not (Pd_not_at_c_1))

                    ; #13836: <==negation-removal== 19235 (pos)
                    (not (Pb_not_at_c_1))

                    ; #15135: <==negation-removal== 18313 (pos)
                    (not (Bd_not_at_c_1))

                    ; #15275: <==negation-removal== 83401 (pos)
                    (not (Ba_not_at_c_1))

                    ; #16626: <==negation-removal== 31096 (pos)
                    (not (Bc_not_at_c_1))

                    ; #16938: <==negation-removal== 79537 (pos)
                    (not (at_c_2))

                    ; #17919: <==negation-removal== 71880 (pos)
                    (not (Bc_at_c_2))

                    ; #18313: <==negation-removal== 15135 (pos)
                    (not (Pd_at_c_2))

                    ; #19235: <==negation-removal== 13836 (pos)
                    (not (Bb_at_c_2))

                    ; #31096: <==negation-removal== 16626 (pos)
                    (not (Pc_at_c_2))

                    ; #32783: <==negation-removal== 76740 (pos)
                    (not (Bb_not_at_c_1))

                    ; #59459: <==negation-removal== 76808 (pos)
                    (not (Pa_not_at_c_1))

                    ; #71880: <==negation-removal== 17919 (pos)
                    (not (Pc_not_at_c_1))

                    ; #76640: <==negation-removal== 32783 (pos)
                    (not (Pb_at_c_2))

                    ; #76808: <==negation-removal== 59459 (pos)
                    (not (Ba_at_c_2))

                    ; #79537: <==negation-removal== 16938 (pos)
                    (not (not_at_c_1))

                    ; #83501: <==negation-removal== 15285 (pos)
                    (not (Pa_at_c_2))))

    (:action left_d
        :precondition (and (at_d_2))
        :effect (and
                    ; #10014: <==closure== 43701 (pos)
                    (Pa_not_at_d_2)

                    ; #10419: <==closure== 69440 (pos)
                    (Pc_not_at_d_2)

                    ; #10451: <==commonly_known== 43519 (pos)
                    (Ba_at_d_1)

                    ; #12270: <==commonly_known== 43519 (pos)
                    (Bc_at_d_1)

                    ; #13784: <==commonly_known== 18662 (pos)
                    (Bb_not_at_d_2)

                    ; #15999: <==closure== 10451 (pos)
                    (Pa_at_d_1)

                    ; #16617: <==closure== 12270 (pos)
                    (Pc_at_d_1)

                    ; #17922: <==commonly_known== 43519 (pos)
                    (Bd_at_d_1)

                    ; #18031: <==closure== 85509 (pos)
                    (Pb_at_d_1)

                    ; #18540: <==closure== 23758 (pos)
                    (Pd_not_at_d_2)

                    ; #18662: origin
                    (not_at_d_2)

                    ; #23758: <==commonly_known== 18662 (pos)
                    (Bd_not_at_d_2)

                    ; #24287: <==closure== 17922 (pos)
                    (Pd_at_d_1)

                    ; #43519: origin
                    (at_d_1)

                    ; #43701: <==commonly_known== 18662 (pos)
                    (Ba_not_at_d_2)

                    ; #57991: <==closure== 13784 (pos)
                    (Pb_not_at_d_2)

                    ; #69440: <==commonly_known== 18662 (pos)
                    (Bc_not_at_d_2)

                    ; #85509: <==commonly_known== 43519 (pos)
                    (Bb_at_d_1)

                    ; #10014: <==negation-removal== 10451 (pos)
                    (not (Pa_not_at_d_1))

                    ; #10419: <==negation-removal== 12270 (pos)
                    (not (Pc_not_at_d_1))

                    ; #10451: <==negation-removal== 10014 (pos)
                    (not (Ba_at_d_2))

                    ; #12270: <==negation-removal== 10419 (pos)
                    (not (Bc_at_d_2))

                    ; #13784: <==negation-removal== 18031 (pos)
                    (not (Bb_not_at_d_1))

                    ; #15999: <==negation-removal== 43701 (pos)
                    (not (Pa_at_d_2))

                    ; #16617: <==negation-removal== 69440 (pos)
                    (not (Pc_at_d_2))

                    ; #17912: <==negation-removal== 18540 (pos)
                    (not (Bd_at_d_2))

                    ; #18031: <==negation-removal== 13784 (pos)
                    (not (Pb_at_d_2))

                    ; #18530: <==negation-removal== 17922 (pos)
                    (not (Pd_not_at_d_1))

                    ; #18662: <==negation-removal== 43519 (pos)
                    (not (not_at_d_1))

                    ; #23758: <==negation-removal== 24287 (pos)
                    (not (Bd_not_at_d_1))

                    ; #24287: <==negation-removal== 23758 (pos)
                    (not (Pd_at_d_2))

                    ; #43519: <==negation-removal== 18662 (pos)
                    (not (at_d_2))

                    ; #43701: <==negation-removal== 15999 (pos)
                    (not (Ba_not_at_d_1))

                    ; #57991: <==negation-removal== 85509 (pos)
                    (not (Pb_not_at_d_1))

                    ; #69440: <==negation-removal== 16617 (pos)
                    (not (Bc_not_at_d_1))

                    ; #85509: <==negation-removal== 57991 (pos)
                    (not (Bb_at_d_2))))

    (:action right_a
        :precondition (and (at_a_1))
        :effect (and
                    ; #10208: <==closure== 83487 (pos)
                    (Pc_not_at_a_1)

                    ; #11666: <==closure== 91206 (pos)
                    (Pb_at_a_2)

                    ; #13391: <==closure== 16925 (pos)
                    (Pd_not_at_a_1)

                    ; #13397: <==closure== 24541 (pos)
                    (Pc_at_a_2)

                    ; #15573: <==commonly_known== 24520 (pos)
                    (Bd_at_a_2)

                    ; #16486: <==closure== 18731 (pos)
                    (Pa_not_at_a_1)

                    ; #16897: <==closure== 23000 (pos)
                    (Pa_at_a_2)

                    ; #16925: <==commonly_known== 96397 (pos)
                    (Bd_not_at_a_1)

                    ; #17262: <==closure== 68167 (pos)
                    (Pb_not_at_a_1)

                    ; #18731: <==commonly_known== 96397 (pos)
                    (Ba_not_at_a_1)

                    ; #23000: <==commonly_known== 24520 (pos)
                    (Ba_at_a_2)

                    ; #24520: origin
                    (at_a_2)

                    ; #24541: <==commonly_known== 24520 (pos)
                    (Bc_at_a_2)

                    ; #54160: <==closure== 15573 (pos)
                    (Pd_at_a_2)

                    ; #68167: <==commonly_known== 96397 (pos)
                    (Bb_not_at_a_1)

                    ; #83487: <==commonly_known== 96397 (pos)
                    (Bc_not_at_a_1)

                    ; #91206: <==commonly_known== 24520 (pos)
                    (Bb_at_a_2)

                    ; #96397: origin
                    (not_at_a_1)

                    ; #10198: <==negation-removal== 24541 (pos)
                    (not (Pc_not_at_a_2))

                    ; #11666: <==negation-removal== 68167 (pos)
                    (not (Pb_at_a_1))

                    ; #13391: <==negation-removal== 15573 (pos)
                    (not (Pd_not_at_a_2))

                    ; #13397: <==negation-removal== 83487 (pos)
                    (not (Pc_at_a_1))

                    ; #15573: <==negation-removal== 13391 (pos)
                    (not (Bd_at_a_1))

                    ; #16476: <==negation-removal== 23000 (pos)
                    (not (Pa_not_at_a_2))

                    ; #16897: <==negation-removal== 18731 (pos)
                    (not (Pa_at_a_1))

                    ; #16925: <==negation-removal== 54160 (pos)
                    (not (Bd_not_at_a_2))

                    ; #17262: <==negation-removal== 91206 (pos)
                    (not (Pb_not_at_a_2))

                    ; #18731: <==negation-removal== 16897 (pos)
                    (not (Ba_not_at_a_2))

                    ; #23000: <==negation-removal== 16486 (pos)
                    (not (Ba_at_a_1))

                    ; #24541: <==negation-removal== 10208 (pos)
                    (not (Bc_at_a_1))

                    ; #24820: <==negation-removal== 96397 (pos)
                    (not (at_a_1))

                    ; #54160: <==negation-removal== 16925 (pos)
                    (not (Pd_at_a_1))

                    ; #68167: <==negation-removal== 11666 (pos)
                    (not (Bb_not_at_a_2))

                    ; #83587: <==negation-removal== 13397 (pos)
                    (not (Bc_not_at_a_2))

                    ; #91206: <==negation-removal== 17262 (pos)
                    (not (Bb_at_a_1))

                    ; #96397: <==negation-removal== 24520 (pos)
                    (not (not_at_a_2))))

    (:action right_b
        :precondition (and (at_b_1))
        :effect (and
                    ; #10868: <==closure== 68888 (pos)
                    (Pa_not_at_b_1)

                    ; #11783: <==commonly_known== 19252 (pos)
                    (Bc_not_at_b_1)

                    ; #13574: <==commonly_known== 87061 (pos)
                    (Bd_at_b_2)

                    ; #17577: <==closure== 17591 (pos)
                    (Pd_not_at_b_1)

                    ; #17591: <==commonly_known== 19252 (pos)
                    (Bd_not_at_b_1)

                    ; #18597: <==closure== 40984 (pos)
                    (Pc_at_b_2)

                    ; #18656: <==closure== 11783 (pos)
                    (Pc_not_at_b_1)

                    ; #19252: origin
                    (not_at_b_1)

                    ; #19311: <==closure== 13574 (pos)
                    (Pd_at_b_2)

                    ; #19724: <==closure== 64225 (pos)
                    (Pb_not_at_b_1)

                    ; #20085: <==commonly_known== 87061 (pos)
                    (Bb_at_b_2)

                    ; #28184: <==closure== 85642 (pos)
                    (Pa_at_b_2)

                    ; #40984: <==commonly_known== 87061 (pos)
                    (Bc_at_b_2)

                    ; #64225: <==commonly_known== 19252 (pos)
                    (Bb_not_at_b_1)

                    ; #68888: <==commonly_known== 19252 (pos)
                    (Ba_not_at_b_1)

                    ; #85642: <==commonly_known== 87061 (pos)
                    (Ba_at_b_2)

                    ; #87061: origin
                    (at_b_2)

                    ; #98376: <==closure== 20085 (pos)
                    (Pb_at_b_2)

                    ; #10878: <==negation-removal== 85642 (pos)
                    (not (Pa_not_at_b_2))

                    ; #11783: <==negation-removal== 18597 (pos)
                    (not (Bc_not_at_b_2))

                    ; #13574: <==negation-removal== 17577 (pos)
                    (not (Bd_at_b_1))

                    ; #17577: <==negation-removal== 13574 (pos)
                    (not (Pd_not_at_b_2))

                    ; #17591: <==negation-removal== 19311 (pos)
                    (not (Bd_not_at_b_2))

                    ; #18597: <==negation-removal== 11783 (pos)
                    (not (Pc_at_b_1))

                    ; #18656: <==negation-removal== 40984 (pos)
                    (not (Pc_not_at_b_2))

                    ; #19252: <==negation-removal== 87061 (pos)
                    (not (not_at_b_2))

                    ; #19311: <==negation-removal== 17591 (pos)
                    (not (Pd_at_b_1))

                    ; #19734: <==negation-removal== 20085 (pos)
                    (not (Pb_not_at_b_2))

                    ; #19985: <==negation-removal== 19724 (pos)
                    (not (Bb_at_b_1))

                    ; #28184: <==negation-removal== 68888 (pos)
                    (not (Pa_at_b_1))

                    ; #40984: <==negation-removal== 18656 (pos)
                    (not (Bc_at_b_1))

                    ; #64225: <==negation-removal== 98376 (pos)
                    (not (Bb_not_at_b_2))

                    ; #68888: <==negation-removal== 28184 (pos)
                    (not (Ba_not_at_b_2))

                    ; #85642: <==negation-removal== 10868 (pos)
                    (not (Ba_at_b_1))

                    ; #87061: <==negation-removal== 19252 (pos)
                    (not (at_b_1))

                    ; #98376: <==negation-removal== 64225 (pos)
                    (not (Pb_at_b_1))))

    (:action right_c
        :precondition (and (at_c_1))
        :effect (and
                    ; #11400: <==commonly_known== 16938 (pos)
                    (Bd_at_c_2)

                    ; #12242: <==closure== 15135 (pos)
                    (Pd_not_at_c_1)

                    ; #13836: <==closure== 32783 (pos)
                    (Pb_not_at_c_1)

                    ; #15135: <==commonly_known== 79537 (pos)
                    (Bd_not_at_c_1)

                    ; #15275: <==commonly_known== 79537 (pos)
                    (Ba_not_at_c_1)

                    ; #16626: <==commonly_known== 79537 (pos)
                    (Bc_not_at_c_1)

                    ; #16938: origin
                    (at_c_2)

                    ; #17919: <==commonly_known== 16938 (pos)
                    (Bc_at_c_2)

                    ; #18313: <==closure== 11400 (pos)
                    (Pd_at_c_2)

                    ; #19235: <==commonly_known== 16938 (pos)
                    (Bb_at_c_2)

                    ; #31096: <==closure== 17919 (pos)
                    (Pc_at_c_2)

                    ; #32783: <==commonly_known== 79537 (pos)
                    (Bb_not_at_c_1)

                    ; #59459: <==closure== 15275 (pos)
                    (Pa_not_at_c_1)

                    ; #71880: <==closure== 16626 (pos)
                    (Pc_not_at_c_1)

                    ; #76640: <==closure== 19235 (pos)
                    (Pb_at_c_2)

                    ; #76808: <==commonly_known== 16938 (pos)
                    (Ba_at_c_2)

                    ; #79537: origin
                    (not_at_c_1)

                    ; #83501: <==closure== 76808 (pos)
                    (Pa_at_c_2)

                    ; #11400: <==negation-removal== 12242 (pos)
                    (not (Bd_at_c_1))

                    ; #12212: <==negation-removal== 11400 (pos)
                    (not (Pd_not_at_c_2))

                    ; #13836: <==negation-removal== 19235 (pos)
                    (not (Pb_not_at_c_2))

                    ; #15135: <==negation-removal== 18313 (pos)
                    (not (Bd_not_at_c_2))

                    ; #15285: <==negation-removal== 83501 (pos)
                    (not (Ba_not_at_c_2))

                    ; #16626: <==negation-removal== 31096 (pos)
                    (not (Bc_not_at_c_2))

                    ; #16938: <==negation-removal== 79537 (pos)
                    (not (at_c_1))

                    ; #17919: <==negation-removal== 71880 (pos)
                    (not (Bc_at_c_1))

                    ; #18313: <==negation-removal== 15135 (pos)
                    (not (Pd_at_c_1))

                    ; #19235: <==negation-removal== 13836 (pos)
                    (not (Bb_at_c_1))

                    ; #31096: <==negation-removal== 16626 (pos)
                    (not (Pc_at_c_1))

                    ; #32783: <==negation-removal== 76640 (pos)
                    (not (Bb_not_at_c_2))

                    ; #59459: <==negation-removal== 76808 (pos)
                    (not (Pa_not_at_c_2))

                    ; #71880: <==negation-removal== 17919 (pos)
                    (not (Pc_not_at_c_2))

                    ; #76740: <==negation-removal== 32783 (pos)
                    (not (Pb_at_c_1))

                    ; #76808: <==negation-removal== 59459 (pos)
                    (not (Ba_at_c_1))

                    ; #79537: <==negation-removal== 16938 (pos)
                    (not (not_at_c_2))

                    ; #83401: <==negation-removal== 15275 (pos)
                    (not (Pa_at_c_1))))

    (:action right_d
        :precondition (and (at_d_1))
        :effect (and
                    ; #10014: <==closure== 43701 (pos)
                    (Pa_not_at_d_1)

                    ; #10419: <==closure== 69440 (pos)
                    (Pc_not_at_d_1)

                    ; #10451: <==commonly_known== 43519 (pos)
                    (Ba_at_d_2)

                    ; #12270: <==commonly_known== 43519 (pos)
                    (Bc_at_d_2)

                    ; #13784: <==commonly_known== 18662 (pos)
                    (Bb_not_at_d_1)

                    ; #15999: <==closure== 10451 (pos)
                    (Pa_at_d_2)

                    ; #16617: <==closure== 12270 (pos)
                    (Pc_at_d_2)

                    ; #17912: <==commonly_known== 43519 (pos)
                    (Bd_at_d_2)

                    ; #18031: <==closure== 85509 (pos)
                    (Pb_at_d_2)

                    ; #18530: <==closure== 23758 (pos)
                    (Pd_not_at_d_1)

                    ; #18662: origin
                    (not_at_d_1)

                    ; #23758: <==commonly_known== 18662 (pos)
                    (Bd_not_at_d_1)

                    ; #24287: <==closure== 17912 (pos)
                    (Pd_at_d_2)

                    ; #43519: origin
                    (at_d_2)

                    ; #43701: <==commonly_known== 18662 (pos)
                    (Ba_not_at_d_1)

                    ; #57991: <==closure== 13784 (pos)
                    (Pb_not_at_d_1)

                    ; #69440: <==commonly_known== 18662 (pos)
                    (Bc_not_at_d_1)

                    ; #85509: <==commonly_known== 43519 (pos)
                    (Bb_at_d_2)

                    ; #10014: <==negation-removal== 10451 (pos)
                    (not (Pa_not_at_d_2))

                    ; #10419: <==negation-removal== 12270 (pos)
                    (not (Pc_not_at_d_2))

                    ; #10451: <==negation-removal== 10014 (pos)
                    (not (Ba_at_d_1))

                    ; #12270: <==negation-removal== 10419 (pos)
                    (not (Bc_at_d_1))

                    ; #13784: <==negation-removal== 18031 (pos)
                    (not (Bb_not_at_d_2))

                    ; #15999: <==negation-removal== 43701 (pos)
                    (not (Pa_at_d_1))

                    ; #16617: <==negation-removal== 69440 (pos)
                    (not (Pc_at_d_1))

                    ; #17922: <==negation-removal== 18530 (pos)
                    (not (Bd_at_d_1))

                    ; #18031: <==negation-removal== 13784 (pos)
                    (not (Pb_at_d_1))

                    ; #18540: <==negation-removal== 17912 (pos)
                    (not (Pd_not_at_d_2))

                    ; #18662: <==negation-removal== 43519 (pos)
                    (not (not_at_d_2))

                    ; #23758: <==negation-removal== 24287 (pos)
                    (not (Bd_not_at_d_2))

                    ; #24287: <==negation-removal== 23758 (pos)
                    (not (Pd_at_d_1))

                    ; #43519: <==negation-removal== 18662 (pos)
                    (not (at_d_1))

                    ; #43701: <==negation-removal== 15999 (pos)
                    (not (Ba_not_at_d_2))

                    ; #57991: <==negation-removal== 85509 (pos)
                    (not (Pb_not_at_d_2))

                    ; #69440: <==negation-removal== 16617 (pos)
                    (not (Bc_not_at_d_2))

                    ; #85509: <==negation-removal== 57991 (pos)
                    (not (Bb_at_d_1))))

    (:action share_a_sa_1
        :precondition (and (Ba_sa)
                           (at_a_1))
        :effect (and
                    ; #10284: origin
                    (when (and (at_a_1))
                          (Ba_sa))

                    ; #11503: origin
                    (when (and (at_d_1))
                          (Bd_sa))

                    ; #16350: <==closure== 21359 (pos)
                    (when (and (at_c_1))
                          (Pc_sa))

                    ; #21359: origin
                    (when (and (at_c_1))
                          (Bc_sa))

                    ; #34318: <==closure== 10284 (pos)
                    (when (and (at_a_1))
                          (Pa_sa))

                    ; #44889: <==closure== 63783 (pos)
                    (when (and (at_b_1))
                          (Pb_sa))

                    ; #63783: origin
                    (when (and (at_b_1))
                          (Bb_sa))

                    ; #96915: <==closure== 11503 (pos)
                    (when (and (at_d_1))
                          (Pd_sa))

                    ; #10383: <==negation-removal== 16350 (pos)
                    (when (and (at_c_1))
                          (not (Bc_not_sa)))

                    ; #12575: <==negation-removal== 96915 (pos)
                    (when (and (at_d_1))
                          (not (Bd_not_sa)))

                    ; #12586: <==uncertain_firing== 63783 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Pb_not_sa)))

                    ; #12766: <==negation-removal== 44889 (pos)
                    (when (and (at_b_1))
                          (not (Bb_not_sa)))

                    ; #14166: <==uncertain_firing== 16350 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Bc_not_sa)))

                    ; #14189: <==uncertain_firing== 11503 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Pd_not_sa)))

                    ; #14682: <==uncertain_firing== 21359 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Pc_not_sa)))

                    ; #15085: <==negation-removal== 11503 (pos)
                    (when (and (at_d_1))
                          (not (Pd_not_sa)))

                    ; #16056: <==uncertain_firing== 34318 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Ba_not_sa)))

                    ; #16213: <==uncertain_firing== 44889 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Bb_not_sa)))

                    ; #17237: <==negation-removal== 34318 (pos)
                    (when (and (at_a_1))
                          (not (Ba_not_sa)))

                    ; #21361: <==uncertain_firing== 96915 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Bd_not_sa)))

                    ; #27042: <==negation-removal== 63783 (pos)
                    (when (and (at_b_1))
                          (not (Pb_not_sa)))

                    ; #79792: <==uncertain_firing== 10284 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Pa_not_sa)))

                    ; #87462: <==negation-removal== 21359 (pos)
                    (when (and (at_c_1))
                          (not (Pc_not_sa)))

                    ; #88519: <==negation-removal== 10284 (pos)
                    (when (and (at_a_1))
                          (not (Pa_not_sa)))))

    (:action share_a_sa_2
        :precondition (and (Ba_sa)
                           (at_a_2))
        :effect (and
                    ; #12970: origin
                    (when (and (at_b_2))
                          (Bb_sa))

                    ; #13993: <==closure== 12970 (pos)
                    (when (and (at_b_2))
                          (Pb_sa))

                    ; #19924: <==closure== 75816 (pos)
                    (when (and (at_c_2))
                          (Pc_sa))

                    ; #20318: <==closure== 72770 (pos)
                    (when (and (at_a_2))
                          (Pa_sa))

                    ; #30274: <==closure== 99493 (pos)
                    (when (and (at_d_2))
                          (Pd_sa))

                    ; #72770: origin
                    (when (and (at_a_2))
                          (Ba_sa))

                    ; #75816: origin
                    (when (and (at_c_2))
                          (Bc_sa))

                    ; #99493: origin
                    (when (and (at_d_2))
                          (Bd_sa))

                    ; #10245: <==uncertain_firing== 72770 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Pa_not_sa)))

                    ; #10319: <==uncertain_firing== 99493 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Pd_not_sa)))

                    ; #10963: <==uncertain_firing== 20318 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Ba_not_sa)))

                    ; #11539: <==uncertain_firing== 19924 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Bc_not_sa)))

                    ; #12689: <==negation-removal== 12970 (pos)
                    (when (and (at_b_2))
                          (not (Pb_not_sa)))

                    ; #14783: <==negation-removal== 75816 (pos)
                    (when (and (at_c_2))
                          (not (Pc_not_sa)))

                    ; #15601: <==negation-removal== 19924 (pos)
                    (when (and (at_c_2))
                          (not (Bc_not_sa)))

                    ; #16030: <==uncertain_firing== 13993 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Bb_not_sa)))

                    ; #18632: <==negation-removal== 99493 (pos)
                    (when (and (at_d_2))
                          (not (Pd_not_sa)))

                    ; #18638: <==negation-removal== 20318 (pos)
                    (when (and (at_a_2))
                          (not (Ba_not_sa)))

                    ; #29110: <==uncertain_firing== 75816 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Pc_not_sa)))

                    ; #29258: <==negation-removal== 30274 (pos)
                    (when (and (at_d_2))
                          (not (Bd_not_sa)))

                    ; #38351: <==negation-removal== 13993 (pos)
                    (when (and (at_b_2))
                          (not (Bb_not_sa)))

                    ; #41716: <==uncertain_firing== 12970 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Pb_not_sa)))

                    ; #51605: <==uncertain_firing== 30274 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Bd_not_sa)))

                    ; #69717: <==negation-removal== 72770 (pos)
                    (when (and (at_a_2))
                          (not (Pa_not_sa)))))

    (:action share_a_sb_1
        :precondition (and (at_a_1)
                           (Ba_sb))
        :effect (and
                    ; #10284: origin
                    (when (and (at_a_1))
                          (Ba_sb))

                    ; #11493: origin
                    (when (and (at_d_1))
                          (Bd_sb))

                    ; #16350: <==closure== 21359 (pos)
                    (when (and (at_c_1))
                          (Pc_sb))

                    ; #21359: origin
                    (when (and (at_c_1))
                          (Bc_sb))

                    ; #34318: <==closure== 10284 (pos)
                    (when (and (at_a_1))
                          (Pa_sb))

                    ; #44789: <==closure== 63783 (pos)
                    (when (and (at_b_1))
                          (Pb_sb))

                    ; #63783: origin
                    (when (and (at_b_1))
                          (Bb_sb))

                    ; #97015: <==closure== 11493 (pos)
                    (when (and (at_d_1))
                          (Pd_sb))

                    ; #10383: <==negation-removal== 16350 (pos)
                    (when (and (at_c_1))
                          (not (Bc_not_sb)))

                    ; #12575: <==negation-removal== 97015 (pos)
                    (when (and (at_d_1))
                          (not (Bd_not_sb)))

                    ; #12586: <==uncertain_firing== 63783 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Pb_not_sb)))

                    ; #12766: <==negation-removal== 44789 (pos)
                    (when (and (at_b_1))
                          (not (Bb_not_sb)))

                    ; #14156: <==uncertain_firing== 16350 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Bc_not_sb)))

                    ; #14189: <==uncertain_firing== 11493 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Pd_not_sb)))

                    ; #14682: <==uncertain_firing== 21359 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Pc_not_sb)))

                    ; #15075: <==negation-removal== 11493 (pos)
                    (when (and (at_d_1))
                          (not (Pd_not_sb)))

                    ; #16056: <==uncertain_firing== 34318 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Ba_not_sb)))

                    ; #16213: <==uncertain_firing== 44789 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Bb_not_sb)))

                    ; #17267: <==negation-removal== 34318 (pos)
                    (when (and (at_a_1))
                          (not (Ba_not_sb)))

                    ; #21361: <==uncertain_firing== 97015 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Bd_not_sb)))

                    ; #27042: <==negation-removal== 63783 (pos)
                    (when (and (at_b_1))
                          (not (Pb_not_sb)))

                    ; #79792: <==uncertain_firing== 10284 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Pa_not_sb)))

                    ; #87462: <==negation-removal== 21359 (pos)
                    (when (and (at_c_1))
                          (not (Pc_not_sb)))

                    ; #88519: <==negation-removal== 10284 (pos)
                    (when (and (at_a_1))
                          (not (Pa_not_sb)))))

    (:action share_a_sb_2
        :precondition (and (at_a_2)
                           (Ba_sb))
        :effect (and
                    ; #12970: origin
                    (when (and (at_b_2))
                          (Bb_sb))

                    ; #13993: <==closure== 12970 (pos)
                    (when (and (at_b_2))
                          (Pb_sb))

                    ; #19924: <==closure== 75916 (pos)
                    (when (and (at_c_2))
                          (Pc_sb))

                    ; #20318: <==closure== 72770 (pos)
                    (when (and (at_a_2))
                          (Pa_sb))

                    ; #30274: <==closure== 99493 (pos)
                    (when (and (at_d_2))
                          (Pd_sb))

                    ; #72770: origin
                    (when (and (at_a_2))
                          (Ba_sb))

                    ; #75916: origin
                    (when (and (at_c_2))
                          (Bc_sb))

                    ; #99493: origin
                    (when (and (at_d_2))
                          (Bd_sb))

                    ; #10245: <==uncertain_firing== 72770 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Pa_not_sb)))

                    ; #10319: <==uncertain_firing== 99493 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Pd_not_sb)))

                    ; #10933: <==uncertain_firing== 20318 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Ba_not_sb)))

                    ; #11539: <==uncertain_firing== 19924 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Bc_not_sb)))

                    ; #12689: <==negation-removal== 12970 (pos)
                    (when (and (at_b_2))
                          (not (Pb_not_sb)))

                    ; #14783: <==negation-removal== 75916 (pos)
                    (when (and (at_c_2))
                          (not (Pc_not_sb)))

                    ; #15601: <==negation-removal== 19924 (pos)
                    (when (and (at_c_2))
                          (not (Bc_not_sb)))

                    ; #16030: <==uncertain_firing== 13993 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Bb_not_sb)))

                    ; #18628: <==negation-removal== 20318 (pos)
                    (when (and (at_a_2))
                          (not (Ba_not_sb)))

                    ; #18632: <==negation-removal== 99493 (pos)
                    (when (and (at_d_2))
                          (not (Pd_not_sb)))

                    ; #29110: <==uncertain_firing== 75916 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Pc_not_sb)))

                    ; #29258: <==negation-removal== 30274 (pos)
                    (when (and (at_d_2))
                          (not (Bd_not_sb)))

                    ; #38351: <==negation-removal== 13993 (pos)
                    (when (and (at_b_2))
                          (not (Bb_not_sb)))

                    ; #41616: <==uncertain_firing== 12970 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Pb_not_sb)))

                    ; #51605: <==uncertain_firing== 30274 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Bd_not_sb)))

                    ; #69717: <==negation-removal== 72770 (pos)
                    (when (and (at_a_2))
                          (not (Pa_not_sb)))))

    (:action share_a_sc_1
        :precondition (and (at_a_1)
                           (Ba_sc))
        :effect (and
                    ; #10284: origin
                    (when (and (at_a_1))
                          (Ba_sc))

                    ; #11493: origin
                    (when (and (at_d_1))
                          (Bd_sc))

                    ; #16350: <==closure== 21359 (pos)
                    (when (and (at_c_1))
                          (Pc_sc))

                    ; #21359: origin
                    (when (and (at_c_1))
                          (Bc_sc))

                    ; #34318: <==closure== 10284 (pos)
                    (when (and (at_a_1))
                          (Pa_sc))

                    ; #44789: <==closure== 64783 (pos)
                    (when (and (at_b_1))
                          (Pb_sc))

                    ; #64783: origin
                    (when (and (at_b_1))
                          (Bb_sc))

                    ; #97015: <==closure== 11493 (pos)
                    (when (and (at_d_1))
                          (Pd_sc))

                    ; #10383: <==negation-removal== 16350 (pos)
                    (when (and (at_c_1))
                          (not (Bc_not_sc)))

                    ; #12575: <==negation-removal== 97015 (pos)
                    (when (and (at_d_1))
                          (not (Bd_not_sc)))

                    ; #12586: <==uncertain_firing== 64783 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Pb_not_sc)))

                    ; #12766: <==negation-removal== 44789 (pos)
                    (when (and (at_b_1))
                          (not (Bb_not_sc)))

                    ; #14156: <==uncertain_firing== 16350 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Bc_not_sc)))

                    ; #14179: <==uncertain_firing== 11493 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Pd_not_sc)))

                    ; #14682: <==uncertain_firing== 21359 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Pc_not_sc)))

                    ; #15075: <==negation-removal== 11493 (pos)
                    (when (and (at_d_1))
                          (not (Pd_not_sc)))

                    ; #16056: <==uncertain_firing== 34318 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Ba_not_sc)))

                    ; #16213: <==uncertain_firing== 44789 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Bb_not_sc)))

                    ; #17237: <==negation-removal== 34318 (pos)
                    (when (and (at_a_1))
                          (not (Ba_not_sc)))

                    ; #21361: <==uncertain_firing== 97015 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Bd_not_sc)))

                    ; #27042: <==negation-removal== 64783 (pos)
                    (when (and (at_b_1))
                          (not (Pb_not_sc)))

                    ; #79792: <==uncertain_firing== 10284 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Pa_not_sc)))

                    ; #87462: <==negation-removal== 21359 (pos)
                    (when (and (at_c_1))
                          (not (Pc_not_sc)))

                    ; #88519: <==negation-removal== 10284 (pos)
                    (when (and (at_a_1))
                          (not (Pa_not_sc)))))

    (:action share_a_sc_2
        :precondition (and (at_a_2)
                           (Ba_sc))
        :effect (and
                    ; #12970: origin
                    (when (and (at_b_2))
                          (Bb_sc))

                    ; #13993: <==closure== 12970 (pos)
                    (when (and (at_b_2))
                          (Pb_sc))

                    ; #15881: <==closure== 75816 (pos)
                    (when (and (at_c_2))
                          (Pc_sc))

                    ; #20318: <==closure== 72770 (pos)
                    (when (and (at_a_2))
                          (Pa_sc))

                    ; #30274: <==closure== 99493 (pos)
                    (when (and (at_d_2))
                          (Pd_sc))

                    ; #72770: origin
                    (when (and (at_a_2))
                          (Ba_sc))

                    ; #75816: origin
                    (when (and (at_c_2))
                          (Bc_sc))

                    ; #99493: origin
                    (when (and (at_d_2))
                          (Bd_sc))

                    ; #10245: <==uncertain_firing== 72770 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Pa_not_sc)))

                    ; #10319: <==uncertain_firing== 99493 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Pd_not_sc)))

                    ; #10933: <==uncertain_firing== 20318 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Ba_not_sc)))

                    ; #11539: <==uncertain_firing== 15881 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Bc_not_sc)))

                    ; #12689: <==negation-removal== 12970 (pos)
                    (when (and (at_b_2))
                          (not (Pb_not_sc)))

                    ; #14783: <==negation-removal== 75816 (pos)
                    (when (and (at_c_2))
                          (not (Pc_not_sc)))

                    ; #15601: <==negation-removal== 15881 (pos)
                    (when (and (at_c_2))
                          (not (Bc_not_sc)))

                    ; #16000: <==uncertain_firing== 13993 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Bb_not_sc)))

                    ; #18632: <==negation-removal== 99493 (pos)
                    (when (and (at_d_2))
                          (not (Pd_not_sc)))

                    ; #18638: <==negation-removal== 20318 (pos)
                    (when (and (at_a_2))
                          (not (Ba_not_sc)))

                    ; #29010: <==uncertain_firing== 75816 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Pc_not_sc)))

                    ; #29258: <==negation-removal== 30274 (pos)
                    (when (and (at_d_2))
                          (not (Bd_not_sc)))

                    ; #38351: <==negation-removal== 13993 (pos)
                    (when (and (at_b_2))
                          (not (Bb_not_sc)))

                    ; #41616: <==uncertain_firing== 12970 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Pb_not_sc)))

                    ; #51605: <==uncertain_firing== 30274 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Bd_not_sc)))

                    ; #69717: <==negation-removal== 72770 (pos)
                    (when (and (at_a_2))
                          (not (Pa_not_sc)))))

    (:action share_a_sd_1
        :precondition (and (at_a_1)
                           (Ba_sd))
        :effect (and
                    ; #10284: origin
                    (when (and (at_a_1))
                          (Ba_sd))

                    ; #11503: origin
                    (when (and (at_d_1))
                          (Bd_sd))

                    ; #16320: <==closure== 21359 (pos)
                    (when (and (at_c_1))
                          (Pc_sd))

                    ; #21359: origin
                    (when (and (at_c_1))
                          (Bc_sd))

                    ; #34318: <==closure== 10284 (pos)
                    (when (and (at_a_1))
                          (Pa_sd))

                    ; #44889: <==closure== 63783 (pos)
                    (when (and (at_b_1))
                          (Pb_sd))

                    ; #63783: origin
                    (when (and (at_b_1))
                          (Bb_sd))

                    ; #96915: <==closure== 11503 (pos)
                    (when (and (at_d_1))
                          (Pd_sd))

                    ; #10383: <==negation-removal== 16320 (pos)
                    (when (and (at_c_1))
                          (not (Bc_not_sd)))

                    ; #12575: <==negation-removal== 96915 (pos)
                    (when (and (at_d_1))
                          (not (Bd_not_sd)))

                    ; #12596: <==uncertain_firing== 63783 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Pb_not_sd)))

                    ; #12766: <==negation-removal== 44889 (pos)
                    (when (and (at_b_1))
                          (not (Bb_not_sd)))

                    ; #14166: <==uncertain_firing== 16320 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Bc_not_sd)))

                    ; #14189: <==uncertain_firing== 11503 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Pd_not_sd)))

                    ; #14682: <==uncertain_firing== 21359 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Pc_not_sd)))

                    ; #15085: <==negation-removal== 11503 (pos)
                    (when (and (at_d_1))
                          (not (Pd_not_sd)))

                    ; #16046: <==uncertain_firing== 34318 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Ba_not_sd)))

                    ; #16213: <==uncertain_firing== 44889 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Bb_not_sd)))

                    ; #17237: <==negation-removal== 34318 (pos)
                    (when (and (at_a_1))
                          (not (Ba_not_sd)))

                    ; #21361: <==uncertain_firing== 96915 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Bd_not_sd)))

                    ; #26042: <==negation-removal== 63783 (pos)
                    (when (and (at_b_1))
                          (not (Pb_not_sd)))

                    ; #79792: <==uncertain_firing== 10284 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Pa_not_sd)))

                    ; #87362: <==negation-removal== 21359 (pos)
                    (when (and (at_c_1))
                          (not (Pc_not_sd)))

                    ; #88519: <==negation-removal== 10284 (pos)
                    (when (and (at_a_1))
                          (not (Pa_not_sd)))))

    (:action share_a_sd_2
        :precondition (and (at_a_2)
                           (Ba_sd))
        :effect (and
                    ; #12920: origin
                    (when (and (at_b_2))
                          (Bb_sd))

                    ; #13993: <==closure== 12920 (pos)
                    (when (and (at_b_2))
                          (Pb_sd))

                    ; #15881: <==closure== 75816 (pos)
                    (when (and (at_c_2))
                          (Pc_sd))

                    ; #20318: <==closure== 72770 (pos)
                    (when (and (at_a_2))
                          (Pa_sd))

                    ; #30274: <==closure== 99493 (pos)
                    (when (and (at_d_2))
                          (Pd_sd))

                    ; #72770: origin
                    (when (and (at_a_2))
                          (Ba_sd))

                    ; #75816: origin
                    (when (and (at_c_2))
                          (Bc_sd))

                    ; #99493: origin
                    (when (and (at_d_2))
                          (Bd_sd))

                    ; #10245: <==uncertain_firing== 72770 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Pa_not_sd)))

                    ; #10329: <==uncertain_firing== 99493 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Pd_not_sd)))

                    ; #10963: <==uncertain_firing== 20318 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Ba_not_sd)))

                    ; #11569: <==uncertain_firing== 15881 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Bc_not_sd)))

                    ; #12689: <==negation-removal== 12920 (pos)
                    (when (and (at_b_2))
                          (not (Pb_not_sd)))

                    ; #14783: <==negation-removal== 75816 (pos)
                    (when (and (at_c_2))
                          (not (Pc_not_sd)))

                    ; #15601: <==negation-removal== 15881 (pos)
                    (when (and (at_c_2))
                          (not (Bc_not_sd)))

                    ; #16000: <==uncertain_firing== 13993 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Bb_not_sd)))

                    ; #18632: <==negation-removal== 99493 (pos)
                    (when (and (at_d_2))
                          (not (Pd_not_sd)))

                    ; #18638: <==negation-removal== 20318 (pos)
                    (when (and (at_a_2))
                          (not (Ba_not_sd)))

                    ; #29010: <==uncertain_firing== 75816 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Pc_not_sd)))

                    ; #29258: <==negation-removal== 30274 (pos)
                    (when (and (at_d_2))
                          (not (Bd_not_sd)))

                    ; #39051: <==negation-removal== 13993 (pos)
                    (when (and (at_b_2))
                          (not (Bb_not_sd)))

                    ; #41716: <==uncertain_firing== 12920 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Pb_not_sd)))

                    ; #51605: <==uncertain_firing== 30274 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Bd_not_sd)))

                    ; #69717: <==negation-removal== 72770 (pos)
                    (when (and (at_a_2))
                          (not (Pa_not_sd)))))

    (:action share_b_sa_1
        :precondition (and (Bb_sa)
                           (at_b_1))
        :effect (and
                    ; #10284: origin
                    (when (and (at_a_1))
                          (Ba_sa))

                    ; #11503: origin
                    (when (and (at_d_1))
                          (Bd_sa))

                    ; #16350: <==closure== 21359 (pos)
                    (when (and (at_c_1))
                          (Pc_sa))

                    ; #21359: origin
                    (when (and (at_c_1))
                          (Bc_sa))

                    ; #34318: <==closure== 10284 (pos)
                    (when (and (at_a_1))
                          (Pa_sa))

                    ; #44889: <==closure== 63783 (pos)
                    (when (and (at_b_1))
                          (Pb_sa))

                    ; #63783: origin
                    (when (and (at_b_1))
                          (Bb_sa))

                    ; #96915: <==closure== 11503 (pos)
                    (when (and (at_d_1))
                          (Pd_sa))

                    ; #10383: <==negation-removal== 16350 (pos)
                    (when (and (at_c_1))
                          (not (Bc_not_sa)))

                    ; #12575: <==negation-removal== 96915 (pos)
                    (when (and (at_d_1))
                          (not (Bd_not_sa)))

                    ; #12586: <==uncertain_firing== 63783 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Pb_not_sa)))

                    ; #12766: <==negation-removal== 44889 (pos)
                    (when (and (at_b_1))
                          (not (Bb_not_sa)))

                    ; #14166: <==uncertain_firing== 16350 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Bc_not_sa)))

                    ; #14189: <==uncertain_firing== 11503 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Pd_not_sa)))

                    ; #14682: <==uncertain_firing== 21359 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Pc_not_sa)))

                    ; #15085: <==negation-removal== 11503 (pos)
                    (when (and (at_d_1))
                          (not (Pd_not_sa)))

                    ; #16056: <==uncertain_firing== 34318 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Ba_not_sa)))

                    ; #16213: <==uncertain_firing== 44889 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Bb_not_sa)))

                    ; #17237: <==negation-removal== 34318 (pos)
                    (when (and (at_a_1))
                          (not (Ba_not_sa)))

                    ; #21361: <==uncertain_firing== 96915 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Bd_not_sa)))

                    ; #27042: <==negation-removal== 63783 (pos)
                    (when (and (at_b_1))
                          (not (Pb_not_sa)))

                    ; #79792: <==uncertain_firing== 10284 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Pa_not_sa)))

                    ; #87462: <==negation-removal== 21359 (pos)
                    (when (and (at_c_1))
                          (not (Pc_not_sa)))

                    ; #88519: <==negation-removal== 10284 (pos)
                    (when (and (at_a_1))
                          (not (Pa_not_sa)))))

    (:action share_b_sa_2
        :precondition (and (Bb_sa)
                           (at_b_2))
        :effect (and
                    ; #12970: origin
                    (when (and (at_b_2))
                          (Bb_sa))

                    ; #13993: <==closure== 12970 (pos)
                    (when (and (at_b_2))
                          (Pb_sa))

                    ; #19924: <==closure== 75816 (pos)
                    (when (and (at_c_2))
                          (Pc_sa))

                    ; #20318: <==closure== 72770 (pos)
                    (when (and (at_a_2))
                          (Pa_sa))

                    ; #30274: <==closure== 99493 (pos)
                    (when (and (at_d_2))
                          (Pd_sa))

                    ; #72770: origin
                    (when (and (at_a_2))
                          (Ba_sa))

                    ; #75816: origin
                    (when (and (at_c_2))
                          (Bc_sa))

                    ; #99493: origin
                    (when (and (at_d_2))
                          (Bd_sa))

                    ; #10245: <==uncertain_firing== 72770 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Pa_not_sa)))

                    ; #10319: <==uncertain_firing== 99493 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Pd_not_sa)))

                    ; #10963: <==uncertain_firing== 20318 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Ba_not_sa)))

                    ; #11539: <==uncertain_firing== 19924 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Bc_not_sa)))

                    ; #12689: <==negation-removal== 12970 (pos)
                    (when (and (at_b_2))
                          (not (Pb_not_sa)))

                    ; #14783: <==negation-removal== 75816 (pos)
                    (when (and (at_c_2))
                          (not (Pc_not_sa)))

                    ; #15601: <==negation-removal== 19924 (pos)
                    (when (and (at_c_2))
                          (not (Bc_not_sa)))

                    ; #16030: <==uncertain_firing== 13993 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Bb_not_sa)))

                    ; #18632: <==negation-removal== 99493 (pos)
                    (when (and (at_d_2))
                          (not (Pd_not_sa)))

                    ; #18638: <==negation-removal== 20318 (pos)
                    (when (and (at_a_2))
                          (not (Ba_not_sa)))

                    ; #29110: <==uncertain_firing== 75816 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Pc_not_sa)))

                    ; #29258: <==negation-removal== 30274 (pos)
                    (when (and (at_d_2))
                          (not (Bd_not_sa)))

                    ; #38351: <==negation-removal== 13993 (pos)
                    (when (and (at_b_2))
                          (not (Bb_not_sa)))

                    ; #41716: <==uncertain_firing== 12970 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Pb_not_sa)))

                    ; #51605: <==uncertain_firing== 30274 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Bd_not_sa)))

                    ; #69717: <==negation-removal== 72770 (pos)
                    (when (and (at_a_2))
                          (not (Pa_not_sa)))))

    (:action share_b_sb_1
        :precondition (and (Bb_sb)
                           (at_b_1))
        :effect (and
                    ; #10284: origin
                    (when (and (at_a_1))
                          (Ba_sb))

                    ; #11493: origin
                    (when (and (at_d_1))
                          (Bd_sb))

                    ; #16350: <==closure== 21359 (pos)
                    (when (and (at_c_1))
                          (Pc_sb))

                    ; #21359: origin
                    (when (and (at_c_1))
                          (Bc_sb))

                    ; #34318: <==closure== 10284 (pos)
                    (when (and (at_a_1))
                          (Pa_sb))

                    ; #44789: <==closure== 63783 (pos)
                    (when (and (at_b_1))
                          (Pb_sb))

                    ; #63783: origin
                    (when (and (at_b_1))
                          (Bb_sb))

                    ; #97015: <==closure== 11493 (pos)
                    (when (and (at_d_1))
                          (Pd_sb))

                    ; #10383: <==negation-removal== 16350 (pos)
                    (when (and (at_c_1))
                          (not (Bc_not_sb)))

                    ; #12575: <==negation-removal== 97015 (pos)
                    (when (and (at_d_1))
                          (not (Bd_not_sb)))

                    ; #12586: <==uncertain_firing== 63783 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Pb_not_sb)))

                    ; #12766: <==negation-removal== 44789 (pos)
                    (when (and (at_b_1))
                          (not (Bb_not_sb)))

                    ; #14156: <==uncertain_firing== 16350 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Bc_not_sb)))

                    ; #14189: <==uncertain_firing== 11493 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Pd_not_sb)))

                    ; #14682: <==uncertain_firing== 21359 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Pc_not_sb)))

                    ; #15075: <==negation-removal== 11493 (pos)
                    (when (and (at_d_1))
                          (not (Pd_not_sb)))

                    ; #16056: <==uncertain_firing== 34318 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Ba_not_sb)))

                    ; #16213: <==uncertain_firing== 44789 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Bb_not_sb)))

                    ; #17267: <==negation-removal== 34318 (pos)
                    (when (and (at_a_1))
                          (not (Ba_not_sb)))

                    ; #21361: <==uncertain_firing== 97015 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Bd_not_sb)))

                    ; #27042: <==negation-removal== 63783 (pos)
                    (when (and (at_b_1))
                          (not (Pb_not_sb)))

                    ; #79792: <==uncertain_firing== 10284 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Pa_not_sb)))

                    ; #87462: <==negation-removal== 21359 (pos)
                    (when (and (at_c_1))
                          (not (Pc_not_sb)))

                    ; #88519: <==negation-removal== 10284 (pos)
                    (when (and (at_a_1))
                          (not (Pa_not_sb)))))

    (:action share_b_sb_2
        :precondition (and (Bb_sb)
                           (at_b_2))
        :effect (and
                    ; #12970: origin
                    (when (and (at_b_2))
                          (Bb_sb))

                    ; #13993: <==closure== 12970 (pos)
                    (when (and (at_b_2))
                          (Pb_sb))

                    ; #19924: <==closure== 75916 (pos)
                    (when (and (at_c_2))
                          (Pc_sb))

                    ; #20318: <==closure== 72770 (pos)
                    (when (and (at_a_2))
                          (Pa_sb))

                    ; #30274: <==closure== 99493 (pos)
                    (when (and (at_d_2))
                          (Pd_sb))

                    ; #72770: origin
                    (when (and (at_a_2))
                          (Ba_sb))

                    ; #75916: origin
                    (when (and (at_c_2))
                          (Bc_sb))

                    ; #99493: origin
                    (when (and (at_d_2))
                          (Bd_sb))

                    ; #10245: <==uncertain_firing== 72770 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Pa_not_sb)))

                    ; #10319: <==uncertain_firing== 99493 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Pd_not_sb)))

                    ; #10933: <==uncertain_firing== 20318 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Ba_not_sb)))

                    ; #11539: <==uncertain_firing== 19924 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Bc_not_sb)))

                    ; #12689: <==negation-removal== 12970 (pos)
                    (when (and (at_b_2))
                          (not (Pb_not_sb)))

                    ; #14783: <==negation-removal== 75916 (pos)
                    (when (and (at_c_2))
                          (not (Pc_not_sb)))

                    ; #15601: <==negation-removal== 19924 (pos)
                    (when (and (at_c_2))
                          (not (Bc_not_sb)))

                    ; #16030: <==uncertain_firing== 13993 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Bb_not_sb)))

                    ; #18628: <==negation-removal== 20318 (pos)
                    (when (and (at_a_2))
                          (not (Ba_not_sb)))

                    ; #18632: <==negation-removal== 99493 (pos)
                    (when (and (at_d_2))
                          (not (Pd_not_sb)))

                    ; #29110: <==uncertain_firing== 75916 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Pc_not_sb)))

                    ; #29258: <==negation-removal== 30274 (pos)
                    (when (and (at_d_2))
                          (not (Bd_not_sb)))

                    ; #38351: <==negation-removal== 13993 (pos)
                    (when (and (at_b_2))
                          (not (Bb_not_sb)))

                    ; #41616: <==uncertain_firing== 12970 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Pb_not_sb)))

                    ; #51605: <==uncertain_firing== 30274 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Bd_not_sb)))

                    ; #69717: <==negation-removal== 72770 (pos)
                    (when (and (at_a_2))
                          (not (Pa_not_sb)))))

    (:action share_b_sc_1
        :precondition (and (Bb_sc)
                           (at_b_1))
        :effect (and
                    ; #10284: origin
                    (when (and (at_a_1))
                          (Ba_sc))

                    ; #11493: origin
                    (when (and (at_d_1))
                          (Bd_sc))

                    ; #16350: <==closure== 21359 (pos)
                    (when (and (at_c_1))
                          (Pc_sc))

                    ; #21359: origin
                    (when (and (at_c_1))
                          (Bc_sc))

                    ; #34318: <==closure== 10284 (pos)
                    (when (and (at_a_1))
                          (Pa_sc))

                    ; #44789: <==closure== 64783 (pos)
                    (when (and (at_b_1))
                          (Pb_sc))

                    ; #64783: origin
                    (when (and (at_b_1))
                          (Bb_sc))

                    ; #97015: <==closure== 11493 (pos)
                    (when (and (at_d_1))
                          (Pd_sc))

                    ; #10383: <==negation-removal== 16350 (pos)
                    (when (and (at_c_1))
                          (not (Bc_not_sc)))

                    ; #12575: <==negation-removal== 97015 (pos)
                    (when (and (at_d_1))
                          (not (Bd_not_sc)))

                    ; #12586: <==uncertain_firing== 64783 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Pb_not_sc)))

                    ; #12766: <==negation-removal== 44789 (pos)
                    (when (and (at_b_1))
                          (not (Bb_not_sc)))

                    ; #14156: <==uncertain_firing== 16350 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Bc_not_sc)))

                    ; #14179: <==uncertain_firing== 11493 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Pd_not_sc)))

                    ; #14682: <==uncertain_firing== 21359 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Pc_not_sc)))

                    ; #15075: <==negation-removal== 11493 (pos)
                    (when (and (at_d_1))
                          (not (Pd_not_sc)))

                    ; #16056: <==uncertain_firing== 34318 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Ba_not_sc)))

                    ; #16213: <==uncertain_firing== 44789 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Bb_not_sc)))

                    ; #17237: <==negation-removal== 34318 (pos)
                    (when (and (at_a_1))
                          (not (Ba_not_sc)))

                    ; #21361: <==uncertain_firing== 97015 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Bd_not_sc)))

                    ; #27042: <==negation-removal== 64783 (pos)
                    (when (and (at_b_1))
                          (not (Pb_not_sc)))

                    ; #79792: <==uncertain_firing== 10284 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Pa_not_sc)))

                    ; #87462: <==negation-removal== 21359 (pos)
                    (when (and (at_c_1))
                          (not (Pc_not_sc)))

                    ; #88519: <==negation-removal== 10284 (pos)
                    (when (and (at_a_1))
                          (not (Pa_not_sc)))))

    (:action share_b_sc_2
        :precondition (and (Bb_sc)
                           (at_b_2))
        :effect (and
                    ; #12970: origin
                    (when (and (at_b_2))
                          (Bb_sc))

                    ; #13993: <==closure== 12970 (pos)
                    (when (and (at_b_2))
                          (Pb_sc))

                    ; #15881: <==closure== 75816 (pos)
                    (when (and (at_c_2))
                          (Pc_sc))

                    ; #20318: <==closure== 72770 (pos)
                    (when (and (at_a_2))
                          (Pa_sc))

                    ; #30274: <==closure== 99493 (pos)
                    (when (and (at_d_2))
                          (Pd_sc))

                    ; #72770: origin
                    (when (and (at_a_2))
                          (Ba_sc))

                    ; #75816: origin
                    (when (and (at_c_2))
                          (Bc_sc))

                    ; #99493: origin
                    (when (and (at_d_2))
                          (Bd_sc))

                    ; #10245: <==uncertain_firing== 72770 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Pa_not_sc)))

                    ; #10319: <==uncertain_firing== 99493 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Pd_not_sc)))

                    ; #10933: <==uncertain_firing== 20318 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Ba_not_sc)))

                    ; #11539: <==uncertain_firing== 15881 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Bc_not_sc)))

                    ; #12689: <==negation-removal== 12970 (pos)
                    (when (and (at_b_2))
                          (not (Pb_not_sc)))

                    ; #14783: <==negation-removal== 75816 (pos)
                    (when (and (at_c_2))
                          (not (Pc_not_sc)))

                    ; #15601: <==negation-removal== 15881 (pos)
                    (when (and (at_c_2))
                          (not (Bc_not_sc)))

                    ; #16000: <==uncertain_firing== 13993 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Bb_not_sc)))

                    ; #18632: <==negation-removal== 99493 (pos)
                    (when (and (at_d_2))
                          (not (Pd_not_sc)))

                    ; #18638: <==negation-removal== 20318 (pos)
                    (when (and (at_a_2))
                          (not (Ba_not_sc)))

                    ; #29010: <==uncertain_firing== 75816 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Pc_not_sc)))

                    ; #29258: <==negation-removal== 30274 (pos)
                    (when (and (at_d_2))
                          (not (Bd_not_sc)))

                    ; #38351: <==negation-removal== 13993 (pos)
                    (when (and (at_b_2))
                          (not (Bb_not_sc)))

                    ; #41616: <==uncertain_firing== 12970 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Pb_not_sc)))

                    ; #51605: <==uncertain_firing== 30274 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Bd_not_sc)))

                    ; #69717: <==negation-removal== 72770 (pos)
                    (when (and (at_a_2))
                          (not (Pa_not_sc)))))

    (:action share_b_sd_1
        :precondition (and (Bb_sd)
                           (at_b_1))
        :effect (and
                    ; #10284: origin
                    (when (and (at_a_1))
                          (Ba_sd))

                    ; #11503: origin
                    (when (and (at_d_1))
                          (Bd_sd))

                    ; #16320: <==closure== 21359 (pos)
                    (when (and (at_c_1))
                          (Pc_sd))

                    ; #21359: origin
                    (when (and (at_c_1))
                          (Bc_sd))

                    ; #34318: <==closure== 10284 (pos)
                    (when (and (at_a_1))
                          (Pa_sd))

                    ; #44889: <==closure== 63783 (pos)
                    (when (and (at_b_1))
                          (Pb_sd))

                    ; #63783: origin
                    (when (and (at_b_1))
                          (Bb_sd))

                    ; #96915: <==closure== 11503 (pos)
                    (when (and (at_d_1))
                          (Pd_sd))

                    ; #10383: <==negation-removal== 16320 (pos)
                    (when (and (at_c_1))
                          (not (Bc_not_sd)))

                    ; #12575: <==negation-removal== 96915 (pos)
                    (when (and (at_d_1))
                          (not (Bd_not_sd)))

                    ; #12596: <==uncertain_firing== 63783 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Pb_not_sd)))

                    ; #12766: <==negation-removal== 44889 (pos)
                    (when (and (at_b_1))
                          (not (Bb_not_sd)))

                    ; #14166: <==uncertain_firing== 16320 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Bc_not_sd)))

                    ; #14189: <==uncertain_firing== 11503 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Pd_not_sd)))

                    ; #14682: <==uncertain_firing== 21359 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Pc_not_sd)))

                    ; #15085: <==negation-removal== 11503 (pos)
                    (when (and (at_d_1))
                          (not (Pd_not_sd)))

                    ; #16046: <==uncertain_firing== 34318 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Ba_not_sd)))

                    ; #16213: <==uncertain_firing== 44889 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Bb_not_sd)))

                    ; #17237: <==negation-removal== 34318 (pos)
                    (when (and (at_a_1))
                          (not (Ba_not_sd)))

                    ; #21361: <==uncertain_firing== 96915 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Bd_not_sd)))

                    ; #26042: <==negation-removal== 63783 (pos)
                    (when (and (at_b_1))
                          (not (Pb_not_sd)))

                    ; #79792: <==uncertain_firing== 10284 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Pa_not_sd)))

                    ; #87362: <==negation-removal== 21359 (pos)
                    (when (and (at_c_1))
                          (not (Pc_not_sd)))

                    ; #88519: <==negation-removal== 10284 (pos)
                    (when (and (at_a_1))
                          (not (Pa_not_sd)))))

    (:action share_b_sd_2
        :precondition (and (Bb_sd)
                           (at_b_2))
        :effect (and
                    ; #12920: origin
                    (when (and (at_b_2))
                          (Bb_sd))

                    ; #13993: <==closure== 12920 (pos)
                    (when (and (at_b_2))
                          (Pb_sd))

                    ; #15881: <==closure== 75816 (pos)
                    (when (and (at_c_2))
                          (Pc_sd))

                    ; #20318: <==closure== 72770 (pos)
                    (when (and (at_a_2))
                          (Pa_sd))

                    ; #30274: <==closure== 99493 (pos)
                    (when (and (at_d_2))
                          (Pd_sd))

                    ; #72770: origin
                    (when (and (at_a_2))
                          (Ba_sd))

                    ; #75816: origin
                    (when (and (at_c_2))
                          (Bc_sd))

                    ; #99493: origin
                    (when (and (at_d_2))
                          (Bd_sd))

                    ; #10245: <==uncertain_firing== 72770 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Pa_not_sd)))

                    ; #10329: <==uncertain_firing== 99493 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Pd_not_sd)))

                    ; #10963: <==uncertain_firing== 20318 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Ba_not_sd)))

                    ; #11569: <==uncertain_firing== 15881 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Bc_not_sd)))

                    ; #12689: <==negation-removal== 12920 (pos)
                    (when (and (at_b_2))
                          (not (Pb_not_sd)))

                    ; #14783: <==negation-removal== 75816 (pos)
                    (when (and (at_c_2))
                          (not (Pc_not_sd)))

                    ; #15601: <==negation-removal== 15881 (pos)
                    (when (and (at_c_2))
                          (not (Bc_not_sd)))

                    ; #16000: <==uncertain_firing== 13993 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Bb_not_sd)))

                    ; #18632: <==negation-removal== 99493 (pos)
                    (when (and (at_d_2))
                          (not (Pd_not_sd)))

                    ; #18638: <==negation-removal== 20318 (pos)
                    (when (and (at_a_2))
                          (not (Ba_not_sd)))

                    ; #29010: <==uncertain_firing== 75816 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Pc_not_sd)))

                    ; #29258: <==negation-removal== 30274 (pos)
                    (when (and (at_d_2))
                          (not (Bd_not_sd)))

                    ; #39051: <==negation-removal== 13993 (pos)
                    (when (and (at_b_2))
                          (not (Bb_not_sd)))

                    ; #41716: <==uncertain_firing== 12920 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Pb_not_sd)))

                    ; #51605: <==uncertain_firing== 30274 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Bd_not_sd)))

                    ; #69717: <==negation-removal== 72770 (pos)
                    (when (and (at_a_2))
                          (not (Pa_not_sd)))))

    (:action share_c_sa_1
        :precondition (and (Bc_sa)
                           (at_c_1))
        :effect (and
                    ; #10284: origin
                    (when (and (at_a_1))
                          (Ba_sa))

                    ; #11503: origin
                    (when (and (at_d_1))
                          (Bd_sa))

                    ; #16350: <==closure== 21359 (pos)
                    (when (and (at_c_1))
                          (Pc_sa))

                    ; #21359: origin
                    (when (and (at_c_1))
                          (Bc_sa))

                    ; #34318: <==closure== 10284 (pos)
                    (when (and (at_a_1))
                          (Pa_sa))

                    ; #44889: <==closure== 63783 (pos)
                    (when (and (at_b_1))
                          (Pb_sa))

                    ; #63783: origin
                    (when (and (at_b_1))
                          (Bb_sa))

                    ; #96915: <==closure== 11503 (pos)
                    (when (and (at_d_1))
                          (Pd_sa))

                    ; #10383: <==negation-removal== 16350 (pos)
                    (when (and (at_c_1))
                          (not (Bc_not_sa)))

                    ; #12575: <==negation-removal== 96915 (pos)
                    (when (and (at_d_1))
                          (not (Bd_not_sa)))

                    ; #12586: <==uncertain_firing== 63783 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Pb_not_sa)))

                    ; #12766: <==negation-removal== 44889 (pos)
                    (when (and (at_b_1))
                          (not (Bb_not_sa)))

                    ; #14166: <==uncertain_firing== 16350 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Bc_not_sa)))

                    ; #14189: <==uncertain_firing== 11503 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Pd_not_sa)))

                    ; #14682: <==uncertain_firing== 21359 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Pc_not_sa)))

                    ; #15085: <==negation-removal== 11503 (pos)
                    (when (and (at_d_1))
                          (not (Pd_not_sa)))

                    ; #16056: <==uncertain_firing== 34318 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Ba_not_sa)))

                    ; #16213: <==uncertain_firing== 44889 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Bb_not_sa)))

                    ; #17237: <==negation-removal== 34318 (pos)
                    (when (and (at_a_1))
                          (not (Ba_not_sa)))

                    ; #21361: <==uncertain_firing== 96915 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Bd_not_sa)))

                    ; #27042: <==negation-removal== 63783 (pos)
                    (when (and (at_b_1))
                          (not (Pb_not_sa)))

                    ; #79792: <==uncertain_firing== 10284 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Pa_not_sa)))

                    ; #87462: <==negation-removal== 21359 (pos)
                    (when (and (at_c_1))
                          (not (Pc_not_sa)))

                    ; #88519: <==negation-removal== 10284 (pos)
                    (when (and (at_a_1))
                          (not (Pa_not_sa)))))

    (:action share_c_sa_2
        :precondition (and (at_c_2)
                           (Bc_sa))
        :effect (and
                    ; #12970: origin
                    (when (and (at_b_2))
                          (Bb_sa))

                    ; #13993: <==closure== 12970 (pos)
                    (when (and (at_b_2))
                          (Pb_sa))

                    ; #19924: <==closure== 75816 (pos)
                    (when (and (at_c_2))
                          (Pc_sa))

                    ; #20318: <==closure== 72770 (pos)
                    (when (and (at_a_2))
                          (Pa_sa))

                    ; #30274: <==closure== 99493 (pos)
                    (when (and (at_d_2))
                          (Pd_sa))

                    ; #72770: origin
                    (when (and (at_a_2))
                          (Ba_sa))

                    ; #75816: origin
                    (when (and (at_c_2))
                          (Bc_sa))

                    ; #99493: origin
                    (when (and (at_d_2))
                          (Bd_sa))

                    ; #10245: <==uncertain_firing== 72770 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Pa_not_sa)))

                    ; #10319: <==uncertain_firing== 99493 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Pd_not_sa)))

                    ; #10963: <==uncertain_firing== 20318 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Ba_not_sa)))

                    ; #11539: <==uncertain_firing== 19924 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Bc_not_sa)))

                    ; #12689: <==negation-removal== 12970 (pos)
                    (when (and (at_b_2))
                          (not (Pb_not_sa)))

                    ; #14783: <==negation-removal== 75816 (pos)
                    (when (and (at_c_2))
                          (not (Pc_not_sa)))

                    ; #15601: <==negation-removal== 19924 (pos)
                    (when (and (at_c_2))
                          (not (Bc_not_sa)))

                    ; #16030: <==uncertain_firing== 13993 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Bb_not_sa)))

                    ; #18632: <==negation-removal== 99493 (pos)
                    (when (and (at_d_2))
                          (not (Pd_not_sa)))

                    ; #18638: <==negation-removal== 20318 (pos)
                    (when (and (at_a_2))
                          (not (Ba_not_sa)))

                    ; #29110: <==uncertain_firing== 75816 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Pc_not_sa)))

                    ; #29258: <==negation-removal== 30274 (pos)
                    (when (and (at_d_2))
                          (not (Bd_not_sa)))

                    ; #38351: <==negation-removal== 13993 (pos)
                    (when (and (at_b_2))
                          (not (Bb_not_sa)))

                    ; #41716: <==uncertain_firing== 12970 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Pb_not_sa)))

                    ; #51605: <==uncertain_firing== 30274 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Bd_not_sa)))

                    ; #69717: <==negation-removal== 72770 (pos)
                    (when (and (at_a_2))
                          (not (Pa_not_sa)))))

    (:action share_c_sb_1
        :precondition (and (Bc_sb)
                           (at_c_1))
        :effect (and
                    ; #10284: origin
                    (when (and (at_a_1))
                          (Ba_sb))

                    ; #11493: origin
                    (when (and (at_d_1))
                          (Bd_sb))

                    ; #16350: <==closure== 21359 (pos)
                    (when (and (at_c_1))
                          (Pc_sb))

                    ; #21359: origin
                    (when (and (at_c_1))
                          (Bc_sb))

                    ; #34318: <==closure== 10284 (pos)
                    (when (and (at_a_1))
                          (Pa_sb))

                    ; #44789: <==closure== 63783 (pos)
                    (when (and (at_b_1))
                          (Pb_sb))

                    ; #63783: origin
                    (when (and (at_b_1))
                          (Bb_sb))

                    ; #97015: <==closure== 11493 (pos)
                    (when (and (at_d_1))
                          (Pd_sb))

                    ; #10383: <==negation-removal== 16350 (pos)
                    (when (and (at_c_1))
                          (not (Bc_not_sb)))

                    ; #12575: <==negation-removal== 97015 (pos)
                    (when (and (at_d_1))
                          (not (Bd_not_sb)))

                    ; #12586: <==uncertain_firing== 63783 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Pb_not_sb)))

                    ; #12766: <==negation-removal== 44789 (pos)
                    (when (and (at_b_1))
                          (not (Bb_not_sb)))

                    ; #14156: <==uncertain_firing== 16350 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Bc_not_sb)))

                    ; #14189: <==uncertain_firing== 11493 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Pd_not_sb)))

                    ; #14682: <==uncertain_firing== 21359 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Pc_not_sb)))

                    ; #15075: <==negation-removal== 11493 (pos)
                    (when (and (at_d_1))
                          (not (Pd_not_sb)))

                    ; #16056: <==uncertain_firing== 34318 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Ba_not_sb)))

                    ; #16213: <==uncertain_firing== 44789 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Bb_not_sb)))

                    ; #17267: <==negation-removal== 34318 (pos)
                    (when (and (at_a_1))
                          (not (Ba_not_sb)))

                    ; #21361: <==uncertain_firing== 97015 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Bd_not_sb)))

                    ; #27042: <==negation-removal== 63783 (pos)
                    (when (and (at_b_1))
                          (not (Pb_not_sb)))

                    ; #79792: <==uncertain_firing== 10284 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Pa_not_sb)))

                    ; #87462: <==negation-removal== 21359 (pos)
                    (when (and (at_c_1))
                          (not (Pc_not_sb)))

                    ; #88519: <==negation-removal== 10284 (pos)
                    (when (and (at_a_1))
                          (not (Pa_not_sb)))))

    (:action share_c_sb_2
        :precondition (and (at_c_2)
                           (Bc_sb))
        :effect (and
                    ; #12970: origin
                    (when (and (at_b_2))
                          (Bb_sb))

                    ; #13993: <==closure== 12970 (pos)
                    (when (and (at_b_2))
                          (Pb_sb))

                    ; #19924: <==closure== 75916 (pos)
                    (when (and (at_c_2))
                          (Pc_sb))

                    ; #20318: <==closure== 72770 (pos)
                    (when (and (at_a_2))
                          (Pa_sb))

                    ; #30274: <==closure== 99493 (pos)
                    (when (and (at_d_2))
                          (Pd_sb))

                    ; #72770: origin
                    (when (and (at_a_2))
                          (Ba_sb))

                    ; #75916: origin
                    (when (and (at_c_2))
                          (Bc_sb))

                    ; #99493: origin
                    (when (and (at_d_2))
                          (Bd_sb))

                    ; #10245: <==uncertain_firing== 72770 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Pa_not_sb)))

                    ; #10319: <==uncertain_firing== 99493 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Pd_not_sb)))

                    ; #10933: <==uncertain_firing== 20318 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Ba_not_sb)))

                    ; #11539: <==uncertain_firing== 19924 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Bc_not_sb)))

                    ; #12689: <==negation-removal== 12970 (pos)
                    (when (and (at_b_2))
                          (not (Pb_not_sb)))

                    ; #14783: <==negation-removal== 75916 (pos)
                    (when (and (at_c_2))
                          (not (Pc_not_sb)))

                    ; #15601: <==negation-removal== 19924 (pos)
                    (when (and (at_c_2))
                          (not (Bc_not_sb)))

                    ; #16030: <==uncertain_firing== 13993 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Bb_not_sb)))

                    ; #18628: <==negation-removal== 20318 (pos)
                    (when (and (at_a_2))
                          (not (Ba_not_sb)))

                    ; #18632: <==negation-removal== 99493 (pos)
                    (when (and (at_d_2))
                          (not (Pd_not_sb)))

                    ; #29110: <==uncertain_firing== 75916 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Pc_not_sb)))

                    ; #29258: <==negation-removal== 30274 (pos)
                    (when (and (at_d_2))
                          (not (Bd_not_sb)))

                    ; #38351: <==negation-removal== 13993 (pos)
                    (when (and (at_b_2))
                          (not (Bb_not_sb)))

                    ; #41616: <==uncertain_firing== 12970 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Pb_not_sb)))

                    ; #51605: <==uncertain_firing== 30274 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Bd_not_sb)))

                    ; #69717: <==negation-removal== 72770 (pos)
                    (when (and (at_a_2))
                          (not (Pa_not_sb)))))

    (:action share_c_sc_1
        :precondition (and (Bc_sc)
                           (at_c_1))
        :effect (and
                    ; #10284: origin
                    (when (and (at_a_1))
                          (Ba_sc))

                    ; #11493: origin
                    (when (and (at_d_1))
                          (Bd_sc))

                    ; #16350: <==closure== 21359 (pos)
                    (when (and (at_c_1))
                          (Pc_sc))

                    ; #21359: origin
                    (when (and (at_c_1))
                          (Bc_sc))

                    ; #34318: <==closure== 10284 (pos)
                    (when (and (at_a_1))
                          (Pa_sc))

                    ; #44789: <==closure== 64783 (pos)
                    (when (and (at_b_1))
                          (Pb_sc))

                    ; #64783: origin
                    (when (and (at_b_1))
                          (Bb_sc))

                    ; #97015: <==closure== 11493 (pos)
                    (when (and (at_d_1))
                          (Pd_sc))

                    ; #10383: <==negation-removal== 16350 (pos)
                    (when (and (at_c_1))
                          (not (Bc_not_sc)))

                    ; #12575: <==negation-removal== 97015 (pos)
                    (when (and (at_d_1))
                          (not (Bd_not_sc)))

                    ; #12586: <==uncertain_firing== 64783 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Pb_not_sc)))

                    ; #12766: <==negation-removal== 44789 (pos)
                    (when (and (at_b_1))
                          (not (Bb_not_sc)))

                    ; #14156: <==uncertain_firing== 16350 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Bc_not_sc)))

                    ; #14179: <==uncertain_firing== 11493 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Pd_not_sc)))

                    ; #14682: <==uncertain_firing== 21359 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Pc_not_sc)))

                    ; #15075: <==negation-removal== 11493 (pos)
                    (when (and (at_d_1))
                          (not (Pd_not_sc)))

                    ; #16056: <==uncertain_firing== 34318 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Ba_not_sc)))

                    ; #16213: <==uncertain_firing== 44789 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Bb_not_sc)))

                    ; #17237: <==negation-removal== 34318 (pos)
                    (when (and (at_a_1))
                          (not (Ba_not_sc)))

                    ; #21361: <==uncertain_firing== 97015 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Bd_not_sc)))

                    ; #27042: <==negation-removal== 64783 (pos)
                    (when (and (at_b_1))
                          (not (Pb_not_sc)))

                    ; #79792: <==uncertain_firing== 10284 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Pa_not_sc)))

                    ; #87462: <==negation-removal== 21359 (pos)
                    (when (and (at_c_1))
                          (not (Pc_not_sc)))

                    ; #88519: <==negation-removal== 10284 (pos)
                    (when (and (at_a_1))
                          (not (Pa_not_sc)))))

    (:action share_c_sc_2
        :precondition (and (Bc_sc)
                           (at_c_2))
        :effect (and
                    ; #12970: origin
                    (when (and (at_b_2))
                          (Bb_sc))

                    ; #13993: <==closure== 12970 (pos)
                    (when (and (at_b_2))
                          (Pb_sc))

                    ; #15881: <==closure== 75816 (pos)
                    (when (and (at_c_2))
                          (Pc_sc))

                    ; #20318: <==closure== 72770 (pos)
                    (when (and (at_a_2))
                          (Pa_sc))

                    ; #30274: <==closure== 99493 (pos)
                    (when (and (at_d_2))
                          (Pd_sc))

                    ; #72770: origin
                    (when (and (at_a_2))
                          (Ba_sc))

                    ; #75816: origin
                    (when (and (at_c_2))
                          (Bc_sc))

                    ; #99493: origin
                    (when (and (at_d_2))
                          (Bd_sc))

                    ; #10245: <==uncertain_firing== 72770 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Pa_not_sc)))

                    ; #10319: <==uncertain_firing== 99493 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Pd_not_sc)))

                    ; #10933: <==uncertain_firing== 20318 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Ba_not_sc)))

                    ; #11539: <==uncertain_firing== 15881 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Bc_not_sc)))

                    ; #12689: <==negation-removal== 12970 (pos)
                    (when (and (at_b_2))
                          (not (Pb_not_sc)))

                    ; #14783: <==negation-removal== 75816 (pos)
                    (when (and (at_c_2))
                          (not (Pc_not_sc)))

                    ; #15601: <==negation-removal== 15881 (pos)
                    (when (and (at_c_2))
                          (not (Bc_not_sc)))

                    ; #16000: <==uncertain_firing== 13993 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Bb_not_sc)))

                    ; #18632: <==negation-removal== 99493 (pos)
                    (when (and (at_d_2))
                          (not (Pd_not_sc)))

                    ; #18638: <==negation-removal== 20318 (pos)
                    (when (and (at_a_2))
                          (not (Ba_not_sc)))

                    ; #29010: <==uncertain_firing== 75816 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Pc_not_sc)))

                    ; #29258: <==negation-removal== 30274 (pos)
                    (when (and (at_d_2))
                          (not (Bd_not_sc)))

                    ; #38351: <==negation-removal== 13993 (pos)
                    (when (and (at_b_2))
                          (not (Bb_not_sc)))

                    ; #41616: <==uncertain_firing== 12970 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Pb_not_sc)))

                    ; #51605: <==uncertain_firing== 30274 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Bd_not_sc)))

                    ; #69717: <==negation-removal== 72770 (pos)
                    (when (and (at_a_2))
                          (not (Pa_not_sc)))))

    (:action share_c_sd_1
        :precondition (and (Bc_sd)
                           (at_c_1))
        :effect (and
                    ; #10284: origin
                    (when (and (at_a_1))
                          (Ba_sd))

                    ; #11503: origin
                    (when (and (at_d_1))
                          (Bd_sd))

                    ; #16320: <==closure== 21359 (pos)
                    (when (and (at_c_1))
                          (Pc_sd))

                    ; #21359: origin
                    (when (and (at_c_1))
                          (Bc_sd))

                    ; #34318: <==closure== 10284 (pos)
                    (when (and (at_a_1))
                          (Pa_sd))

                    ; #44889: <==closure== 63783 (pos)
                    (when (and (at_b_1))
                          (Pb_sd))

                    ; #63783: origin
                    (when (and (at_b_1))
                          (Bb_sd))

                    ; #96915: <==closure== 11503 (pos)
                    (when (and (at_d_1))
                          (Pd_sd))

                    ; #10383: <==negation-removal== 16320 (pos)
                    (when (and (at_c_1))
                          (not (Bc_not_sd)))

                    ; #12575: <==negation-removal== 96915 (pos)
                    (when (and (at_d_1))
                          (not (Bd_not_sd)))

                    ; #12596: <==uncertain_firing== 63783 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Pb_not_sd)))

                    ; #12766: <==negation-removal== 44889 (pos)
                    (when (and (at_b_1))
                          (not (Bb_not_sd)))

                    ; #14166: <==uncertain_firing== 16320 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Bc_not_sd)))

                    ; #14189: <==uncertain_firing== 11503 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Pd_not_sd)))

                    ; #14682: <==uncertain_firing== 21359 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Pc_not_sd)))

                    ; #15085: <==negation-removal== 11503 (pos)
                    (when (and (at_d_1))
                          (not (Pd_not_sd)))

                    ; #16046: <==uncertain_firing== 34318 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Ba_not_sd)))

                    ; #16213: <==uncertain_firing== 44889 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Bb_not_sd)))

                    ; #17237: <==negation-removal== 34318 (pos)
                    (when (and (at_a_1))
                          (not (Ba_not_sd)))

                    ; #21361: <==uncertain_firing== 96915 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Bd_not_sd)))

                    ; #26042: <==negation-removal== 63783 (pos)
                    (when (and (at_b_1))
                          (not (Pb_not_sd)))

                    ; #79792: <==uncertain_firing== 10284 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Pa_not_sd)))

                    ; #87362: <==negation-removal== 21359 (pos)
                    (when (and (at_c_1))
                          (not (Pc_not_sd)))

                    ; #88519: <==negation-removal== 10284 (pos)
                    (when (and (at_a_1))
                          (not (Pa_not_sd)))))

    (:action share_c_sd_2
        :precondition (and (Bc_sd)
                           (at_c_2))
        :effect (and
                    ; #12920: origin
                    (when (and (at_b_2))
                          (Bb_sd))

                    ; #13993: <==closure== 12920 (pos)
                    (when (and (at_b_2))
                          (Pb_sd))

                    ; #15881: <==closure== 75816 (pos)
                    (when (and (at_c_2))
                          (Pc_sd))

                    ; #20318: <==closure== 72770 (pos)
                    (when (and (at_a_2))
                          (Pa_sd))

                    ; #30274: <==closure== 99493 (pos)
                    (when (and (at_d_2))
                          (Pd_sd))

                    ; #72770: origin
                    (when (and (at_a_2))
                          (Ba_sd))

                    ; #75816: origin
                    (when (and (at_c_2))
                          (Bc_sd))

                    ; #99493: origin
                    (when (and (at_d_2))
                          (Bd_sd))

                    ; #10245: <==uncertain_firing== 72770 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Pa_not_sd)))

                    ; #10329: <==uncertain_firing== 99493 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Pd_not_sd)))

                    ; #10963: <==uncertain_firing== 20318 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Ba_not_sd)))

                    ; #11569: <==uncertain_firing== 15881 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Bc_not_sd)))

                    ; #12689: <==negation-removal== 12920 (pos)
                    (when (and (at_b_2))
                          (not (Pb_not_sd)))

                    ; #14783: <==negation-removal== 75816 (pos)
                    (when (and (at_c_2))
                          (not (Pc_not_sd)))

                    ; #15601: <==negation-removal== 15881 (pos)
                    (when (and (at_c_2))
                          (not (Bc_not_sd)))

                    ; #16000: <==uncertain_firing== 13993 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Bb_not_sd)))

                    ; #18632: <==negation-removal== 99493 (pos)
                    (when (and (at_d_2))
                          (not (Pd_not_sd)))

                    ; #18638: <==negation-removal== 20318 (pos)
                    (when (and (at_a_2))
                          (not (Ba_not_sd)))

                    ; #29010: <==uncertain_firing== 75816 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Pc_not_sd)))

                    ; #29258: <==negation-removal== 30274 (pos)
                    (when (and (at_d_2))
                          (not (Bd_not_sd)))

                    ; #39051: <==negation-removal== 13993 (pos)
                    (when (and (at_b_2))
                          (not (Bb_not_sd)))

                    ; #41716: <==uncertain_firing== 12920 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Pb_not_sd)))

                    ; #51605: <==uncertain_firing== 30274 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Bd_not_sd)))

                    ; #69717: <==negation-removal== 72770 (pos)
                    (when (and (at_a_2))
                          (not (Pa_not_sd)))))

    (:action share_d_sa_1
        :precondition (and (at_d_1)
                           (Bd_sa))
        :effect (and
                    ; #10284: origin
                    (when (and (at_a_1))
                          (Ba_sa))

                    ; #11503: origin
                    (when (and (at_d_1))
                          (Bd_sa))

                    ; #16350: <==closure== 21359 (pos)
                    (when (and (at_c_1))
                          (Pc_sa))

                    ; #21359: origin
                    (when (and (at_c_1))
                          (Bc_sa))

                    ; #34318: <==closure== 10284 (pos)
                    (when (and (at_a_1))
                          (Pa_sa))

                    ; #44889: <==closure== 63783 (pos)
                    (when (and (at_b_1))
                          (Pb_sa))

                    ; #63783: origin
                    (when (and (at_b_1))
                          (Bb_sa))

                    ; #96915: <==closure== 11503 (pos)
                    (when (and (at_d_1))
                          (Pd_sa))

                    ; #10383: <==negation-removal== 16350 (pos)
                    (when (and (at_c_1))
                          (not (Bc_not_sa)))

                    ; #12575: <==negation-removal== 96915 (pos)
                    (when (and (at_d_1))
                          (not (Bd_not_sa)))

                    ; #12586: <==uncertain_firing== 63783 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Pb_not_sa)))

                    ; #12766: <==negation-removal== 44889 (pos)
                    (when (and (at_b_1))
                          (not (Bb_not_sa)))

                    ; #14166: <==uncertain_firing== 16350 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Bc_not_sa)))

                    ; #14189: <==uncertain_firing== 11503 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Pd_not_sa)))

                    ; #14682: <==uncertain_firing== 21359 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Pc_not_sa)))

                    ; #15085: <==negation-removal== 11503 (pos)
                    (when (and (at_d_1))
                          (not (Pd_not_sa)))

                    ; #16056: <==uncertain_firing== 34318 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Ba_not_sa)))

                    ; #16213: <==uncertain_firing== 44889 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Bb_not_sa)))

                    ; #17237: <==negation-removal== 34318 (pos)
                    (when (and (at_a_1))
                          (not (Ba_not_sa)))

                    ; #21361: <==uncertain_firing== 96915 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Bd_not_sa)))

                    ; #27042: <==negation-removal== 63783 (pos)
                    (when (and (at_b_1))
                          (not (Pb_not_sa)))

                    ; #79792: <==uncertain_firing== 10284 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Pa_not_sa)))

                    ; #87462: <==negation-removal== 21359 (pos)
                    (when (and (at_c_1))
                          (not (Pc_not_sa)))

                    ; #88519: <==negation-removal== 10284 (pos)
                    (when (and (at_a_1))
                          (not (Pa_not_sa)))))

    (:action share_d_sa_2
        :precondition (and (Bd_sa)
                           (at_d_2))
        :effect (and
                    ; #12970: origin
                    (when (and (at_b_2))
                          (Bb_sa))

                    ; #13993: <==closure== 12970 (pos)
                    (when (and (at_b_2))
                          (Pb_sa))

                    ; #19924: <==closure== 75816 (pos)
                    (when (and (at_c_2))
                          (Pc_sa))

                    ; #20318: <==closure== 72770 (pos)
                    (when (and (at_a_2))
                          (Pa_sa))

                    ; #30274: <==closure== 99493 (pos)
                    (when (and (at_d_2))
                          (Pd_sa))

                    ; #72770: origin
                    (when (and (at_a_2))
                          (Ba_sa))

                    ; #75816: origin
                    (when (and (at_c_2))
                          (Bc_sa))

                    ; #99493: origin
                    (when (and (at_d_2))
                          (Bd_sa))

                    ; #10245: <==uncertain_firing== 72770 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Pa_not_sa)))

                    ; #10319: <==uncertain_firing== 99493 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Pd_not_sa)))

                    ; #10963: <==uncertain_firing== 20318 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Ba_not_sa)))

                    ; #11539: <==uncertain_firing== 19924 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Bc_not_sa)))

                    ; #12689: <==negation-removal== 12970 (pos)
                    (when (and (at_b_2))
                          (not (Pb_not_sa)))

                    ; #14783: <==negation-removal== 75816 (pos)
                    (when (and (at_c_2))
                          (not (Pc_not_sa)))

                    ; #15601: <==negation-removal== 19924 (pos)
                    (when (and (at_c_2))
                          (not (Bc_not_sa)))

                    ; #16030: <==uncertain_firing== 13993 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Bb_not_sa)))

                    ; #18632: <==negation-removal== 99493 (pos)
                    (when (and (at_d_2))
                          (not (Pd_not_sa)))

                    ; #18638: <==negation-removal== 20318 (pos)
                    (when (and (at_a_2))
                          (not (Ba_not_sa)))

                    ; #29110: <==uncertain_firing== 75816 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Pc_not_sa)))

                    ; #29258: <==negation-removal== 30274 (pos)
                    (when (and (at_d_2))
                          (not (Bd_not_sa)))

                    ; #38351: <==negation-removal== 13993 (pos)
                    (when (and (at_b_2))
                          (not (Bb_not_sa)))

                    ; #41716: <==uncertain_firing== 12970 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Pb_not_sa)))

                    ; #51605: <==uncertain_firing== 30274 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Bd_not_sa)))

                    ; #69717: <==negation-removal== 72770 (pos)
                    (when (and (at_a_2))
                          (not (Pa_not_sa)))))

    (:action share_d_sb_1
        :precondition (and (Bd_sb)
                           (at_d_1))
        :effect (and
                    ; #10284: origin
                    (when (and (at_a_1))
                          (Ba_sb))

                    ; #11493: origin
                    (when (and (at_d_1))
                          (Bd_sb))

                    ; #16350: <==closure== 21359 (pos)
                    (when (and (at_c_1))
                          (Pc_sb))

                    ; #21359: origin
                    (when (and (at_c_1))
                          (Bc_sb))

                    ; #34318: <==closure== 10284 (pos)
                    (when (and (at_a_1))
                          (Pa_sb))

                    ; #44789: <==closure== 63783 (pos)
                    (when (and (at_b_1))
                          (Pb_sb))

                    ; #63783: origin
                    (when (and (at_b_1))
                          (Bb_sb))

                    ; #97015: <==closure== 11493 (pos)
                    (when (and (at_d_1))
                          (Pd_sb))

                    ; #10383: <==negation-removal== 16350 (pos)
                    (when (and (at_c_1))
                          (not (Bc_not_sb)))

                    ; #12575: <==negation-removal== 97015 (pos)
                    (when (and (at_d_1))
                          (not (Bd_not_sb)))

                    ; #12586: <==uncertain_firing== 63783 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Pb_not_sb)))

                    ; #12766: <==negation-removal== 44789 (pos)
                    (when (and (at_b_1))
                          (not (Bb_not_sb)))

                    ; #14156: <==uncertain_firing== 16350 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Bc_not_sb)))

                    ; #14189: <==uncertain_firing== 11493 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Pd_not_sb)))

                    ; #14682: <==uncertain_firing== 21359 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Pc_not_sb)))

                    ; #15075: <==negation-removal== 11493 (pos)
                    (when (and (at_d_1))
                          (not (Pd_not_sb)))

                    ; #16056: <==uncertain_firing== 34318 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Ba_not_sb)))

                    ; #16213: <==uncertain_firing== 44789 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Bb_not_sb)))

                    ; #17267: <==negation-removal== 34318 (pos)
                    (when (and (at_a_1))
                          (not (Ba_not_sb)))

                    ; #21361: <==uncertain_firing== 97015 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Bd_not_sb)))

                    ; #27042: <==negation-removal== 63783 (pos)
                    (when (and (at_b_1))
                          (not (Pb_not_sb)))

                    ; #79792: <==uncertain_firing== 10284 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Pa_not_sb)))

                    ; #87462: <==negation-removal== 21359 (pos)
                    (when (and (at_c_1))
                          (not (Pc_not_sb)))

                    ; #88519: <==negation-removal== 10284 (pos)
                    (when (and (at_a_1))
                          (not (Pa_not_sb)))))

    (:action share_d_sb_2
        :precondition (and (Bd_sb)
                           (at_d_2))
        :effect (and
                    ; #12970: origin
                    (when (and (at_b_2))
                          (Bb_sb))

                    ; #13993: <==closure== 12970 (pos)
                    (when (and (at_b_2))
                          (Pb_sb))

                    ; #19924: <==closure== 75916 (pos)
                    (when (and (at_c_2))
                          (Pc_sb))

                    ; #20318: <==closure== 72770 (pos)
                    (when (and (at_a_2))
                          (Pa_sb))

                    ; #30274: <==closure== 99493 (pos)
                    (when (and (at_d_2))
                          (Pd_sb))

                    ; #72770: origin
                    (when (and (at_a_2))
                          (Ba_sb))

                    ; #75916: origin
                    (when (and (at_c_2))
                          (Bc_sb))

                    ; #99493: origin
                    (when (and (at_d_2))
                          (Bd_sb))

                    ; #10245: <==uncertain_firing== 72770 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Pa_not_sb)))

                    ; #10319: <==uncertain_firing== 99493 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Pd_not_sb)))

                    ; #10933: <==uncertain_firing== 20318 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Ba_not_sb)))

                    ; #11539: <==uncertain_firing== 19924 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Bc_not_sb)))

                    ; #12689: <==negation-removal== 12970 (pos)
                    (when (and (at_b_2))
                          (not (Pb_not_sb)))

                    ; #14783: <==negation-removal== 75916 (pos)
                    (when (and (at_c_2))
                          (not (Pc_not_sb)))

                    ; #15601: <==negation-removal== 19924 (pos)
                    (when (and (at_c_2))
                          (not (Bc_not_sb)))

                    ; #16030: <==uncertain_firing== 13993 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Bb_not_sb)))

                    ; #18628: <==negation-removal== 20318 (pos)
                    (when (and (at_a_2))
                          (not (Ba_not_sb)))

                    ; #18632: <==negation-removal== 99493 (pos)
                    (when (and (at_d_2))
                          (not (Pd_not_sb)))

                    ; #29110: <==uncertain_firing== 75916 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Pc_not_sb)))

                    ; #29258: <==negation-removal== 30274 (pos)
                    (when (and (at_d_2))
                          (not (Bd_not_sb)))

                    ; #38351: <==negation-removal== 13993 (pos)
                    (when (and (at_b_2))
                          (not (Bb_not_sb)))

                    ; #41616: <==uncertain_firing== 12970 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Pb_not_sb)))

                    ; #51605: <==uncertain_firing== 30274 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Bd_not_sb)))

                    ; #69717: <==negation-removal== 72770 (pos)
                    (when (and (at_a_2))
                          (not (Pa_not_sb)))))

    (:action share_d_sc_1
        :precondition (and (at_d_1)
                           (Bd_sc))
        :effect (and
                    ; #10284: origin
                    (when (and (at_a_1))
                          (Ba_sc))

                    ; #11493: origin
                    (when (and (at_d_1))
                          (Bd_sc))

                    ; #16350: <==closure== 21359 (pos)
                    (when (and (at_c_1))
                          (Pc_sc))

                    ; #21359: origin
                    (when (and (at_c_1))
                          (Bc_sc))

                    ; #34318: <==closure== 10284 (pos)
                    (when (and (at_a_1))
                          (Pa_sc))

                    ; #44789: <==closure== 64783 (pos)
                    (when (and (at_b_1))
                          (Pb_sc))

                    ; #64783: origin
                    (when (and (at_b_1))
                          (Bb_sc))

                    ; #97015: <==closure== 11493 (pos)
                    (when (and (at_d_1))
                          (Pd_sc))

                    ; #10383: <==negation-removal== 16350 (pos)
                    (when (and (at_c_1))
                          (not (Bc_not_sc)))

                    ; #12575: <==negation-removal== 97015 (pos)
                    (when (and (at_d_1))
                          (not (Bd_not_sc)))

                    ; #12586: <==uncertain_firing== 64783 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Pb_not_sc)))

                    ; #12766: <==negation-removal== 44789 (pos)
                    (when (and (at_b_1))
                          (not (Bb_not_sc)))

                    ; #14156: <==uncertain_firing== 16350 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Bc_not_sc)))

                    ; #14179: <==uncertain_firing== 11493 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Pd_not_sc)))

                    ; #14682: <==uncertain_firing== 21359 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Pc_not_sc)))

                    ; #15075: <==negation-removal== 11493 (pos)
                    (when (and (at_d_1))
                          (not (Pd_not_sc)))

                    ; #16056: <==uncertain_firing== 34318 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Ba_not_sc)))

                    ; #16213: <==uncertain_firing== 44789 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Bb_not_sc)))

                    ; #17237: <==negation-removal== 34318 (pos)
                    (when (and (at_a_1))
                          (not (Ba_not_sc)))

                    ; #21361: <==uncertain_firing== 97015 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Bd_not_sc)))

                    ; #27042: <==negation-removal== 64783 (pos)
                    (when (and (at_b_1))
                          (not (Pb_not_sc)))

                    ; #79792: <==uncertain_firing== 10284 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Pa_not_sc)))

                    ; #87462: <==negation-removal== 21359 (pos)
                    (when (and (at_c_1))
                          (not (Pc_not_sc)))

                    ; #88519: <==negation-removal== 10284 (pos)
                    (when (and (at_a_1))
                          (not (Pa_not_sc)))))

    (:action share_d_sc_2
        :precondition (and (Bd_sc)
                           (at_d_2))
        :effect (and
                    ; #12970: origin
                    (when (and (at_b_2))
                          (Bb_sc))

                    ; #13993: <==closure== 12970 (pos)
                    (when (and (at_b_2))
                          (Pb_sc))

                    ; #15881: <==closure== 75816 (pos)
                    (when (and (at_c_2))
                          (Pc_sc))

                    ; #20318: <==closure== 72770 (pos)
                    (when (and (at_a_2))
                          (Pa_sc))

                    ; #30274: <==closure== 99493 (pos)
                    (when (and (at_d_2))
                          (Pd_sc))

                    ; #72770: origin
                    (when (and (at_a_2))
                          (Ba_sc))

                    ; #75816: origin
                    (when (and (at_c_2))
                          (Bc_sc))

                    ; #99493: origin
                    (when (and (at_d_2))
                          (Bd_sc))

                    ; #10245: <==uncertain_firing== 72770 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Pa_not_sc)))

                    ; #10319: <==uncertain_firing== 99493 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Pd_not_sc)))

                    ; #10933: <==uncertain_firing== 20318 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Ba_not_sc)))

                    ; #11539: <==uncertain_firing== 15881 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Bc_not_sc)))

                    ; #12689: <==negation-removal== 12970 (pos)
                    (when (and (at_b_2))
                          (not (Pb_not_sc)))

                    ; #14783: <==negation-removal== 75816 (pos)
                    (when (and (at_c_2))
                          (not (Pc_not_sc)))

                    ; #15601: <==negation-removal== 15881 (pos)
                    (when (and (at_c_2))
                          (not (Bc_not_sc)))

                    ; #16000: <==uncertain_firing== 13993 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Bb_not_sc)))

                    ; #18632: <==negation-removal== 99493 (pos)
                    (when (and (at_d_2))
                          (not (Pd_not_sc)))

                    ; #18638: <==negation-removal== 20318 (pos)
                    (when (and (at_a_2))
                          (not (Ba_not_sc)))

                    ; #29010: <==uncertain_firing== 75816 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Pc_not_sc)))

                    ; #29258: <==negation-removal== 30274 (pos)
                    (when (and (at_d_2))
                          (not (Bd_not_sc)))

                    ; #38351: <==negation-removal== 13993 (pos)
                    (when (and (at_b_2))
                          (not (Bb_not_sc)))

                    ; #41616: <==uncertain_firing== 12970 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Pb_not_sc)))

                    ; #51605: <==uncertain_firing== 30274 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Bd_not_sc)))

                    ; #69717: <==negation-removal== 72770 (pos)
                    (when (and (at_a_2))
                          (not (Pa_not_sc)))))

    (:action share_d_sd_1
        :precondition (and (Bd_sd)
                           (at_d_1))
        :effect (and
                    ; #10284: origin
                    (when (and (at_a_1))
                          (Ba_sd))

                    ; #11503: origin
                    (when (and (at_d_1))
                          (Bd_sd))

                    ; #16320: <==closure== 21359 (pos)
                    (when (and (at_c_1))
                          (Pc_sd))

                    ; #21359: origin
                    (when (and (at_c_1))
                          (Bc_sd))

                    ; #34318: <==closure== 10284 (pos)
                    (when (and (at_a_1))
                          (Pa_sd))

                    ; #44889: <==closure== 63783 (pos)
                    (when (and (at_b_1))
                          (Pb_sd))

                    ; #63783: origin
                    (when (and (at_b_1))
                          (Bb_sd))

                    ; #96915: <==closure== 11503 (pos)
                    (when (and (at_d_1))
                          (Pd_sd))

                    ; #10383: <==negation-removal== 16320 (pos)
                    (when (and (at_c_1))
                          (not (Bc_not_sd)))

                    ; #12575: <==negation-removal== 96915 (pos)
                    (when (and (at_d_1))
                          (not (Bd_not_sd)))

                    ; #12596: <==uncertain_firing== 63783 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Pb_not_sd)))

                    ; #12766: <==negation-removal== 44889 (pos)
                    (when (and (at_b_1))
                          (not (Bb_not_sd)))

                    ; #14166: <==uncertain_firing== 16320 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Bc_not_sd)))

                    ; #14189: <==uncertain_firing== 11503 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Pd_not_sd)))

                    ; #14682: <==uncertain_firing== 21359 (pos)
                    (when (and (not (not_at_c_1)))
                          (not (Pc_not_sd)))

                    ; #15085: <==negation-removal== 11503 (pos)
                    (when (and (at_d_1))
                          (not (Pd_not_sd)))

                    ; #16046: <==uncertain_firing== 34318 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Ba_not_sd)))

                    ; #16213: <==uncertain_firing== 44889 (pos)
                    (when (and (not (not_at_b_1)))
                          (not (Bb_not_sd)))

                    ; #17237: <==negation-removal== 34318 (pos)
                    (when (and (at_a_1))
                          (not (Ba_not_sd)))

                    ; #21361: <==uncertain_firing== 96915 (pos)
                    (when (and (not (not_at_d_1)))
                          (not (Bd_not_sd)))

                    ; #26042: <==negation-removal== 63783 (pos)
                    (when (and (at_b_1))
                          (not (Pb_not_sd)))

                    ; #79792: <==uncertain_firing== 10284 (pos)
                    (when (and (not (not_at_a_1)))
                          (not (Pa_not_sd)))

                    ; #87362: <==negation-removal== 21359 (pos)
                    (when (and (at_c_1))
                          (not (Pc_not_sd)))

                    ; #88519: <==negation-removal== 10284 (pos)
                    (when (and (at_a_1))
                          (not (Pa_not_sd)))))

    (:action share_d_sd_2
        :precondition (and (Bd_sd)
                           (at_d_2))
        :effect (and
                    ; #12920: origin
                    (when (and (at_b_2))
                          (Bb_sd))

                    ; #13993: <==closure== 12920 (pos)
                    (when (and (at_b_2))
                          (Pb_sd))

                    ; #15881: <==closure== 75816 (pos)
                    (when (and (at_c_2))
                          (Pc_sd))

                    ; #20318: <==closure== 72770 (pos)
                    (when (and (at_a_2))
                          (Pa_sd))

                    ; #30274: <==closure== 99493 (pos)
                    (when (and (at_d_2))
                          (Pd_sd))

                    ; #72770: origin
                    (when (and (at_a_2))
                          (Ba_sd))

                    ; #75816: origin
                    (when (and (at_c_2))
                          (Bc_sd))

                    ; #99493: origin
                    (when (and (at_d_2))
                          (Bd_sd))

                    ; #10245: <==uncertain_firing== 72770 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Pa_not_sd)))

                    ; #10329: <==uncertain_firing== 99493 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Pd_not_sd)))

                    ; #10963: <==uncertain_firing== 20318 (pos)
                    (when (and (not (not_at_a_2)))
                          (not (Ba_not_sd)))

                    ; #11569: <==uncertain_firing== 15881 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Bc_not_sd)))

                    ; #12689: <==negation-removal== 12920 (pos)
                    (when (and (at_b_2))
                          (not (Pb_not_sd)))

                    ; #14783: <==negation-removal== 75816 (pos)
                    (when (and (at_c_2))
                          (not (Pc_not_sd)))

                    ; #15601: <==negation-removal== 15881 (pos)
                    (when (and (at_c_2))
                          (not (Bc_not_sd)))

                    ; #16000: <==uncertain_firing== 13993 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Bb_not_sd)))

                    ; #18632: <==negation-removal== 99493 (pos)
                    (when (and (at_d_2))
                          (not (Pd_not_sd)))

                    ; #18638: <==negation-removal== 20318 (pos)
                    (when (and (at_a_2))
                          (not (Ba_not_sd)))

                    ; #29010: <==uncertain_firing== 75816 (pos)
                    (when (and (not (not_at_c_2)))
                          (not (Pc_not_sd)))

                    ; #29258: <==negation-removal== 30274 (pos)
                    (when (and (at_d_2))
                          (not (Bd_not_sd)))

                    ; #39051: <==negation-removal== 13993 (pos)
                    (when (and (at_b_2))
                          (not (Bb_not_sd)))

                    ; #41716: <==uncertain_firing== 12920 (pos)
                    (when (and (not (not_at_b_2)))
                          (not (Pb_not_sd)))

                    ; #51605: <==uncertain_firing== 30274 (pos)
                    (when (and (not (not_at_d_2)))
                          (not (Bd_not_sd)))

                    ; #69717: <==negation-removal== 72770 (pos)
                    (when (and (at_a_2))
                          (not (Pa_not_sd)))))

)